<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initRouter() {

        // bloquear IP rompermarcas
        if($_SERVER['REMOTE_ADDR'] == '191.191.213.72')
                exit('Estamos em manutenção.');

        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/router.ini', 'production');

        $router->addConfig($config, 'routes');
    }

    protected function _initPaginator()
    {
        Zend_Paginator::setDefaultScrollingStyle('Sliding');
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('partial/pagination.phtml');
    }
   
   
    protected function _initForceSSL() {
        
        // $whitelist = array(
        //     '192.168.25.209',
        //     '::1'
        // );    

        // if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
        //     header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        //     exit();
        // }
        
        if($_SERVER['SERVER_PORT'] != '443' && $_SERVER['SERVER_NAME'] != 'localhost') {
            header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
            exit();
        }
    } 

}
