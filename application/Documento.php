<?php

class Application_Model_Documento {

    public function getById($documentoId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('documento')->where("id = ?",$documentoId);

        return $db->fetchRow($select);
    }
    
    private function getSql($funcionario){
        $db = Zend_Db_Table::getDefaultAdapter();
        
        return $db->select()->from('documento')
                ->join('documento_acesso','documento.id = documento_acesso.documento_id',array())
                ->where('documento_acesso.funcionario_id = '.$funcionario["id"].' OR (documento_acesso.unidade_id = '.$funcionario["unidade_id"].' AND departamento_id IS NULL) OR (documento_acesso.unidade_id = '.$funcionario["unidade_id"].' AND departamento_id = '.$funcionario["departamento_id"].')');
    }
    public function getByIdByFuncionario($documentoId,$funcionario) {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select = $this->getSql($funcionario);
        $select->where("documento.id = ?",$documentoId);
 
        return $db->fetchRow($select);
    }
    
    public function getUltimosByFuncionario($funcionario,$limit = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getSql($funcionario);
        $select->group("documento.id");
        $select->order('id DESC');
        if($limit){
            $select->limit($limit);
        }
        return $db->fetchAll($select);
    }
    
    public function getAllPage($funcionario,$q = null, $ordem = null, $page = 1, $tipo = null) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $this->getSql($funcionario);

        if ($q) {
            $select->where('documento.titulo LIKE ?', '%' . $q . '%');
        }
        
        if ($tipo) {
            $select->where('documento.tipo IN (?)', $tipo);
        }
        
        if ($ordem) {
            $select->order('id DESC');
        }else{
            $select->order('titulo');
            
        }
        

        $paginas = Zend_Paginator::factory($db->fetchAll($select));
        $paginas->setItemCountPerPage(12);
        $paginas->setCurrentPageNumber($page);
        
        

        return $paginas;
    }
    
    

}
