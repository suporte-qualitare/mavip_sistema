<?php

require_once ('Zend/Form.php');

class Application_Form_Default_Cliente extends Zend_Form {

    public $decorators = array('CompositeNoLabel');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');


        $name = $this->createElement('text', 'nome', array('label' => 'Nome Completo', 'class' => 'Form-input', 'placeholder' => 'Digite o nome completo'));
        $name->setDecorators($this->decorators)
                 ->addValidator('regex', false, array('/^[a-zA-Z]+/'))
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true);
        $this->addElement($name);

        
        $name = $this->createElement('text', 'nome2', array('label' => 'Outro Responsável', 'class' => 'Form-input', 'placeholder' => 'Opcional'));
        $name->setDecorators($this->decorators)
                 ->addValidator('regex', false, array('/^[a-zA-Z]+/'))
                ->addValidator('stringLength', false, array(0, 80));
        $this->addElement($name);

        $name = $this->createElement('text', 'telefone', array('label' => 'Telefone', 'class' => 'Form-input', 'placeholder' => 'Digite o Telefone'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 18))
                ->setRequired(true);
        $this->addElement($name);
    

        $name = $this->createElement('text', 'email', array('label' => 'E-mail', 'class' => 'Form-input', 'placeholder' => 'Digite o E-mail'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true)
                ->addValidator(
                        'Db_NoRecordExists', true, array(
                    'table' => 'cliente',
                    'field' => 'email',
                    'exclude' => array(
                        'field' => 'id',
                        'value' => $isEditing
                    ),
                    'messages' => array(
                        'recordFound' => 'E-mail existente'
                    )
                        )
                );
        $this->addElement($name);


        $username = $this->createElement('text', 'login', array('label' => 'Login', 'class' => 'Form-input','placeholder' => 'Login com mínimo 7 caracteres'));
        $username->setDecorators($this->decorators)
                ->addValidator('alnum')
             //   ->addValidator('regex', false, array('/^[a-zA-Z0-9]+/'))
                ->addValidator('stringLength', false, array(7, 20))
                ->setRequired(true)
                ->addValidator(
                        'Db_NoRecordExists', true, array(
                    'table' => 'cliente',
                    'field' => 'login',
                    'exclude' => array(
                        'field' => 'id',
                        'value' => $isEditing
                    ),
                    'messages' => array(
                        'recordFound' => 'login existente'
                    )
                        )
                )
                ->addFilter('StringToLower');
        $this->addElement($username);




        $password = $this->createElement('password', 'senha', array('label' => 'Senha', 'class' => 'Form-input', 'placeholder' => 'Digite sua senha com até 8 caracteres'));
        $password->setAttrib('maxlength', '8');
        $password->setDecorators($this->decorators)
                ->addValidator('StringLength', true, array(3, 8));
        if ($isEditing) {
            $password->setRequired(false);
        } else {
            $password->setRequired(true);
        }
        $this->addElement($password);

        $name = $this->createElement('password', 'senha_confirm', array('label' => 'Confirmar senha', 'placeholder' => 'Confirme sua senha', 'class' => 'Form-input'));
        $name->setAttrib('maxlength', '8');
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(3, 8));
        $this->addElement($name);

      

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
