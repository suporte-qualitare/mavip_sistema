<?php

require_once ('Zend/Form.php');

class Application_Form_Default_ClienteUpdate extends Zend_Form {

    public $decorators = array('CompositeNoLabel');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');


        $name = $this->createElement('text', 'nome', array('label' => 'Nome Completo', 'class' => 'Form-input', 'placeholder' => 'Digite o nome completo'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setAttrib('required','')
                ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'nome2', array('label' => 'Outro Responsável', 'class' => 'Form-input', 'placeholder' => 'Opcional'));
        $name->setDecorators($this->decorators)
                 ->addValidator('regex', false, array('/^[a-zA-Z]+/'))
                ->addValidator('stringLength', false, array(0, 80));
        $this->addElement($name);


        $name = $this->createElement('text', 'marca', array('label' => 'Nome da marca', 'class' => 'Form-input', 'placeholder' => 'Digite o nome da marca'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setAttrib('required','')
                ->setRequired(false);
        $this->addElement($name);

        $name = $this->createElement('text', 'marca_processo', array('label' => 'Número do Processo', 'class' => 'Form-input', 'placeholder' => 'Número do processo da marca'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setAttrib('required','')
                ->setRequired(false);
        $this->addElement($name);

        // Se o cliente for Pessoa Física
        if ($options["user"]['cliente_natureza_id'] == 2):

            $name = $this->createElement('text', 'cnpj', array('label' => 'CPF', 'class' => 'Form-input', 'placeholder' => 'Digite o CPF', 'required'=>TRUE));
            $name->setDecorators($this->decorators)
                    ->addValidator('stringLength', false, array(0, 80))
                    ->setAttrib('required','')
                    ->setRequired(true);
            $this->addElement($name);

        else:

            $name = $this->createElement('text', 'cnpj', array('label' => 'CNPJ', 'class' => 'Form-input', 'placeholder' => 'Digite o CNPJ', 'required'=>true));
            $name->setDecorators($this->decorators)
                    ->addValidator('stringLength', false, array(0, 80))
                    ->setAttrib('required','')
                    ->setRequired(true);
            $this->addElement($name);

            
            $name = $this->createElement('text', 'cpf_pessoa_juridica', array('label' => 'CPF', 'class' => 'Form-input', 'placeholder' => 'Digite o CPF'));
            $name->setDecorators($this->decorators)
                    ->addValidator('stringLength', false, array(0, 80));
                   $this->addElement($name);

            $name = $this->createElement('text', 'razao', array('label' => 'Resposta', 'class' => 'Form-input', 'placeholder' => 'Digite a razão sozial'));
            $name->setDecorators($this->decorators)
                    ->addValidator('stringLength', false, array(0, 120));

            $this->addElement($name);

        endif;

        // Se o cliente for Pessoa Física ou Jurídica
        $name = $this->createElement('text', 'rg', array('label' => 'RG', 'class' => 'Form-input', 'placeholder' => 'Digite o RG'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80));
        $this->addElement($name);

        
        $name = $this->createElement('text', 'email', array('label' => 'E-mail', 'class' => 'Form-input', 'placeholder' => 'Digite o E-mail'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setAttrib('required','')
                ->setRequired(true)
                ->addValidator(
                        'Db_NoRecordExists', true, array(
                    'table' => 'cliente',
                    'field' => 'email',
                    'exclude' => array(
                        'field' => 'id',
                        'value' => $isEditing
                    ),
                    'messages' => array(
                        'recordFound' => 'E-mail existente'
                    )
                        )
        );
        $this->addElement($name);

        $username = $this->createElement('text', 'inpi_login', array('label' => 'Login', 'class' => 'Form-input', 'placeholder' => 'Informe o login do INPI'));
        $username->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(3, 20))
                ->setAttrib('required','')
                ->setRequired(false);
                //->addFilter('StringToLower');
        $this->addElement($username);

        $password = $this->createElement('password', 'inpi_senha', array('label' => 'Senha', 'class' => 'Form-input', 'placeholder' => 'Informe a senha do INPI com 6 até 10 caracteres'));
        $password->setAttrib('maxlength', '10');
        $password->setDecorators($this->decorators)
                ->addValidator('StringLength', true, array(6, 10));
        $this->addElement($password);



        $password = $this->createElement('password', 'senha', array('label' => 'Senha', 'class' => 'Form-input', 'placeholder' => 'Digite sua senha com até 10 caracteres'));
        $password->setAttrib('maxlength', '8');
        $password->setDecorators($this->decorators)
                //->addValidator('StringLength', true, array(3, 20));
                ->addValidator('StringLength', true, array(3, 8));
        if ($isEditing) {
            $password->setRequired(false);
        } else {
            $password->setRequired(true);
        }
        $this->addElement($password);

        $name = $this->createElement('password', 'senha_confirm', array('label' => 'Confirmar senha', 'placeholder' => 'Confirme sua senha', 'class' => 'Form-input'));
        $password->setAttrib('maxlength', '8');
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(3, 8));
        $this->addElement($name);

        $name = $this->createElement('text', 'pergunta', array('label' => 'Pergunta', 'class' => 'Form-input _pergunta', 'placeholder' => 'Digite a pergunta secreta'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 200))                          
                ->setRequired(false);
        $this->addElement($name);

        $name = $this->createElement('text', 'resposta', array('label' => 'Resposta', 'class' => 'Form-input _resposta', 'placeholder' => 'Digite a resposta secreta'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 200))
                ->setRequired(false);
        $this->addElement($name);

        $name = $this->createElement('text', 'logradouro', array('label' => 'Logradouro', 'class' => 'Form-input', 'placeholder' => 'Digite o logradouro'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 200))
                ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'bairro', array('label' => 'Bairro', 'class' => 'Form-input', 'placeholder' => 'Digite o bairro'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 200))
                ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'complemento', array('label' => 'No / Complemento', 'class' => 'Form-input', 'placeholder' => 'Digite o número e complemento'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 200))
                ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'cep', array('label' => 'CEP', 'class' => 'Form-input', 'placeholder' => 'Digite o CEP e pressione a tecla tab'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 9))
                ->setAttrib('required','')
                ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'telefone', array('label' => 'Telefone', 'class' => 'Form-input', 'placeholder' => 'Digite o Telefone'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 18))
                ->setAttrib('required','')
                ->setRequired(false);
        $this->addElement($name);

        $name = $this->createElement('text', 'celular', array('label' => 'Celular', 'class' => 'Form-input', 'placeholder' => 'Digite o Celular'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 18))
                ->setAttrib('required','')
                ->setRequired(false);
        $this->addElement($name);

        $name = $this->createElement('text', 'fax', array('label' => 'FAX', 'class' => 'Form-input', 'placeholder' => 'Digite o FAX'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 18))
                ->setRequired(false);
        $this->addElement($name);


        $clienteModel = new Application_Model_Cliente();
        $name = $this->createElement('select', 'atividade', array('label' => 'Atividade', 'class' => 'Form-select', 'placeholder' => 'Escolha sua atividade'));
        $name->setDecorators($this->decorators)
                ->setMultiOptions($clienteModel->atividades);
        $this->addElement($name);

        $clienteNaturezaModel = new Application_Model_ClienteNatureza();
        $name = $this->createElement('select', 'natureza', array('label' => 'Natureza', 'class' => 'Form-select', 'disable' => ($options["user"]['cliente_natureza_id'])));
        $name->setDecorators($this->decorators)
                ->setMultiOptions($clienteNaturezaModel->getAllToSelect());
        $this->addElement($name);


        $paisesModel = new Application_Model_Paises();
        $name = $this->createElement('select', 'pais', array('label' => 'País', 'id' => 'pais',  'class' => 'Form-select'));
        $name->setDecorators($this->decorators)
                ->setValue('26')//deixando Brasil selecionado
                ->setAttrib('required','')
                ->setRequired(true)
                ->setMultiOptions($paisesModel->getAllToSelect());
        $this->addElement($name);


        $estadoModel = new Application_Model_Estados();
        $name = $this->createElement('select', 'estado', array('label' => 'Estado', 'class' => 'Form-select'));
        $name->setDecorators($this->decorators)
                ->setAttrib('required','')
                ->setRequired(true)
                ->setMultiOptions($estadoModel->getAllToSelect("Selecione o Estado"));
        $this->addElement($name);

        if ($options['estado']) {
            $cidades = new Application_Model_Cidades();
            $cids = $cidades->getAllToSelect($options['estado'], 'Selecione a cidade');
        } else {
            $cids = array(null => 'Selecione a cidade');
        }

        $name = $this->createElement('select', 'cidade', array('label' => 'Selecione a cidade', 'class' => 'Form-select'));
        $name->setDecorators($this->decorators)
                ->setAttrib('required','')
                ->setRequired(true)
                ->setMultiOptions($cids);
        $this->addElement($name);


        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
