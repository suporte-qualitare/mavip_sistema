<?php

require_once ('Zend/Form.php');

class Application_Form_Default_ContatoForm extends Zend_Form {
	
	public function __construct($options = null) {
		parent::__construct ( $options );
		$this->generate ( );
	}
	
	//public $elementDecorators = array('ViewHelper',);
	public $elementDecorators = array('CompositeFront2');
    
  
	private function generate() {
		//$this->setTranslator(Zend_Registry::get("translate"));
		$this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
		
		$nome = new Zend_Form_Element_Text ( 'nome' );
		$nome->setAttrib('placeholder','Nome')->setDecorators($this->elementDecorators)->setRequired ( true )->addFilter ( 'StripTags' )->addValidator ( 'NotEmpty' )->addValidator ( 'StringLength', false, array (0, 80 ) );
		
		$email = new Zend_Form_Element_Text ( 'email' );
		$email->setAttrib('placeholder','E-mail')->setDecorators($this->elementDecorators)->setRequired ( true )->addFilter ( 'StripTags' )->addValidator ( 'EmailAddress' )->addValidator ( 'NotEmpty' )->addValidator ( 'StringLength', false, array (0, 80 ) );

		//$fone = new Zend_Form_Element_Text ( 'telefone' );
		//$fone->setAttrib('placeholder','Telefone')->setDecorators($this->elementDecorators)->setRequired ( true )->addFilter ( 'StripTags' )->addValidator ( 'StringLength', false, array (0, 80 ) );
		
		$assunto = new Zend_Form_Element_Text ( 'assunto' );
		$assunto->setAttrib('placeholder','Assunto')->setDecorators($this->elementDecorators)->setRequired ( true )->addFilter ( 'StripTags' )->addValidator ( 'NotEmpty' )->addValidator ( 'StringLength', false, array (3, 80 ) );
		
		
		
		$mensagem = new Zend_Form_Element_Textarea( 'mensagem' );
		$mensagem->setAttrib('placeholder','Mensagem')->setDecorators($this->elementDecorators)->setRequired ( true )->addFilter ( 'StripTags' )->addValidator ( 'NotEmpty' )->addValidator ( 'StringLength', false, array (0, 1024 ) );

		$this->addElements (array($nome, $email,$mensagem,$assunto));
	}
}