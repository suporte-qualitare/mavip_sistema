<?php

require_once ('Zend/Form.php');

class Application_Form_Default_Depoimento extends Zend_Form {

    public $decorators = array('CompositeNoLabel');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $name = $this->createElement('text', 'nome', array('label' => 'Nome Completo', 'class' => 'col-md-8 Form-input', 'placeholder' => 'Digite seu nome completo'));
        $name->setDecorators($this->decorators)                
                ->addValidator('stringLength', false, array(0, 50))
                ->setAttrib("required", "required")
                ->setRequired(false);
        $this->addElement($name);

        $name = $this->createElement('textarea', 'depoimento', array('label' => 'Depoimento', 'class' => 'col-md-8 Form-input', 'placeholder' => 'Digite seu Depoimento'));
        $name->setDecorators($this->decorators)
                ->setAttrib('maxLength', 200)
                ->setAttrib('rows', 5)
                ->setAttrib("required", "required")
                //->addValidator('stringLength', false, array(0, 255))
                ->setRequired(false);
        $this->addElement($name);
        
        $caminho = PATH_ARQS.'depoimentos';
        if (!file_exists($caminho)) {
            mkdir($caminho, 0755, true);
        }
        
        $arq = $this->createElement('file', 'imagem', array('label' => '', 'class' => 'col-md-8 Form-input input-file'));
        $arq->addValidator('Extension', false, 'jpg,jpeg,png,gif')
                ->setDestination($caminho);
        if($isEditing){
            $arq->setRequired(false);
        }else{
            $arq->setRequired(false);
        }     
        if ($arq->getFileName()) {
            $arrayExt = explode(".", $arq->getFileName());
            $ext = end($arrayExt);
            $novoNomeArquivo = date('Ymdhisu') . mt_rand(1000, 9999) . "." . $ext;
            $arq->addFilter("Rename", array("target" => $caminho . '/' . $novoNomeArquivo, "overwrite" => true));
        }
        $this->addElement($arq);       
        
        

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'col-xs-12 col-sm-3 col-sm-offset-6 col-md-3 col-md-offset-7'));
    }

}
