<?php

require_once ('Zend/Form.php');

class Application_Form_Default_Marca extends Zend_Form {
	
	public $decorators = array('CompositeNoLabel');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        
        
        $name = $this->createElement('text', 'marca', array('label' => 'Nome da marca', 'class' => 'Form-input','placeholder'=>'Digite o nome da marca'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setAttrib("required", "required")
                ->setRequired(false);
        $this->addElement($name);
        
        $name = $this->createElement('text', 'marca_processo', array('label' => 'Número do Processo', 'class' => 'Form-input','placeholder'=>'Digite o número do processo ou deixe em branco'));
        $name->setDecorators($this->decorators)
                //->setAttrib("required", "required")
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(false);
        $this->addElement($name);

        $name = $this->createElement('text', 'data_publicacao_processo', array('label' => 'Data da Publicação do Processo', 'class' => 'Form-input datepicker', 'placeholder'=>'Selecione aqui a data da publicação do processo ou deixe em branco'));
        $name->setDecorators($this->decorators)
                ->setRequired(false)
                ->addFilter('StringToLower');
        $this->addElement( $name);
        
        
        
        $name = $this->createElement('hidden', 'op', array());
        $name->setDecorators($this->decorators)
                ->setRequired(false);
        $this->addElement($name);
        
     
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }
}