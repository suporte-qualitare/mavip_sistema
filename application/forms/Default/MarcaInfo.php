<?php

require_once ('Zend/Form.php');

class Application_Form_Default_MarcaInfo extends Zend_Form {
	
	public $decorators = array('CompositeNoLabel');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        
        
        $name = $this->createElement('text', 'descricao', array('label' => 'Nome da marca', 'class' => 'Form-input','placeholder'=>'Digite o nome da marca'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(false);
        $this->addElement($name);
        
        $name = $this->createElement('text', 'marca', array('label' => 'Número do Processo', 'class' => 'Form-input','placeholder'=>'Número do processo da marca'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(false);
        $this->addElement($name);
        
        $caminho = PATH_ARQS.'marca';
        if (!file_exists($caminho)) {
            mkdir($caminho, 0755, true);
        }
        
        $arq = $this->createElement('file', 'logo', array('label' => 'Logo'));
        $arq->addValidator('Extension', false, 'jpg,png')
                ->setDestination($caminho);
            
        if ($arq->getFileName()) {
            $arrayExt = explode(".", $arq->getFileName());
            $ext = end($arrayExt);
            $novoNomeArquivo = date('Ymdhisu') . mt_rand(1000, 9999) . "." . $ext;
            $arq->addFilter("Rename", array("target" => $caminho . '/' . $novoNomeArquivo, "overwrite" => true));
        }
        $this->addElement($arq);
        
        
     
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }
}