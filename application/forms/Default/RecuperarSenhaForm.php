<?php

require_once ('Zend/Form.php');

class Application_Form_Default_RecuperarSenhaForm extends Zend_Form {
	
	public function __construct($options = null) {
		parent::__construct ( $options );
		$this->generate ( );
	}
	
	public $elementDecorators = array('ViewHelper',);    
  
	private function generate() {
		//$this->setTranslator(Zend_Registry::get("translate"));
		
        $email = new Zend_Form_Element_Text('email');
        $email->setAttrib('placeholder', 'E-mail')
                ->setLabel('E-mail')
                ->setDecorators($this->elementDecorators)
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addValidator('EmailAddress', true)
                ->addValidator('NotEmpty', true)
                ->addValidator('StringLength', true, array(3, 200))
                ->setErrorMessages(array('Email inválido'));

        $this->addElement($email);

        $login = new Zend_Form_Element_Text('login');
        $login->setAttrib('placeholder', 'Login')
                ->setLabel('Login')
                ->setDecorators($this->elementDecorators)
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addValidator('NotEmpty', true)
                ->addValidator('StringLength', true, array(3, 40));

        $this->addElement($login);
	}
}