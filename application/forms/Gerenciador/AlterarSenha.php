<?php

class Application_Form_Gerenciador_AlterarSenha extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        

        $name = $this->createElement('password', 'senha', array('label' => 'Senha atual', 'placeholder' => 'Digite sua senha', 'class' => 'form-control'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(3, 50))
                ->setRequired(false);
        $this->addElement($name);

        $name = $this->createElement('password', 'nova_senha', array('label' => 'Nova senha', 'placeholder' => 'Digite sua nova senha', 'class' => 'form-control'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(3, 50))
                ->setRequired(false);
        $this->addElement($name);

        $name = $this->createElement('password', 'senha_confirm', array('label' => 'Confirmar nova senha', 'placeholder' => 'Confirme sua nova senha', 'class' => 'form-control'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(3, 50))
                ->setRequired(false);
        $this->addElement($name);


        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
