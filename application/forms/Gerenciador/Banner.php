<?php

class Application_Form_Gerenciador_Banner extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');
        
//        $name = $this->createElement('text', 'titulo', array('label' => 'Título', 'class' => 'form-control','placeholder'=>'Título'));
//        $name->setDecorators($this->decorators)
//                ->addValidator('stringLength', false, array(0, 80))
//                ->setRequired(true);
//        $this->addElement($name);
//        
//        $name = $this->createElement('text', 'url', array('label' => 'URL', 'class' => 'form-control','placeholder'=>'URL'));
//        $name->setDecorators($this->decorators)
//                ->addValidator('stringLength', false, array(0, 80))
//                ->setRequired(false);
//        $this->addElement($name);

        $caminho = PATH_ARQS.'banner';
        if (!file_exists($caminho)) {
            mkdir($caminho, 0755, true);
        }
        
        $arq = $this->createElement('file', 'arquivo', array('label' => 'Imagem', 'accept' => 'image/png, image/jpg, image/jpeg'));
        $arq->addValidator('Extension', false, 'jpg,png,jpeg')
                ->setDestination($caminho);
            
        if ($arq->getFileName()) {
            $arrayExt = explode(".", $arq->getFileName());
            $ext = end($arrayExt);
            $novoNomeArquivo = date('Ymdhisu') . mt_rand(1000, 9999) . "." . $ext;
            $arq->addFilter("Rename", array("target" => $caminho . '/' . $novoNomeArquivo, "overwrite" => true));
        }
        $this->addElement($arq);
      
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn'));
    }

}

