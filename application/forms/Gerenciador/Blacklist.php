<?php

class Application_Form_Gerenciador_Blacklist extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $username = $this->createElement('text', 'email', array('label' => 'E-mail', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                //->addValidator('EmailAddress')
                ->addValidator('stringLength', false, array(0, 150))
                ->setRequired(true)
                ->addFilter('StringToLower');
        $this->addElement($username);


        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
