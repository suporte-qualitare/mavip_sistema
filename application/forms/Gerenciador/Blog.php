<?php

class Application_Form_Gerenciador_Blog extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');
        
        $categoriasModel = new Application_Model_DbTable_Categoriablog();
        $role = $this->createElement('select', 'categoria_blog_id', array('multioptions' =>  $categoriasModel->getAllToSelect(),'class' => 'form-control'));
        $role->setLabel('Categoria')->setDecorators($this->decorators);
        $this->addElement($role);
        
        $name = $this->createElement('text', 'titulo', array('label' => 'Título', 'class' => 'form-control','placeholder'=>'Título da postagem'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 220))
                ->setRequired(true);
        $this->addElement($name);   
        
        $name = $this->createElement('text', 'legenda', array('label' => 'Legenda', 'class' => 'form-control','placeholder'=>'Legenda para imagem'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 220))
                ->setRequired(true);
        $this->addElement($name);   
        
        $name = $this->createElement('text', 'data_inicio', array('label' => 'Data Inicio', 'class' => 'form-control datetimepicker','placeholder'=>''));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 220))
                ->setRequired(true);
        $this->addElement($name);
        
        
      
        $name = $this->createElement('textarea', 'texto', array('label' => 'Descrição', 'class' => 'form-control ckeditor','placeholder'=>'digite a descrição da categoria'));
        $name->setDecorators($this->decorators)
                ->setRequired(false);
        $this->addElement($name);
        
        $name = $this->createElement('textarea', 'resumo', array('label' => 'Resumo', 'class' => 'form-control ckeditor','placeholder'=>'Resumo da postagem'));
        $name->setDecorators($this->decorators)
                ->setAttrib('rows', '1')
                ->setRequired(false);
        $this->addElement($name);
        
        $caminho = PATH_ARQS.'blog';
        if (!file_exists($caminho)) {
            mkdir($caminho, 0755, true);
        }
        
        $arq = $this->createElement('file', 'imagem', array('label' => 'Imagem (jpg ou png) 922x538px'));
        $arq->addValidator('Extension', false, 'jpg,png')
                ->setDestination($caminho);
        if($isEditing){
            $arq->setRequired(false);
        }else{
            $arq->setRequired(false);
        }     
        if ($arq->getFileName()) {
            $arrayExt = explode(".", $arq->getFileName());
            $ext = end($arrayExt);
            $novoNomeArquivo = date('Ymdhisu') . mt_rand(1000, 9999) . "." . $ext;
            $arq->addFilter("Rename", array("target" => $caminho . '/' . $novoNomeArquivo, "overwrite" => true));
        }
        $this->addElement($arq);

     
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

