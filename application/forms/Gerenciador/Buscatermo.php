<?php

class Application_Form_Gerenciador_Buscatermo extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        
        $name = $this->createElement('text', 'nome', array('label' => 'Nome', 'class' => 'form-control','placeholder'=>'Nome'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true);
        $this->addElement($name);
        
        $name = $this->createElement('text', 'id_inpi', array('label' => 'ID do INPI', 'class' => 'form-control','placeholder'=>'digite o ID do INPI'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 10))
                ->setRequired(true);
        $this->addElement($name);
   
        
     
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

