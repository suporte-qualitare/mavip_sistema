<?php

class Application_Form_Gerenciador_Categoriablog extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');
        
        $name = $this->createElement('text', 'nome', array('label' => 'Nome', 'class' => 'form-control', 'placeholder'=>'Nome'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true);
        $this->addElement($name);  
    }

}

