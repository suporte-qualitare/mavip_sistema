<?php

class Application_Form_Gerenciador_Cliente extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $username = $this->createElement('text', 'inpi_login', array('label' => 'Login INPI', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                ->addValidator('alnum')
                ->addValidator('regex', false, array('/^[A-z0-9_-]+/'))
                ->addValidator('stringLength', false, array(3, 20))
                ->setRequired(false);
        //->addFilter('StringToLower');
        $this->addElement($username);

        $name = $this->createElement('text', 'email', array('label' => 'E-mail', 'class' => 'form-control', 'placeholder' => 'Digite o E-mail'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 80))
            ->setRequired(true)
            ->addValidator(
                'Db_NoRecordExists', true, array(
                    'table' => 'cliente',
                    'field' => 'email',
                    'exclude' => array(
                        'field' => 'id',
                        'value' => $isEditing
                    ),
                    'messages' => array(
                        'recordFound' => 'E-mail existente'
                    )
                )
            );
        $this->addElement($name);

        $name = $this->createElement('text', 'nome', array('label' => 'Responsável Principal', 'class' => 'form-control', 'placeholder' => 'Digite o nome completo'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 80))
            ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'nome2', array('label' => 'Responsável Secundário', 'class' => 'form-control', 'placeholder' => 'Opcional'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 80));
        $this->addElement($name);

        $name = $this->createElement('text', 'razao_social', array('label' => 'Razão Social', 'class' => 'form-control', 'placeholder' => 'Razão Social'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 80));
        $this->addElement($name);


        $name = $this->createElement('text', 'telefone', array('label' => 'Telefone', 'class' => 'form-control', 'placeholder' => 'Digite o Telefone'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 18))
            ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'cnpj', array('label' => 'CPF / CNPJ', 'class' => 'form-control', 'placeholder' => 'Digite o CPF', 'required'=>TRUE));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 80))
            ->setAttrib('required','')
            ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'cpf_pessoa_juridica', array('label' => 'CPF Pessoa Juridica', 'class' => 'form-control' ));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 80));
        $this->addElement($name);

        $name = $this->createElement('text', 'telefone', array('label' => 'Telefone', 'class' => 'form-control', 'placeholder' => 'Digite o Telefone'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 18))
            ->setAttrib('required','')
            ->setRequired(false);
        $this->addElement($name);


        $name = $this->createElement('text', 'celular', array('label' => 'Celular', 'class' => 'form-control', 'placeholder' => 'Digite o Celular'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 18))
            ->setAttrib('required','')
            ->setRequired(false);
        $this->addElement($name);

        $username = $this->createElement('text', 'inpi_email', array('label' => 'E-mail INPI', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                ->addValidator('EmailAddress')
                ->addValidator('stringLength', false, array(0, 150))
                ->setRequired(false);
        //->addFilter('StringToLower');
        $this->addElement($username);

        $password = $this->createElement('text', 'inpi_senha', array('label' => 'Senha INPI', 'class' => 'form-control'));
        $password->setDecorators($this->decorators)
                ->addValidator('StringLength', true, array(3, 20));
        $password->setRequired(false);
        $this->addElement($password);


        $name = $this->createElement('text', 'logradouro', array('label' => 'Logradouro', 'class' => 'form-control', 'placeholder' => 'Digite o logradouro'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 200))
            ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'bairro', array('label' => 'Bairro', 'class' => 'form-control', 'placeholder' => 'Digite o bairro'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 200))
            ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'complemento', array('label' => 'No / Complemento', 'class' => 'form-control', 'placeholder' => 'Digite o número e complemento'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 200))
            ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'cep', array('label' => 'CEP', 'class' => 'form-control', 'placeholder' => 'Digite o CEP e pressione a tecla tab'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 9))
            ->setAttrib('required','')
            ->setRequired(true);
        $this->addElement($name);

        $username_sec = $this->createElement('text', 'inpi_login_sec', array('label' => 'Login Secundário INPI', 'class' => 'form-control'));
        $username_sec->setDecorators($this->decorators)
                ->addValidator('alnum')
                //->addValidator('regex', false, array('/^[a-z0-9_-]+/'))
                ->addValidator('stringLength', false, array(3, 20))
                ->setRequired(false);
        //->addFilter('StringToLower');
        $this->addElement($username_sec);

        $password_sec = $this->createElement('text', 'inpi_senha_sec', array('label' => 'Senha Secundária INPI', 'class' => 'form-control'));
        $password_sec->setDecorators($this->decorators)
                ->addValidator('StringLength', true, array(3, 20));
        $password_sec->setRequired(false);
        $this->addElement($password_sec);

        $clienteNaturezaModel = new Application_Model_ClienteNatureza();
        $name = $this->createElement('select', 'cliente_natureza_id', array('label' => 'Natureza', 'class' => 'form-control', 'placeholder' => 'Escolha a natureza Jurídica'));
        $name->setDecorators($this->decorators)
                ->setRequired(true)
                ->setMultiOptions($clienteNaturezaModel->getAllToSelect("Selecione a Natureza Jurídica"));
        $this->addElement($name);

        $clienteModel = new Application_Model_Cliente();
        $name = $this->createElement('select', 'atividade', array('label' => 'Atividade', 'class' => 'form-control', 'placeholder' => 'Escolha a atividade'));
        $name->setDecorators($this->decorators)
                ->setMultiOptions($clienteModel->atividades);
        $this->addElement($name);

        $password = $this->createElement('password', 'senha', array('label' => 'Senha (deixar em branco para não alterar)', 'class' => 'form-control'));
        $password->setDecorators($this->decorators)
                ->setDescription("deixar em branco para não alterar")
                ->setRequired(false)
                ->addValidator('StringLength', true, array(3, 20));
        $this->addElement($password);

        $password = $this->createElement('password', 'senha_repetir', array('label' => 'Repetir Senha', 'class' => 'form-control'));
        $password->setDecorators($this->decorators)
                ->addValidator('StringLength', true, array(3, 20))
                ->setRequired(false)
                ->addValidator('Identical', true, array('token' => 'senha', 'messages' => array('notSame' => 'Senha digitadas são diferente!')));

        $this->addElement($password);

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
