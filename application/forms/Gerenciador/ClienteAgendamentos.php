<?php

class Application_Form_Gerenciador_ClienteAgendamentos extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $name = $this->createElement('text', 'titulo', array('label' => 'Título', 'class' => 'form-control'));
        $name->setDecorators($this->decorators);
        $this->addElement($name);

        $usersModel = new Application_Model_Users();
        $role = $this->createElement('select', 'consultor', array('multioptions' =>  $usersModel->toSelectConsultor(),'class' => 'form-control'));
        $role->setLabel('Consultor')->setDecorators($this->decorators)
            ->setRequired(true);
        $this->addElement($role);


        $role = $this->createElement('checkbox', 'sendClient', array('1' => 'SIM' ,'class' => 'form-control'));
        $role->setLabel('Enviar email para o cliente?')->setDecorators($this->decorators);
        $this->addElement($role);

        $name = $this->createElement('text', 'data', array('label' => 'Data / Hora', 'class' => 'form-control datetimepicker','placeholder'=>''));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);


        $name = $this->createElement('textarea', 'observacao', array('label' => 'Observação', 'class' => 'form-control', 'placeholder'=>'Opcional'));
        $name->setDecorators($this->decorators)
            ->setAttrib('rows', 2);
        $this->addElement($name);


        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
