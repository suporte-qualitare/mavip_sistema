<?php

class Application_Form_Gerenciador_ClienteAgente extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $users = new Application_Model_Users();
        
        $name = $this->createElement('select', 'agente_id', array('multioptions' => $users->selectAll() ,'label' => 'Agente', 'class' => 'form-control','placeholder'=>'Digite aqui o nome ou razão social'));
        $name//->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
