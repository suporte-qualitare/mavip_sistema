<?php

class Application_Form_Gerenciador_ClienteLeadFields extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $name = $this->createElement('text', 'valor', array('label' => 'Campo', 'class' => 'form-control', 'placeholder' => 'Digite uma origem de lead'));
        $name->setDecorators($this->decorators)->setRequired(true);
        $this->addElement($name);

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
