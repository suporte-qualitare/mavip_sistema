<?php

class Application_Form_Gerenciador_ClienteObservacoes extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $name = $this->createElement('textarea', 'observacoes_internas', array('label' => 'Observações', 'class' => 'form-control ckeditor'));
        $name->setDecorators($this->decorators)
            ->setAttrib('rows', 7)
            ->setRequired(true);
        $this->addElement($name);

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
