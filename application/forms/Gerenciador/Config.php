<?php

class Application_Form_Gerenciador_Config extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');


        foreach($options['campos'] as $campo => $texto){
          $username = $this->createElement('text', $campo, array('label' => $texto, 'class' => 'form-control'));
          $username->setDecorators($this->decorators)
                 ->addValidator('stringLength', false, array(0, 300))
                 ->setRequired(true);
           $this->addElement($username);
        }



        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
