<?php

class Application_Form_Gerenciador_Cores extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        
        $name = $this->createElement('text', 'nome', array('label' => 'Nome', 'class' => 'form-control','placeholder'=>'Ex: Vermelho'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(1, 50))
                ->setRequired(true);
        $this->addElement($name);
        
        $name = $this->createElement('text', 'hexadecimal', array('label' => 'Hexadecimal', 'class' => 'form-control','placeholder'=>'Ex: #FF0000'));
        $name->setDecorators($this->decorators)
        ->addValidator('stringLength', false, array(1, 7))
                ->setRequired(true);
        $this->addElement($name);

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

