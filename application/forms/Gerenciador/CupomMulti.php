<?php

class Application_Form_Gerenciador_CupomMulti extends Twitter_Bootstrap3_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        
        $name = $this->createElement('text', 'nome', array('label' => 'Nome', 'class' => 'form-control','placeholder'=>'Nome de Prefixo'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true);
        $this->addElement($name);
        
        $name = $this->createElement('number', 'quantidade', array('label' => 'Quantidade', 'class' => 'form-control','placeholder'=>'Quantidade de Cupons'));
        $name//->setDecorators($this->decorators)
                ->addValidator(new Zend_Validate_Between(array('min' => 1, 'max' => 100)))
                ->addValidator(new Zend_Validate_Int())
                ->setRequired(true)
                ->setValue(1);
        $this->addElement($name);
        
        $name = $this->createElement('text', 'desconto', array('label' => 'Desconto', 'class' => 'form-control decimal','placeholder'=>'Valor do desconto (real ou porcentagem)'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 12))
                ->setRequired(true);
        $this->addElement($name);
        
        $cupomModel = new Application_Model_DbTable_Cupom();
        $role = $this->createElement('select', 'tipo', array('multioptions' =>  $cupomModel->tipo,'class' => 'form-control'));
        $role->setLabel('Tipo de desconto')->setDecorators($this->decorators);
        $this->addElement($role);
        
        
   
      
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

