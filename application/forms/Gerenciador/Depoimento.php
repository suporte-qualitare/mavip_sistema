<?php

class Application_Form_Gerenciador_Depoimento extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');  
        
        $name = $this->createElement('text', 'nome', array('label' => 'Nome', 'class' => 'form-control','placeholder'=>'Digite o nome'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 120))
                ->setRequired(true);
        $this->addElement($name);
        
        $name = $this->createElement('text', 'link', array('label' => 'Link', 'class' => 'form-control','placeholder'=>'Digite o link'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 500))
                ->setRequired(false);
        $this->addElement($name);
                 
        $name = $this->createElement('textarea', 'depoimento', array('label' => 'Depoimento', 'class' => 'form-control ', 'placeholder'=>'Depoimento'));
        $name->setDecorators($this->decorators)
                ->setAttrib('rows', '3')
                ->setRequired(false);
        $this->addElement($name);
        
        $caminho = PATH_ARQS.'depoimentos';
        if (!file_exists($caminho)) {
            mkdir($caminho, 0755, true);
        }
        
        $arq = $this->createElement('file', 'imagem', array('label' => 'Imagem (jpg ou png) 200x225px'));
        $arq->addValidator('Extension', false, 'jpg,png')
                ->setDestination($caminho);
        if($isEditing){
            $arq->setRequired(false);
        }else{
            $arq->setRequired(false);
        }     
        if ($arq->getFileName()) {
            $arrayExt = explode(".", $arq->getFileName());
            $ext = end($arrayExt);
            $novoNomeArquivo = date('Ymdhisu') . mt_rand(1000, 9999) . "." . $ext;
            $arq->addFilter("Rename", array("target" => $caminho . '/' . $novoNomeArquivo, "overwrite" => true));
        }
        $this->addElement($arq);

     
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

