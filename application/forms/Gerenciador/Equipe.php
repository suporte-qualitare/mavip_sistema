<?php

class Application_Form_Gerenciador_Equipe extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $name = $this->createElement('text', 'nome', array('label' => 'Nome', 'class' => 'form-control','placeholder'=>'Nome'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true);
        $this->addElement($name);
        
        
        $name = $this->createElement('textarea', 'descricao', array('label' => 'Descrição', 'class' => 'form-control','placeholder'=>'digite o contrato para não usar o padrão'));
        $name->setDecorators($this->decorators)
                ->setAttrib('rows', 2)
                ->setRequired(false);
        $this->addElement($name);
      
     
        $caminho = PATH_ARQS.'equipe';
        if (!file_exists($caminho)) {
            mkdir($caminho, 0755, true);
        }
        
        $arq = $this->createElement('file', 'imagem', array('label' => 'Imagem (jpg ou png) 310x350px'));
        $arq->addValidator('Extension', false, 'jpg,png')
                ->setDestination($caminho);
        
        if ($arq->getFileName()) {
            $arrayExt = explode(".", $arq->getFileName());
            $ext = end($arrayExt);
            $novoNomeArquivo = date('Ymdhisu') . mt_rand(1000, 9999) . "." . $ext;
            $arq->addFilter("Rename", array("target" => $caminho . '/' . $novoNomeArquivo, "overwrite" => true));
        }
        $this->addElement($arq);
        
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

