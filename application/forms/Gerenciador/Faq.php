<?php

class Application_Form_Gerenciador_Faq extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        
        $name = $this->createElement('text', 'titulo', array('label' => 'Título', 'class' => 'form-control','placeholder'=>'Título'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(1, 120))
                ->setRequired(true);
        $this->addElement($name);
        
        $name = $this->createElement('textarea', 'resposta', array('label' => 'Resposta', 'class' => 'form-control ckeditor','placeholder'=>'Resposta'));
        $name->setDecorators($this->decorators)
                ->setAttrib('rows', '3')
                ->setRequired(true);
        $this->addElement($name);

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

