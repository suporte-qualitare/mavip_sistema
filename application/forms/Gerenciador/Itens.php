<?php

class Application_Form_Gerenciador_Itens extends Twitter_Bootstrap3_Form  {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        
        $name = $this->createElement('text', 'descricao', array('label' => 'Item', 'class' => 'form-control','placeholder'=>'Digite aqui'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(1, 120))
                ->setRequired(true);
        $this->addElement($name);
        
        $name = $this->createElement('textarea', 'texto_tooltip', array('label' => 'Texto do tooltip', 'class' => 'form-control','placeholder'=>'Digite aqui'));
        $name->setDecorators($this->decorators)
                ->setAttrib('rows', '3')
                ->setRequired(true);
        $this->addElement($name);

        $role = $this->createElement('select', 'destaque', array('multioptions' =>  array('0'=>'Não','1'=>'Sim'),'class' => 'form-control'));
        $role->setLabel('Destaque verde?')->setDecorators($this->decorators);
        $this->addElement($role);

        $role = $this->createElement('select', 'novidade', array('multioptions' =>  array('0'=>'Não','1'=>'Sim'),'class' => 'form-control'));
        $role->setLabel('Novo?')->setDecorators($this->decorators);
        $this->addElement($role);
   


        $name = $this->createElement('number', 'ordem', array('label' => 'Ordem', 'class' => 'form-control','placeholder'=>'Ordem sequencial'));
        $name//->setDecorators($this->decorators)
                ->addValidator(new Zend_Validate_Int())
                ->setRequired(true)
                ->setValue(1);
        $this->addElement($name);
      

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

