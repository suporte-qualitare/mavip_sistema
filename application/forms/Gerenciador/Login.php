<?php

class Application_Form_Gerenciador_Login extends Zend_Form {

    public function init() {

        $this->addElement('text', 'login', array(
            'placeholder' => 'Seu e-mail',
            'label' => 'E-mail',
            'required' => true,
            'class' => 'form-control'
        ));

        $this->addElement('password', 'senha', array(
            'placeholder' => 'Senha',
            'label' => 'Senha',
            'required' => true,
            'class' => 'form-control'
        ));
    }

}
