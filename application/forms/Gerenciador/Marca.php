<?php

class Application_Form_Gerenciador_Marca extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $validator = new Zend_Validate_Db_NoRecordExists(
                array(
            'table' => 'marca',
            'field' => 'processo',
            'exclude' => array(
                'field' => 'id',
                'value' => $isEditing
            )
                )
        );

        $username = $this->createElement('text', 'processo', array('label' => 'Número do Processo', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                ->addValidator('int')
                ->addValidator('stringLength', false, array(3, 20))
                ->setRequired(false)
                //->addValidator($validator)
                ->addFilter('StringToLower');
        $this->addElement($username);

        $clientesModel = new Application_Model_Cliente();
        $cliente = $this->createElement('select', 'cliente_id', array('multioptions' => $clientesModel->toSelect(), 'class' => 'form-control select2'));
        $cliente->setLabel('Cliente')->setDecorators($this->decorators);
        $this->addElement($cliente);

        $username = $this->createElement('text', 'marca', array('label' => 'Marca', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(false)
                ->addFilter('StringToLower');
        $this->addElement($username);

        $username = $this->createElement('text', 'nosso_numero', array('label' => 'Nosso Número', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(false)
                ->addFilter('StringToLower');
        $this->addElement($username);

        $username = $this->createElement('text', 'data_acompanhamento', array('label' => 'Fim de acompanhamento', 'class' => 'form-control datepicker'));
        $username->setDecorators($this->decorators)
                ->setRequired(false)
                ->addFilter('StringToLower');
        $this->addElement($username);
        
           $username = $this->createElement('text', 'publicacao_inpi', array('label' => 'Data de registro (decênio)', 'class' => 'form-control datepicker'));
        $username->setDecorators($this->decorators)
                ->setRequired(false)
                ->addFilter('StringToLower');
        $this->addElement($username);

        $role = $this->createElement('select', 'acompanhamento', array('multioptions' =>  array('0'=>'Não','1'=>'Sim'),'class' => 'form-control'));
        $role->setLabel('Acompanhamento')->setDecorators($this->decorators);
        $this->addElement($role);


        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
