<?php

class Application_Form_Gerenciador_MarcaConcluir extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null, $id = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');       
       
        
        $username = $this->createElement('text', 'processo', array('label' => 'Número do Processo', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 20))
                ->setRequired(false)
                //->addValidator($validator)
                ->addFilter('StringToLower');
        $this->addElement($username);
        
        $username = $this->createElement('hidden', 'atualizar');
        $username->setDecorators($this->decorators);
        $this->addElement($username);

        $username = $this->createElement('text', 'nosso_numero', array('label' => 'Nosso Número', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(false)
                ->addFilter('StringToLower');
        $this->addElement($username);


        $username = $this->createElement('text', 'publicacao_inpi', array( 'class' => 'form-control datepicker', 'id' =>  'publicacao_inpi'));
        $username->setDecorators($this->decorators)
            ->setRequired(false)
            ->addFilter('StringToLower');
        $this->addElement($username);


        $username = $this->createElement('text', 'gru_paga', array('label' => 'Gru Paga?', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(false)
                ->addFilter('StringToLower');
        $this->addElement($username);

        $role = $this->createElement('select', 'gru_paga', array('multioptions' => array('0' => 'Não', '1' => 'Sim'), 'class' => 'form-control'));
        $role->setLabel('Gru Paga?')->setDecorators($this->decorators);
        $this->addElement($role);        
        
        $categoriaModel = new Application_Model_DbTable_Categoria();
        $role = $this->createElement('select', 'categoria_id', array('multioptions' => $categoriaModel->getAllToSelect(), 'class' => 'form-control'));
        $role->setLabel('NCL')->setDecorators($this->decorators);
        $this->addElement($role);        
        
        $textarea = $this->createElement('textarea', 'observacao', array('label' => 'Observação', 'class' => 'form-control', 'placeholder' => 'Observações'));
        $textarea->setDecorators($this->decorators)
                ->setAttrib('rows', '6')
                ->setRequired(false);
        $this->addElement($textarea);


        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
