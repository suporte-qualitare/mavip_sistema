<?php

class Application_Form_Gerenciador_MarcaHistoricoMensagem extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $name = $this->createElement('text', 'codigo_despacho', array('label' => 'Código de despacho no INPI', 'class' => 'form-control', 'placeholder' => 'digite o Código de despacho do INPI'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 10))
                ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'titulo', array('label' => 'Título', 'class' => 'form-control', 'placeholder' => 'Título'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'prazo', array('label' => 'Prazo', 'class' => 'form-control', 'placeholder' => 'Prazo'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 8))
                ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('textarea', 'mensagem', array('label' => 'Mensagem', 'class' => 'form-control', 'placeholder' => 'digite a mensagem', 'rows' => '4'));
        $name->setDecorators($this->decorators)
                ->setRequired(false);
        $this->addElement($name);



        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
