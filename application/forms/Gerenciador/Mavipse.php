<?php

class Application_Form_Gerenciador_Mavipse extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');
        
        $name = $this->createElement('text', 'marca', array('label' => 'Nome ou Razão Social', 'class' => 'form-control','placeholder'=>'Digite aqui o nome ou razão social'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

        $name = $this->createElement('text', 'resposanvel_2_cargo', array('label' => 'Cargo', 'class' => 'form-control','placeholder'=>'Opcional'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);


        $name = $this->createElement('text', 'responsavel', array('label' => 'Responsável (Master)', 'class' => 'form-control','placeholder'=>'Digite aqui o responsável principal pela marca'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

        $name = $this->createElement('text', 'responsavel_2', array('label' => 'Nome', 'class' => 'form-control','placeholder'=>'Opcional'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

        $name = $this->createElement('text', 'cargo', array('label' => 'Cargo', 'class' => 'form-control','placeholder'=>'Digite aqui o cargo do responsável'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

        $name = $this->createElement('text', 'resposanvel_2_cargo', array('label' => 'Cargo', 'class' => 'form-control','placeholder'=>'Opcional'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

        $name = $this->createElement('text', 'cnpj_cpf', array('label' => 'CPF ou CNPJ', 'class' => 'form-control','placeholder'=>'Digite aqui o CNPJ ou CPF'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

        $name = $this->createElement('text', 'cpf_cnpj_2', array('label' => 'CPF ou CNPJ', 'class' => 'form-control','placeholder'=>'Opcional'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

        $name = $this->createElement('text', 'email', array('label' => 'Email', 'class' => 'form-control','placeholder'=>'Digite aqui o email'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

        $name = $this->createElement('text', 'email_2', array('label' => 'Email', 'class' => 'form-control','placeholder'=>'Opcional'));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

        $telefone = $this->createElement('text', 'telefone_1', array('label' => 'Telefone', 'class' => 'form-control phone', 'placeholder' => 'Digite aqui o telefone'));
        $telefone->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 255));
        $this->addElement($telefone);

        $telefone = $this->createElement('text', 'telefone_2', array('label' => 'Telefone ', 'class' => 'form-control phone', 'placeholder' => 'Opcional'));
        $telefone->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 255));
        $this->addElement($telefone);

        $telefone = $this->createElement('textarea', 'observacoes', array('label' => 'Observações', 'class' => 'form-control', 'rows' => '5', 'placeholder' => 'Opcional'));
        $telefone->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 255));
        $this->addElement($telefone);

       include 'Mavipse_filtros_1.php';
       include 'Mavipse_filtros_2.php';

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

