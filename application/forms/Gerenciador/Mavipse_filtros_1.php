<?php

/*    $name = $this->createElement('text', 'filtros_1');
    $name->setDecorators($this->decorators);
    $name->setValue(1);
    $this->addElement($name);*/

        $name = $this->createElement('radio', 'p_marca', array('label' => 'Possui marca?'));
        $name->setDecorators($this->decorators)
             ->setMultiOptions(array('a'=>'SIM', 'b'=>'NÃO'))
             ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

    $name = $this->createElement('radio', 'p_marca_s', array('label' => 'Possui mais de uma marca?'));
    $name->setDecorators($this->decorators)
        ->setMultiOptions(array('c'=>'SIM', 'd'=>'NÃO'))
        ->addValidator('stringLength', false, array(0, 220));
    $this->addElement($name);

    $name = $this->createElement('text', 'p_numero_processo', array('label' => 'Numero do processo',
        'class' => 'form-control'));
    $name->setDecorators($this->decorators)
        ->addValidator('stringLength', false, array(0, 220));
    $this->addElement($name);

    $name = $this->createElement('text', 'p_comunicacao_cliente', array('label' => 'Como você se comunica com seu cliente?',
                                'class' => 'form-control'));
    $name->setDecorators($this->decorators)
        ->addValidator('stringLength', false, array(0, 220));
    $this->addElement($name);

    $name = $this->createElement('radio', 'p_canal_divulgacao', array('label' => 'Qual canal de divulgação?'));
    $name->setDecorators($this->decorators)
        ->setMultiOptions(array('j'=>'Instagram', 'l'=>'Facebook', 'm'=>'Google', 'n'=>'Linkedin', 'o'=>'Nenhum Canal'))
        ->addValidator('stringLength', false, array(0, 220));
    $this->addElement($name);

    $name = $this->createElement('textarea', 'p_observacao_f1', array('label' => 'Observação', 'class' => 'form-control', 'rows' => '8', 'placeholder' => 'Opcional'));
    $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(0, 255));
        $this->addElement($name);