<?php

        $name = $this->createElement('radio', 'p_marca_representa', array('label' => 'No seu ponto de vista o quanto a marca representa para sua empresa?'));
        $name->setDecorators($this->decorators)
             ->setMultiOptions(array('a'=>'Muita Importância', 'b'=>'Importante', 'c'=>'Irrelevante', 'd'=>'Sem Importância'))
             ->addValidator('stringLength', false, array(0, 220));
        $this->addElement($name);

    $name = $this->createElement('radio', 'p_experiencia_registro', array('label' => 'Você já teve alguma experiência com registro de marca?'));
    $name->setDecorators($this->decorators)
        ->setMultiOptions(array('e'=>'SIM', 'f'=>'NÃO'))
        ->addValidator('stringLength', false, array(0, 220));
    $this->addElement($name);

    $name = $this->createElement('radio', 'p_experiencia_descricao', array('label' => 'Como foi a experiência? Você teve uma...'));
    $name->setDecorators($this->decorators)
        ->setMultiOptions(array('g'=>'Experiência de satisfação', 'h'=>'Boa Experiência ou Sem Experiência', 'i'=>'Experiência Terrível'))
        ->addValidator('stringLength', false, array(0, 220));
    $this->addElement($name);

    $name = $this->createElement('radio', 'p_gestao_registro', array('label' => 'Como realiza a gestão dos registros ?'));
    $name->setDecorators($this->decorators)
        ->setMultiOptions(array('j'=>'Tenho gestão', 'l'=>'Não tenho gestão'))
        ->addValidator('stringLength', false, array(0, 220));
    $this->addElement($name);

    $name = $this->createElement('radio', 'p_marca_tempo', array('label' => 'Quanto tempo você tem de marca?'));
    $name->setDecorators($this->decorators)
        ->setMultiOptions(array('m'=>'Menos de 1 ano', 'n'=>'De 1 a 3 anos', 'o'=>'De 4 à 5 anos',  'p'=>'Mais de 5 anos'))
        ->addValidator('stringLength', false, array(0, 220));
    $this->addElement($name);

    $name = $this->createElement('radio', 'p_registro_tempo', array('label' => 'Em quanto tempo você pensa em fazer o registro de marca?'));
    $name->setDecorators($this->decorators)
        ->setMultiOptions(array('q'=>'Imediato', 'r'=>'Em 01 mês', 's'=>'Em mais de 03 meses'))
        ->addValidator('stringLength', false, array(0, 220));
    $this->addElement($name);

    $name = $this->createElement('radio', 'p_porte_empresa', array('label' => 'Qual é o porte da Empresa? (DE ACORDO COM SEU CNPJ)'));
    $name->setDecorators($this->decorators)
        ->setMultiOptions(array('t'=>'MEI', 'u'=>'ME', 'v'=>'EPP', 'x'=>'DEMAIS', 'y'=>'PF'))
        ->addValidator('stringLength', false, array(0, 220));
    $this->addElement($name);

    $name = $this->createElement('textarea', 'p_observacao_f2', array('label' => 'Observação', 'class' => 'form-control', 'rows' => '8', 'placeholder' => 'Opcional' ));
    $name->setDecorators($this->decorators)
        ->addValidator('stringLength', false, array(0, 255));
    $this->addElement($name);