<?php

class Application_Form_Gerenciador_Options extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        

        $optionsModel = new Application_Model_Options();
        $opt = $optionsModel->getById($isEditing);

        

        if ($opt) {
            if($opt["type"]=="text"){
            $name = $this->createElement('text', 'value', array('label' => $opt["name"], 'class' => 'form-control', 'placeholder' => $opt["name"]));
            $name->setDecorators($this->decorators)
                    //->addValidator('stringLength', false, array(0, 80))
                    ->setRequired(true);
            }elseif($opt["type"]=="textarea"){
                $name = $this->createElement('textarea', 'value', array('label' => $opt["name"], 'class' => 'form-control ckeditor','placeholder'=>$opt["name"]));
                $name->setDecorators($this->decorators)
                ->setRequired(true);
            }
            $this->addElement($name);
        }
    }

}
