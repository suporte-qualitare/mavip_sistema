<?php

class Application_Form_Gerenciador_Pagina extends Zend_Form {
   
    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
     
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        
        $name = $this->createElement('text', 'titulo', array('label' => 'Título', 'class' => 'form-control','placeholder'=>'Título'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 200))
                ->setRequired(true);
        $this->addElement($name);
   
        $name = $this->createElement('textarea', 'texto', array('label' => 'Texto', 'class' => 'form-control ckeditor','placeholder'=>'Texto'));
        $name->setDecorators($this->decorators)
                ->setAttrib('rows', '3')
                ->setRequired(false);
        $this->addElement($name);
      
        $name = $this->createElement('text', 'keywords', array('label' => 'Keywords', 'class' => 'form-control','placeholder'=>'Palavras chave'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 200))
                ->setRequired(false);
        $this->addElement($name);
        
        $name = $this->createElement('text', 'description', array('label' => 'Description', 'class' => 'form-control','placeholder'=>'Descrição'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 200))
                ->setRequired(false);
        $this->addElement($name);
        
     
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

