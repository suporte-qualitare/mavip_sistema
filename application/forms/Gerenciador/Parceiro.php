<?php

class Application_Form_Gerenciador_Parceiro extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');


        $name = $this->createElement('text', 'nome', array('label' => 'Nome', 'class' => 'form-control', 'placeholder' => 'Nome'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true);
        $this->addElement($name);

        $name = $this->createElement('text', 'hash', array('label' => 'Hash', 'class' => 'form-control', 'placeholder' => 'Digite a chave'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(false);
        $this->addElement($name);


        $categoriasModel = new Application_Model_DbTable_ParceiroCategoria();
        $role = $this->createElement('select', 'parceiro_categoria_id', array('multioptions' => $categoriasModel->getAllToSelect(), 'class' => 'form-control'));
        $role->setLabel('Categoria')->setDecorators($this->decorators);
        $this->addElement($role);



        $username = $this->createElement('text', 'login', array('label' => 'Login', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                ->addValidator('alnum')
                ->addValidator('regex', false, array('/^[a-z0-9_-]+/'))
                ->addValidator('stringLength', false, array(3, 20))
                ->setRequired(true)
                ->addFilter('StringToLower');
        $this->addElement($username);


        $password = $this->createElement('password', 'senha', array('label' => 'Senha', 'class' => 'form-control'));
        $password->setDecorators($this->decorators)
                ->addValidator('StringLength', true, array(3, 20));
        if ($isEditing) {
            $password->setRequired(false);
        } else {
            $password->setRequired(true);
        }
        $this->addElement($password);


        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
