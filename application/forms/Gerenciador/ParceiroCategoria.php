<?php

class Application_Form_Gerenciador_ParceiroCategoria extends Twitter_Bootstrap3_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $name = $this->createElement('text', 'nome', array('label' => 'Nome', 'class' => 'form-control','placeholder'=>'Nome'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true);
        $this->addElement($name);
        
        $name = $this->createElement('number', 'comissao', array('label' => 'Comissão', 'class' => 'form-control','placeholder'=>'valor da comissão (porcentagem)'));
        $name//->setDecorators($this->decorators)
                ->addValidator(new Zend_Validate_Between(array('min' => 0, 'max' => 100)))
                ->addValidator(new Zend_Validate_Int())
                ->setRequired(true);
        $this->addElement($name);
        
        $name = $this->createElement('number', 'desconto', array('label' => 'Desconto', 'class' => 'form-control','placeholder'=>'Valor do desconto (porcentagem)'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 12))
                ->addValidator(new Zend_Validate_Between(array('min' => 0, 'max' => 100)))
                ->setRequired(true)
                ->setValue(0);
        $this->addElement($name);
        
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

