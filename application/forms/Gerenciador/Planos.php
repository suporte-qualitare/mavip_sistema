<?php

class Application_Form_Gerenciador_Planos extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');


        $name = $this->createElement('text', 'titulo', array('label' => 'Título de exibição', 'class' => 'form-control', 'placeholder' => 'Título exibido no site'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true);
        $this->addElement($name);

       
        $servicoModel = new Application_Model_Servico();
        $role = $this->createElement('select', 'servico_id', array('multioptions' => $servicoModel->getAllToSelect(), 'class' => 'form-control'))
        ->setRequired(true);
        $role->setLabel('Serviço')->setDecorators($this->decorators);
        $this->addElement($role);

        $coresModel = new Application_Model_Cores();
        $cor = $this->createElement('select', 'cor', array('multioptions' => $coresModel->toSelect(), 'class' => 'form-control'));
        $cor->setLabel('Cor')->setDecorators($this->decorators);
        $this->addElement($cor);

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
