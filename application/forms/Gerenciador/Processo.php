<?php

class Application_Form_Gerenciador_Processo extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null, $id_pedido, $id_marca = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        //$pedidosModel = new Application_Model_DbTable_Pedido();

        $role = $this->createElement('hidden', 'id_pedido', array('class' => 'form-control'));
        $role->setDecorators($this->decorators);
        $role->setValue($id_pedido);
        $this->addElement($role);

        $role = $this->createElement('hidden', 'id_marca', array('class' => 'form-control'));
        $role->setDecorators($this->decorators);
        $role->setValue($id_marca);
        $this->addElement($role);

        $name = $this->createElement('text', 'descricao', array('label' => 'Descrição', 'class' => 'form-control', 'placeholder' => 'Descrição'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 255))
                ->setRequired(true);
        $this->addElement($name);

        $caminho = PATH_ARQS . 'processo';
        if (!file_exists($caminho)) {
            mkdir($caminho, 0755, true);
        }

        $arq = $this->createElement('file', 'arquivo', array('label' => 'Arquivo'));
        $arq->addValidator('MimeType', false, array(
                    'application/pdf', // PDF
                    'image/jpg', // JPG
                    'image/png', // PNG
                    'image/jpeg', // JPEG
                    'application/msword', // DOC
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' // DOCX
                ))
                ->setDestination($caminho);

        if ($arq->getFileName()) {
            $arrayExt = explode(".", $arq->getFileName());
            $ext = end($arrayExt);
            $novoNomeArquivo = date('Ymdhisu') . mt_rand(1000, 9999) . "." . $ext;
            $arq->addFilter("Rename", array("target" => $caminho . '/' . $novoNomeArquivo, "overwrite" => true));
        }
        $this->addElement($arq);

        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
