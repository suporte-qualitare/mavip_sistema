<?php

class Application_Form_Gerenciador_Servico extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $name = $this->createElement('text', 'titulo', array('label' => 'Título', 'class' => 'form-control','placeholder'=>'título'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 80))
                ->setRequired(true);
        $this->addElement($name);
        
        $name = $this->createElement('text', 'termo', array('label' => 'Termo do carrinho de compra', 'class' => 'form-control','placeholder'=>'digite os termos'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 200))
                ->setRequired(false);
        $this->addElement($name);
   
        $name = $this->createElement('textarea', 'descricao', array('label' => 'Descrição', 'class' => 'form-control ckeditor','placeholder'=>'digite a descrição da categoria'));
        $name->setDecorators($this->decorators)
                ->setAttrib('rows', 7)
                ->setRequired(true);
        $this->addElement($name);
        
        $name = $this->createElement('textarea', 'contrato', array('label' => 'Contrato', 'class' => 'form-control ckeditor','placeholder'=>'digite o contrato para não usar o padrão'));
        $name->setDecorators($this->decorators)
                ->setAttrib('rows', 7)
                ->setRequired(false);
        $this->addElement($name);
      
       $name = $this->createElement('text', 'valor', array('label' => 'Valor', 'class' => 'form-control decimal','placeholder'=>'Valor do serviço'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 12))
                ->setRequired(false);
        $this->addElement($name);

        $role = $this->createElement('select', 'acompanhamento', array('multioptions' => array('0' => 'Não', '1' => 'Sim'), 'class' => 'form-control'));
        $role->setLabel('Com Acompanhamento?')->setDecorators($this->decorators);
        $this->addElement($role);

        $name = $this->createElement('text', 'valor_gru', array('label' => 'Valor GRU (PF, MEI, ME, EPP)', 'class' => 'form-control decimal','placeholder'=>'Valor da GRU'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 12))
                ->setRequired(false);
        $this->addElement($name);
        
        $name = $this->createElement('text', 'valor_gru_pj', array('label' => 'Valor Gru (Empresa)', 'class' => 'form-control decimal','placeholder'=>'Valor da GRU'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 12))
                ->setRequired(false);
        $this->addElement($name);
        
   
        $name = $this->createElement('text', 'ordem', array('label' => 'Ordem', 'class' => 'form-control','placeholder'=>'Ordem na listagem'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 12))
                ->setRequired(false);
        $this->addElement($name);


        $caminho = PATH_ARQS.'contrato';
        if (!file_exists($caminho)) {
            mkdir($caminho, 0755, true);
        }

        $arq = $this->createElement('file', 'contrato_pdf', array('label' => 'ANEXAR CONTRATO ( PDF )'));
        $arq->addValidator('Extension', false, 'pdf')
            ->setDestination($caminho);

        if ($arq->getFileName()) {
            $arrayExt = explode(".", $arq->getFileName());
            $ext = end($arrayExt);
            $novoNomeArquivo = date('Ymdhisu') . mt_rand(1000, 9999) . "." . $ext;
            $arq->addFilter("Rename", array("target" => $caminho . '/' . $novoNomeArquivo, "overwrite" => true));
        }
        $this->addElement($arq);
     
        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}

