<?php

class Application_Form_Gerenciador_Textobotao extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');
        
        $name = $this->createElement('text', 'texto', array('label' => 'Texto', 'class' => 'form-control', 'placeholder'=>'Texto'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(0, 255))
                ->setRequired(true);
        $this->addElement($name);  
    }

}

