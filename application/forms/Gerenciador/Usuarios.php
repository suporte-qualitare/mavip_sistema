<?php

class Application_Form_Gerenciador_Usuarios extends Zend_Form {

    public $decorators = array('Composite');

    public function __construct($isEditing = 0, $options = null) {
        parent::__construct($options);
        $this->addElementPrefixPath('Util_Decorator', 'Util/Decorator/', 'decorator');
        $this->setMethod('post');

        $name = $this->createElement('text', 'name', array('label' => 'Nome', 'class' => 'form-control','placeholder'=>'Nome'));
        $name->setDecorators($this->decorators)
                ->addValidator('stringLength', false, array(3, 80))
                ->setRequired(true);
        $this->addElement($name);

 
        $username = $this->createElement('text', 'login', array('label' => 'Login', 'class' => 'form-control'));
        $username->setDecorators($this->decorators)
                ->addValidator('alnum')
                ->addValidator('regex', false, array('/^[a-z0-9_-]+/'))
                ->addValidator('stringLength', false, array(3, 20))
                ->setRequired(true);
                //->addFilter('StringToLower');
        $this->addElement($username);

        $aclRolesDbTable = new Application_Model_DbTable_AclRoles();
        $role = $this->createElement('select', 'role_id', array('multioptions' =>  $aclRolesDbTable->toSelect(),'class' => 'form-control'));
        $role->setLabel('Perfil')->setDecorators($this->decorators);
        $this->addElement($role);
        
        $password = $this->createElement('password', 'password', array('label' => 'Senha', 'class' => 'form-control'));
        $password->setDecorators($this->decorators)
                ->addValidator('StringLength', true, array(3, 20));
        if ($isEditing) {
            $password->setRequired(false);
        } else {
            $password->setRequired(true);
        }
        $this->addElement($password);

        $name = $this->createElement('text', 'email', array('label' => 'Email', 'class' => 'form-control','placeholder'=>''));
        $name->setDecorators($this->decorators)
            ->addValidator('stringLength', false, array(4, 30))
            ->setRequired(true);
        $this->addElement($name);

        $password = $this->createElement('password', 'password_repetir', array('label' => 'Repetir Senha', 'class' => 'form-control'));
        $password->setDecorators($this->decorators)
                ->addValidator('StringLength', true, array(3, 20))
                
                ->addValidator('Identical', true, array('token' =>'password','messages' => array('notSame' => 'Senha digitadas são diferente!')));
        
        //new Zend_Validate_Identical();
        if ($isEditing) {
            $password->setRequired(false);
        } else {
            $password->setRequired(true);
        }
        $this->addElement($password);


        $this->addElement('submit', 'salvar', array('label' => 'Salvar', 'class' => 'btn '));
    }

}
