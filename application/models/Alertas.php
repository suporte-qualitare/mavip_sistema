<?php
class Application_Model_Alertas {
    
       public function getAll()  {
        
        $db = Zend_Db_Table::getDefaultAdapter();
      
        $select = $db->select()
        ->from('alertas');
        
        return $db->fetchAll($select);
    }
    
    public function getByIpas($ipas)  {
        $db = Zend_Db_Table::getDefaultAdapter();
        
        $select = $db->select()
        ->from('alertas')
        ->where('ipas = ?', $ipas);
        
        return  $db->fetchAll($select);
    }
    
    public function insert($dados)  {
        $alertasMapper = new Application_Model_DbTable_Alertas();
        
        $row = $alertasMapper->createRow();
        $row->marca = $dados['razao'];
        $row->email = $dados['email'];
        $row->ipas = $dados['ipas'];
        $row->processo = $dados['processo'];
        return $row->save();
    }
    
    public function  delete($marca){
        
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "DELETE from alertas where marca = '$marca'";
  
       $delete = $db->query($sql);
        echo var_dump($delete);
        exit();
        
        if($delete)
            return $delete;
        else 
            return false;
        
    }
    
}