<?php

class Application_Model_Bancodemarcas
{


    public function getAll($paginate = false)
    {
        $db = Zend_Db_Select::getDefaultAdapter();
        $select = $db->select()->from('banco_de_marcas');
        return ($paginate) ? $select : $this->fetchAll($select);
        return $db->fetchAll($select);
    }

    public function getAllByTipoAndClasse($tipo, $classe,$letra = null,$busca = null)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $tipo = '%' . $tipo . "%";
        
        $select = $db->select()->from('banco_de_marcas')
            ->where('tipo LIKE  ?', $tipo)
            ->where('marca NOT LIKE ?', '-')
            ->order('marca');
        
        if($letra){
            $select->where('marca LIKE  ?', $letra.'%');
        }
        
        if($classe){
           $classe = (int)$classe;
           $select->where('classe LIKE  ?', $classe);
        }
        
        if($busca){
            $select->where('marca LIKE ?', $busca.'%');
        }
        
        return $db->fetchAll($select);
    }

    public function getAllByLetra($tipo, $classe, $letra)
    {
        // SELECT marca FROM banco_de_marcas WHERE marca LIKE '$letra%'
        $db = Zend_Db_Table::getDefaultAdapter();
        $letra = $letra.'%';
        $select = $db->select()->from('banco_de_marcas')
        ->where('marca LIKE  ?', $letra)
        ->where('LOWER (tipo) LIKE  ?', '%'.$tipo.'%');
        
         if($classe){
           $classe = (int)$classe;
           $select->where('classe LIKE  ?', $classe);
        }
        
        

        return $db->fetchAll($select);
    }


    public function getAllByNomeAndTipo($nome, $tipo)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $nome = '%' . $nome . "%";
        if ($tipo == "servico"):
            $tp = "%Serviço%";
        endif;
        if ($tipo == "produto"):
            $tp = "%Produto%";
        endif;
        if ($tipo == "comercio"):
            $tp = "%Comércio%";
        endif;
        
        $select = $db->select()->from('banco_de_marcas')
            ->where('marca LIKE ?', $nome);
            if($tipo !== ''){
           $select = $select->where('tipo LIKE ?', $tp);
            }
        $resultado = $db->fetchAll($select);

        if ($resultado) {
            return $resultado;
        }

    }
}
