<?php

class Application_Model_Banner {

    public function getPublicidadeRand() {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()->from('banner')
                ->where('active = 1')
                ->order('RAND()');

       return $db->fetchAll($select);
    }
    
    
}
