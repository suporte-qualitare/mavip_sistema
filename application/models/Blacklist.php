<?php

class Application_Model_Blacklist {

    public function isValid($email) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('blacklist', array('email'));
        $res = $db->fetchAll($select);

        $k = explode('@', $email);

        $teste = FALSE;

        foreach ($res as $value) {
            if ($value['email'] == $email) {
                $teste = TRUE;
            }

            $e = explode('*@', $value['email']);

            if ($e) {
                if ($e[1] == $k[1]) {
                    $teste = TRUE;
                }
            }

            $h = explode('#', $value['email']); 
            if (count($h) > 1) {                
                $pattern = '/' . $h[1] . '/';
                if (preg_match($pattern, $email)) {
                    $teste = TRUE;
                } 
            }
        }
        
        return $teste;
    }

}
