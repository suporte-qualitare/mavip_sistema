<?php

class Application_Model_Blog {

    public function getAllPage($q = null, $categoria = null, $page = 1) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $this->auth = Zend_Auth::getInstance();

        $select = $db->select()->from('blog')
                ->where('active = 1')
                ->where('data_inicio <= NOW()')
                ->order('data_inicio DESC');
        if ($q) {
            $select->where('titulo LIKE ?', '%' . $q . '%');
        }
        
        if (!$this->auth->hasIdentity()) {
            $select->where('categoria_blog_id != 1'); // CATEGORIA REFERENTE A CLIENTE
        }

        if ($categoria) {
            $select->where('categoria_blog_id = ?', $categoria);
        }

        $paginas = Zend_Paginator::factory($db->fetchAll($select));

        $paginas->setItemCountPerPage(10);

        $paginas->setCurrentPageNumber($page);

        return $paginas;
    }    
   
    public function getCategorias() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $this->auth = Zend_Auth::getInstance();

        $select = $db->select()->from('categoria_blog',array("categoria_blog.id", "categoria_blog.nome", "count(blog.id) as quant"))
                ->join('blog', 'categoria_blog.id=categoria_blog_id', array())
                ->order('nome ASC')
                ->order('data_inicio DESC')
                ->where('blog.active = 1')
                ->where('data_inicio <= NOW()')
                ->group(array("categoria_blog.id"));
        
        if (!$this->auth->hasIdentity()) {
            $select->where('blog.categoria_blog_id != 1'); // CATEGORIA REFERENTE A CLIENTE
        }

       return $db->fetchAll($select);
    }

    public function getUltimas($quant = 5) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()->from('blog')
                ->where('active = 1')
                ->where('data_inicio <= NOW()')
                ->order('data_inicio DESC')
                ->limit($quant);

        return $db->fetchAll($select);
    }

    public function getMaisLidas($quant = 5) {
        $db = Zend_Db_Table::getDefaultAdapter();        

        $select = $db->select()->from('blog')
                ->where('active = 1')
                ->order('acessos DESC')
                ->where('data_inicio <= NOW()')
                ->limit($quant);              

        return $db->fetchAll($select);
    }


    public function getRow($id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $id = $this->getIdUrl($id);
        $select = $db->select()->from('blog')
                ->where('active = 1')
                ->where('data_inicio <= NOW()')
                ->where('id = ?', $id)
                ->order('id DESC');

        return $db->fetchRow($select);
    }

    public function incrementaLeitura($id) {
        $blogDb = new Application_Model_DbTable_Blog();

        $id = $this->getIdUrl($id);

        $row = $blogDb->fetchRow(array("id = ?" => $id));
        if($row){  
            $row->acessos = $row->acessos + 1;
            return $row->save();
        }
        return false;
      
    }

    protected function getIdUrl($url) {

        if (strrpos($url, '-'))
            $v = 1;
        else
            $v = 0;

        $id = substr($url, strrpos($url, '-') + $v, strlen($url));

        $filter = new Zend_Filter_Int();

        return $filter->filter($id);
    }

}
