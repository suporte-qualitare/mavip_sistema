<?php

use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

require 'vendor/autoload.php';

class Application_Model_Busca
    {

    public $excessoes;
    public $termos;

    public function inserir($dados)
    {
        
        $buscaMapper = new Application_Model_DbTable_Busca();

        $row = $buscaMapper->createRow();
        $row->marca = $dados['marca'];
        if($dados['resultado'] == '-2')
            $row->resultado = $dados['resultado'];
        $row->email = $dados['email'];
        $row->ip = $dados['ip'];
        $row->cliente_id = $dados['cliente_id'];
        $row->data_cadastro = new Zend_Db_Expr('NOW()');

        return $row->save();
    }

    public function update($id, $dados)
    {
        $buscaMapper = new Application_Model_DbTable_Busca();
        $row = $buscaMapper->fetchRow(array('id = ?' => $id));
        if($dados['marca'])
            $row->marca = $dados['marca'];

        if($dados['email'])
            $row->email = $dados['email'];

        if($dados['nome'])
            $row->nome = $dados['nome'];

        if($dados['telefone'])
            $row->telefone = $dados['telefone'];

        if($dados['estilo'])
            $row->estilo = $dados['estilo'];

        if($dados['tipo'])
            $row->tipo = $dados['tipo'];

        if($dados['categoria_id'])
            $row->categoria_id = $dados['categoria_id'];

        $row->resultado = $dados['resultado'];
        $row->porcentagem = $dados['porcentagem'];

        return $row->save();
    }

    public function getById($idBusca)
    {
        $buscaMapper = new Application_Model_DbTable_Busca();

        $row = $buscaMapper->fetchRow(array('id = ?' => $idBusca));

        return $row;
    }
    
        public function getByClienteId($idClienteBusca)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from("busca")
        ->join("categoria", "busca.categoria_id = categoria.id", array('categoria.nome as classe'))
        ->where('cliente_id = ?', $idClienteBusca)
        ->order('data_cadastro desc');

        return $db->fetchAll($select);
    }
   

    public function getByInfo($marca, $tipo, $categoria_id)
    {
        $buscaMapper = new Application_Model_DbTable_Busca();

        $row = $buscaMapper->fetchRow(array(
            'marca = ?' => $marca,
            'tipo = ?' => $tipo,
            'categoria_id = ?' => $categoria_id
        ));

        return $row;

    }

    public function preparaTexto($termo)
    {
        $termo = mb_strtolower($termo, 'UTF-8');
        $termo = preg_replace("/(á|à|â|ã|ä)/", "a", $termo);
        $termo = preg_replace("/(é|è|ê)/", "e", $termo);
        $termo = preg_replace("/(í|ì)/", "i", $termo);
        $termo = preg_replace("/(ó|ò|ô|õ|ö)/", "o", $termo);
        $termo = preg_replace("/(ú|ù|ü)/", "u", $termo);
        $termo = str_replace("-", " ", $termo);

        return $termo;
    }

    public function busca($termo, $categoria = null)
    {

        if ($categoria AND $categoria < 10) {
            $categoria = "0" . $categoria;
        }

        $termoFiltrado = $this->filtraExcecao($termo, $categoria);


        if (strlen($termoFiltrado) < 3) {
            return 0;
        }

        $existeNaoExata = $this->fetchINPI($termoFiltrado, false, $categoria);
        //$existeExata = $this->fetchINPI($termoFiltrado, true, $categoria);
        try {

//echo $existeExata;exit;
            //echo $existeNaoExata.' - '.$existeExata;exit;
            //testa se é terminado em "S"
            if ($existeExata == 2) {
                $letra = substr($termoFiltrado, -1);
                if ($letra == "s" || $letra == "S") {
                    $termoFiltrado2 = substr($termoFiltrado, 0, -1);
                    $existeNaoExata2 = $this->fetchINPI($termoFiltrado2, false, $categoria);
                    $existeExata2 = $this->fetchINPI($termoFiltrado2, true, $categoria);
                    //echo $existeNaoExata;exit;
                    //$existeNaoExata = !$existeNaoExata ? $existeNaoExata : $existeNaoExata2;
                    $existeExata = !$existeExata ? $existeExata : $existeExata2;
                }
            }
        } catch (Exception $exc) {
            return -1;
        }

        //echo "fim:". $existeExata;
        return $existeNaoExata;
        //return $existeNaoExata;

        if ($existeExata) {
            return $existeExata;
        } else {
            return $existeNaoExata;
        }

        /* if ($existeExata AND $existeNaoExata) {
          return 0;
          } elseif (!$existeExata AND ! $existeNaoExata) {
          return 2;
          } elseif (!$existeExata || !$existeNaoExata) {
          return 0;
          } else {
          return 1;
          } */
    }

    public function filtraExcecao($termo, $categoria = null)
    {
        $termo = mb_strtolower($termo, 'UTF-8');


        $termo = trim($termo);
        $termoFiltrado = $termo;

        $termoArray = explode(" ", $termoFiltrado);


        $db = Zend_Db_Table::getDefaultAdapter();
        if (!$this->excessoes) {
            $this->excessoes = $db->fetchAll($db->select()->from("busca_excecao"));
        }

        foreach ($this->excessoes as $exc) {
            $exc["nome"] = mb_strtolower($exc["nome"], 'UTF-8');
            if ($exc["nome"] == $termo) {
                return '';
            }
            foreach ($termoArray as $key => $v) {
                if ($exc["nome"] == $v) {
                    unset($termoArray[$key]);
                }
            }
            //$termoFiltrado = str_ireplace($exc["nome"], '', $termoFiltrado);
            //$termoFiltrado = str_replace('  ', ' ', $termoFiltrado);
        }
        if (!$this->termos) {
            $this->termos = $db->fetchAll($db->select()->from("busca_termo")->where('id_inpi = ?', $categoria));
        }


        foreach ($this->termos as $exc) {

            $exc["nome"] = mb_strtolower($exc["nome"], 'UTF-8');
            foreach ($termoArray as $key => $v) {
                if ($exc["nome"] == $v) {
                    unset($termoArray[$key]);
                }
            }
            //$termoFiltrado = str_ireplace($exc["nome"], '', $termoFiltrado);
            //$termoFiltrado = str_replace('  ', ' ', $termoFiltrado);
        }

        //$termoFiltrado = trim($termoFiltrado);
        $termoFiltrado = implode(" ", $termoArray);

        $termoFiltrado = $this->preparaTexto($termoFiltrado);

        return $termoFiltrado;
    }

    public $quantTabela = 1;
    public $temTermo;
    public $registrada;
    public $marca;
    public $categoria;
    public $emVigor;

    private function log($data)
    {
        $log = new Application_Model_Logs();
        $log->create(1, $data);
    }

    /**
     *
     * @param type $marca texto da marca a ser buscada
     * @param type $pesquisaExata se é busca exata ou radical
     * @param type $categoria categoria do INPI
     * @return 0 = indisponivel, 1 = crítica e 2 = disponível
     * @throws Exception
     */
    private function fetchINPI($marca, $pesquisaExata, $categoria = null)
    {
        $url2 = 'https://busca.inpi.gov.br/pePI/servlet/MarcasServletController';
        $url = 'https://busca.inpi.gov.br/pePI/servlet/LoginController?action=login';

        if ($pesquisaExata) {
            $pesquisaExata = 'sim';
        } else {
            $pesquisaExata = 'nao';
        }

        $urlMarca = urlencode(utf8_decode($marca));

        $post_data = "Action=searchMarca&tipoPesquisa=BY_MARCA_CLASSIF_BASICA&buscaExata=$pesquisaExata&marca=$marca&registerPerPage=100";


        if ($categoria) {
            $post_data = $post_data . "&classeInter=" . $categoria;
        }


        $dadosArray = array(
            'Action' => 'searchMarca',
            'tipoPesquisa' => 'BY_MARCA_CLASSIF_BASICA',
            'buscaExata' => $pesquisaExata,
            'marca' => $marca,
            'registerPerPage' => '100',
        );

        if ($categoria) {
            $dadosArray["classeInter"] = $categoria;
        }
        $this->goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 10,
            'verify' => false,
        ));
        $this->goutteClient->setClient($guzzleClient);

        $this->crawler1 = $this->goutteClient->request("GET", $url);
        $this->crawler = $this->goutteClient->request("POST", $url2, $dadosArray);

        $content = $this->crawler->html();
        //echo $content;exit;

        $pos = strripos($content, 'Nenhum resultado foi encontrado para a sua pesquisa');
        $pos2 = strripos($content, 'RESULTADO DA PESQUISA');

        if (!$pos AND !$pos2) {
            throw new Exception('O banco de dados do INPI está indisponível no momento');
        }

        if ($pos) {

            return 2;
        } else {
            //$retorno = $this->consultaTabela($content, $marca,$categoria);
            try {
                $this->marca = $marca;
                $this->categoria = $categoria;

                $tabela = $this->crawler->filter('table')->eq(2);

                $tabela->filter('tr')->each(function ($tr) {
                    $marcaTitulo = $tr->filter('td')->eq(3)->filter('b');

                    if ($marcaTitulo->count() AND $marcaTitulo->text() != "Marca") {
                        //echo $marcaTitulo->text().'<br />';
                        $valor = mb_strtolower(trim($marcaTitulo->text()), 'UTF-8');
                        $textoPreparado = $this->preparaTexto($valor);

                        $valor = $this->filtraExcecao($valor, $this->categoria);
                      /*  echo "valor 1: ";
                        echo $valor;
                        echo " Categoria: ".$this->categoria;
                        echo "<br>";

                        $valor = $this->filtraExcecao($valor, $this->categoria);
                       
                        echo "valor depois excecao: ";
                        echo $valor;
                        echo "<br> prepara texto: ".$textoPreparado;
                        echo "<br> marca: ".$this->marca;
                        echo "<br> filter td: ".$tr->filter('td')->eq(7)->html();

                        die;
                        */

                        //echo $valor.' <-> '.$this->marca.'<br/>';
                        if ($valor AND ($valor == $this->marca || $textoPreparado == $this->marca) AND stripos($tr->filter('td')->eq(7)->html(), $this->categoria)) {
                           
                            if (strripos($tr->html(), "Marca Registrada")) {
                                $this->temTermo = true;
                                $this->registrada = true;
                            } elseif (strripos($tr->html(), "Marca Arquivada")) {
                                //$temTermo = false;
                            } elseif (strripos($tr->html(), "marca em vigor")) {
                                $this->emVigor = true;
                            } else {
                                $this->temTermo = true;
                            }
                        }
                      
                    }
                    //echo $textoPreparado.' <-> '.$valor.' <-> '.$this->marca.' <-> '.$this->temTermo.' <-> '.$this->registrada.'<br/>';
                });

          
//exit;
                if (($this->temTermo AND $this->registrada) || $this->emVigor) {
                    return 0;
                } elseif ($this->temTermo) {
                    return 1;
                } else {
                    return 2;
                }
            } catch (Exception $ex) {
                echo $ex->getMessage();
                exit;
            }

            //exit;
            //echo $retorno;
            //echo 'retorno: '.$retorno."<br />";exit;
            //return $retorno;
        }
    }
    public function noSpecials($texto){
        
        $texto = trim(html_entity_decode($texto));
        //tirando os acentos
        $texto= preg_replace('![ÁáÀàÃãÂâä]+!u','a',$texto);
        $texto= preg_replace('![ÉéÈèÊêë]+!u','e',$texto);
        $texto= preg_replace('![ÍíÌìÎîï]+!u','i',$texto);
        $texto= preg_replace('![ÓóÒòÕõÔôö]+!u','o',$texto);
        $texto= preg_replace('![ÚúÙùÛûü]+!u','u',$texto);
        //parte que tira o cedilha e o ñ
        $texto= preg_replace('![çÇ]+!u','c',$texto);
        $texto= preg_replace('![ñÑ]+!u','n',$texto);
      
        return utf8_encode(strtolower($texto));
    }

    public function create($dados)
   {
        $url =  "https://meu.inpi.gov.br/e-inpi/termo/Termo.jsp?action=28";
        $url2 = "https://meu.inpi.gov.br/e-inpi/servlet/ClienteAgenteController?action=28";
        $url3 = "https://meu.inpi.gov.br/e-inpi/servlet/CadClienteAgenteExecController?action=CadClienteExec";
         // $old = "https://formulario.inpi.gov.br/";

        $mavipCelular = "(11) 98802-1158";
        $dadosINPI = array(
            'PessoaAgenteCliente' => str_pad($dados['cliente_natureza_id'], 2, '0', STR_PAD_LEFT),
            'NumCPFCNPJ' => $dados['cnpj'],
            'NomCliente' => ($dados['razao_social']) ? $this->noSpecials($dados['razao_social']) : $this->noSpecials($dados['nome']),
            'PaisAgenteCliente' => $dados['pais'],
            'UfClienteAgente' => $dados['estado'],
            'CidadeAgenteCliente' => $dados['cidade_id'],
            'DscEndereco' => $this->noSpecials($dados['endereco']),
            'Cep' => $dados['cep'],
            'NumTelefone' => $mavipCelular,
            'NumTelefoneCel' => $mavipCelular,
            'NumFax' => $dados['fax'],
            'DscEmail' => $dados['inpi_email'],
            'DscLogin' => trim($dados['inpi_login']),
            'DscSenha' => trim($dados['inpi_senha']),
            'DscConfSenha' => $dados['inpi_senha'],
            'DscPergunta' => ($dados['pergunta']) ? $dados['pergunta'] : $dados['pergunta_secreta'],
            'DscResposta' => ($dados['resposta']) ? $dados['resposta'] : $dados['resposta_secreta'],
            'NR_Acao' => '2',
            'Log' => '01000l',
            'declaracaoConcordancia' => 'true',
            'BT_Voltar' => ' Voltar '
        );


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content1 = curl_exec($ch);

        curl_setopt($ch, CURLOPT_URL, $url2);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $content2 = curl_exec($ch);

        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $dadosINPI = http_build_query($dadosINPI);
        curl_setopt($ch, CURLOPT_URL, $url3);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dadosINPI);
        $content3 = curl_exec($ch);
        $response_code = curl_getinfo($ch);

        $log = new Application_Model_Logs();
        $log->create(1, "Criando cadastro no INPI para o CNPJ: ". $dados['cnpj']);
        $log->create(1, "INPI RESPONSE CONTENT: " . json_encode($response_code, true));
        $log->create(1, "INPI RESPONSE INFO: ". json_encode($content3, true));

        //echo($content3);exit;
       //// $response_code = curl_getinfo($ch);

       //// var_dump($response_code);
      //// var_dump(json_decode($content3, true));
      ////  die;

        //var_dump($response_code);
        //cadastrando no INPI


        curl_close($ch);

        $pos = strripos($content3, 'foi cadastrado com sucesso.');

        return ($pos);
    }

    public function testaLoginInpi($login, $senha)
    {
        //$url = 'https://gru.inpi.gov.br/e-inpi/servlet/InternetLoginController';
        $url = 'https://marcas.inpi.gov.br/emarcas/autenticar';

        /* $dadosINPI = array(
          'T_Login' => $login,
          'T_Senha' => $senha,
          'Acao' => 'login',
          ); */
        $dadosINPI = array(
            'usuario.login' => $login,
            'usuario.senha' => $senha,
            'browser' => 'chrome/54.0.2840.71',
        );


        $ch = curl_init();

        $dadosINPI = http_build_query($dadosINPI);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dadosINPI);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');

        $content1 = curl_exec($ch);

        curl_close($ch);
        //$content = utf8_encode($content1);
        //echo $content;exit;
        $pos = strripos($content1, 'Nosso Número (nº da GRU):');

        return ($pos);
    }

    /**
     * função busca pelo nome da empresa e retorna o e-mail e cnpj
     * @param type $nomeEmpresa nome da empresa completo
     */
    public function getDadosEmpresa($nomeEmpresa)
    {
        try {
            $cnpj = getCNPJ();
            
            if ($cnpj) {
                $email = $this->getEmail($cnpj);
            }
        } catch (Exception $ex) {
            $cnpj = NULL;
            $email = NULL;
        }
        /*
        try {
            $cnpj = $this->getCNPJ($nomeEmpresa);
            if ($cnpj) {
                $email = $this->getEmail($cnpj);
            }
        } catch (Exception $ex) {
            $cnpj = NULL;
            $email = NULL;
        }
        */
        return array('cnpj' => $cnpj, 'email' => $email, 'nome' => $nomeEmpresa);
    }

    public function getEmail($cnpj)
    {
        $url = 'https://formulario.inpi.gov.br/e-inpi/servlet/SenhaController?action=PesquisarCpfCnpj';
        $dadosArray = array(
            'T_CpfCnpj' => $cnpj,
        );

        //$url = 'https://formulario.inpi.gov.br/e-inpi/servlet/SenhaController?action=PesquisarCpfCnpj';
        $url2 = 'https://formulario.inpi.gov.br/e-inpi/servlet/SenhaController?action=RespSecretaEmail';


        $this->goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
            'verify' => false,
        ));
        $this->goutteClient->setClient($guzzleClient);

        $this->crawler = $this->goutteClient->request("POST", $url, $dadosArray);
        $this->crawler1 = $this->goutteClient->request("POST", $url2);

        $texto = $this->crawler1->filter('.labeltexto')->html();
        if (strpos($texto, 'o endereço:')) {
            $email = substr($texto, strpos($texto, 'o endereço:') + 13, -1);
            $email = substr($email, 0, strpos($email, '<'));
            return $email;
        } else {
            return false;
        }   
    }
      
    public function getCNPJ($razao)
    {
        // token temporario
        $token = "4d3364e9-2bda-45c8-9207-75d159cf3990";
        
      
             // Logica de implementacao...
               // consultar o ifaro para disponibilizar tal metodo
         $url = "http://ws.ifaro.com.br/APIDados.svc/ConsultaRazao/$razao/$token";
         $dadosJson = file_get_contents($url);
         $dados = json_decode($dadosJson, true);
  
         return $dados;
  


        // LOGICA ANTERIOR
        
        /*
        $url = "http://cnpj.info/busca";

        $dadosArray = array(
            'q' => $nomeEmpresa,
        );

        $this->goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
            'verify' => false,
        ));

        $this->goutteClient->setClient($guzzleClient);

        $this->crawler = $this->goutteClient->request("POST", $url, $dadosArray);

        //print_r($this->crawler); die();

        $cnpj = $this->crawler->filter('ul')->eq(0)->filter('li')->eq(0)->filter('a')->eq(0)->text();
        $nome = $this->crawler->filter('ul')->eq(0)->filter('li')->eq(0)->filter('a')->eq(1)->text();

        //print_r($cnpj); die();

        if (strpos(strtolower($nome), strtolower($nomeEmpresa)) === false) {
            return false;
        } else {
            return $cnpj;
        }
        */
    }


    public function getNomeEmpresa($numProcesso)
    {
        $url2 = 'https://gru.inpi.gov.br/pePI/servlet/MarcasServletController';
        $url = 'https://gru.inpi.gov.br/pePI/servlet/LoginController?action=login';

        $dadosArray = array(
            'NumPedido' => $numProcesso,
            'registerPerPage' => '100',
            'Action' => 'searchMarca',
            'tipoPesquisa' => 'BY_NUM_PROC'
        );

        $this->goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
            'verify' => false,
        ));
        $this->goutteClient->setClient($guzzleClient);

        $this->crawler1 = $this->goutteClient->request("GET", $url);
        $this->crawler = $this->goutteClient->request("POST", $url2, $dadosArray);

        $linha = $this->crawler->filter('table')->eq(2)->filter('tr')->eq(1);
        $marca = trim($linha->filter('td')->eq(3)->text());

        if ($marca) {
            return $marca;
        } else {
            return false;
        }
    }

    public function getNatureza($numProcesso)
    {
        $url2 = 'https://gru.inpi.gov.br/pePI/servlet/MarcasServletController';
        $url = 'https://gru.inpi.gov.br/pePI/servlet/LoginController?action=login';

        $dadosArray = array(
            'NumPedido' => $numProcesso,
            'registerPerPage' => '100',
            'Action' => 'searchMarca',
            'tipoPesquisa' => 'BY_NUM_PROC'
        );

        $this->goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
            'verify' => false,
        ));
        $this->goutteClient->setClient($guzzleClient);

        $this->crawler1 = $this->goutteClient->request("GET", $url);
        $this->crawler = $this->goutteClient->request("POST", $url2, $dadosArray);


        $linha = $this->crawler->filter('table')->eq(2)->filter('tr')->eq(1);
        $link = $linha->filter('td')->eq(0)->filter('a');

        $marca = trim($linha->filter('td')->eq(3)->text());
        $detalhes = $this->goutteClient->click($link->link());

        $natureza = $detalhes->filter('table')->eq(1)->filter('tr')->eq(4)->filter

                                                                                ('td')->eq(1)->text();
        $natureza = explode("De", $natureza);

        if ($natureza) {
            return $natureza[1];
        } else {
            return false;
        }
    }

    public function getClasse($numProcesso)
    {
        $url2 = 'https://gru.inpi.gov.br/pePI/servlet/MarcasServletController';
        $url = 'https://gru.inpi.gov.br/pePI/servlet/LoginController?action=login';

        $dadosArray = array(
            'NumPedido' => $numProcesso,
            'registerPerPage' => '100',
            'Action' => 'searchMarca',
            'tipoPesquisa' => 'BY_NUM_PROC'
        );

        $this->goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
            'verify' => false,
        ));
        $this->goutteClient->setClient($guzzleClient);

        $this->crawler1 = $this->goutteClient->request("GET", $url);
        $this->crawler = $this->goutteClient->request("POST", $url2, $dadosArray);

        $linha = $this->crawler->filter('table')->eq(2)->filter('tr')->eq(1);
        $classe = trim($linha->filter('td')->eq(7)->text());
        $classe = explode(" ", $classe);

        if ($classe) {
            return $classe[1];
        } else {
            return false;
        }
    }

    public function semApostilhaProcesso($numProcesso)
    {
        $url2 = 'https://gru.inpi.gov.br/pePI/servlet/MarcasServletController';
        $url = 'https://gru.inpi.gov.br/pePI/servlet/LoginController?action=login';

        $dadosArray = array(
            'NumPedido' => $numProcesso,
            'registerPerPage' => '100',
            'Action' => 'searchMarca',
            'tipoPesquisa' => 'BY_NUM_PROC'
        );

        $this->goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
            'verify' => false,
        ));
        $this->goutteClient->setClient($guzzleClient);

        $this->crawler1 = $this->goutteClient->request("GET", $url);

        $this->crawler = $this->goutteClient->request("POST", $url2, $dadosArray);

        $linha = $this->crawler->filter('table')->eq(2)->filter('tr')->eq(1);
        $link = $linha->filter('td')->eq(0)->filter('a');

        $marca = trim($linha->filter('td')->eq(3)->text());
        $detalhes = $this->goutteClient->click($link->link());

        $postila = $detalhes->filter('#apostila');
        if ($postila->count()) {
            //echo $postila->filter('#apostila')->filter('tr')->eq(2)->html();
            return false;
        } else {
            return $marca;
        }

        //echo $detalhes->filter('#apostila')->filter('tr')->eq(2)->html();
        //echo $detalhes->filter('#apostila')->html();
        //var_dump ();
    }

    public function setMavipse($id){
        $buscaMapper = new Application_Model_DbTable_Busca();
        $row = $buscaMapper->fetchRow(array('id = ?' => $id));
        if($row){
            $row->mavipse = 1;
            $row->save();
        }
    }
    public function unsetMavipse($id){
        $buscaMapper = new Application_Model_DbTable_Busca();
        $row = $buscaMapper->fetchRow(array('id = ?' => $id));
        if($row){
            $row->mavipse = 0;
            $row->save();
        }
    }
}
