<?php

class Application_Model_Categoria {

    public function getById($id) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('categoria')
                ->where('id = ?', $id);

        return $db->fetchRow($select);
    }

    public function getAllByTipo($tipo) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('categoria')
                ->where('tipo = ?', $tipo)
                ->order('nome');
        return $db->fetchAll($select);
    }

    public function getIdInpi($catId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('categoria', array('id_inpi'))
                ->where('id = ?', $catId);
        return $db->fetchAll($select);
    }
}
