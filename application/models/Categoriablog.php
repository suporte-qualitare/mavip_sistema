<?php

class Application_Model_CategoriaBlog {
    
    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()->from('categoria_blog');
        return $db->fetchRow($sql);
    }

    public function getById($id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('categoria_blog')
                ->where('id = ?', $id);
        return $db->fetchRow($select);
    }
}