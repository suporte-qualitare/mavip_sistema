<?php

class Application_Model_Cidades {

    public function getAllToSelect($estado = null, $preTexto = null) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('cidade');
        if ($estado) {
            $select->where('estado_id = ?', $estado);
        }
        $result = $db->fetchAll($select);

        $retorno = array();

        if ($preTexto) {
            $retorno[null] = $preTexto;
        }
        foreach ($result as $val) {
            $retorno[$val["id"]] = $val["nome"];
        }
        return $retorno;
    }

    public function getAllToJson($estado, $preTexto = null) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from("cidade")->order('nome');
        if ($estado) {
            $select->where('estado_id = ?', $estado);
        }
        $result = $db->fetchAll($select);

        $retorno = array();

        if ($preTexto) {
            $retorno[] = array(
                'id' => "",
                'nome' => $preTexto,
            );
        }

        foreach ($result as $cid) {
            $retorno[] = array(
                'id' => $cid['id'],
                'nome' => $cid['nome'],
            );
        }
        return json_encode($retorno);
    }

    public function getByCidNome($nome, $estadoId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from("cidade")->where('nome = ?',$nome)->where('estado_id = ?',$estadoId);
        $row = $db->fetchRow($select);

        if ($row) {
            return $row['id'];
        } else {
            return 0;
        }
    }

    public function getById($cidadeId) {
        if(!$cidadeId)
            return null;
        $cidadeDb = new Application_Model_DbTable_Cidades();
        $row = $cidadeDb->fetchRow(array('id = ?' => $cidadeId));

        return $row;
    }

}
