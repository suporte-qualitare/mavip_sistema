<?php

class Application_Model_Cliente {

    public $atividades = array(
        "0" => "Escolha uma Área Profissional",
        "1240" => "Advogados, Procuradores e Afins",
        "1170" => "Agrônomos e Afins",
        "2110" => "Ajudantes de Obras Civis",
        "5010" => "Arquitetos",
        "1650" => "Artistas de Artes Populares e Modelos",
        "666" => "Astrólogos e Adivinhos",
        "1660" => "Atletas, Desportistas e Afins",
        "1730" => "Auxiliares de Serviços de Biblioteca, Documentação e Correios",
        "1160" => "Biólogos e Afins",
        "1750" => "Caixas, Bilheteiros e Afins",
        "1020" => "Chefes de Pequenas Populações",
        "1260" => "Cientistas Sociais, Psicólogos e Afins",
        "2450" => "Condutores de Veículos e Operadores de Equipamentos de Elevação e de Movimentação de Cargas",
        "2610" => "Confeccionadores de Produtos de Papel e Papelão",
        "5020" => "Contadores e Auditores",
        "5030" => "Contínuos",
        "1640" => "Decoradores e Vitrinistas",
        "1400" => "Desenhistas Técnicos e Modelistas",
        "1770" => "Despachantes",
        "1040" => "Diretores Gerais",
        "1050" => "Diretores de Produção e Operações",
        "1060" => "Diretores de Áreas de Apoio",
        "1010" => "Dirigentes de Produção, Operações e Apoio da Administração Pública",
        "1140" => "Dirigentes e Administradores de Organizações de Interesse Público",
        "2770" => "Eletricistas-Eletrônicos de Manutenção Industrial, Comercial e Residencial",
        "2780" => "Eletricistas-Eletrônicos de Manutenção Veicular",
        "2470" => "Embaladores e Alimentadores de Produção",
        "2890" => "Engenheiros",
        "1100" => "Engenheiros Mecatrônicos",
        "1780" => "Entrevistadores e Afins",
        "1710" => "Escriturários Contábeis e de Finanças",
        "1720" => "Escriturários de Controle de Materiais, Despachos e de Transportes",
        "1690" => "Escriturários em Geral, Agentes, Assistentes e Auxiliares Administrativos",
        "2020" => "Extrativistas Florestais",
        "1130" => "Físicos, Químicos e Afins",
        "1070" => "Gerentes de Produção e Operações",
        "1080" => "Gerentes de Áreas de Apoio",
        "1530" => "Inspetores de Alunos e Afins",
        "1910" => "Instaladores de Produtos e Acessórios",
        "2200" => "Instaladores e Reparadores de Linhas e Cabos Elétricos e de Comunicações",
        "1520" => "Instrutores e Professores de Escolas Livres",
        "2250" => "Joalheiros e Ourives",
        "2790" => "Mantenedores Eletromecânicos",
        "2360" => "Marceneiros e Afins",
        "1110" => "Matemáticos e Estatísticos",
        "2730" => "Mecânicos de Manutenção Veicular",
        "2720" => "Mecânicos de Manutenção de Máquinas Pesadas e Equipamentos Agrícolas",
        "1000" => "Membros Superiores do Poder Legislativo, Executivo e Judiciário",
        "1310" => "Membros de Cultos Religiosos e Afins",
        "2820" => "Militares da Aeronáutica",
        "2840" => "Militares da Marinha",
        "2830" => "Militares do Exercito",
        "2170" => "Montadores de Máquinas e Aparelhos Mecânicos",
        "2230" => "Montadores e Ajustadores de Instrumentos Musicais",
        "2220" => "Montadores e Ajustadores de Instrumentos de Precisão",
        "2190" => "Montadores e Instaladores de Equipamentos Eletro-Eletrônicos em Geral",
        "2870" => "Oficiais de Bombeiro Militar",
        "2850" => "Oficiais de Polícia Militar",
        "2630" => "Operadores da Preparação de Alimentos e Bebidas",
        "2490" => "Operadores de Instalações Químicas, Petroquímicas e Afins",
        "2670" => "Operadores de Instalações de Geração e Distribuição de Energia Térmica, Elétrica e Nuclear",
        "2550" => "Operadores de Instalações e Equipamentos de Produção de Metais e Ligas -2ª Fusão",
        "2540" => "Operadores de Instalações e Equipamentos de Produção de Metais e Ligas-1ª Fusão",
        "2520" => "Operadores de Operação Unitária de Laboratório (Transversal Para Toda Indústria de Processos)",
        "2690" => "Operadores de Outras Instalações Industriais  Outros Trabalhadores Elementares Industriais",
        "2510" => "Operadores de Outras Instalações Químicas, Petroquímicas e Afins",
        "2440" => "Operadores de Robôs e Equipamentos Especiais",
        "2640" => "Operadores de Tratamento de Fumo e de Fabricação de Charutos e Cigarros (Inclusive Artesanais)",
        "2680" => "Operadores de Utilidades",
        "2750" => "Outros Mecânicos de Manutenção",
        "1230" => "Outros Professores de Ensino Não Classificados Anteriormente",
        "2800" => "Outros Trabalhadores da Conservação e da Conservação e Manutenção (Exceto Trabalhadores Elementares)",
        "1870" => "Outros Trabalhadores de Serviços Diversos",
        "2010" => "Pescadores, Caçadores",
        "5080" => "Policiais e Guardas de Transito",
        "2880" => "Praças de Bombeiro Militar",
        "2860" => "Praças de Polícia Militar",
        "1930" => "Produtores na Exploração Agropecuária em Geral",
        "1940" => "Produtores na Exploração Agrícola",
        "1950" => "Produtores na Exploração Pecuária",
        "1510" => "Professores Leigos na Educação Infantil , no Ensino Fundamental e Profissionalizante",
        "1190" => "Professores da Educação Infantil e do Ensino Fundamental (Com Formação Superior)",
        "1200" => "Professores do Ensino Médio (Com Formação Superior)",
        "1220" => "Professores do Ensino Superior",
        "1210" => "Professores e Instrutores do Ensino Profissional (Com Formação Superior)",
        "1500" => "Professores na Educação Infantil, no Ensino Fundamental e Profissionalizante (Com Formação de Nível Médio)",
        "1270" => "Profissionais da Administração",
        "1090" => "Profissionais da Bioengenharia, Biotecnologia, Engenharia Genética e da Metrologia",
        "1290" => "Profissionais da Comunicação",
        "1120" => "Profissionais da Informática",
        "1180" => "Profissionais da Medicina, Saúde e Afins",
        "1300" => "Profissionais de Espetáculos e das Artes",
        "1280" => "Profissionais de Marketing, Publicidade e Comercialização",
        "1250" => "Profissionais do Poder Judiciário e da Segurança Pública",
        "1150" => "Profissionais em Navegação Aérea, Marítima e Fluvial",
        "5070" => "Recepcionistas, Telefonistas e Afins",
        "2740" => "Reparadores de Instrumentos e Equipamentos de Precisão",
        "1900" => "Repositores, Remarcadores do Comércio",
        "1700" => "Secretários de Expediente e Operadores de Máquinas de Escritórios",
        "5060" => "Serviços de Correio",
        "2060" => "Supervisores da Extração Mineral e da Construção Civil",
        "2620" => "Supervisores da Fabricação de Alimentos, Bebidas e Fumo Agroindustriais",
        "2580" => "Supervisores da Fabricação de Celulose e Papel",
        "2350" => "Supervisores da Indústria da Madeira, Mobiliário e da Carpintaria Veicular",
        "2210" => "Supervisores da Mecânica de Precisão e Instrumentos Musicais",
        "2530" => "Supervisores da Siderurgia e de Materiais de Construção",
        "2120" => "Supervisores da Transformação de Metais e de Compósitos",
        "2480" => "Supervisores das Indústrias Químicas, Petroquímicas e Afins",
        "2270" => "Supervisores das Indústrias Têxteis, do Curtimento, do Vestúario e das Artes Gráficas",
        "1740" => "Supervisores de Atendimento Ao Público",
        "2430" => "Supervisores de Embalagem e Etiquetagem",
        "2660" => "Supervisores de Instalações de Produção e Distribuição de Energia, Utilidades, Captação, Tratamento e Distribuição de Ág",
        "2240" => "Supervisores de Joalheria, Vidraria, Cerâmica e Afins",
        "2760" => "Supervisores de Manutenção Eletro-Eletrônica e Eletromecânica",
        "2180" => "Supervisores de Montagens e Instalações Eletroeletrônicas",
        "2710" => "Supervisores de Reparação e Manutenção Mecânica",
        "2700" => "Supervisores de Reparação e Manutenção Veicular",
        "1680" => "Supervisores de Serviços Administrativos",
        "1880" => "Supervisores de Vendas e de Prestação de Serviços do Comércio",
        "1790" => "Supervisores dos Serviços e do Comércio",
        "1960" => "Supervisores na Exploração Agropecuária",
        "2000" => "Supervisores na Exploração Florestal, Caça e Pesca",
        "1980" => "Trabalhadores Agrícolas",
        "2650" => "Trabalhadores Artesanais da Agroindustria e da Indústria de Alimentos",
        "2410" => "Trabalhadores Artesanais da Madeira e do Mobiliário",
        "2340" => "Trabalhadores Artesanais das Atividades Têxteis, do Vestuário e das Artes Gráficas",
        "2570" => "Trabalhadores Artesanais de Materiais de Construção",
        "2810" => "Trabalhadores Elementares da Manutenção",
        "2420" => "Trabalhadores da Carpintaria Veicular",
        "2320" => "Trabalhadores da Confecção de Artefatos de Tecidos e Couros",
        "2310" => "Trabalhadores da Confecção de Calçados",
        "2300" => "Trabalhadores da Confecção de Roupas",
        "2090" => "Trabalhadores da Construção Civil e Obras Públicas",
        "2070" => "Trabalhadores da Extração Mineral",
        "2500" => "Trabalhadores da Fabricação de Munição e Explosivos Químicos",
        "2600" => "Trabalhadores da Fabricação de Papel",
        "2050" => "Trabalhadores da Irrigação e Drenagem",
        "2030" => "Trabalhadores da Mecanização Agropecuária",
        "2040" => "Trabalhadores da Mecanização Florestal",
        "2370" => "Trabalhadores da Preparação das Madeiras e do Mobiliário",
        "2590" => "Trabalhadores da Preparação de Pasta de Papel",
        "2330" => "Trabalhadores da Produção Gráfica",
        "2380" => "Trabalhadores da Transformação de Madeiras e da Fabricaçao do Mobiliario",
        "2280" => "Trabalhadores das Industrias Têxteis",
        "2100" => "Trabalhadores de Acabamento de Obras Civis",
        "2080" => "Trabalhadores de Beneficiamento de Minérios e Pedras",
        "2140" => "Trabalhadores de Conformação de Metais e de Compósitos",
        "1760" => "Trabalhadores de Informações Ao Público",
        "2560" => "Trabalhadores de Instalações e Equipamentos de Material de Construção, Cerâmica e Vidro",
        "2460" => "Trabalhadores de Logística e Acompanhamento de Serviços de Transporte",
        "2390" => "Trabalhadores de Montagem",
        "2160" => "Trabalhadores de Montagem de Tubulações, Estrutras Metálicas e de Compósitos",
        "2150" => "Trabalhadores de Tratamento Térmico e de Superfícies de Metais e de Compósitos",
        "2130" => "Trabalhadores de Usinagem de Metais e de Compósitos",
        "2400" => "Trabalhadores do Acabamento de Madeira e Mobiliário",
        "2290" => "Trabalhadores do Tratamento de Couros e Peles",
        "1810" => "Trabalhadores dos Serviços Domésticos em Geral",
        "1820" => "Trabalhadores dos Serviços de Hotelaria e Alimentação",
        "1840" => "Trabalhadores dos Serviços de Saúde",
        "1800" => "Trabalhadores dos Serviços de Transporte e Turismo",
        "1970" => "Trabalhadores na Exploração Agropecuária em Geral",
        "1990" => "Trabalhadores na Pecuária",
        "1830" => "Trabalhadores nos Serviços de Administração, Conservação e Manutenção de Edifícios e Logradouros",
        "1850" => "Trabalhadores nos Serviços de Embelezamento e Cuidados Pessoais",
        "1860" => "Trabalhadores nos Serviços de Proteção e Segurança",
        "1340" => "Técnico em Ciências Físicas e Químicas",
        "5050" => "Técnico em Estatística",
        "1320" => "Técnicos Eletromecânicos e Mecatrônicos",
        "1460" => "Técnicos da Ciência da Saúde Animal",
        "1450" => "Técnicos da Ciência da Saúde Humana",
        "1440" => "Técnicos da Produção Agropecuária",
        "1560" => "Técnicos das Ciências Administrativas",
        "1480" => "Técnicos de Bioquímica e da Biotecnologia",
        "1490" => "Técnicos de Conservação, Dissecação e Empalhamento de Corpos",
        "1570" => "Técnicos de Inspeção, Fiscalização e Coordenação Administrativa",
        "1590" => "Técnicos de Nivel Médio em Operações Comerciais",
        "1670" => "Técnicos de Nivel Médio em Operações Industriais",
        "1580" => "Técnicos de Nível Médio em Operações Financeiras",
        "1600" => "Técnicos de Serviços Culturais",
        "1420" => "Técnicos do Mobiliário e Afins",
        "1410" => "Técnicos do Vestuário",
        "1430" => "Técnicos em Biologia",
        "1350" => "Técnicos em Construção Civil, de Edificações e Obras de Infraestrutura",
        "1360" => "Técnicos em Eletro-Eletrônica e Fotônica",
        "1890" => "Técnicos em Informática",
        "1330" => "Técnicos em Laboratório",
        "1370" => "Técnicos em Metalmecânica",
        "1380" => "Técnicos em Mineralogia e Geologia",
        "1540" => "Técnicos em Navegação Aérea, Marítima, Fluvial e Metroferroviária",
        "1630" => "Técnicos em Operação de Aparelhos de Sonorização, Cenografia e Projeção",
        "1610" => "Técnicos em Operação de Câmara Fotográfica , Cinema e Transmissão de Dados",
        "1470" => "Técnicos em Operação de Equipamentos e Instrumentos de Diagnóstico",
        "1620" => "Técnicos em Operação de Estações de Rádio e Televisão",
        "1550" => "Técnicos em Transportes (Logística)",
        "1920" => "Vendedores Ambulantes e Camelôs",
        "1895" => "Vendedores, Demonstradores",
        "2260" => "Vidreiros, Ceramistas e Afins"
    );

    public function isActive($login) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('cliente', array('active'))
            ->where('login = ?', $login);
        return $db->fetchRow($select);
    }

    public function getById($id = null) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('cliente')
            ->joinLeft('cidade', 'cidade_id = cidade.id', array('cidade.nome as cidade', 'estado_id'))
            ->joinLeft('estado', 'estado_id = estado.id', array('estado.nome as estado', 'estado.sigla as sigla'))
            ->where('cliente.id = ?', $id);

        return $db->fetchRow($select);
    }

    public function getByLogin($login) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('cliente')
            ->joinLeft('cidade', 'cidade_id = cidade.id', array('cidade.nome as cidade', 'estado_id'))
            ->joinLeft('estado', 'estado_id = estado.id', array('estado.nome as estado', 'estado.sigla as sigla'))
            ->where('cliente.login = ?', $login);

        return $db->fetchRow($select);
    }

    public function create($dados) {
        $mapper = new Application_Model_DbTable_Cliente();
        $row = $mapper->createRow();

        $exclude = array(",", ".", "-", "/");

        $row->nome = $dados['nome'];
        $row->nome2 = $dados['nome2'] ?:'';
        $row->login = $dados['login'];
        $row->cnpj = $dados['cnpj'] ? str_replace($exclude,"",$dados['cnpj']) : "";
        $row->rg = $dados['rg'];
        $row->email = $dados['email'];
        $row->ip = $dados['ip'];

        $row->telefone = $dados['telefone'];
        $row->celular = $dados['celular'];
        $row->fax = $dados['fax'];

        $row->pergunta_secreta = $dados['pergunta_secreta'];
        $row->resposta_secreta = $dados['resposta_secreta'];
        $row->complemento = $dados['complemento'];
        $row->bairro = $dados['bairro'];
        $row->logradouro = $dados['logradouro'];
        $row->endereco = $dados['endereco'];

        if($dados['cep']){
            $cep = explode("-",$dados['cep']);
            $row->cep = $cep[0].$cep[1];
        }
        if ($dados['cidade_id']) {
            $row->cidade_id = $dados['cidade_id'];
        }
        $row->pais_id = $dados['pais_id'];

        $row->cliente_natureza_id = $dados['cliente_natureza_id'];
        $row->atividade = $dados['atividade'];
        $row->razao_social = $dados['razao_social'];

        if ($dados['parceiro_id']) {
            $row->parceiro_id = $dados['parceiro_id'];
        }

        $row->salt = SHA1(date('Ymdhisu') . mt_rand(1000, 9999));
        $row->senha = SHA1($dados["senha"] . $row->salt);

        $row->inpi_login = $dados['inpi_login'];
        $row->inpi_senha = $dados['inpi_senha'];
        $row->inpi_email = $dados['inpi_email'];

        $row->data_cadastro = new Zend_Db_Expr("NOW()");


        if ($row->save()) {
            return $row;
        } else {
            return false;
        }
    }

    public function update($dados) {
        $mapper = new Application_Model_DbTable_Cliente();
        $row = $mapper->fetchRow(array('id =?' => $dados['id']));

        if (!is_null($row)) {
            $exclude = array(",", ".", "-", "/");

            $row->nome = $dados['nome'];
            $row->nome2 = $dados['nome2'] ?: '';
            $row->login = $dados['login'];
            $row->cnpj = $dados['cnpj'] ? str_replace($exclude,"",$dados['cnpj']) : "";
            $row->rg = $dados['rg'];
            $row->email = $dados['email'];

            $row->telefone = $dados['telefone'];
            $row->celular = $dados['celular'];
            $row->fax = $dados['fax'];

            $row->pergunta_secreta = $dados['pergunta_secreta'];
            $row->resposta_secreta = $dados['resposta_secreta'];
            $row->cpf_pessoa_juridica = $dados['cpf_pessoa_juridica'] ? str_replace($exclude,"",$dados['cpf_pessoa_juridica']) : "";
            $row->logradouro = $dados['logradouro'];
            $row->complemento = $dados['complemento'];
            $row->bairro = $dados['bairro'];
            $row->endereco = $dados['endereco'];

            if($dados['cep']){
                $cep = explode("-",$dados['cep']);
                $row->cep = $cep[0].$cep[1];
            }
            $row->cidade_id = $dados['cidade_id'];
            $row->pais_id = $dados['pais_id'];
            $row->atividade = $dados['atividade'];

            $row->cliente_natureza_id = $dados['cliente_natureza_id'];
            $row->razao_social = $dados['razao_social'];

            if ($dados["senha"]) {
                $row->salt = SHA1(date('Ymdhisu') . mt_rand(1000, 9999));
                $row->senha = SHA1($dados["senha"] . $row->salt);
            }

            $row->inpi_login = $dados['inpi_login'];
            $row->inpi_senha = $dados['inpi_senha'];
            $row->inpi_email = $dados['inpi_email'];

            $row->inpi_login_sec = $dados['inpi_login_sec'];
            $row->inpi_senha_sec = $dados['inpi_senha_sec'];

            if ($row->save()) {
                return $row;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function updateParceiro($clienteId, $parceiroId) {
        $mapper = new Application_Model_DbTable_Cliente();
        $row = $mapper->fetchRow(array('id =?' => $clienteId));

        $row->parceiro_id = $parceiroId;

        return $row->save();
    }

    public function updateSenha($dados) {
        $mapper = new Application_Model_DbTable_Associados();
        $row = $mapper->fetchRow(array('id =?' => $dados['id']));

        if (!is_null($row)) {
            if (!empty($dados["nova_senha"])) {
                $row->salt = SHA1(date('Ymdhisu') . mt_rand(1000, 9999));
                $row->senha = SHA1($dados["nova_senha"] . $row->salt);
            }


            if ($row->save()) {
                return $row;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function toSelect() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('cliente')->order('razao_social');

        $rows = $db->fetchAll($select);

        $array = array();
        foreach ($rows as $value) {
            $array[$value["id"]] = $value["razao_social"] . ' - (' . $value["nome"] . ') ' . ' - ' . $value["login"];
        }
        return $array;
    }

}
