<?php

class Application_Model_ClienteAgendamentos {

    public function getById($cliente) {
        $dbTable = new Application_Model_DbTable_ClienteAgendamentos();
        $rows =  $dbTable->fetchAll(array('cliente_id = ?' => $cliente), 'data desc');
        return $rows->toArray();
    }

    public function getAll() {
        $dbTable = new Application_Model_DbTable_ClienteAgendamentos();
        $rows =  $dbTable->fetchAll(array(), 'data desc');
        return $rows->toArray();
    }

    public function getNextDay(){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('cliente_agendamentos')
            ->where("data between NOW() and DATE_ADD(NOW(), INTERVAL 2 DAY)")
            ->where("sendClient = ?", 1)
            ->order('data asc');

        return $db->fetchAll($select);
    }

    public function create($params){

        $dbTableAgendamentos = new Application_Model_DbTable_ClienteAgendamentos();
        $row = $dbTableAgendamentos->createRow();
        $row->cliente_id = $params['cliente_id'];
        $row->titulo  = $params['titulo'];
        $row->consultor = $params['consultor'];
        $row->sendClient = $params['sendClient'];
        $row->observacao = $params['observacao'];
        if($params['data']){
            $data =  new Zend_Date($params['data']);
            $row->data = $data->toString('YYYY-MM-dd HH:mm');
        }
        return $row->save();
    }

}
