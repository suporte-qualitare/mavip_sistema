<?php

class Application_Model_ClienteLead {

    public function getAll() {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('cliente_lead_origens', array('nome' => 'cliente.nome', 'cliente.data_cadastro', 'valor' => new Zend_Db_Expr('GROUP_CONCAT(valor)')))
         ->join('cliente', 'cliente_lead_origens.cliente_id = cliente.id', array())
         ->order('cliente.data_cadastro desc')->group('nome');
         return $db->fetchAll($select);
    }

        public function getByOrigem($origem) {

                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()->from('cliente_lead_origens', array('valor' => 'valor', 'nome' => new Zend_Db_Expr('GROUP_CONCAT(nome)')))
                 ->join('cliente', 'cliente_lead_origens.cliente_id = cliente.id', array())
                 ->where('cliente_lead_origens.valor = ?', $origem)->group('valor');
               return $db->fetchAll($select);
        }

    public function getByClienteNome($name) {

            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()->from('cliente_lead_origens', array('nome' => 'cliente.nome', 'valor' => new Zend_Db_Expr('GROUP_CONCAT(valor)')))
             ->join('cliente', 'cliente_lead_origens.cliente_id = cliente.id', array())
             ->where('cliente.nome LIKE ?',"%".$name. "%")->group('nome');
           return $db->fetchAll($select);
    }

    public function getLeadOrigensByCliente($cliente) {

        $dbTable = new Application_Model_DbTable_ClienteLeadOrigens();
        $rows =  $dbTable->fetchAll(array('cliente_id = ?' => $cliente));
        return $rows->toArray();
    }

    public  function updateLead($data) {

        try{
            $dbTable = new Application_Model_DbTable_ClienteLeadOrigens();
            $dbTable->delete(array('cliente_id = ?' => $data['cliente_id']));

            if(count($data['alternative'])){
                foreach ($data['alternative'] as $key => $value){
                    $row = $dbTable->createRow();
                    $row->cliente_id = $data['cliente_id'];
                    $row->valor = $value;
                    $row->data = date("Y-m-d H:i:s");
                    $row->save();
                }
            }
        }
        catch (Exception $e){
            return false;
        }
        return true;
    }

    public function createLeadField($valor){

      try{
            $tbLeadFields = new Application_Model_DbTable_ClienteLeadFields();
            $row = $tbLeadFields->createRow();
            $row->valor = $valor;
            return $row->save();
         }
        catch (Exception $e){
            return false;
        }
    }

    public function getLeadFields() {

            $dbTable = new Application_Model_DbTable_ClienteLeadFields();
            $rows =  $dbTable->fetchAll(array(), 'valor asc');
            return $rows->toArray();
    }
}