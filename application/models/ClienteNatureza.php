<?php

class Application_Model_ClienteNatureza {

    public function getAllToSelect($preTexto = null) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('cliente_natureza')->order("order");
        
        $result = $db->fetchAll($select);

        $retorno = array();

        if ($preTexto) {
            $retorno[null] = $preTexto;
        }
        foreach ($result as $val) {
            $retorno[$val["id"]] = $val["nome"];
        }
        return $retorno;
    }

   
}
