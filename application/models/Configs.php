<?php

class Application_Model_Configs {
    public function getAllToArray() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('configs');
        $result = $db->fetchAll($select);
        $retorno = array();
        foreach ($result as $val) {
            $retorno[$val["chave"]] = $val["valor"];
        }
        return $retorno;
    }

    public function save($chave, $valor){
     
      $configModel = new Application_Model_DbTable_Configs();
      $row = $configModel->fetchRow(array('chave = ?'=>$chave));

      if(!$row){
        $row = $configModel->createRow();
        $row->chave = $chave;
      }

      $row->valor = $valor;
      return $row->save();
    }




}
