<?php

class Application_Model_Cores {

   
    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()->from('cores');
        return $db->fetchAll($sql);
    }
    

    public function toSelect() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('cores');

        $rows = $db->fetchAll($select);

  
        $array = array();
        foreach ($rows as $value) {
            $array[$value["hexadecimal"]] = $value["nome"];
        }
        return $array;
    }
}
