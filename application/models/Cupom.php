<?php

class Application_Model_Cupom {

    public function getById($idCupom) {
        $cupomMapper = new Application_Model_DbTable_Cupom();
        return $cupomMapper->fetchRow(array('id = ?' => $idCupom));
    }

    public function getByChave($chave) {
        $cupomMapper = new Application_Model_DbTable_Cupom();
        return $cupomMapper->fetchRow(array('chave = ?' => $chave));
    }

    public function calculaDesconto($valor, $cupomId, $porcentagemParceiro = null) {
        if (!$cupomId AND ! $porcentagemParceiro) {
            return 0;
        };
        
        
        $valorDesconto = 0;

        if ($cupomId) {
            $cupom = $this->getById($cupomId);
            if ($cupom['tipo'] == 'valor') {
                if ($cupom['desconto'] > $valor) {
                    $valorDesconto = $valor;
                } else {
                    $valorDesconto = $cupom['desconto'];
                }
            } elseif ($cupom['tipo'] == 'porcentagem') {
                //echo $valor;exit; 
                $valorDesconto = number_format(($valor * ($cupom['desconto'] / 100)), 2, '.', '');
            }
        }
        
        if($porcentagemParceiro){
            $valorParceiro = number_format(($valor * ($porcentagemParceiro / 100)), 2, '.', '');
            if($valorParceiro > $valorDesconto){
                $valorDesconto = $valorParceiro;
            }
        }

        return $valorDesconto;
    }

}
