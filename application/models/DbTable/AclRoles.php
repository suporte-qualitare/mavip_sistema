<?php

class Application_Model_DbTable_AclRoles extends Zend_Db_Table_Abstract
{

    protected $_name = 'acl_roles';


    public function toSelect() {
        $rows = $this->fetchAll('active = 1','name');
        $array = array();
        foreach ($rows as $value) {
            $array[$value->id] = $value->name;
        }
        return $array;
    }
}

