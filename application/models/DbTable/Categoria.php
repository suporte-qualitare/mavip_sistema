<?php

class Application_Model_DbTable_Categoria extends Zend_Db_Table_Abstract {

    protected $_name = 'categoria';
    public $tipo = array('servico' => 'Serviço', 'produto' => 'Produto', 'comercio' => 'Comércio');

    public function getAllToSelect($preTexto = null) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                ->from('categoria', array(
                    'id' => 'id', 
                    'categoria_id' => 'id_inpi',
                    'nome' => 'nome'
                    ))                
                ->order('id_inpi');
        $result = $db->fetchAll($select);

        $retorno = array();

        if ($preTexto) {
            $retorno[null] = $preTexto;
        }
                
        foreach ($result as $val) {
            $retorno[$val["id"]] = $val["categoria_id"] . ' - ' . $val["nome"];
        }
        return $retorno;
    }

}
