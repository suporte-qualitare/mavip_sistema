<?php

class Application_Model_DbTable_Categoriablog extends Zend_Db_Table_Abstract {
    protected $_name = 'categoria_blog';
    
    public function getAllToSelect($preTexto = null) {
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('categoria_blog');
        $result = $db->fetchAll($select);
        
        $retorno = array();
        
        if($preTexto){
            $retorno[null] = $preTexto;
        }
        foreach ($result as $val) {
            $retorno[$val["id"]] = $val["nome"];
        }
        return $retorno;
    }
}