<?php

class Application_Model_DbTable_ParceiroCategoria extends Zend_Db_Table_Abstract {
    protected $_name = 'parceiro_categoria';
    
    public function getAllToSelect($preTexto = null) {
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('parceiro_categoria')->where('active = 1');
        $result = $db->fetchAll($select);
        
        $retorno = array();
        
        if($preTexto){
            $retorno[null] = $preTexto;
        }
        foreach ($result as $val) {
            $retorno[$val["id"]] = $val["nome"];
        }
        return $retorno;
    }
}

