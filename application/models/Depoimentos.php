<?php

class Application_Model_Depoimentos {

    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('depoimentos');
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getTopActive($limite) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('depoimentos')
                ->where('active = 1')
                ->order('id DESC')
                ->limit($limite);
        return $db->fetchAll($select);
    }

    public function inserir($dados) {
        $depoimentoMapper = new Application_Model_DbTable_Depoimento();
        $row = $depoimentoMapper->createRow();

        $row->nome = $dados['nome'];
        $row->depoimento = $dados['depoimento'];
        $row->imagem = $dados['imagem'];
        $row->active = 0;
        $row->data_cadastro = new Zend_Db_Expr('NOW()');

        return $row->save();
    }

}
