<?php

class Application_Model_Emails {

    public function sendEmail($to = array(), $subject, $template, $dados = array()) {

        $log = new Application_Model_Logs();

        try {

            $html = new Zend_View();
            $html->setScriptPath(APPLICATION_PATH . '/modules/default/views/scripts/partial/mail/');
            
            $html->assign('dados', $dados);
            $msg = $html->render("{$template}.phtml");

             $from['mail'] = 'sistema@mavip.com.br';
             $from['senha'] = '!{s2!P3ThguM';
             $from['nome'] = 'MAVIP Tecnologia';


            $configuracao = array(
                'ssl' => 'SSL',
                'auth' => 'login',
                'username' => $from['mail'],
                'password' => $from['senha'],
                'port' => '465'
            );
            
            header('Content-type: text/html; charset=utf-8');

            $transporte = new Zend_Mail_Transport_Smtp('mail.mavip.com.br', $configuracao);
            $mail = new Zend_Mail();
            $mail->setBodyHtml(utf8_decode($msg));
            $mail->setFrom($from['mail'], $from['nome']);
            $mail->addTo($to['mail'], $to['nome']);
            $mail->setSubject('=?UTF-8?B?'.base64_encode($subject).'?=');

            if($to['mail'] != 'atendimento@mavip.com.br'){
              $mail->addBcc("atendimento@mavip.com.br");
             }

            $send = $mail->send($transporte);
            
            if(!$send || empty($send))
                return $log->create(1, "Erro. Email  Nﾃグ  enviado para: ".$to['mail'].' - '.$to['nome']);

            return $log->create(1, "Email ENVIADO para: ".$to['mail'].' - '.$to['nome']);

        } catch (Exception $exc) {

            return $log->create(1, "ERRO AO ENVIAR EMAIL: ". $exc->getMessage());
        }
    }

}
