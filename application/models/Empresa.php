<?php

class Application_Model_Empresa {

    public function getAllProcesso() {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()->from('empresa');
        $paginas = $db->fetchAll($select);

        return $paginas;
    }
    
    public function getAllIPAS($ipas) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()->from('empresa')
                ->where('ipas = ?', $ipas); 
        $paginas = $db->fetchAll($select);

        return $paginas;
    }

}
