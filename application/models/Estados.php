<?php

class Application_Model_Estados {

    public function getAllToSelect($preTexto = null) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('estado');

        $result = $db->fetchAll($select);

        $retorno = array();

        if ($preTexto) {
            $retorno[null] = $preTexto;
        }
        foreach ($result as $val) {
            $retorno[$val["id"]] = $val["nome"];
        }
        return $retorno;
    }
    
    public function getUfToSelect($preTexto = null) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('estado');

        $result = $db->fetchAll($select);

        $retorno = array();

        if ($preTexto) {
            $retorno[null] = $preTexto;
        }
        foreach ($result as $val) {
            $retorno[$val["sigla"]] = $val["sigla"];
        }
        return $retorno;
    }

    public function getBySigla($sigla) {
        
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('estado')->where('sigla = ?',$sigla);

        $row = $db->fetchRow($select);
        
        if ($row) {
            return $row['id'];
        } else {
            return false;
        }
    }
    
    public function getById($idEstado) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('estado')->where('id = ?',$idEstado);

        return $db->fetchRow($select);
    }

}
