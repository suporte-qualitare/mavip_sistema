<?php

class Application_Model_Faq {

   
    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()->from('faq')->where('active = 1')->order('titulo');
        return $db->fetchAll($sql);
    }
    
}
