<?php

class Application_Model_Itens {

   
    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()->from('itens')->where('active = 1')->order('ordem');
        return $db->fetchAll($sql);
    }

    public function getItensPlanos(){
        $db = Zend_Db_Table::getDefaultAdapter();

         $sql = $db->select()->from('planos_itens', array('planos_itens.id'))
                ->join('planos', 'planos.id = planos_itens.plano_id')
                ->join('itens', 'itens.id = planos_itens.item_id', array('itens.ordem as ordem_item', 'itens.id as itens_id','*'))
            ->where('planos.active = 1');

            return $db->fetchAll($sql);
    }
    
}
