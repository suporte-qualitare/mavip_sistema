<?php

class Application_Model_Leitura {

    public function getById($idMarca) {
        $modelMapper = new Application_Model_DbTable_Leitura();
        return $modelMapper->fetchRow(array('id = ?' => $idMarca));
    }   

    public function getDataUltimaLeitura() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('leitura')->order('id DESC');
        $row = $db->fetchRow($select);
        return new Zend_Date($row["data_cadastro"]);

    }

    public function leituraIPAS(){

        $ipas029 = "((leitura.data >= DATE_SUB(curdate(), INTERVAL 90 DAY) and 
                                     mh.codigo_despacho = 'IPAS029'))"; // até 90 dias (1x semana)
        $ipas404 = "((leitura.data >= DATE_SUB(curdate(), INTERVAL 60 DAY) and 
                                     mh.codigo_despacho = 'IPAS404'))"; // até 60 dias (1x semana)
        $ipas106 = "((leitura.data >= DATE_SUB(curdate(), INTERVAL 60 DAY) and 
                                     mh.codigo_despacho = 'IPAS106'))"; // até 60 dias (1x semana)
        $ipas139 = "((leitura.data >= DATE_SUB(curdate(), INTERVAL 60 DAY) and 
                                     mh.codigo_despacho = 'IPAS139'))"; // até 60 dias (1x semana)
        $ipas157 = "((leitura.data >= DATE_SUB(curdate(), INTERVAL 60 DAY) and 
                                     mh.codigo_despacho = 'IPAS157'))"; // até 60 dias (1x semana)
        $ipas161 = "((leitura.data >= DATE_SUB(curdate(), INTERVAL 60 DAY) and 
                                     mh.codigo_despacho = 'IPAS161'))"; // até 60 dias (1x semana)
        $ipas024 = "((leitura.data >= DATE_SUB(curdate(), INTERVAL 60 DAY) and 
                                     mh.codigo_despacho = 'IPAS024'))"; // até 60 dias (1x semana)
        $ipas423 = "((leitura.data >= DATE_SUB(curdate(), INTERVAL 60 DAY) and 
                                     mh.codigo_despacho = 'IPAS423'))"; // até 60 dias (1x semana)
        $ipas359 = "((leitura.data >= DATE_SUB(curdate(), INTERVAL 5 DAY) and 
                                     mh.codigo_despacho = 'IPAS359'))"; // até 5 dias (1x)
        $ipas005 = "((mh.codigo_despacho = 'IPAS005'))"; // marca extinta (1x semana)
        $ipas304 = "((mh.codigo_despacho = 'IPAS304'))"; // extincao por caducidade (1x semana)
        $ipas414 = "((mh.codigo_despacho = 'IPAS414'))"; // extincao por falta de procurador no brasil (1x semana)
        $ipas033 = "((mh.codigo_despacho = 'IPAS033'))"; // pedido de registro inexistente (1x semana)
        $ipas047 = "((mh.codigo_despacho = 'IPAS047'))"; // pedido de registro inexistente pago (1x semana)
        $ipas091 = "((mh.codigo_despacho = 'IPAS091'))"; // pedido de registro inexistente pago (1x semana)
        $ipas112 = "((mh.codigo_despacho = 'IPAS112'))"; // pedido de registro inexistente pago (1x semana)
        $ipas113 = "((mh.codigo_despacho = 'IPAS113'))"; // pedido de registro para oposicao (1x semana)
        $ipas009 = "((mh.codigo_despacho = 'IPAS009'))"; // pedido de registro para publicado oposicao (1x semana)
        $ipas135 = "((mh.codigo_despacho = 'IPAS135'))"; // pedido de registro para publicado oposicao (1x semana)
        $ipas421 = "((mh.codigo_despacho = 'IPAS421'))"; // pedido de registro para publicado oposicao (1x semana)
        $ipas142 = "((mh.codigo_despacho = 'IPAS142'))"; // sobrestamento do exame de pedido de registro (1x semana)

        $allIpas = array($ipas029, $ipas404, $ipas106 , $ipas139, $ipas157,
                        $ipas161, $ipas024, $ipas423, $ipas359, $ipas005,
                        $ipas304, $ipas414, $ipas033, $ipas047, $ipas091,
                        $ipas112, $ipas113, $ipas009, $ipas135, $ipas421,
                        $ipas142);

        $db = Zend_Db_Table::getDefaultAdapter();
            for($i= 0; $i <=21; $i++){
                $sql = "select mh.codigo_despacho,
                (select m.marca from marca as m where m.id = mh.marca_id) as marca,
                (select l.data from leitura as l where l.id = mh.leitura_id) as data
                from marca_historico as  mh
                JOIN leitura on (mh.leitura_id = leitura.id) 
                WHERE {$allIpas[$i++]} ";

                $stmt = $db->query($sql);
                return  $res = $stmt->fetchAll();
              }
        }


    // RETORNA LEITURAS COM LIMITE
    public function leituraDespachosLimite($despacho, $limite = NULL) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()->from('marca_historico AS m', array('m.id', 'm.codigo_despacho'))
        ->joinLeft('leitura AS l', 'l.id = m.leitura_id', array('l.data', 'l.data_cadastro', 'DATEDIFF(CURDATE(), l.data) AS corridos'))
        ->join('marca AS c', 'c.id = m.marca_id', array('c.marca', 'c.id AS marca_id'))
        ->where('m.codigo_despacho IN (?)', $despacho);

        if ($limite) {
            $select->where("DATEDIFF(CURDATE(), l.data) <= $limite")
            ->order('id ASC');
        }

        $res = $db->fetchAll($select);

        $i = 0;
        $j = 0;
        foreach($res as $val) {

            if ($val['corridos'] % 7 == 0) { // POR SEMANA
                echo $val['id'] . ' - ' .  $val['marca'];
                echo '<br>';
                $j++;
            }

            $i++;
        }

        echo '<br>';
        echo 'Total Enviados: ' . $j;
        echo '<br>';
        echo 'Total Geral: ' . $i;
        //print_r($res);

        // APENAS PARA TESTES
        // EM PRODUCAO FARA A LOGICA DE ENVIO DE EMAILS
        exit;
    }

}
