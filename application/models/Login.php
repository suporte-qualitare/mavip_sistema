<?php

class Application_Model_Login {

    private $login;
    private $password;
    private $question1;
    private $question2;
    private $question3;
    private $questionCod1;
    private $questionCod2;
    private $questionCod3;

    function __construct($login, $password) {
        $this->login = $login;
        $this->password = $password;
    }

    public function isValid() {
        $this->_auth = Zend_Auth::getInstance();
        $this->_auth->setStorage(new Zend_Auth_Storage_Session('Zend_Auth_Mavip'));
        $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter(), //adaptador do banco de dados
                'cliente', // tabela
                'login', // login
                'senha', // senha
                'SHA1(CONCAT(?,salt))'
        ); //regra da senha	
        // Set the input credential values (e.g., from a login form)
        $authAdapter->setIdentity($this->login)->setCredential($this->password);

        // Perform the authentication query, saving the result
        $result = $this->_auth->authenticate($authAdapter);

        if ($result->isValid()) {
            $usuario = $authAdapter->getResultRowObject();
            $this->_auth->getStorage()->write($usuario);

            return true;
        } else {

            return false;
        }
    }
    
    public function isValidSecret() {
        $this->_auth = Zend_Auth::getInstance();
        $this->_auth->setStorage(new Zend_Auth_Storage_Session('Zend_Auth_Mavip'));
        $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter(), //adaptador do banco de dados
                'cliente', // tabela
                'login', // login
                'senha' // senha                
        ); //regra da senha	
        // Set the input credential values (e.g., from a login form)
        $authAdapter->setIdentity($this->login)->setCredential($this->password);

        // Perform the authentication query, saving the result
        $result = $this->_auth->authenticate($authAdapter);

        if ($result->isValid()) {
            $usuario = $authAdapter->getResultRowObject();
            $this->_auth->getStorage()->write($usuario);

            return true;
        } else {

            return false;
        }
    }

}
