<?php

class Application_Model_Logs
{
    function create($usuarioId,$log) {
        $logsDbTable = new Application_Model_DbTable_Logs();
        
        $row = $logsDbTable->createRow();
        $row->users_id = $usuarioId;
        $row->acao = $log;
        $row->data = new Zend_Db_Expr('NOW()');
        return $row->save();
    }
    
    function getByUsuario($usuarioId,$quant = null) {
        $logsDbTable = new Application_Model_DbTable_Logs();
        
        return $logsDbTable->fetchAll(array('usuario_id = ?'=>$usuarioId), 'id DESC', $quant);
    }

}

