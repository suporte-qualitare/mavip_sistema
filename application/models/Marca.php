<?php

class Application_Model_Marca {

    public function getById($idMarca) {
        try
        {
            $marcaMapper = new Application_Model_DbTable_Marca();
            return $marcaMapper -> fetchRow(array('id = ?' => $idMarca));
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
            exit;
        }
    }

    public function getAllById($idsMarcas){
        try
        {
            $marcaMapper = new Application_Model_DbTable_Marca();
            $ids = implode(',', $idsMarcas);
            return $marcaMapper->fetchAll(array("id in ($ids)"));
        }
        catch (Exception $e)
        {
            throw new exception($e->getMessage());
        }

    }

    public function inserir($dados) {
        $marcaMapper = new Application_Model_DbTable_Marca();
        
        if($dados["processo"]){
            $row = $marcaMapper->fetchRow(array('processo = ?'=>$dados["processo"],'cliente_id = ?'=>$dados['cliente_id']));
        }
        if(!$row){
            $row = $marcaMapper->createRow();
        }

        $row->marca = $dados['marca'];
        if($dados["processo"])

            $row->processo = $dados['processo'];
        if($dados["observacao"])
            $row->observacao = $dados['observacao'];

        $row->cliente_id = $dados['cliente_id'];
        $row->acompanhamento = $dados['acompanhamento'];
        $row->urgencia = $dados['urgencia'];
        $row->estilo = $dados['estilo'];
        $row->tipo = $dados['tipo'];
        $row->categoria_id = $dados['categoria_id'];
        if($dados['data_publicacao_processo'])
            $row->data_publicacao_processo =  $dados['data_publicacao_processo'];
        
        $row->data_cadastro = new Zend_Db_Expr('NOW()');
        return $row->save();
    }

    public function updateInfo($idMarca, $dados) {
        $marcaMapper = new Application_Model_DbTable_Marca();
        //var_dump($dados);exit;
        $row = $marcaMapper->fetchRow(array('id = ?' => $idMarca));
        $row->logo = $dados['logo'];
        $row->descricao = $dados['descricao'];
        
        
        if($dados["logo_data"]){
            $row->logo_data = $dados["logo_data"];
        }
        
        if($dados["descricao_data"]){
            $row->descricao_data = $dados["descricao_data"];
        }

        return $row->save();
    }

    public function inserirItem($dados) {
        $marcaHasServicoMapper = new Application_Model_DbTable_MarcaHasServico();

        $row = $marcaHasServicoMapper->fetchRow(array("marca_id = ?"=>$dados['marca_id'],"servico_id"=>$dados['servico_id']));
        
        if($row){
            return true;
        }
        
        $row = $marcaHasServicoMapper->createRow();
        $row->marca_id = $dados['marca_id'];
        $row->servico_id = $dados['servico_id'];

        return $row->save();
    }

    public function getServicosByMarca($marcaId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('marca', array('marca', 'processo'))
                ->join('marca_has_servico', 'marca.id = marca_has_servico.marca_id', array())
                ->join('pedido_has_marca', 'marca_has_servico.marca_id = pedido_has_marca.marca_id', array())
                ->join('pedido_has_servico', 'pedido_has_marca.pedido_id = pedido_has_servico.pedido_id', array('*'))
                ->join('servico', 'marca_has_servico.servico_id = servico.id', array('*'))
                ->where("marca.id = ?", $marcaId)->group('servico.id');
        return $db->fetchAll($select);
    }

    public function getAllByCliente($cliente) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
                ->from('marca')
                ->join('categoria', 'marca.categoria_id = categoria.id', array('id_inpi', 'categoria.nome as classe'))
                ->where('cliente_id = ?', $cliente)
            //    ->where('marca.active = ?', 1)
                ->order('atual DESC')
                ->order('id DESC');
        return $db->fetchAll($select);
    }

    public function getAllAbertoByCliente($cliente) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from('pedido_has_marca')
            ->join('marca', 'pedido_has_marca.marca_id = marca.id')
            ->join('marca_has_servico', 'marca.id = marca_has_servico.marca_id', array())
            ->join('servico', 'marca_has_servico.servico_id = servico.id', array('servico.titulo as titulo'))
            ->join('pedido', 'marca.cliente_id = pedido.cliente_id', array('pedido.id as id_pedido', 'pedido.data_cadastro as data_pedido'))
            ->where('marca.cliente_id = ?', $cliente)
            ->where("status != 'cancelado'")
            ->where('marca.active = ?', 1)
            ->group('pedido_has_marca.marca_id')
            ->order('pedido_has_marca.pedido_id asc');

        return $db->fetchAll($select);
    }

    public function getAllAbertoByClienteEspelhamentos($cliente) {
        
        $db = Zend_Db_Table::getDefaultAdapter();

        $s = $db->select()
            ->from('clientes_espelhamentosdeprocessos', 'marca_id')
            ->where('cliente_id = ?', $cliente);

        $marcas = $db->fetchCol($s);

        if(!$marcas)
            return false;
        
        $select = $db->select()
            ->from('pedido_has_marca')
            ->join('marca', 'pedido_has_marca.marca_id = marca.id')
            ->join('marca_has_servico', 'marca.id = marca_has_servico.marca_id', array())
            ->join('servico', 'marca_has_servico.servico_id = servico.id', array('servico.titulo as titulo'))
            ->join('pedido', 'marca.cliente_id = pedido.cliente_id', array('pedido.id as id_pedido', 'pedido.data_cadastro as data_pedido'))
            ->join('clientes_espelhamentosdeprocessos', 'pedido_has_marca.marca_id = clientes_espelhamentosdeprocessos.marca_id')
            ->where("status != 'cancelado'")
            ->where('marca.active = ?', 1)
            ->where('clientes_espelhamentosdeprocessos.marca_id IN (?)', $marcas)
            ->where('clientes_espelhamentosdeprocessos.cliente_id = ?', $cliente)
            ->group('pedido_has_marca.marca_id')
            ->order('pedido_has_marca.pedido_id asc');

        return $db->fetchAll($select);

    }


    public function getHistorico($idMarca) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                ->from('marca_historico')
                ->joinLeft('leitura', 'leitura_id = leitura.id', array('data'))
                ->where('marca_historico.marca_id = ?', $idMarca)
                ->order('leitura.data');

        return $db->fetchAll($select);
    }
    
    public function getUltimoHistorico($idMarca) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
                ->from('marca_historico')
                ->join('leitura', 'leitura_id = leitura.id', array('data'))
                ->where('marca_historico.marca_id = ?', $idMarca)
                ->order('leitura.data DESC');

        return $db->fetchRow($select);
    }
    
    public function getHistoricoMensagem($codigoDespacho){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()
                ->from('marca_historico_mensagem')
                ->where('codigo_despacho = ?', $codigoDespacho);

        return $db->fetchRow($select);
    }


    public function cronAcompanhamento(){

        $date = new Zend_Date();
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
    
        $join = ' inner join cliente as c  on c.id=m.cliente_id';
        $where =  "where data_acompanhamento and  m.active = 1";
        $having =  " having prazo in (60,30,5,-1)";
        $order = " order by prazo desc";
        $sql = "select m.marca, m.acompanhamento, data_acompanhamento, DATEDIFF(m.data_acompanhamento, CURDATE()) as prazo ,
                m.processo, c.nome, c.email from marca as m $join $where $having $order";
                
        $stmt = $db->query($sql);
        $res = $stmt->fetchAll();


        return $res;
    }

}
