<?php

class Application_Model_MarcaHistorico {

    public function getDespachos($registro)
    {
        if($registro === 'ativos')
            return ['IPAS158'];
    
        if($registro === 'indeferidos')
            return ['IPAS024'];
    
        if($registro === 'pedidos')
            return ['IPAS009', 'IPAS005', 'IPAS029', 'IPAS033', 'IPAS135', 'IPAS136', 'IPAS142', 'IPAS395', 'IPAS0423', 'IPAS421'];
    
        if($registro === 'arquivados')
            return['PAS033', 'IPAS047', 'IPAS091', 'IPAS106', 'IPAS112', 'IPAS113', 'IPAS139', 'IPAS157', 'IPAS289', 'IPAS291'];
    
        if($registro === 'extintos')
            return ['PAS157', 'IPAS161', 'IPAS304', 'IPAS409', 'IPAS414'];

        if($registro === 'recursos')
            return ['IPAS360', 'IPAS235', 'IPAS237', 'IPAS238', 'IPAS369', 'IPAS370', 'IPAS535', 'IPAS536'];
    
        return [];
    }

}
