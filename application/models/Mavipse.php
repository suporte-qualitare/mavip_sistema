<?php


class Application_Model_Mavipse
    {

    public function getRow($id) {
        try
        {
            $buscaMapper = new Application_Model_DbTable_Mavipse();
            return $buscaMapper -> fetchRow(array('id = ?' => $id));
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
            exit;
        }
    }

    public function getByClient($id) {
        try
        {
            $buscaMapper = new Application_Model_DbTable_Mavipse();
            return $buscaMapper -> fetchRow(array('cliente_id = ?' => $id));
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
            exit;
        }
    }
}
