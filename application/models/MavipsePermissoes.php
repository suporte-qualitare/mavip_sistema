<?php


class Application_Model_MavipsePermissoes
    {

    public function getAll(){
        try
        {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                ->from('mavipse_permissoes')
                ->joinLeft('mavipse', 'mavipse.id = mavipse_permissoes.mavipse_id', array('id as codigo'))
                ->joinLeft('users', 'mavipse.user_id = users.id', array('name as operador'))
                ->joinLeft('mavipse_filtros_1', 'mavipse.id = mavipse_filtros_1.mavipse_id', array(''))
                ->joinLeft('mavipse_filtros_2', 'mavipse.id = mavipse_filtros_2.mavipse_id', array(''))
                ->where('mavipse_permissoes.status = ?', '0')
                ->order('mavipse_permissoes.id desc');

            return $db->fetchAll($select);
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
            exit;
        }
    }

    public function getByMavipse($mavipse, $tela) {
        try
        {
            $buscaMapper = new Application_Model_DbTable_MavipsePermissoes();
            return $buscaMapper -> fetchRow(array('mavipse_id = ?' => $mavipse, 'tela = ?' => $tela ));
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
            exit;
        }
    }

}
