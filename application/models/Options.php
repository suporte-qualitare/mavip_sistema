<?php

class Application_Model_Options {

    public function getById($id) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('options')
                ->where('id = ?', $id);

        return $db->fetchRow($select);
    }
    
    public function getAllToArray() {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('options');

        $results =  $db->fetchAll($select);
        $retorno = array();
        foreach ($results as $opt){
            $retorno[$opt["id"]] = $opt["value"];
        }
        return $retorno;
    }

}
