<?php

class Application_Model_PagseguroTransacao {

    public function pagamento($pedido, $cliente, $urlRedirecionamento = null) {

        $configsModel = new Application_Model_Configs();
        $configs = $configsModel->getAllToArray();
        require_once 'PagSeguroLibrary/PagSeguroLibrary.php';
        $paymentRequest = new PagSeguroPaymentRequest();
        $pedidoMapper = new Application_Model_Pedido();
        $servicos = $pedidoMapper->getServicosByPedido($pedido["id"]);

        $credentials = PagSeguroConfig::getAccountCredentials();


        foreach ($servicos as $servico) {
            $paymentRequest->addItem($servico['id'], $servico['titulo'], 1, $servico['valor']);
        }

        if($pedido['desconto']){
            $paymentRequest->setExtraAmount(-$pedido['desconto']);
        }

        $paymentRequest->setShippingType(3); //sem méthodo definido
        //$paymentRequest->setShippingCost(0);

        /* $paymentRequest->setShippingAddress(
          $associado['cep'], $associado['endereco'], $associado['numero'], $associado['complemento'], $associado['bairro'], $associado['cidade'], $associado['sigla'], 'BRA'
          ); */

        $telefone = $this->formataTelefone($cliente);
        $cliente['email'] = 'c88754929536290844835@sandbox.pagseguro.com.br';
        $paymentRequest->setSender(
                //$cliente['nome'], $associado['email'], $telefone['ddd'], $telefone['telefone'], 'CPF', $associado['cpf']
                $cliente['nome'], $cliente['email'], $telefone['ddd'], $telefone['telefone']
        );


        $paymentRequest->setCurrency("BRL");
        $paymentRequest->setReference($pedido['id']);

        if ($urlRedirecionamento) {
            $paymentRequest->setRedirectUrl($urlRedirecionamento);
        }

        try {
            $credentials = PagSeguroConfig::getAccountCredentials(); // getApplicationCredentials() 
            $return = $paymentRequest->register($credentials, true);            
            return $return;
        } catch (PagSeguroServiceException $e) {
           /* echo "<pre>";
            print_r($e);
            exit;*/
            return false;
        }
    }

    public function getTransacao($trasacao) {

        require_once 'PagSeguroLibrary/PagSeguroLibrary.php';

        $paymentRequest = new PagSeguroPaymentRequest();

        $credentials = PagSeguroConfig::getAccountCredentials();
        $transaction = PagSeguroNotificationService::checkTransaction($credentials, $trasacao);

        var_dump($transaction);
        exit;


        if ($pedido->cupom_id) {
            $cupomModel = new Application_Model_Cupom();
            $valor = $cupomModel->calculaDesconto($pedido['total'], $pedido->cupom_id);
            $paymentRequest->setExtraAmount(-$valor);
        }


        $paymentRequest->setShippingType(3); //sem méthodo definido
        //$paymentRequest->setShippingCost(0);

        /* $paymentRequest->setShippingAddress(
          $associado['cep'], $associado['endereco'], $associado['numero'], $associado['complemento'], $associado['bairro'], $associado['cidade'], $associado['sigla'], 'BRA'
          ); */

        $telefone = $this->formataTelefone($cliente);
        $paymentRequest->setSender(
                //$cliente['nome'], $associado['email'], $telefone['ddd'], $telefone['telefone'], 'CPF', $associado['cpf']
                $cliente['nome'], $cliente['email'], $telefone['ddd'], $telefone['telefone']
        );


        $paymentRequest->setCurrency("BRL");
        $paymentRequest->setReference($pedido['id']);

        if ($urlRedirecionamento) {
            $paymentRequest->setRedirectUrl($urlRedirecionamento);
        }

        try {
            $credentials = PagSeguroConfig::getAccountCredentials(); // getApplicationCredentials()  
            return $paymentRequest->register($credentials, true);
        } catch (PagSeguroServiceException $e) {
            return false;
        }
    }

    public function formataTelefone($associado) {
        $retorno = array();
        if ($associado['telefone']) {
            $retorno['ddd'] = substr($associado['telefone'], 1, 2);
            $retorno['telefone'] = substr($associado['telefone'], 5);
        } elseif ($associado['celular']) {
            $retorno['ddd'] = substr($associado['celular'], 1, 2);
            $retorno['telefone'] = substr($associado['celular'], 5);
        }
        $retorno['telefone'] = str_replace('-', '', $retorno['telefone']);
        return $retorno;
    }

    public function inserir($transacao, $status, $referencia) {
        $pagseguroTransacaoMapper = new Application_Model_DbTable_PagseguroTransacao();

        $trans = $pagseguroTransacaoMapper->createRow();

        $trans->transacao = $transacao;
        $trans->data = new Zend_Db_Expr("NOW()");
        $trans->status = $status;
        $trans->pedido_id = $referencia;

        $trans->save();

        return true;
    }

}
