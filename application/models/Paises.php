<?php

class Application_Model_Paises {

    public function getAllToSelect($preTexto = null) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('pais');

        $result = $db->fetchAll($select);

        $retorno = array();

        if ($preTexto) {
            $retorno[null] = $preTexto;
        }
        foreach ($result as $val) {
            $retorno[$val["id"]] = $val["nome"];
        }
        return $retorno;
    }
    
    public function getById($idPais) {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('pais')->where('id = ?',$idPais);

        return $db->fetchRow($select);
    }
    
   

}
