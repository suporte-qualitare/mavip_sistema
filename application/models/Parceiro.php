<?php

class Application_Model_Parceiro {

   
    public function getByHash($hash) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()->from('parceiro')->where('active = 1')->where('hash = ?',$hash);
        return $db->fetchRow($sql);
    }
    
    public function getCategoriaById($idCategoria) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()->from('parceiro_categoria')->where('active = 1')->where('id = ?',$idCategoria);
        return $db->fetchRow($sql);
    }
    
    public function getById($id) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()->from('parceiro')->where('active = 1')->where('id = ?',$id);
        return $db->fetchRow($sql);
    }
    
    public function insereRendimento($dados){
        $rendimentoDb = new Application_Model_DbTable_Rendimento();
        $row = $rendimentoDb->createRow();
        $row['parceiro_id'] = $dados['parceiro_id'];
        $row['pedido_id'] = $dados['pedido_id'];
        $row['valor'] = $dados['valor'];
        $row['data_cadastro'] = new Zend_Db_Expr('NOW()');
        $row->save();
        
        $parceiroDb = new Application_Model_DbTable_Parceiro();
        $parceiro = $parceiroDb->fetchRow(array('id = ?'=>$dados['parceiro_id']));
        $parceiro['saldo'] = $parceiro['saldo']+$dados['valor'];
        $parceiro->save();  
    }
    
}
