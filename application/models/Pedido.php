<?php

class Application_Model_Pedido {

    public function getById($idPedido) {
        $peididoMapper = new Application_Model_DbTable_Pedido();
        return $peididoMapper->fetchRow(array('id = ?' => $idPedido));
    }

    public function getAllByMarca($idMarca) {
        $peididoMapper = new Application_Model_DbTable_Pedido();
        return $peididoMapper->fetchAll(array('marca_id = ?' => $idMarca));
    }

    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('pedido');

        return $db->fetchAll($select);
    }

    public function getByHash($idPedidoHash) {
        $peididoMapper = new Application_Model_DbTable_Pedido();
        return $peididoMapper->fetchRow(array('sha1(id) = ?' => $idPedidoHash));
    }

    public function inserir($dados) {
        $peididoMapper = new Application_Model_DbTable_Pedido();

        $row = $peididoMapper->createRow();
        $row->cliente_id = $dados['cliente_id'];
        $row->total = $dados['total'];
        $row->subtotal = $dados['subtotal'];
        $row->desconto = $dados['desconto'];
        $row->cupom_id = $dados['cupom_id'];
        $row->busca_id = $dados['busca_id'];

        $row->data_cadastro = new Zend_Db_Expr('NOW()');

        $row->ip = $dados['ip'];

        return $row->save();
    }

    public function inserirItem($dadosServico, $dadosMarca = null) {

        try
        {
            $peididoHasServicoMapper = new Application_Model_DbTable_PedidoHasServico();
            $row = $peididoHasServicoMapper->createRow();
            $row->pedido_id = $dadosServico['pedido_id'];
            $row->servico_id = $dadosServico['servico_id'];
            $row->valor = $dadosServico['valor'];
            $row->save();
        }
        catch (Exception $e){
           return false;
        }

        if($dadosMarca){
            try
            {
                $marcaHasServicoMapper = new Application_Model_DbTable_MarcaHasServico();
                $rowMarca = $marcaHasServicoMapper->createRow();
                $rowMarca->servico_id = $dadosMarca['servico_id'];
                $rowMarca->marca_id = $dadosMarca['marca_id'];
                $rowMarca->save();
            }
            catch (Exception $e){
                return false;
            }
        }

        return true;
    }

    public function createPedidoHasMarca($dados){
        $pedidoHasMarcaMapper = new Application_Model_DbTable_PedidoHasMarca();
        $rowPMarca = $pedidoHasMarcaMapper->createRow();
        $rowPMarca->pedido_id = $dados['pedido_id'];
        $rowPMarca->marca_id = $dados['marca_id'];
        $rowPMarca->cliente_id = $dados['cliente_id'];
        $rowPMarca->save();
    }

    public function getServicosByPedido($pedidoId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('servico')
            ->join('pedido_has_servico', 'pedido_has_servico.servico_id = servico.id', array("pedido_has_servico.valor as item_valor", "pedido_has_servico.valor_gru as item_valor_gru"))
            ->where("pedido_has_servico.pedido_id = ?", $pedidoId);
        return $db->fetchAll($select);
    }

    public function getAllByCliente($clienteId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('pedido', array('pedido.id as id', '*'))
            ->join('cliente', 'pedido.cliente_id = cliente.id', array('cliente.nome as cliente'))
            ->join('pedido_has_marca', 'pedido.id = pedido_has_marca.pedido_id', array('status', 'info_gru', 'gru_paga', 'novo_prazo', 'data_prazo'))
            ->join('marca', 'pedido_has_marca.marca_id = marca.id', array('marca.marca as marca','marca.id as marca_id'))
            ->where("pedido.cliente_id = ?", $clienteId);
        return $db->fetchAll($select);
    }

    public function cronDiario(){
        $email = new Application_Model_Emails();

        $pedidos = $this->getDiasPedidoAberto(7);
        if(count($pedidos)){
            $return = $email->sendEmail(array('mail' => 'vtravassos@hotmail.com.br', 'nome' => 'Mavip'), "MAVIP - Pedido faltando 3 dias", "pedido-dias", array('dias'=>3,'pedidos'=>$pedidos));
        }

        $pedidos = $this->getDiasPedidoAberto(8);
        if(count($pedidos)){
            $return = $email->sendEmail(array('mail' => 'vtravassos@hotmail.com.br', 'nome' => 'Mavip'), "MAVIP - Pedido faltando 2 dias", "pedido-dias", array('dias'=>2,'pedidos'=>$pedidos));
        }

        $pedidos = $this->getDiasPedidoAberto(9);
        if(count($pedidos)){
            $return = $email->sendEmail(array('mail' => 'vtravassos@hotmail.com.br', 'nome' => 'Mavip'), "MAVIP - Pedido faltando 1 dia", "pedido-dias", array('dias'=>1,'pedidos'=>$pedidos));
        }
    }

    public function getDiasPedidoAberto($dias){
        $data = new Zend_Date();

        $data->sub($dias, Zend_Date::DAY);

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('pedido')
            ->join('cliente', 'pedido.cliente_id = cliente.id', array('cliente.nome as cliente'))
            ->join('pedido_has_marca', 'pedido.id = pedido_has_marca.pedido_id', array('status', 'info_gru', 'gru_paga', 'novo_prazo', 'data_prazo'))
            ->join('marca', 'pedido_has_marca.marca_id = marca.id', array('marca.marca as marca'))
            ->where("DATE(pedido.data_cadastro) = DATE(?)", $data->get('yyyy-MM-dd'))
            ->where("status = 'pago' OR status = 'processando'");

        return $db->fetchAll($select);
    }

    public function infoGruPedido($dados){
        $pedidoMapper = new Application_Model_DbTable_Pedido();
        $row = $pedidoMapper->fetchRow(array('id = ?' => $dados['id']));
        $info = $dados['gru'] + $dados['gru_pgo'];
        $row->info_gru = $info ? $info : 0;
        return $row->save();
    }

    public function getVencimentos(){
        $status = "'pago' or status = 'assumido - processando'  or status = 'processando' or status = 'concluido' or status = 'novo' or status = 'liberado'";
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "select marca_historico.id as mh_id, 
        (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1)) as data_leitura,
        (select mh.codigo_despacho from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as despacho,
        (select mh.texto from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as texto,
        (select mh.ocultar from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as ocultar,
        (select mh.leitura_id from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as revista,
        (select prazo from marca_historico_mensagem where marca_historico_mensagem.codigo_despacho = despacho and prazo > 0 order by id desc limit 1) as prazo,
        marca.marca as marca, 
        cliente.razao_social as cliente, 
        users.name as responsavel,
        marca.marca as marca,
        marca.processo as processo,
        pedido.data_cadastro,
        pedido.status,
        pedido.info_gru,
        pedido.novo_prazo,
        pedido.data_prazo,
        pedido_has_marca.marca_id, DATEDIFF(CURDATE(), pedido.data_cadastro) as diff,
        DATEDIFF(CURDATE(), (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1))) as leitura,
        pedido.id
                from pedido
                join cliente on (pedido.cliente_id = cliente.id)
                left join users on (pedido.responsavel_users_id = users.id)
                left join pedido_has_marca on (pedido.id = pedido_has_marca.pedido_id)
                left join marca on (pedido_has_marca.marca_id = marca.id and marca.active = 1)
                left join marca_historico on (marca.id = marca_historico.marca_id)
                where status = $status
                group by pedido.id
                order by (prazo - leitura)>0 DESC,(prazo - leitura) desc";

        $stmt = $db->query($sql);

        return $stmt->fetchAll();
    }

    public function setStatus($marcaID, $status){

        $dbPedidoHasMarca = new Application_Model_DbTable_PedidoHasMarca();
        $row = $dbPedidoHasMarca->fetchRow(array('pedido_marca_id = ?' => $marcaID));
        $row->status = $status;
        return $row->save();
    }

    public function PedidoMarcaStatus($idPedido, $status){

        try {
            $pedidoMarcaDb = new Application_Model_DbTable_PedidoHasMarca();
            $row = $pedidoMarcaDb->fetchRow(array('pedido_id = ?' => $idPedido), 'pedido_id desc');
            if($row){
                $row->status = $status;
                $row->save();
            }
        }
        catch (Exception $e){
            echo $e->getMessage();
            exit;
        }
    }

    public function cancela($hash) {
        $pedido = $this->getByHash($hash);

        if ($pedido['id']) {
            $pedido['status'] = 'cancelado';
            return $pedido->save();
        }
        return false;
    }

    // inativando flag de prazo extra de 3 dias
    public function inativarPrazo($hash) {
        $pedido = $this->getByHash($hash);

        if ($pedido['id']) {
            $pedido['novo_prazo'] = "0";
            return $pedido->save();
        }
        return false;
    }
}
