<?php

class Application_Model_PedidoHasMarca {

    public function getByPedido($idPedido) {
        $dbPedMarca = new Application_Model_DbTable_PedidoHasMarca();
        return $dbPedMarca->fetchAll(array('pedido_id = ?' => $idPedido));
    }

    public function getByMarca($idMarca) {
        $dbPedMarca = new Application_Model_DbTable_PedidoHasMarca();
        return $dbPedMarca->fetchRow(array('marca_id = ?' => $idMarca));
    }

    public function getByCliente($idCliente) {
        $dbPedMarca = new Application_Model_DbTable_PedidoHasMarca();
        return $dbPedMarca->fetchAll(array('cliente_id = ?' => $idCliente));
    }

    public function getByStatus($status) {
        $dbPedMarca = new Application_Model_DbTable_PedidoHasMarca();
        return $dbPedMarca->fetchAll(array('status = ?' => $status));
    }

    public function getMarcaByStatus($idMarca,  $status) {
        $dbPedMarca = new Application_Model_DbTable_PedidoHasMarca();
        return $dbPedMarca->fetchRow(array('marca_id = ?' => $idMarca, 'status = ?' => $status));
    }

    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('pedido_has_marca');

        return $db->fetchAll($select);
    }

    public function getByHash($marcaId) {

        $pedMarca = new Application_Model_DbTable_PedidoHasMarca();
        return $pedMarca->fetchRow(array('sha1(marca_id) = ?' => $marcaId));
    }

    public function inserir($dados) {
        $pedMarca = new Application_Model_DbTable_PedidoHasMarca();

        $row = $pedMarca->createRow();
        $row->pedido_id = $dados['pedido_id'];
        $row->marca_id = $dados['marca_id'];
        $row->marca_id = $dados['cliente_id'];
        if($dados['status']){
            $row->status = $dados['status'];
        }

        return $row->save();
    }

    public function confirma($idPedido, $marcas = null) {

        $pedidoMarca = $this->getByPedido($idPedido);

        if(count($pedidoMarca) < 1)
            return false;

        foreach ($pedidoMarca as $row){
            $row['status'] = 'pago';
            $row->save();
        }

        $pedidoDb = new Application_Model_DbTable_Pedido();
        $pedido =  $pedidoDb->fetchRow(array('id  = ?' => $pedidoMarca[0]['pedido_id']));

        $clienteModel = new Application_Model_Cliente();
        $cliente = $clienteModel->getById($pedido['cliente_id']);

        if ($cliente['parceiro_id']) {
            $parceiroModel = new Application_Model_Parceiro();
            $parceiroCategoriaModel = new Application_Model_ParceiroCategoria();
            $parceiro = $parceiroModel->getById($cliente['parceiro_id']);

            if ($cliente['parceiro_id']) {
                $categoria = $parceiroCategoriaModel->getById($parceiro['parceiro_categoria_id']);
                $valor = $pedido['total'] * ($categoria['comissao'] / 100);

                $dados = array(
                    'parceiro_id' => $cliente['parceiro_id'],
                    'pedido_id' => $pedido['id'],
                    'valor' => $valor,
                );

                $parceiroModel->insereRendimento($dados);
            }
        }

        if($marcas)
        {
            $marcaHistoricoDb = new Application_Model_DbTable_MarcaHistorico();

            foreach ($marcas as $marca){
                try
                {
                    $marcaHistorico = $marcaHistoricoDb->createRow();
                    $marcaHistorico->marca_id = $marca['id'];
                    $marcaHistorico->texto = "Início das atividades na MAVIP";
                    $marcaHistorico->save();

                    $marcaDb = new Application_Model_DbTable_Marca();
                    $rowMarca = $marcaDb->fetchRow(array('id = ?' => $marca['id']));
                    if($rowMarca->acompanhamento){
                        $rowMarca->data_acompanhamento = date('Y-m-d H:i:s', strtotime('+1 year'));
                        $rowMarca->save();
                    }

                }
                catch (Exception $exc)
                {
                    echo $exc->getMessage();
                    exit;
                }
            }
        }
    }


    public function cancela($hash) {
        $row = $this->getByHash($hash);

        if(count($row) < 1)
            return false;

        $row['status'] = 'cancelado';
        return $row->save();

    }

// concedendo prazo extra 3 dias
    public function concederPrazo($hash) {
        $pedido = $this->getByHash($hash);

        if ($pedido['marca_id']) {
            $pedido['novo_prazo'] = 1;
            $pedido['data_prazo'] = date('Y-m-d');
            if($pedido['status'] == 'perdeu'){
                $pedido['status'] =  'processando';
            }
            return $pedido->save();
        }
        return false;
    }

// inativando flag de prazo extra de 3 dias
    public function inativarPrazo($hash) {
        $pedidoMarca = $this->getByHash($hash);

        if($pedidoMarca){
            $pedidoMarca['novo_prazo'] = "0";
            return $pedidoMarca->save();
        }
        return false;
    }

    public function cronDiario(){
        $email = new Application_Model_Emails();

        $pedidos = $this->getDiasPedidoAberto(7);
        if(count($pedidos)){
            $return = $email->sendEmail(array('mail' => 'vtravassos@hotmail.com.br', 'nome' => 'Mavip'), "MAVIP - Pedido faltando 3 dias", "pedido-dias", array('dias'=>3,'pedidos'=>$pedidos));
        }

        $pedidos = $this->getDiasPedidoAberto(8);
        if(count($pedidos)){
            $return = $email->sendEmail(array('mail' => 'vtravassos@hotmail.com.br', 'nome' => 'Mavip'), "MAVIP - Pedido faltando 2 dias", "pedido-dias", array('dias'=>2,'pedidos'=>$pedidos));
        }

        $pedidos = $this->getDiasPedidoAberto(9);
        if(count($pedidos)){
            $return = $email->sendEmail(array('mail' => 'vtravassos@hotmail.com.br', 'nome' => 'Mavip'), "MAVIP - Pedido faltando 1 dia", "pedido-dias", array('dias'=>1,'pedidos'=>$pedidos));
        }
    }

    public function getDiasPedidoAberto($dias){
        $data = new Zend_Date();

        $data->sub($dias, Zend_Date::DAY);

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('pedido')
            ->join('cliente', 'pedido.cliente_id = cliente.id', array('cliente.nome as cliente'))
            ->join('pedido_has_marca', 'pedido.id = pedido_has_marca.pedido_id', 'status')
            ->join('marca', 'pedido_has_marca.marca_id = marca.id', array('marca.marca as marca'))
            ->where("DATE(pedido.data_cadastro) = DATE(?)", $data->get('yyyy-MM-dd'))
            ->where("status = 'pago' OR status = 'processando'");

        return $db->fetchAll($select);
    }

    public function getVencimentos(){
        $status = "'pago' or status = 'assumido - processando'  or status = 'processando' or status = 'concluido' or status = 'novo' or status = 'liberado'";
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "select marca_historico.id as mh_id, 
        (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1)) as data_leitura,
        (select mh.codigo_despacho from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as despacho,
        (select mh.texto from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as texto,
        (select mh.ocultar from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as ocultar,
        (select mh.leitura_id from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as revista,
        (select prazo from marca_historico_mensagem where marca_historico_mensagem.codigo_despacho = despacho and prazo > 0 order by id desc limit 1) as prazo,
        marca.marca as marca, 
        cliente.razao_social as cliente, 
        users.name as responsavel,
        marca.marca as marca,
        marca.processo as processo,
        pedido.data_cadastro,
        pedido.status,
        pedido.info_gru,
        pedido.novo_prazo,
        pedido.data_prazo,
        pedido_has_marca.marca_id, DATEDIFF(CURDATE(), pedido.data_cadastro) as diff,
        DATEDIFF(CURDATE(), (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1))) as leitura,
        pedido.id
                from pedido
                join cliente on (pedido.cliente_id = cliente.id)
                left join users on (pedido.responsavel_users_id = users.id)
                left join pedido_has_marca on (pedido.id = pedido_has_marca.pedido_id)
                left join marca on (pedido_has_marca.marca_id = marca.id and marca.active = 1)
                left join marca_historico on (marca.id = marca_historico.marca_id)
                where status = $status
                group by pedido.id
                order by (prazo - leitura)>0 DESC,(prazo - leitura) desc";

        $stmt = $db->query($sql);

        return $stmt->fetchAll();
    }

    public function setStatus($marcaID, $status){

        $dbPedidoHasMarca = new Application_Model_DbTable_PedidoHasMarca();
        $row = $dbPedidoHasMarca->fetchRow(array('marca_id = ?' => $marcaID));
        $row->status = $status;
        if($row->save() && $status == 'pago'){
            $dbMarca = new Application_Model_DbTable_Marca();
            $rowMarca = $dbMarca->fetchRow(array('id = ?' => $marcaID));
            if($rowMarca){
                if($rowMarca->acompanhamento){
                    $rowMarca->data_acompanhamento = date('Y-m-d H:i:s', strtotime('+1 year'));
                    $rowMarca->save();
                }
            }
        }
        return true;
    }

    public function pedidoMarcaStatus($pedido){

        try {
            $pedidoMarcaDb = new Application_Model_DbTable_PedidoHasMarca();
            $rows = $pedidoMarcaDb->fetchAll(array('pedido_id = ?' => $pedido['id']), 'pedido_id desc');
            if(count($rows) < 1)
                return;

            foreach ($rows as $row)
            {
                if($row){
                    $row->status = $pedido['status'];
                    $row->gru_paga = $pedido['gru_paga'];
                    $row->novo_prazo = $pedido['novo_prazo'];
                    $row->data_prazo = $pedido['data_prazo'];
                    $row->info_gru = $pedido['info_gru'];
                    $row->responsavel_users_id = $pedido['responsavel_users_id'];
                    $row->observacao = $pedido['observacao'];
                    $row->save();
                }
            }

        }
        catch (Exception $e){
            echo $e->getMessage();
            exit;
        }
    }

    public function infoGruPedido($dados){
        $pedidoMapper = new Application_Model_DbTable_PedidoHasMarca();
        $row = $pedidoMapper->fetchRow(array('marca_id = ?' => $dados['id']));
        if(!$row)
            return false;

        $info = $dados['gru'] + $dados['gru_pgo'];
        $row->info_gru = $info ? $info : 0;
        return $row->save();
    }
}
