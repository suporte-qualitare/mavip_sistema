<?php

class Application_Model_Planos {
    
    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()->from('planos')
                ->join('servico', 'servico.id = planos.servico_id', array('servico.titulo as servico'))
                ->order('ordem');
        return $db->fetchAll($sql);
    }

    public function getActives() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()->from('planos', array('planos.titulo as plano', 'cor', 'planos.id as plano_id'))
        ->join('servico', 'servico.id = planos.servico_id', array('servico.id as servico', 'servico.valor as valor'))
                ->where('planos.active = 1')->order('planos.ordem');
        return $db->fetchAll($sql);
    }

    public function getById($id) { 
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('planos')
                ->where('id = ?', $id); 
        return $db->fetchAll($select);
    }
    
    public function getByServico($id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()->from('planos')
                ->join('servico', 'servico.id = planos.servico_id', array())
                ->where('servico.id =?', $id);
        return $db->fetchAll($sql);
    }
}