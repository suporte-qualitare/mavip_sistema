<?php

class Application_Model_Processo {
    
    public function getAll($cliente_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()->from('processo')
                ->join('pedido', 'pedido.id = processo.id_pedido', array())
                ->where('processo.active = 1')
                ->where("pedido.cliente_id = $cliente_id")
                ->order('pedido.id DESC')
                ->limit(5);
        return $db->fetchAll($sql);
    }

    public function getById($id) { 
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('processo')
                ->where('id_marca = ?', $id);
        
        return $db->fetchAll($select);
       
    }
    
        public function getByDocumentos($id) { 
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('processo')
           ->where("processo.id_pedido =  $id"); 
        
        return $db->fetchAll($select);
       
    }
}