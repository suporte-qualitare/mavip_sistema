<?php

class Application_Model_Rendimento {

   
    public function getAllByParceiro($idParceiro) {
        $db = Zend_Db_Table::getDefaultAdapter();

        $sql = $db->select()->from('rendimento')
                ->join('pedido','rendimento.pedido_id = pedido.id',array())
                ->join('cliente','pedido.cliente_id = cliente.id',array('cliente.nome as cliente'))
                ->where('rendimento.parceiro_id = ?',$idParceiro);
        return $db->fetchAll($sql);
    }
   
    
}
