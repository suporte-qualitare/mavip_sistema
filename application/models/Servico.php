<?php

class Application_Model_Servico {

    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('servico')->order("ordem")->where("active = 1");
        return $db->fetchAll($select);
    }

    public function getAllToSelect() {

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('servico')->order("titulo")->where("active = 1");

        $result = $db->fetchAll($select);

        $retorno = array();

        foreach ($result as $val) {
            $retorno[$val["id"]] = $val["titulo"]." - " . $val["valor"] ;
        }
        return $retorno;
    }


    public function getById($idServico) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('servico')->where("id = ?",$idServico);
        return $db->fetchRow($select);
    }

    public function getByIds($ids) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('servico')->where("id IN (?)",  $ids );
        return $db->fetchAll($select);
    }

    public function getAcompanhamentos() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('servico', 'id')->where("acompanhamento = ?",  1 );

        $records = array_column($db->fetchAll($select), 'id');
        return count($records > 0) ? array_values ($records) : array();
    }

}
