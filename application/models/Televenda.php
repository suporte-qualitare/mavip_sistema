<?php

class Application_Model_Televenda {
    
    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()->from('televenda');
        return $db->fetchRow($sql);
    }

    public function getById($id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = $db->select()->from('televenda')
                ->where('id = ?', $id);
        return $db->fetchRow($sql);
    }
}