<?php

class Application_Model_Textobotao {

    public function getById($id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('texto_botao')
                ->where('id = ?', $id);
        return $db->fetchRow($select);
    }

    public function getAll() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('texto_botao');
        return $db->fetchAll($select);
    }

}
