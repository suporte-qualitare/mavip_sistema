<?php

class Application_Model_Users {

    function getRow($id) {
        if (!$id) {
            return false;
        }
        
       $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()->from('users')
                ->join('acl_roles', 'users.role_id = acl_roles.id', array('acl_roles.name as role'))
                ->where('users.id = ?', $id);
        return $db->fetchRow($select);
    }

    public function  getByName($name) {
        if (!$name) {
            return false;
        }

        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select()->from('users')
            ->where('users.name = ?', $name);
        return $db->fetchRow($select);
    }

    public function checkPassword($id, $password) {
        $mapper = new Application_Model_DbTable_Users();
        $row = $mapper->fetchRow(array('id=?' => $id, 'password = SHA1(CONCAT(?,salt))' => $password));
        if (!is_null($row)) {
            return true;
        } else {
            return false;
        }
    }

    public function updateSenha($dados) {
        $mapper = new Application_Model_DbTable_Users();
        $row = $mapper->fetchRow(array('id =?' => $dados['id']));

        if (!is_null($row)) {
            if (!empty($dados["nova_senha"])) {
                $row->salt = SHA1(date('Ymdhisu') . mt_rand(1000, 9999));
                $row->password = SHA1($dados["nova_senha"] . $row->salt);
            }


            if ($row->save()) {
                return $row;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function toSelectConsultor() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('users')
            ->where('role_id IN (?)', array(1, 4))
            ->where('active = ?', 1)
            ->order('name');

        $rows = $db->fetchAll($select);

        $array = array();
        $array['Vinicius'] = 'Vinicius';
        foreach ($rows as $value) {
            $array[$value["name"]] = $value["name"]. " - ( ". $value["email"] . ")";
        }
        return $array;
    }

    public function selectAll () {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()->from('users')
        ->where('active = ?', 1)
        ->order('name');

        $rows = $db->fetchAll($select);

        $array = array();
        foreach ($rows as $value) {
            $array[$value["id"]] = $value["name"];
        }
        return $array;
    }


}
