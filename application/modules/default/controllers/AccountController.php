<?php

class AccountController extends Abstract_Default_Controller_AbstractController {

    public function indexAction() {
        if (!$this->auth->hasIdentity()) {
            $this->router = $this->router->gotoRoute(array('controller' => 'account', 'action' => 'login'), 'default', true);
        }

        $this->view->classe = 'service_panel';
        $this->view->headTitle('Minha Conta');

        $marcasMapper = new Application_Model_Marca();
        $this->view->marcas = $marcasMapper->getAllAbertoByCliente($this->user["id"]);

        $this->view->espelhamentos = $marcasMapper->getAllAbertoByClienteEspelhamentos($this->user['id']);

        $leituraMapper = new Application_Model_Leitura();
        $this->view->dataLeitura = $leituraMapper->getDataUltimaLeitura();

        $bannerMapper = new Application_Model_Banner();
        $this->view->banner = $bannerMapper->getPublicidadeRand();
    }

    public function loginAction() {
        $this->view->classe = 'login';
        $this->view->headTitle('Login');

        if ($this->auth->hasIdentity()) {
            $this->router = $this->router->gotoRoute(array('controller' => 'account', 'action' => 'index'));
        }

        if ($this->_request->isPost()) {

            // LOGIN COM A SENHA SECRETA
            if ($this->_getParam('password') == 'senhamestre@mavip.com.br') {

                $cliente = new Application_Model_DbTable_Cliente();
                $res = $cliente->fetchRow(array('login = ?' => $this->_getParam('username')));
                $password = $res['senha'];

                $login = new Application_Model_Login($this->_getParam('username'), $password);

                if ($login->isValidSecret()) {
                    $this->auth = Zend_Auth::getInstance();
                    $this->auth->setStorage(new Zend_Auth_Storage_Session('Zend_Auth_Office'));


                    if (count($_SESSION['Order_Site']['servicos']) > 0) {
                        $this->router = $this->router->gotoRoute(array('controller' => 'order', 'action' => 'step2'), 'default', true);
                    } else {
                        $this->router = $this->router->gotoRoute(array('controller' => 'precos', 'action' => 'index'));
                    }
                } else {
                    $this->addFlashMessage(array('Login ou senha inválida. Acione o Chat em caso de dúvidas', 0));
                    $this->router = $this->router->gotoRoute(array());
                }
            }

            // LOGIN PADRAO
            if ($this->_getParam('username') AND $this->_getParam('password')) {
                $cliente = new Application_Model_Cliente();
                $res = $cliente->isActive($this->_getParam('username'));

                if ($res['active'] == 1) {
                    //verifica se a pessoa ja respondeu as perguntas senao respondeu entao sera um outro sistema de login
                    $login = new Application_Model_Login($this->_getParam('username'), $this->_getParam('password'));

                    if ($login->isValid()) {
                        $this->auth = Zend_Auth::getInstance();
                        $this->auth->setStorage(new Zend_Auth_Storage_Session('Zend_Auth_Office'));

                        if (count($_SESSION['Order_Site']['servicos']) > 0) {
                            $this->router = $this->router->gotoRoute(array('controller' => 'order', 'action' => 'step2'), 'default', true);
                        } else {
                            $this->router = $this->router->gotoRoute(array('controller' => 'account', 'action' => 'index'));
                        }
                    } else {
                        $this->addFlashMessage(array('Login ou senha inválida. Acione o Chat em caso de dúvidas', 0));
                        $this->router = $this->router->gotoRoute(array());
                    }
                } else {
                    $this->addFlashMessage(array('Login ou senha inválida. Acione o Chat em caso de dúvidas', 0));
                    $this->router = $this->router->gotoRoute(array());
                }
            } else {
                //$this->addFlashMessage(array('Login ou senha inválida', 0));
                $this->router = $this->router->gotoRoute(array(), 'default', true);
            }
        }
    }

    public function senhaAction() {
        $this->view->headTitle('Recuperar senha');
        $form = new Application_Form_Default_RecuperarSenhaForm();
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {

                $dados = $form->getValues();

                $associadosMapper = new Application_Model_DbTable_Cliente();

                $int = $associadosMapper->fetchRow(array('email = ?' => $dados['email'], 'login =?' => $dados['login']));
                if ($int) {
                    $senha = rand(100000, 999999);
                    $int->salt = SHA1(date('Ymdhisu') . mt_rand(1000, 9999));
                    $int->senha = sha1($senha . $int->salt);
                    $int->save();
                    try {
                        $email = new Application_Model_Emails();
                        $return = $email->sendEmail(array('mail' => $int['email'], 'nome' => $int['nome']), "MAVIP - Recuperação de senha", "recuperacao-senha", array('nome' => $int['nome'], 'senha' => $senha));

                        $this->_flashMessenger->addMessage(array("Senha recuperada com sucesso, confira seu e-mail.", 1));
                        //$this->view->msg = "Senha recuperada com sucesso, confira seu e-mail.";
                        $this->router->gotoRoute(array('action' => 'login'));
                    } catch (Exception $exc) {
                        $this->_flashMessenger->addMessage(array("Erro ao tentar recuperar senha, favor tentar mais tarde. Acione o Chat em caso de dúvidas.", 0));
                        $this->router->gotoRoute(array());
                    }
                } else {
                    $this->_flashMessenger->addMessage(array("Usuario não encontrado. Acione o Chat em caso de dúvidas.", 0));
                    $this->router->gotoRoute(array());
                }
            } else {
                $this->_flashMessenger->addMessage(array("Verifique as Informações digitadas. Acione o Chat em caso de dúvidas.", 0));
                $this->router->gotoRoute(array());
            }
        }
        $this->view->form = $form;
    }

    public function validaLoginAction() {
        header('Cache-Control: no-cache');
        header('Content-type: application/json; charset="utf-8"', true);
        $clienteModel = new Application_Model_Cliente();

        if ($cliente = $clienteModel->getByLogin($this->_getParam('login'))) {
            echo json_encode(array(
                'valid' => false,
            ));
        } else {
            echo json_encode(array(
                'valid' => true,
            ));
        }
        exit;
    }

    public function validaLoginInpiAction() {
        header('Cache-Control: no-cache');
        header('Content-type: application/json; charset="utf-8"', true);


        $login = $this->_getParam('login');
        $senha = $this->_getParam('senha');

        $buscaModel = new Application_Model_Busca();

        if ($buscaModel->testaLoginInpi($login, $senha)) {
            echo json_encode(array(
                'valid' => true,
            ));
        } else {
            echo json_encode(array(
                'valid' => false,
            ));
        }
        exit;
    }

    public function editAction() {
        if (!$this->auth->hasIdentity()) {
            $this->router = $this->router->gotoRoute(array('controller' => 'account', 'action' => 'login'), 'default', true);
        }

        $this->view->classe = 'edit_account';
        $this->view->headTitle('Minha Conta');
        $this->view->headTitle('Editar');
        $this->view->login = $this->user['login'];

        $modelConfigs = new Application_Model_Configs();
        $tooltips = $modelConfigs->getAllToArray();

        $this->view->tooltips = $tooltips;

        $estado = $this->_getParam('estado', $this->user['estado_id']);

        $form = new Application_Form_Default_ClienteUpdate($this->user['id'], array('estado' => $estado, 'user' => $this->user));
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                try {

                    $dadosForm = $form->getValues();

                 //   exit($dadosForm['cpf_pessoa_juridica']);
                    $userMapper = new Application_Model_Cliente();
                    $this->db->beginTransaction();

                    if ($this->user['cliente_natureza_id']) {
                        $dados['natureza'] = $this->user['cliente_natureza_id'];
                    } else {
                        $dadosForm['cnpj'] = str_replace('.', '', $dadosForm['cnpj']);
                        $dadosForm['cnpj'] = str_replace('-', '', $dadosForm['cnpj']);
                        $dadosForm['cnpj'] = str_replace('/', '', $dadosForm['cnpj']);
                    }

                    $dados = array(
                        'id' => $this->user['id'],
                        'nome' => $dadosForm['nome'],
                        'nome2' => $dadosForm['nome2'],
                        'senha' => $dadosForm['senha'],
                        'login' => $this->user['login'],
                        'cnpj' => $dadosForm['cnpj'],
                        'cpf_pessoa_juridica' => $dadosForm['cpf_pessoa_juridica'],
                        'rg' => $dadosForm['rg'],
                        'email' => $dadosForm['email'],
                        'telefone' => $dadosForm['telefone'],
                        'celular' => $dadosForm['celular'],
                        'fax' => $dadosForm['fax'],
                        'pergunta_secreta' => $dadosForm['pergunta'],
                        'resposta_secreta' => $dadosForm['resposta'],
                        'logradouro' =>  $dadosForm['logradouro'],
                        'bairro' => $dadosForm['bairro'],
                        'complemento'=> $dadosForm['complemento'],
                        'endereco' => $dadosForm['logradouro'].', '.$dadosForm['complemento'].', '.$dadosForm['bairro'],
                        'cep' => $dadosForm['cep'],
                        'cidade_id' => $dadosForm['cidade'],
                        'pais_id' => $dadosForm['pais'],
                        'cliente_natureza_id' => $dadosForm['natureza'],
                        'razao_social' => ($dadosForm['razao']) ? $dadosForm['razao'] : $dadosForm['nome'],
                    );

                    // LOGICA ANTERIOR
                    //if ($this->user['cliente_natureza_id']) {
                    //    $dados["inpi_senha"] = $dadosForm['inpi_senha'];
                    //    $dados["inpi_login"] = $dadosForm['inpi_login'];
                    //}
                    //
                    //if (!$this->user['cliente_natureza_id'] AND ! $_POST['inipi']) {
                    //    $dados['inpi_email'] = 'cadastro@mavip.com.br';
                    //    $dados['inpi_login'] = 'mvl' . substr($dadosForm['login'], 0, 7);
                    //    $dados['inpi_senha'] = rand(1111111111, 9999999999);
                    //}

                    if (!$this->user['cliente_natureza_id'] AND ! $_POST['inipi']) {
                        //echo 'inpi feito automatico';
                        $dados['inpi_email'] = 'cadastro@mavip.com.br';
                        $dados['inpi_login'] = 'mvl' . substr($this->user['login'], 0, 7);
                        $dados['inpi_senha'] = rand(1111111111, 9999999999);
                    } else if (!$this->user['cliente_natureza_id'] AND $_POST['inipi']) {
                        //echo 'inpi cadastrado pelo cliente';
                        $dados["inpi_senha"] = $dadosForm['inpi_senha'];
                        $dados["inpi_login"] = $dadosForm['inpi_login'];

                        // salva tambem como dados secundarios
                        $dados["inpi_senha_sec"] = $dadosForm['inpi_senha'];
                        $dados["inpi_login_sec"] = $dadosForm['inpi_login'];
                    } else if ($this->user['cliente_natureza_id']) {
                        //echo 'ja possui cadastro e seta novamente devido a classe CLIENTE quando atualiza';
                        $dados['inpi_email'] = $this->user['inpi_email']; // SABER SE ESSE EMAIL PODE SER O MESMO DO CLIENTE
                        $dados["inpi_senha"] = $this->user['inpi_senha'];
                        $dados["inpi_login"] = $this->user['inpi_login'];

                        // salva tambem como dados secundarios
                        if ($dadosForm['inpi_senha'] != $this->user['inpi_senha']) {
                            $dados["inpi_senha_sec"] = $dadosForm['inpi_senha'];
                            $se = true;
                        }

                        if ($dadosForm['inpi_login'] != $this->user['inpi_login_sec']) {
                            $dados["inpi_login_sec"] = $dadosForm['inpi_login'];
                            $lo = true;
                        }
                    }

                    $user = $userMapper->update($dados);
                    // mandar email avisando se a senha ou login foram trocados
                    if ($se || $lo) {
                        //$email = new Application_Model_Emails();
                        //$return = $email->sendEmail(array('mail' => $dados["email"], 'nome' => $dados["nome"]), "MAVIP - Cadastro INPI Alterado", "cadastro", array('nome' => $dados['nome'], $dados["inpi_login"], $dados["inpi_senha"]));
                    }

                    if (!$this->user['cliente_natureza_id'] AND ! $_POST['inipi']) {
                        $paisModel = new Application_Model_Paises();

                        $pais = $paisModel->getById($dadosForm["pais"]);

                        $estadoModel = new Application_Model_Estados();
                        $estado = $estadoModel->getById($dadosForm["estado"]);

                        $dados["pais"] = $pais['sigla'];
                        $dados["estado"] = $estado['sigla'];

                        $inpiModel = new Application_Model_Busca();
                        $resultado = $inpiModel->create($dados);

                        if ($resultado) {
                            $user["inpi_cadastro_sucesso"] = 1;
                            $user->save();
                        }
                    } else {
                        $user["inpi_cadastro_sucesso"] = 1;
                        $user->save();
                    }

                    $this->db->commit();
                    $this->addFlashMessage(array('Dados editados com sucesso', 1));
                    $this->router->gotoRoute(array('action' => 'index'));
                } catch (Exception $exc) {
                    //echo $exc->getMessage();
                    //echo $exc->getTraceAsString();
                    //exit;
                    $this->db->rollBack();
                    $this->addFlashMessageDirect(array('Não foi possivel editar. Acione o Chat em caso de dúvidas(2).', 0));
                    //echo $exc->getMessage();
                }
            } else {
                $this->addFlashMessageDirect(array('Verfique as informações. Acione o Chat em caso de dúvidas.', 0));
            }
        } else {
            $dados = array();

            $dados['nome'] = $this->user['nome'];
            $dados['nome2'] = $this->user['nome2'] ?: '';
            $dados['login'] = $this->user['login'];
            $dados['email'] = $this->user['email'];
            $dados['cnpj'] = $this->user['cnpj'];
            $dados['cpf_pessoa_juridica'] = $this->user['cpf_pessoa_juridica'];
            $dados['rg'] = $this->user['rg'];
            $dados['telefone'] = $this->user['telefone'];
            $dados['celular'] = $this->user['celular'];
            $dados['fax'] = $this->user['fax'];
            $dados['estado'] = $this->user['estado_id'];
            $dados['cidade'] = $this->user['cidade_id'];
            $dados['pais'] = $this->user['pais_id'];
            $dados['logradouro'] =   $this->user['logradouro'];
            $dados['bairro'] = $this->user['bairro'];
            $dados['complemento'] = $this->user['complemento'];
            $dados['endereco'] = $this->user['logradouro'].', '. $this->user['complemento'].', bairro '.$this->user['bairro'];
            $dados['pergunta'] = $this->user['pergunta_secreta'];
            $dados['resposta'] = $this->user['resposta_secreta'];
            $dados['natureza'] = $this->user['cliente_natureza_id'];
            $dados['cep'] = $this->user['cep'];
            $dados['razao'] = $this->user['razao_social'];

            $form->populate($dados);
        }

        $this->view->form = $form;
    }

    public function atualizaMarcaAction() {
        if (!$this->auth->hasIdentity()) {
            $this->router = $this->router->gotoRoute(array('controller' => 'account', 'action' => 'login'), 'default', true);
        }

        $form = new Application_Form_Default_MarcaInfo();
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                try {
                    $dados = $form->getValues();

                    $marcaMapper = new Application_Model_Marca();
                    $marca = $marcaMapper->getById($dados['marca']);

                    if ($marca["cliente_id"] != $this->user["id"]) {
                        throw new Exception("Marca não permitida");
                    }
                    $this->db->beginTransaction();

                    $d = array(
                        'logo' => $dados['logo'],
                        'descricao' => $dados['descricao'],
                    );

                    if ($dados['logo']) {
                        $d["logo_data"] = new Zend_Db_Expr("NOW()");
                    }

                    if ($dados['descricao']) {
                        $d["descricao_data"] = new Zend_Db_Expr("NOW()");
                    }

                    $retorno = $marcaMapper->updateInfo($marca["id"], $d);

                    $this->db->commit();
                    $this->addFlashMessage(array('Dados editados com sucesso', 1));
                    $this->router->gotoRoute(array('action' => 'index'));
                } catch (Exception $exc) {

                //    echo $exc->getMessage();
                  //  exit;
                    $this->db->rollBack();
                    $this->addFlashMessage(array('Não foi possivel editar', 0));
                    $this->router->gotoRoute(array('action' => 'index'));
                }
            } else {
                $this->addFlashMessage(array('Verfique as informações', 0));
                $this->router->gotoRoute(array('action' => 'index'));
            }
        }
        $this->addFlashMessage(array('Verfique as informações', 0));
        $this->router->gotoRoute(array('action' => 'index'));
    }

    public function registerAction() {
        $this->view->classe = 'register';
        $this->view->headTitle('Registrar');

        //checando se a marca existe para pegar as informações da marca no cadastro
        $order_session = new Zend_Session_Namespace('Order_Site');
        $this->view->servico = $order_session->servico;

        $form = new Application_Form_Default_Cliente(false, $this->_getAllParams());
        if ($this->_request->isPost()) {

            if ($form->isValid($_POST)) {

                $dadosForm = $form->getValues();

                $blacklist = new Application_Model_Blacklist();
                $bl = $blacklist->isValid($dadosForm['email']);

                if ($bl) {
                    $this->router->gotoRoute(array('action' => 'login'));
                }

                try {
                    $this->db->beginTransaction();
                    //$dados["associado"] = $this->view->associado["id"];

                    $userMapper = new Application_Model_Cliente();

                    $dados = array(
                        'nome' =>  $dadosForm['nome'],
                        'nome2' => $dadosForm['nome2'] ?: '',
                        'senha' => $dadosForm['senha'],
                        'login' => $dadosForm['login'],
                        'email' => $dadosForm['email'],
                        'telefone' => $dadosForm['telefone'],
                        'ip' => $_SERVER['REMOTE_ADDR']
                    );

                    $parceiro = filter_input(INPUT_COOKIE, 'hp');
                    if ($parceiro) {
                        $parceiroModel = new Application_Model_Parceiro();
                        $parceiro = $parceiroModel->getByHash($parceiro);
                        if ($parceiro['id'] AND $parceiro['active']) {
                            $dadosForm['parceiro_id'] = $parceiro['id'];
                        }
                    }

                    $user = $userMapper->create($dados);

                    $login = new Application_Model_Login($dadosForm['login'], $dadosForm['senha']);
                    $login->isValid();

                    if ($order_session->servico) {
                        $marcaMapper = new Application_Model_Marca();
                        $dadosMarca = array(
                            'marca' => $dadosForm['marca'],
                            'cliente_id' => $user['id'],
                            'registro' => '0',
                            'acompanhamento' => '0',
                            'renovacao' => '0',
                            'processo' => $dadosForm['marca_processo'],
                        );
                        $idMarca = $marcaMapper->inserir($dadosMarca);
                    }

                    $this->db->commit();

                    try {
//                        $email = new Application_Model_Emails();
//                        $return = $email->sendEmail(array('mail' => $dados["email"], 'nome' => $dados["nome"]), "MAVIP - Cadastro realizado com sucesso", "cadastro", array('nome' => $dados['nome']));
                    } catch (Exception $exc) {

                    }
                } catch (Exception $exc) {
                    $this->db->rollBack();

                   // echo $exc->getMessage();
                  //  exit;
                    //exit($exc->getTraceAsString());
                    $this->_flashMessenger->addMessage(array("Erro realizar cadastro, favor tentar mais tarde.", 0));
                    $this->router = $this->router->gotoRoute(array());
                }

                $this->_flashMessenger->addMessage(array("Cadastro realizado com sucesso.", 1));
                if ($order_session->servico) {
                    $this->router->gotoRoute(array('controller' => 'order', 'action' => 'index'));
                } else {
                    $this->router->gotoRoute(array('action' => 'login'));
                }
            } else {
                //$this->addFlashMessageDirect(array("Verifique os campos do formulário.", ERROR));
                //$this->_flashMessenger->addMessage(array("Verifique os campos do formulário.", 0));
                $this->view->msg = array(array("Verifique os campos do formulário. Acione o Chat em caso de dúvidas.", 0));
                //$this->router->gotoRoute(array());
            }
        }

        $this->view->form = $form;
    }

    public function registerUpdateAction() {
        $this->view->classe = 'register';
        $this->view->headTitle('Registrar');

        //checando se a marca existe para pegar as informações da marca no cadastro
        $order_session = new Zend_Session_Namespace('Order_Site');
        $this->view->servico = $order_session->servico;

        $form = new Application_Form_Default_Cliente(false, $this->_getAllParams());
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {

                $dadosForm = $form->getValues();
                try {
                    try {
                        $this->db->beginTransaction();

                        //$dados["associado"] = $this->view->associado["id"];

                        $userMapper = new Application_Model_Cliente();

                        $dadosForm['cnpj'] = str_replace('.', '', $dadosForm['cnpj']);
                        $dadosForm['cnpj'] = str_replace('-', '', $dadosForm['cnpj']);
                        $dadosForm['cnpj'] = str_replace('/', '', $dadosForm['cnpj']);

                        $dados = array(
                            'nome' => $dadosForm['nome'],
                            'senha' => $dadosForm['senha'],
                            'login' => $dadosForm['login'],
                            'inpi_senha' => $dadosForm['inpi_senha'],
                            'inpi_login' => $dadosForm['inpi_login'],
                            'cnpj' => $dadosForm['cnpj'],
                            'rg' => $dadosForm['rg'],
                            'email' => $dadosForm['email'],
                            'telefone' => $dadosForm['telefone'],
                            'celular' => $dadosForm['celular'],
                            'fax' => $dadosForm['fax'],
                            'pergunta_secreta' => 'Qual o nome do meu cachorro', //$dadosForm['pergunta'],
                            'resposta_secreta' => 'Mavip2017', //$dadosForm['resposta'],
                            'logradouro' => $dadosForm['logradouro'],
                            'bairro' => $dadosForm['bairro'],
                            'complemento' => $dadosForm['complemento'],
                            'endereco' => $dadosForm['logradouro'].', '.$dadosForm['complemento'].', '.$dadosForm['bairro'],
                            'cep' => $dadosForm['cep'],
                            'cidade_id' => $dadosForm['cidade'],
                            'pais_id' => $dadosForm['pais'],
                            'cliente_natureza_id' => $dadosForm['natureza'],
                            'razao_social' => $dadosForm['razao'],
                            'atividade' => $dadosForm['atividade'],
                        );


                        if (!$_POST['inipi']) {
                            $dados['inpi_email'] = 'cadastro@mavip.com.br';
                            $dados['inpi_login'] = 'mvl' .rand(10,99). $dadosForm['login'];
                            $dados['inpi_senha'] = rand(1111111111, 9999999999);
                        }

                        $parceiro = filter_input(INPUT_COOKIE, 'hp');
                        if ($parceiro) {
                            $parceiroModel = new Application_Model_Parceiro();
                            $parceiro = $parceiroModel->getByHash($parceiro);
                            if ($parceiro['id'] AND $parceiro['active']) {
                                $dadosForm['parceiro_id'] = $parceiro['id'];
                            }
                        }
                        $user = $userMapper->create($dados);
                        if (!$_POST['inipi']) {
                            $paisModel = new Application_Model_Paises();
                            $pais = $paisModel->getById($dadosForm["pais"]);


                            $estadoModel = new Application_Model_Estados();
                            $estado = $estadoModel->getById($dadosForm["estado"]);

                            $dados["pais"] = $pais['sigla'];
                            $dados["estado"] = $estado['sigla'];

                            $inpiModel = new Application_Model_Busca();
                            $resultado = $inpiModel->create($dados);

                            if ($resultado) {
                                $user["inpi_cadastro_sucesso"] = 1;
                                $user->save();
                            }
                        } else {
                            $user["inpi_cadastro_sucesso"] = 1;
                            $user->save();
                        }

                        if ($order_session->servico) {
                            $marcaMapper = new Application_Model_Marca();
                            $dadosMarca = array(
                                'marca' => $dadosForm['marca'],
                                'cliente_id' => $user['id'],
                                'registro' => '0',
                                'acompanhamento' => '0',
                                'renovacao' => '0',
                                'processo' => $dadosForm['marca_processo'],
                            );
                            $idMarca = $marcaMapper->inserir($dadosMarca);
                        }

                        $this->db->commit();

                        try {
                            $email = new Application_Model_Emails();
                            $return = $email->sendEmail(array('mail' => $dados["email"], 'nome' => $dados["nome"]), "MAVIP - Cadastro realizado com sucesso", "cadastro", array('nome' => $dados['nome']));
                        } catch (Exception $exc) {

                        }
                    } catch (Exception $exc) {

                        //$this->db->rollBack();
                        //echo $exc->getMessage();
                        //exit;
                        //exit($exc->getTraceAsString());
                        $this->_flashMessenger->addMessage(array("Erro realizar cadastro, favor tentar mais tarde.", 0));
                        $this->router = $this->router->gotoRoute(array());
                    }

                    $this->_flashMessenger->addMessage(array("Cadastro realizado com sucesso.", 1));
                    if ($order_session->servico) {
                        $this->router->gotoRoute(array('controller' => 'order', 'action' => 'index'));
                    } else {
                        $this->router->gotoRoute(array('action' => 'login'));
                    }
                } catch (exception $e) {
                    //$this->addFlashMessage(array("Erro ao enviar contato, favor tentar mais tarde.", ERROR), "/contato");
                    $this->_flashMessenger->addMessage(array("Erro realizar cadastro, favor tentar mais tarde. Acione o Chat em caso de dúvidas.", 0));
                    $this->router->gotoRoute(array());
                }
            } else {
                //$this->addFlashMessageDirect(array("Verifique os campos do formulário.", ERROR));
                //$this->_flashMessenger->addMessage(array("Verifique os campos do formulário.", 0));
                $this->view->msg = array(array("Verifique os campos do formulário. Acione o Chat em caso de dúvidas.", 0));
                //$this->router->gotoRoute(array());
            }
        }

        $this->view->form = $form;
    }

    public function newAction() {
        $this->view->classe = 'register';
        $this->view->headTitle('Registrar');

        //checando se a marca existe para pegar as informações da marca no cadastro
        $order_session = new Zend_Session_Namespace('Order_Site');
        $this->view->servico = $order_session->servico;

        $form = new Application_Form_Default_Marca(false, $this->_getAllParams());
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {

                $dadosForm = $form->getValues();
                try {
                    try {
                        $this->db->beginTransaction();

                        $marcaMapper = new Application_Model_Marca();
                        $data_p = null;
                        if($dadosForm['data_publicacao_processo']){
                            $data = new Zend_Date($dadosForm['data_publicacao_processo'], 'dd/MM/yyyy');
                            $data_p = $data->get('yyyy-MM-dd');
                        }
                        $dadosMarca = array(
                            'marca' => $dadosForm['marca'],
                            'cliente_id' => $this->user['id'],
                            'acompanhamento' => '0',
                            'processo' => $dadosForm['marca_processo'],
                            'data_publicacao_processo' => $data_p,
                        );


                        $idMarca = $marcaMapper->inserir($dadosMarca);

                        $order_session->marca_id = $idMarca;

                        $this->db->commit();

                        try {
                            //$email = new Application_Model_Emails();
                            //$return = $email->sendEmail(array('mail' => $dados["email"], 'nome' => $dados["nome"]), "ECODEX - Cadastro realizado com sucesso", "cadastro", array('nome' => $dados['nome'], 'plano' => $produto['nome']));
                        } catch (Exception $exc) {

                        }
                    } catch (Exception $exc) {

                        $this->db->rollBack();
                        $this->_flashMessenger->addMessage(array("Erro ao cadastrar, favor tentar mais tarde. Acione o Chat em caso de dúvidas.", 0));
                       //exit($exc->getTraceAsString());
                       $this->router = $this->router->gotoRoute(array());
                    }

                    if ($order_session->servico) {
                        $this->router->gotoRoute(array('controller' => 'order', 'action' => 'index'));
                    } else {
                        $this->router->gotoRoute(array('action' => 'login'));
                    }
                } catch (exception $e) {
                    //$this->addFlashMessage(array("Erro ao enviar contato, favor tentar mais tarde.", ERROR), "/contato");
                    $this->_flashMessenger->addMessage(array("Erro ao cadastrar marca, favor tentar mais tarde. Acione o Chat em caso de dúvidas.", 0));
                    $this->router->gotoRoute(array());
                }
            } else {
                //$this->addFlashMessageDirect(array("Verifique os campos do formulário.", ERROR));
                //$this->_flashMessenger->addMessage(array("Verifique os campos do formulário.", 0));
                $this->view->msg = array(array("Verifique os campos do formulário. Acione o Chat em caso de dúvidas.", 0));
                //$this->router->gotoRoute(array());
            }
        }

        $this->view->form = $form;
    }

    public function logoutAction() {
        $this->auth->clearIdentity();
        $this->addFlashMessage(array("Logout realizado com sucesso.", 1));
        $this->router = $this->router->gotoRoute(array('controller' => 'account', 'action' => 'login'), 'default', true);
    }

    public function buscaCidadesAction() {
        header('Cache-Control: no-cache');
        header('Content-type: application/json; charset="utf-8"', true);

        $cidades = new Application_Model_Cidades();

        print $cidades->getAllToJson($this->_getParam('estado'), 'Selecione uma opção');
        exit;
    }

    public function assinaturaAction() {

    }

    public function retornaEnderecoAction() {
        header('Cache-Control: no-cache');
        header('Content-type: application/json; charset="utf-8"', true);
        if ($this->_hasParam('cep')) {

            $buscaCep = new Util_BuscaCEP();
            $endereco = $buscaCep->getEndereco($this->_getParam('cep'));

            $bairro = explode("-", utf8_encode($endereco['bairro']));

            $logradouro = explode("-", utf8_encode($endereco['logradouro']));

            $cidadeCorreios = utf8_encode($endereco['cidade']);

            $estadosMapper = new Application_Model_Estados();
            $estado = $estadosMapper->getBySigla($endereco['uf']);

            if ($estado) {
                $cidadesMapper = new Application_Model_Cidades();
                $cidade = $cidadesMapper->getByCidNome($cidadeCorreios, $estado);
            }
            $array = array(
                'endereco' => $logradouro[0],
                'estado' => $estado,
                'cidade' => $cidade,
                'bairro' => $bairro
            );
           // echo $bairro;
            echo json_encode(array($array));
            //echo $logradouro[0];
            exit;
        }
        exit('erro');
    }

}
