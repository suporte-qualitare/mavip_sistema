<?php

class ApiController extends Zend_Rest_Controller {
    public function init(){
        $this->_helper->viewRenderer->setNoRender(true);
    }
    //salvando a marca na tabela busca
    //Não terá e-mail por conta da limitação encontrada na LGPD
    //o ip ainda tem que ser analisado.

    public function iniciarMarcaAction () {
        $marca = $_GET['marca'];

        if ($marca){
            $buscaModel = new Application_Model_Busca();
            $dados['marca'] = $marca;
            $dados['ip'] = $_SERVER["REMOTE_ADDR"];
            $dados['resultado'] = '-2'; // pesquisa ainda não concluida
            //se passar o id da busca, irá atualizar, senão cria
            if ($_GET['id_busca']){
                $id = $_GET['id_busca'];
                $buscaModel->update($_GET['id_busca'], $dados);
            }
            else{
                $id = $buscaModel->inserir($dados);
            }
            $this->getResponse()->setHttpResponseCode(200);
            $this->_helper->json(['success' => true, 'message' => 'Dados inseridos com successo', 'id_busca' => $id]);
        }
        else{
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json(['success' => false, 'message' => 'Por favor, informe o nome da marca']);
        }
    }

    //buscar categoria de acordo com o tipo da marca
    public function buscarCategoriaAction () {
        $categoriaModel = new Application_Model_Categoria();
        $tipo = $_GET['tipo'];
        if (!$tipo) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json(['success' => false, 'message' => 'Por favor, informe a categoria']);
        }
        else{
            $categorias = $categoriaModel->getAllByTipo($tipo);
            $this->getResponse()->setHttpResponseCode(200);
            $this->_helper->json(['success' => true, 'message' => 'Dados inseridos com successo', 'categorias' => $categorias]);
        }

    }
    //finalizar cadastro da marca e retornar média
    public function criarMarcaAction () {
        $objeto ['email'] = $_GET['email'];
        $objeto ['id_busca'] = $_GET['id_busca'];
        $objeto['nome'] = $_GET['nome'];
        $objeto['marca'] = $_GET['marca'];
        $objeto['id_categoria'] = $_GET['id_categoria'];
        $objeto['estilo'] = $_GET['estilo'];
        $objeto['tipo'] = $_GET['tipo'];

        $buscaModel = new Application_Model_Busca();

        //eventuais campos vazios
        $objeto_errors = [];

        //percorrendo o array afim de encontrar campos vazios
        foreach ($objeto as $key => $obj) {
            if (!$objeto[$key]) {
                $objeto_errors[$key] = "Campo $key obrigatório";
            }
        }

        //verificar se e-mail é válido
        //Não será necessário blacklist, pois a validação do e-mail agora é no final.
        $validateEmail = new Zend_Validate_EmailAddress();
        if (!$validateEmail->isValid($objeto['email'])) {
            $objeto_errors['email'] = "E-mail inválido";
        }

        //campos vazios encontrados? Retornar ao usuário
        if (count($objeto_errors) > 0) {
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json(['success' => false, 'message' => 'Campos obrigatórios não preenchidos', 'erros' => $objeto_errors]);
        }

        $categoriaModel = new Application_Model_Categoria();
        $categoria = $categoriaModel->getById($objeto['id_categoria']);

        //se a INPI estiver fora, irá lançar uma exceção aqui.
        try{
            $objeto['resultado'] = $buscaModel->busca($objeto['marca'], $categoria['id_inpi']);
            $pesquisa = $buscaModel->getByInfo($objeto['marca'], $objeto['tipo'], $objeto['id_categoria']);

            $porcento = 100;

            if ($pesquisa['porcentagem']){
                $porcento = $pesquisa['porcentagem'];
            }else {
                if ($objeto['resultado'] == 0){
                    $porcento = (int) mt_rand(0, 2);
                }elseif($objeto['resultado'] == 1){
                    $porcento = (int) mt_rand(48, 52);
                }
                elseif($objeto['resultado'] == 2){
                    $porcento = (int) mt_rand(88, 92);
                }
            }

            $objeto['porcentagem'] = $porcento;
            $objeto['telefone'] = "(61) 932181000";
            $buscaModel->update($objeto['id_busca'], $objeto);

            $this->getResponse()->setHttpResponseCode(200);
            $this->_helper->json(['success' => true, 'message' => 'Dados inseridos com successo', 'busca' => $objeto]);
        }
        catch (Exception $exc){
            $objeto['resultado'] = -1;
            $buscaModel->update($objeto['id_busca'], $objeto);
            $this->getResponse()->setHttpResponseCode(400);
            $this->_helper->json(['success' => false, 'message' => 'Sistema INPI indisponível!', 'erros' => $objeto_errors]);
        }
    }

    public function indexAction () {

    }

    public function getAction()
    {

    }

    public function headAction()
    {
        $this->getResponse()->setBody(null);
    }

    public function postAction()
    {
        
    }

    public function putAction()
    {

    }

    public function deleteAction()
    {

    }
}