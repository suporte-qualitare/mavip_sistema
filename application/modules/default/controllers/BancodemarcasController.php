<?php

class BancodemarcasController extends Abstract_Default_Controller_AbstractController
{
    public function init(){
        // desativando exibição do banco de marcas
        $this->_helper->redirector('index', 'index', 'default');
    }
    public function indexAction()
    { 
        
       
        try {

            $modelCategoria = new Application_Model_Categoria();

            $tipoProduto = $modelCategoria->getAllByTipo("produto");
            $tipoServico = $modelCategoria->getAllByTipo("servico");
            $tipoComercio = $modelCategoria->getAllByTipo("comercio");

            $this->view->tipoProduto = $tipoProduto;
            $this->view->tipoServico = $tipoServico;
            $this->view->tipoComercio = $tipoComercio;

        } catch (Exception $exc) {
            echo $exc;
            //     ini_set('display_errors', 0);
            //  ini_set('display_startup_errors', 0);
            //   error_reporting(0);
            //   $this->router->gotoRoute(array(), 'default', true);
        }
       
    }

    public function bancoDeMarcasSugestaoAction()
    {
        try {

            $page = $this->_getParam('page', 1);
            $tipo = $this->getParam('tipo');
            $classe = $this->getParam('classe');

            $letra = $this->getParam('letra');
            $busca = $this->getParam('nome');
            $this->view->busca = $busca;
            
            $bmModel = new Application_Model_Bancodemarcas();
            $dados = $bmModel->getAllByTipoAndClasse($tipo, $classe,$letra,$busca);

            $adapter = new Zend_Paginator_Adapter_Array($dados);
            $paginator = new Zend_Paginator($adapter);
            $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage(20);


            if ($tipo == "servico"):
                $tp = "Serviço";
            endif;
            if ($tipo == "produto"):
                $tp = "Produto";
            endif;
            if ($tipo == "comercio"):
                $tp = "Comércio";
            endif;

            if ($paginator):
                $this->view->assign('paginator', $paginator);
                //    $this->view->dados = $dados;
                
            else:
                $this->view->null = "Sem dados.";
            endif;
            
            $this->view->classe = $classe;
            $this->view->tipo = $tipo;
            $this->view->tipoTexto = $tp;


        } catch (Exception $exc) {
            echo $exc;
            //     ini_set('display_errors', 0);
            //  ini_set('display_startup_errors', 0);
            //   error_reporting(0);
            //   $this->router->gotoRoute(array(), 'default', true);
        }
    }

    public function exibirPorLetraAction()
    {

        try {

            $page = $this->_getParam('page', 1);

            $bmModel = new Application_Model_Bancodemarcas();
            $letra = $this->getParam('letra');
            $tipo = $this->getParam('tipo');
            $classe = $this->getParam('classe');
            
            if ($tipo == "servico"):
                $tp = "Serviço";
            endif;
            if ($tipo == "produto"):
                $tp = "Produto";
            endif;
            if ($tipo == "comercio"):
                $tp = "Comércio";
            endif;
           
            $dados = $bmModel->getAllByLetra($tipo, $classe, $letra);

            $adapter = new Zend_Paginator_Adapter_Array($dados);
            $paginator = new Zend_Paginator($adapter);
            $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage(20);

            if ($dados != null) {
                $this->view->assign('paginator', $paginator);
                //  $this->view->dadosByLetra = $dados;
            } else {

                $this->view->null = "Sem dados.";

            }
            
            $this->view->classe = $classe;
            $this->view->tipo = $tipo;
            $this->view->tipoTexto = $tp;

            $this->render('banco-de-marcas-sugestao');

        } catch (Exception $exc) {

            $this->render('banco-de-marcas-sugestao');
        }
    }


    public function buscarMarcaAction()
    {

        try {
            $bmModel = new Application_Model_Bancodemarcas();

            if (isset($_POST["nome"]) and isset($_POST["tipo"])) {
                $nome = $_POST["nome"];
                $tipo = $_POST["tipo"];
                $resultado = $bmModel->getAllByNomeAndTipo($nome, $tipo);

                if ($resultado) {
                    $this->view->resultado = $resultado;
                } else {
                    $this->view->null = "Nenhum Resultado encontrado";
                }
                
              $classe = $_POST["classe"];
              $this->view->tipo = $tipo;
              $this->view->classe = $classe;
              $this->render('banco-de-marcas-sugestao');
            } else {
                $this->render('banco-de-marcas-sugestao');
            }


        } catch (Exception $exc) {
            echo $exc;
        }

    }

}