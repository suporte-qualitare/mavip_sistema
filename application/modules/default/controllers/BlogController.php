<?php

class BlogController extends Abstract_Default_Controller_AbstractController {

    public function indexAction() {
                
        if ($this->_getParam('buscar')) {
            $this->_redirect('blog/q/' . $this->_getParam('buscar'));
        }
        
        $bannerModel = new Application_Model_Banner();
        $this->view->publicidade = $bannerModel->getPublicidadeRand();
        
        $blogModel = new Application_Model_Blog();
        $this->view->blogs = $blogModel->getAllPage($this->_getParam('q'), $this->_getParam('c'), $this->_getParam('pag'));

        $this->view->categorias = $blogModel->getCategorias();
        $this->view->maisLidas = $blogModel->getMaisLidas(5);
        
        $this->view->headTitle('Blog');
    }

    public function internaAction() {
        if ($this->_getParam('id')) {
            $blogModel = new Application_Model_Blog();
            $return = $blogModel->incrementaLeitura($this->_getParam('id'));
            if(!$return)
                throw new Exception("Postagem não localizada");

            $this->view->post = $blogModel->getRow($this->_getParam('id'));
            $this->view->headTitle($this->view->post['titulo']);
            $this->view->maisLidas = $blogModel->getMaisLidas(5);
            $this->view->headTitle('Blog');
            $this->view->headMeta()->appendName('description', strip_tags($this->view->post['texto']));
            $this->view->headMeta()->appendName('keywords', '');
        }else{
            throw new Exception("Postagem não localizada");
        }
    }

}
