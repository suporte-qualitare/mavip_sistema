<?php

class BuscaController extends Abstract_Default_Controller_AbstractController {
    protected $token = "6LeajgEdAAAAADI5HmhjNiSOF0nYvSzl1nB3AMuK";

    public function indexAction() {

        $this->view->classe = 'home2';
        $this->view->headTitle('Pesquisa');

        $buscaModel = new Application_Model_Busca();
        $auth_session = new Zend_Session_Namespace('Order_Site');
        $filterMarca = new Zend_Filter_StripTags();

        if ($auth_session->busca_id) {
            $busca = $buscaModel->getById($auth_session->busca_id);
        }

        //verificando se é robô
        if (!$auth_session->busca_id){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $this->token, 'response' => $this->_getParam('token'))));
            $response = curl_exec($ch);
            curl_close($ch);
            $arrResponse = json_decode($response, true);
            if(!($arrResponse["success"] && $arrResponse["score"] >= 0.5)) {
                if ($_SERVER["REMOTE_ADDR"] != '127.0.0.1')
                  throw new Exception();
            }
        }
       

        // recebe requisicao ajax na segunda pagina
        if( $this->_getParam('telefone')){
            
            $dados['telefone'] = $this->_getParam('telefone');
            $dados['nome'] =  $this->_getParam('nome');
            $dados['estilo'] = $filterMarca->filter($this->_getParam('estilo'));
            $dados['tipo'] = $filterMarca->filter($this->_getParam('tipo'));
            $dados['resultado'] = '-2';
            $dados['email'] =  $busca["email"];
            $dados['marca'] = $busca["marca"];
            $buscaModel->update($busca['id'], $dados);
        }
            
        if ($this->_getParam('email', $busca["email"])) {

            $validateEmail = new Zend_Validate_EmailAddress();

            $email = $this->_getParam('email', $busca["email"]);
    
            if (!$validateEmail->isValid($email)) {
                $this->_flashMessenger->addMessage(array("E-mail ou marca inválida.", 0));
                $this->router->gotoRoute(array(), 'default', true);
            }
    
            $blacklistDb = new Application_Model_DbTable_Blacklist();
            $bl = $blacklistDb->fetchRow(array("email = ?" => $email));
    
            if ($bl) {
                $this->_flashMessenger->addMessage(array("E-mail ou marca inválida.", 0));
                $this->router->gotoRoute(array(), 'default', true);
            }
               $this->view->email = $email;
            
        }

        //verificando se tem marca e usuário da sessão para poder preencher a tabela pesquisa
        if ($this->_getParam('marca')) {
            $this->view->marca = $filterMarca->filter($this->_getParam('marca', $busca["marca"]));
            if ($this->_getParam('token')){
                $dados = array();
                $dados['email'] = $this->view->email;
                $dados['ip'] = $_SERVER["REMOTE_ADDR"];
                $dados['marca'] = $this->view->marca ;
                $idUser = Zend_Auth::getInstance()->getIdentity()->id;
                $dados['cliente_id'] =  $idUser;
                $dados['resultado'] = '-2'; // pesquisa ainda não concluida
        
            $id = $buscaModel->inserir($dados);
            $auth_session->busca_id = $id;
            }
            
        }

        $categoriaModel = new Application_Model_Categoria();
        $this->view->produtos = $categoriaModel->getAllByTipo('produto');
        $this->view->servicos = $categoriaModel->getAllByTipo('servico');
        $this->view->comercio = $categoriaModel->getAllByTipo('comercio');
    }

    public function resultadoAction() {
        $forms = false;
        $categoriaModel = new Application_Model_Categoria();
        $textoButaoModel = new Application_Model_Textobotao();
        $buscaModel = new Application_Model_Busca();

        $this->view->classe = 'result';
        $this->view->headTitle('Resultado');
    
        $auth_session = new Zend_Session_Namespace('Order_Site');

        $textoBotao = $textoButaoModel->getAll();

        if ($auth_session->busca_id) {
            
            $busca = $buscaModel->getById($auth_session->busca_id);
        }

        $validateEmail = new Zend_Validate_EmailAddress();
        $filterMarca = new Zend_Filter_StripTags();

        $email = $this->_getParam('email', $busca["email"]);
        $nome = $this->_getParam('nome', $busca["nome"]);
        $telefone = $this->_getParam('telefone', $busca["telefone"]);

        if (!$validateEmail->isValid($email)) {
            $this->_flashMessenger->addMessage(array("E-mail ou marca inválida.", 0));
            $this->router->gotoRoute(array(), 'default', true);
        }

        $blacklistDb = new Application_Model_DbTable_Blacklist();
        $bl = $blacklistDb->fetchRow(array("email = ?" => $email));

        if ($bl) {
            $this->_flashMessenger->addMessage(array("E-mail ou marca inválida.", 0));
            $this->router->gotoRoute(array(), 'default', true);
        }
        try {

        $dados = array();
        $dados['email'] = $email;
        $dados['nome'] = $nome;
        $dados['telefone'] = $telefone;
        $dados['marca'] = $filterMarca->filter($this->_getParam('marca', $busca["marca"]));
        $dados['estilo'] = $filterMarca->filter($this->_getParam('estilo', $busca["estilo"]));
        $dados['tipo'] = $filterMarca->filter($this->_getParam('tipo', $busca["tipo"]));

        if ($dados['tipo']) {
            if ($dados['tipo'] == 'servico') {
                $dados['categoria_id'] = $filterMarca->filter($this->_getParam('categoria_servico'));
            } elseif ($dados['tipo'] == 'produto') {
                $dados['categoria_id'] = $filterMarca->filter($this->_getParam('categoria_produto'));
            } elseif ($dados['tipo'] == 'comercio') {
                $dados['categoria_id'] = $filterMarca->filter($this->_getParam('categoria_comercio'));
            }
        } else {
            $dados['categoria_id'] = $busca["categoria_id"];
        }

            $cat = $categoriaModel->getById($dados['categoria_id'] ? $dados['categoria_id'] : $busca["categoria_id"] );
            $classe = $categoriaModel->getIdInpi($dados['categoria_id'] ? $dados['categoria_id'] : $busca["categoria_id"]);
            $forms = true;

            $dados['resultado'] = $buscaModel->busca($dados['marca'], $cat['id_inpi']);

             if ($auth_session->busca_id)
                    $buscaModel->update($auth_session->busca_id, $dados);

        } catch (Exception $exc) {
            if($forms == false){
                $this->_flashMessenger->addMessage(array("Por favor, preencha todos os campos.", 0));
                $this->router->gotoRoute(array('controller' => 'busca'), 'default', true);
            }
            $dados['resultado'] = '-1'; // Sistema INPI Indisponivel
            if ($auth_session->busca_id) {
                $buscaModel->update($auth_session->busca_id, $dados);
            }
            $this->_flashMessenger->addMessage(array("Sistema do INPI indisponível no momento, tente mais tarde.", 0));
            $this->router->gotoRoute(array(), 'default', true);
       }
        
        // PESQUISA NOME, TIPO E CATEGORIA_ID NA TABELA BUSCA
        $pesq = $buscaModel->getByInfo($dados['marca'], $dados['tipo'], $dados['categoria_id']);
        // VERIFICA SE EXISTE PORCENTAGEM NA MARCA PESQUISADA
        if ($pesq['porcentagem']) {
            $porcento = $pesq['porcentagem'];
        } else {
            // DEIXAR VALOR DO RESULTADO MAIS RANDOMICO        
            if ($dados['resultado'] == 0) {
                $porcento = (int) mt_rand(0, 2);
            }

            if ($dados['resultado'] == 1) {
                $porcento = (int) mt_rand(48, 52);
                // 48 52
            }

            if ($dados['resultado'] == 2) {
                $porcento = (int) mt_rand(88, 92);
                // 88 92
            }

            if ($dados['resultado'] == 3) {
                $porcento = 100;
            }
        }

        $dados['porcentagem'] = $porcento;

        $classe = $categoriaModel->getIdInpi($dados['categoria_id']);
        $this->view->tipo = $dados['tipo'];
        $this->view->classe_ = $classe;
        $this->view->marca = $dados['marca'];
        $this->view->resultado = $dados['resultado'];
        $this->view->porcento = $porcento;
        $this->view->textobotao = $textoBotao[0]['texto'];

        //$auth_session = new Zend_Session_Namespace('Busca_Site');

        if ($auth_session->busca_id) {
            $buscaModel->update($auth_session->busca_id, $dados);
        }
        if ($this->_getParam('marca')) {
            $this->router->gotoRoute(array());
        }
        
    }
    
    	public function contatooffAction($dados) {
		
                    //$dados = $this->getAllParams();
            if($dados){
          
         	try {
			$email = new Application_Model_Emails();
			$return = $email->sendEmail(array('mail' => 'atendimento@mavip.com.br', 'nome' => 'atendimento',
			), 'MAVIP - Atendimento MAVIP', 'atendimento-mavip',
                                    array('nome' => $dados['nome'], 
                                        'marca' => $dados['marca'], 
                                        'tipo' => $dados['tipo'],
					'email' => $dados['email']));
                //         this->_flashMessenger->addMessage(array("Contato realizado com sucesso!.", 1));
			//$this->view->msg = "Senha recuperada com sucesso, confira seu e-mail.";
		//	$this->router->gotoRoute(array(), 'default', true);

		} catch (Exception $exc) {

			$this->_flashMessenger->addMessage(array("Erro ao enviar contato.", 0));
			$this->router->gotoRoute(array(), 'default', true);
		}
            }
            else{
                    $this->router->gotoRoute(array());
            }
	}


}
