<?php

class CronController extends Zend_Controller_Action {

    public function diarioAction() {
        $pesquisaModel = new Application_Model_Pedido();
        $pesquisaModel->cronDiario();
        exit;
    }

    public function pesquisaNomeAction() {
        $pesquisaModel = new Application_Model_Busca();
        //echo $pesquisaModel->semApostilhaProcesso(817727167);
        //echo $pesquisaModel->semApostilhaProcesso(826097847);

        $empresaModel = new Application_Model_Empresa();
        $empresaBd = new Application_Model_DbTable_Empresa();

        $processos = $empresaModel->getAllProcesso();
        
        foreach ($processos as $value) {
            $nomeEmpresa = $pesquisaModel->semApostilhaProcesso($value['processo']);
            $dadosEmpresa = $pesquisaModel->getDadosEmpresa($nomeEmpresa);
            
            if ($nomeEmpresa == '' || $nomeEmpresa == '-') {
                $nomeEmpresa = NULL;
            }
            
            $emp = $empresaBd->fetchRow(array('processo = ?' => $value['processo']));
            $emp->nome = $nomeEmpresa;
            $emp->cnpj = ($dadosEmpresa['cnpj'] == '') ? NULL : $dadosEmpresa['cnpj'];
            $emp->email = ($dadosEmpresa['email'] == '') ? NULL : $dadosEmpresa['email'];
            $emp->save();
        }

        exit;
    }

    public function alertasAction() {        
        $leitura = new Application_Model_Leitura();
        $modelBusca = new Application_Model_Busca();
        $modelAlertas = new Application_Model_Alertas();
        
       //teste
        $nomeEmpresa = 'MAVIP TECNOLOGIA SERVICOS LTDA';
        
         $modelbusca = new Application_Model_Busca();
         $CNPJ = $modelbusca->getCNPJ($nomeEmpresa);
        //$modelAlertas->delete($nomeEmpresa['nome']);
         exit();
        
        foreach ($nomeEmpresa as $n){
            $dadosEmpresa = $modelBusca->getDadosEmpresa($n);
            $IPAS = array('ipas' => 'IPAS359');
            $dados = array($dadosEmpresa, $IPAS);
            $insert = $modelAlertas->insert($dados);
            
            if($insert){
                echo "salvo com sucesso!";
                echo"<br><br>";
                echo $dados[0]['email'];
                echo"<br><br>";
                echo $dados[0]['nome'];
                echo"<br><br>";
                echo $dados[1]['ipas'];
                
            }
            else {
                echo "erro ao salvar!";
            }
            exit();
        }

        // RETORNA AS LEITURAS, MARCAS E MARCAS_HISTORICOS REALIZADAS

        $IPAS = array('IPAS029');
        $limite = 90;
        $leitura->leituraDespachosLimite($IPAS, $limite);

        // $IPAS = array('IPAS404', 'IPAS106', 'IPAS139', 'IPAS157', 'IPAS161', 'IPAS024', 'IPAS423');
        // $limite = 60;
        // $leitura->leituraDespachosLimite($IPAS, $limite);

        // $IPAS = array('IPAS359');
        // $limite = 5;
        // $leitura->leituraDespachosLimite($IPAS, $limite);

        // $IPAS = array('IPAS005', 'IPAS304', 'IPAS414', 'IPAS033', 'IPAS047', 'IPAS091', 
        //                 'IPAS112', 'IPAS113', 'IPAS009', 'IPAS135', 'IPAS421', 'IPAS142');
        // $leitura->leituraDespachosLimite($IPAS); 

        exit;
    }

    public function acompanhamentoAction(){

        $marcaModel = new Application_Model_Marca();
        $marcas = $marcaModel->cronAcompanhamento();

        if($marcas){
            $email = new Application_Model_Emails();
            foreach($marcas as $marca){

                if($marca['prazo'] > 60)
                    return false;

                switch($marca['prazo']){
                    case 60:
                        $template = 'acompanhamento_dias_restantes';
                        $assunto = 'Mavip - Restam apenas 60 dias';
                        break;
                    case 30:
                        $template = 'acompanhamento_dias_restantes';
                        $assunto = 'Mavip - Restam apenas 30 dias';
                        break;
                    case 5:
                        $template = 'acompanhamento_dias_restantes';
                        $assunto = 'Mavip - Restam apenas 5 dias';
                        break;
                    case -1:
                        $template = 'acompanhamento_fim';
                        $assunto = 'Mavip - Seu acompanhamento chegou ao fim';
                        break;
                    default: return false;
                }
                $data = new Zend_Date($marca['data_acompanhamento']);
                $nome = explode(" ", $marca['nome']);
                $dados = array(
                    'nome_reponsavel' => $nome[0],
                    'marca' => $marca['marca'],
                    'data_acompanhamento' => $data->get('dd/MM/yyyy'),
                    'prazo' => $marca['prazo']
                );
                  if($marca['email'])
                        $return = $email->sendEmail(array('mail' => $marca['email'], 'nome' => $marca['marca']), $assunto, $template, $dados);
               }

        }
        exit($return);
    }

    public function decenioAction() {

        switch ($this->_getParam('tipo')):
            case 'ordinario':
                $where = "(DATE_SUB(CURDATE(), INTERVAL '10' YEAR)) <= marca.publicacao_inpi and (DATE_SUB(CURDATE(), INTERVAL '9-11' YEAR_MONTH)) >= marca.publicacao_inpi and ocultar_decenio = 0 and marca.active = 1";
                $tipo = "Decênio Ordinário";
                break;
            case 'extraordinario':
                $where = "(DATE_SUB(CURDATE(), INTERVAL '10-6' YEAR_MONTH)) <= marca.publicacao_inpi and (DATE_SUB(CURDATE(), INTERVAL '10-05' YEAR_MONTH)) >= marca.publicacao_inpi  and ocultar_decenio = 0 and marca.active = 1";
                $tipo = "Decênio Extraordinário";
                break;
            default:
                $where = false;
        endswitch;

        if(!$where)
            exit('Parametro nao definido.');

        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
            ->from('marca', array('marca.marca as marca','marca.processo as processo', 'marca.publicacao_inpi as data_publicacao'))
            ->join('cliente', 'marca.cliente_id = cliente.id', array('cliente.nome as cliente', 'cliente.email as email'))
            ->where($where);
        $records =  $db->fetchAll($select);

        $dataAtual = date_create(date('Y-m-d'));

        foreach ($records as $rec){
            $dataRegistro = date_create($rec['data_publicacao']);
            $interval = date_diff($dataAtual, $dataRegistro);

            $mailModel = new Application_Model_Emails();
            if($this->_getParam('tipo') == 'ordinario' && ($interval->y == '9' && $interval->m == '11' && $interval->d  == '20')){
                $rec['decenio'] = 'Ordinário';
                $mailModel->sendEmail(array('nome' => 'Sistema Mavip', 'mail'=> 'atendimento@mavip.com.br'),"ALERTA PARA FIM DE PRAZO ORDINÁRIO", 'decenio', $rec);
            }
            if($this->_getParam('tipo') == 'extraordinario' && ($interval->y == '10' && $interval->m == '05' && $interval->d  == '20')){
                $rec['decenio'] = 'Extraordinário';
                $mailModel->sendEmail(array('nome' => 'Sistema Mavip', 'mail'=> 'atendimento@mavip.com.br'), "ALERTA PARA FIM DE PRAZO EXTRAORDINÁRIO", 'decenio', $rec);
            }
        }

        exit;
    }


    public function agendamentosAction(){
        $agendamentoModel = new Application_Model_ClienteAgendamentos();
        $agendamentos = $agendamentoModel->getNextDay();

        if(!$agendamentos)
            die;
        $count = 0;
        foreach ($agendamentos as $row){
                $this->dispararEmail($row);
                $count++;
            }
        die($count);
    }

    public function dispararEmail($param){

        if(!$param)
            return false;

        $modelUsers = new Application_Model_Users();
        $modelCliente = new Application_Model_Cliente();

        $user = $modelUsers->getByName($param['consultor']);
        $cliente = $modelCliente->getById($param[ 'cliente_id']);

        if($user && $cliente &&  $param['data']){
            $data =  new Zend_Date($param['data']);
            $param['data'] = $data->toString('dd/MM/YYYY HH:mm');
            $param['nome'] = $cliente['nome'];

            $modelEmail = new Application_Model_Emails();

            if($param['sendClient']){
                $clienteEmail = array('mail' => $cliente['email'], 'nome'=> $cliente['nome']);
                $modelEmail->sendEmail($clienteEmail, "MAVIP - Novo agendamento de {$cliente['nome']}", 'agendamento-cliente', $param);
            }
            $userEmail = array('mail' => $user['email'], 'nome'=> $user['name']);
            $modelEmail->sendEmail($userEmail, "MAVIP - Novo agendamento: {$cliente['nome']}", 'agendamento-mavip', $param);
        }
    }

}
