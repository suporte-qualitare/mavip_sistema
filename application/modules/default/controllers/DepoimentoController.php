<?php

class DepoimentoController extends Abstract_Default_Controller_AbstractController {

    public function indexAction() {
//        if (!$this->auth->hasIdentity()) {
//            $this->router = $this->router->gotoRoute(array('controller' => 'account', 'action' => 'login'), 'default', true);
//        }

        $depoimentoModel = new Application_Model_Depoimentos();
        $this->view->depoimento = $depoimentoModel->getAll();

        $form = new Application_Form_Default_Depoimento();
        $this->view->form = $form;

        $this->view->headTitle('Depoimentos');

        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                
                $dadosForm = $form->getValues();                
                $depoimentoMapper = new Application_Model_Depoimentos();
                $this->db->beginTransaction();                
                
                $dados = array(
                    //'nome' => $this->user['nome'],
                    'nome' => $dadosForm['nome'],
                    'depoimento' => $dadosForm['depoimento'],
                    'imagem' => $dadosForm['imagem']
                );
                                
                $res = $depoimentoMapper->inserir($dados);                  
                $this->db->commit();
                
                if ($res) {                    
                    $form->reset();
                    $this->addFlashMessageDirect(array('Depoimento realizado com sucesso.', 1));
                } else {
                    $this->db->rollBack();
                    $form->reset();
                    $this->addFlashMessageDirect(array("Depoimento não realizado.", 0));
                }  
                
            } else {   
                $form->reset();
                $this->addFlashMessageDirect(array("Depoimento não realizado.", 0));                
            }            
           
        }
        $form->reset();
    }

}
