<?php

class FaqController extends Abstract_Default_Controller_AbstractController {

    public function indexAction() {
        $faqModel = new Application_Model_Faq();
        $this->view->faq = $faqModel->getAll();
        
        $depoimentoModel = new Application_Model_Depoimentos();
        $limite = 6; // QUANTIDADE DE DEPOIMENTOS PARA SER VISUALIZADO NO SITE
        $this->view->depoimentos = $depoimentoModel->getTopActive($limite);
    }

    public function internaAction() {
        
    }

}
