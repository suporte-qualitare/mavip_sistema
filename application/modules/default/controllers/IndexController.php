<?php

class IndexController extends Abstract_Default_Controller_AbstractController {

	public function indexAction() {
		$this->view->classe = 'home';

		$busca = new Application_Model_Busca();
		//$busca->create();

		$this->view->off = $this->_getParam('off');

		if ($this->_getParam('hp')) {

			$parceiroMapper = new Application_Model_Parceiro;
			$parceiro = $parceiroMapper->getByHash($this->_getParam('hp'));

			if ($parceiro['id']) {
				setcookie("hp", $this->_getParam('hp'), null, '/');
				$this->redirect('/');
			}
		}

		$buscaModel = new Application_Model_Busca();
		$auth_session = new Zend_Session_Namespace('Order_Site');

		if ($auth_session->busca_id) {
			$this->view->busca = $buscaModel->getById($auth_session->busca_id);
		}

		$blogModel = new Application_Model_Blog();
		$this->view->posts = $blogModel->getUltimas(3);

		$depoimentoModel = new Application_Model_Depoimentos();
		$limite = 6; // QUANTIDADE DE DEPOIMENTOS PARA SER VISUALIZADO NO SITE
		$this->view->depoimentos = $depoimentoModel->getTopActive($limite);
	}

	public function testeAction() {

		//$buscaModel = new Application_Model_Busca();
		//$buscaModel->create(array('tesste'));
	}

	public function historicoAction() {
          
                $idUser = Zend_Auth::getInstance()->getIdentity()->id;
                $buscaModel = new Application_Model_Busca();
		$records =  $buscaModel->getByClienteId($idUser);
                
                $this->view->records = $records;
	}

	public function sobreAction() {
		$this->view->classe = 'about';
		$this->view->headTitle('Sobre o MAVIP');

		$paginaMapper = new Application_Model_DbTable_Pagina();
		$this->view->pagina = $paginaMapper->fetchRow(array('id = 1'));

		$equipeMapper = new Application_Model_DbTable_Equipe();
		$this->view->equipes = $equipeMapper->fetchAll(array('active = 1'));
	}

	public function servicosAction() {
		$this->view->classe = 'services';
		$this->view->headTitle('Nossos serviços');

		$servicoModel = new Application_Model_Servico();
		$this->view->servicos = $servicoModel->getAll();
	}

	public function termosAction() {
		$this->view->classe = 'termos';
		$this->view->headTitle('Termos de uso da ferramenta');
	}

	public function legislacaoAction() {
		$this->view->classe = 'legislacao';
		$this->view->headTitle('Legislação');
	}

	public function pesquisaAction() {
		$this->view->classe = 'pesquisa';
		$this->view->headTitle('Pesquisa');

		$buscaModel = new Application_Model_Busca();
		$auth_session = new Zend_Session_Namespace('Order_Site');

		if ($auth_session->busca_id) {
			$this->view->busca = $buscaModel->getById($auth_session->busca_id);
		}
	}

	public function precosAction() {
		$this->view->classe = 'precos';
		$this->view->headTitle('Preços');

		$servicoModel = new Application_Model_Servico();
		$this->view->servicos = $servicoModel->getAll();
		$this->view->servico1 = $servicoModel->getById(21);
		$this->view->servico2 = $servicoModel->getById(25);
		$this->view->servico3 = $servicoModel->getById(26);
		
		$this->view->planos = ['Prime', 'Select', 'One'];
		$this->view->mavip_one = 21; // SERVICO MAVIP ONE
		$this->view->mavip_select = 25; // SERVICO MAVIP SELECT
		$this->view->mavip = 26; // SERVICO MAVIP Prime
                
        $this->view->servicoPedidoRegistro = $servicoModel->getById(1);
        $this->view->servicoAcompanhamentoAnual = $servicoModel->getById(2);
		$this->view->servicoOnline = $servicoModel->getById(3);
	
		$this->view->ro_extraordinario = $servicoModel->getById(4);
	
		$this->view->rro_ordinario = $servicoModel->getById(8);
		$this->view->rro_extraordinario = $servicoModel->getById(9);

		$this->view->manif_oposicao = $servicoModel->getById(6);
				 
	}

	public function precosAtualizadoAction() {
		$this->view->classe = 'precos';
		$this->view->headTitle('Preços');

		$servicoModel = new Application_Model_Servico();
		//$this->view->servicos = $servicoModel->getAll();
		//$this->view->servico1 = $servicoModel->getById(24); // SERVICO MAVIP ONE
		//$this->view->servico2 = $servicoModel->getById(25); // SERVICO MAVIP SELECT
	}


	public function contatoAction() {
		$this->view->classe = 'contact';
		$this->view->headTitle('Contato');

		$form = new Application_Form_Default_ContatoForm();
		if ($this->_request->isPost()) {
			if ($form->isValid($_POST)) {

				try {
					$dados = $form->getValues();

					$msg = "Nome: " . $dados['nome'] . "<br />
                           Email: " . $dados['email'] . "<br />
                           Assunto: " . $dados['assunto'] . "<br />
                           Mensagem: " . $dados['mensagem'];

					$mail = new Zend_Mail();
					$mail->setBodyHtml(utf8_decode($msg));
					$mail->setFrom($dados['email'], $dados['nome']);
					$mail->addTo('contato@mavip.com.br', 'MAVIP');
					$mail->setSubject(utf8_decode('Contato - do site MAVIP'));
					$mail->send();

					$retorno['retorno'] = 'ok';

					$retorno['msn'] = 'Contato realizado com sucesso!';
				} catch (exception $e) {
					$retorno['retorno'] = 'fail';
					$retorno['msn'] = 'Erro ao enviar contato!';
				}
			} else {
				$retorno['retorno'] = 'fail';
				$retorno['msn'] = 'Verifique os campos do formulário. Acione o Chat em caso de dúvidas.';
				$this->view->msg = array(array("Verifique os campos do formulário. Acione o Chat em caso de dúvidas.", 0));
			}

			header('Cache-Control: no-cache');
			header('Content-type: application/json; charset="utf-8"', true);
			echo json_encode($retorno);
			exit;
		}

		$this->view->form = $form;
	}

	public function faqAction() {

	}

	public function faqinternaAction() {

	}

}
