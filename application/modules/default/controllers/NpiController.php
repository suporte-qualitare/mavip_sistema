<?php

class NpiController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->disableLayout();
    }

    public function pagamentoAction() {
        require_once 'PagSeguroLibrary/PagSeguroLibrary.php';
        $writer = new Zend_Log_Writer_Stream(PATH_ARQS . 'teste.log');
        $log = new Zend_Log($writer);


        if ($this->_getParam('notificationCode')) {
            $credentials = PagSeguroConfig::getAccountCredentials();
            $transaction = PagSeguroNotificationService::checkTransaction($credentials, $this->_getParam('notificationCode'));


            $pagseguroTransacao = new Application_Model_PagseguroTransacao();
            $pagseguroTransacao->inserir($transaction->getCode(), $transaction->getStatus()->getValue(), $transaction->getReference());

            

            if ($transaction->getStatus()->getValue() == 3) {
                $pedidoMapper = new Application_Model_PedidoHasMarca();
                $pedidoMapper->confirma($transaction->getReference());
                
                $pedido =$pedidoMapper->getById($transaction->getReference());
                
                $clienteMapper = new Application_Model_Cliente();
                $cliente = $clienteMapper->getById($pedido["cliente_id"]);
                
                $email = new Application_Model_Emails();
                $return = $email->sendEmail(array('mail' => $cliente['email'], 'nome' => $cliente['nome']), "MAVIP - Pagamento confirmado", "pagamento", array());
            
            }
        }
        exit;
    }

}
