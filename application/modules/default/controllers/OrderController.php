<?php

class OrderController extends Abstract_Default_Controller_AbstractController
{
    public function indexAction(){

        if (!$this->auth->hasIdentity())
            $this->router->gotoRoute(array('controller' => 'account', 'action' => 'login'), 'default', true);

        if($this->_getParam('s'))
        {
            $this->view->servico = $this->_getParam('s');
        }
        else if(count($_SESSION['Order_Site']['servicos']))
        {
            $this->router->gotoRoute(array('controller' => 'order', 'action' => 'step2'), 'default', true);
        }
        else
        {
            $this->router->gotoRoute(array('controller' => 'precos'), 'default', true);
        }
    }

    public function step2Action()
    {
        if (!$this -> auth -> hasIdentity()) {
            $this -> router -> gotoRoute(array('controller' => 'account', 'action' => 'login'), 'default', true);
        }

        $servicoModel = new Application_Model_Servico();

        if ($this -> _request -> isPost()) {

            if ($this -> get_numeric($this -> getParam('nservico'))) {
                $servico = $servicoModel->getById($this->getParam('nservico'));
                if(!$servico)
                    $this -> router -> gotoRoute(array(), 'default', true);

                $_SESSION['Order_Site']['servicos'][] =
                    array('marca' => $this -> getParam('marca'),
                        'processo' => $this -> getParam('processo'),
                        'observacao' => $this -> getParam('observacao'),
                        'servico' => $servico['id'],
                        'nome' => $servico['termo'],
                        'valor' => $servico['valor'],
                    );
            }

        }

        if(!count($_SESSION['Order_Site']['servicos']))
            $this->router->gotoRoute(array('controller' => 'precos'), 'default', true);


        sort( $_SESSION['Order_Site']['servicos']);
        $itens = $_SESSION['Order_Site']['servicos'];
        $total = 0;
        foreach ($itens as $item) {
            $servico = $servicoModel -> getById($item['servico']);
            if($servico){
                $total += $servico['valor'];
                if ($item['urgencia'])
                    $total += $servicoModel -> getById(22)['valor'];
            }
        }

        //  Desconto de parceiro ou cupom

        $hashParceiro = filter_input(INPUT_COOKIE, 'hp');
        $descontoParceiro = null;
        if ($hashParceiro) {
            $parceiroModel = new Application_Model_Parceiro();
            $parceiro = $parceiroModel -> getByHash($hashParceiro);

            if ($parceiro['id'] AND $parceiro['active']) {
                $categoria = $parceiroModel -> getCategoriaById($parceiro["parceiro_categoria_id"]);
                $descontoParceiro = $categoria["desconto"];
            }
        }

        if ($_SESSION['Order_Site']['cupom'] OR $descontoParceiro) {
            $cupomModel = new Application_Model_Cupom();
            $this -> view -> desconto = $cupomModel -> calculaDesconto($total, $_SESSION['Order_Site']['cupom'], $descontoParceiro);
        }

        $this -> view -> classe = 'order';
        $this -> view -> headTitle('Minha Conta');
        $this -> view -> servicos = $itens;
        $this -> view -> total = $total;

        //Servicos
        $this -> view -> pedidoRegistro = $servicoModel -> getById(1);
        $this -> view -> acompanhamento = $servicoModel -> getById(2);
        $this -> view -> registroOnline = $servicoModel -> getById(3);
        $this -> view -> rOnlineExtraordinario = $servicoModel -> getById(4);
        $this -> view -> renovacaoRegistroOrdinario = $servicoModel -> getById(8);
        $this -> view -> renovacaoRegistroExtraOrdinario = $servicoModel -> getById(9);
        $this -> view -> urgencia = $servicoModel -> getById(22);
        $this -> view -> mavipOnePlus = $servicoModel -> getById(21);
        $this -> view -> mavipOne = $servicoModel -> getById(24);
        $this -> view -> mavipSelect = $servicoModel -> getById(25);
        $this -> view -> mavipPrime = $servicoModel -> getById(26);

    }

    public function step3Action(){

        if($this->_request->isPost()){

            // verifica autenticação de usuario
            if (!$this->auth->hasIdentity()) {
                $this->router->gotoRoute(array('controller' => 'account', 'action' => 'login'), 'default', true);
            }
            if(count($_SESSION['Order_Site']['servicos']) < 1)
                $this->router->gotoRoute(array(), 'default', true);

        }
        $order_session['servicos'] =  $_SESSION['Order_Site']['servicos'];
        $servicoModel = new Application_Model_Servico();
        $total = 0;
        $this->view->urgenciaValor = $servicoModel->getById(22)['valor'];
        $itens = $_SESSION['Order_Site']['servicos'];

        foreach ($itens as $item) {
            $total += $item['valor'];
            if($item['urgencia'])
                $total +=  $this->view->urgenciaValor;

        }
        $hashParceiro = filter_input(INPUT_COOKIE, 'hp');

        $descontoParceiro = null;
        if ($hashParceiro) {
            $parceiroModel = new Application_Model_Parceiro();
            $parceiro = $parceiroModel->getByHash($hashParceiro);

            if ($parceiro['id'] AND $parceiro['active']) {
                $categoria = $parceiroModel->getCategoriaById($parceiro["parceiro_categoria_id"]);
                $descontoParceiro = $categoria["desconto"];
            }
        }


        if ($_SESSION['Order_Site']['cupom'] OR $descontoParceiro) {

            $cupomModel = new Application_Model_Cupom();
            $this->view->desconto = $cupomModel->calculaDesconto($total, $_SESSION['Order_Site']['cupom'], $descontoParceiro);
        }

        $this->view->classe = 'order';
        $this->view->headTitle('Minha Conta');
        $this->view->servicos = $_SESSION['Order_Site']['servicos'];
        $this->view->total = $total;

    }

    public function addUrgenciaAction(){

        if($this->_request->isPost()){

            $urgenciaKey = $this->getParam('serv');
            if($_SESSION['Order_Site']['servicos'][$urgenciaKey]['urgencia'] !== 1)
            {
                $_SESSION['Order_Site']['servicos'][$urgenciaKey]['urgencia'] = 1;
                $texto = "Ao contratar o Pedido de Urgência, o cliente ABRE MÃO EXPRESSAMENTE do Art. 49 do Código de Defesa do Consumidor,' +
                        ' DIREITO DE ARREPENDIMENTO. Posto que, a seu requerimento, a Mavip destinará  recursos e mão-de-obra especializada para cumprir o' +
                        ' prazo de urgência de até 48 horas úteis após a confirmação do pagamento.";
                $this->addFlashMessage(array($texto, 1), array('controller' => 'order','action' => 'step2'));
            }
            else
            {
                $_SESSION['Order_Site']['servicos'][$urgenciaKey]['urgencia'] = 0;
            }

        }
        $this->router->gotoRoute(array('controller' => 'order','action' => 'step2'), 'default', true);
    }

    public function checkoutAction()
    {
        if (!$this->auth->hasIdentity()) {
            $this->router = $this->router->gotoRoute(array('controller' => 'account', 'action' => 'login'), 'default', true);
        }

        $itens = $_SESSION['Order_Site']['servicos'];

        if(!$itens)
            $this->router = $this->router->gotoRoute(array(), 'default', true);

        $this->view->classe = 'checkout';
        $this->view->headTitle('Finalizar compra');

        $marcaMapper = new Application_Model_Marca();
        $pedidoMapper = new Application_Model_Pedido();
        $servicoModel = new Application_Model_Servico();

        $valor = 0;
        foreach ($itens as $item){
            $servico = $servicoModel->getById($item['servico']);
            $valor += $servico['valor'];
        }

        $hashParceiro = filter_input(INPUT_COOKIE, 'hp');
        $descontoParceiro = null;
        if ($hashParceiro) {
            $parceiroModel = new Application_Model_Parceiro();
            $parceiro = $parceiroModel->getByHash($hashParceiro);
            if ($parceiro['id'] AND $parceiro['active']) {
                $categoria = $parceiroModel->getCategoriaById($parceiro["parceiro_categoria_id"]);
                $descontoParceiro = $categoria["desconto"];
            }
        }

        $dadosPedido['cupom_id'] = $_SESSION['Order_Site']['cupom'];
        $cupomModel = new Application_Model_Cupom();
        $desconto = $cupomModel->calculaDesconto($valor, $_SESSION['Order_Site']['cupom'], $descontoParceiro);

        if($desconto > 0)
            $cupom = '1';
        else
            $cupom = '0';

        $dadosPedido['total'] = $valor - $desconto;
        $dadosPedido['subtotal'] = $valor;
        $dadosPedido['desconto'] = $desconto;
        $dadosPedido['cliente_id'] = Zend_Auth::getInstance()->getIdentity()->id;
        $dadosPedido['ip'] = $_SERVER['REMOTE_ADDR'];
        $idPedido = $pedidoMapper->inserir($dadosPedido);

        foreach ($itens as $item){
            $servico = $servicoModel->getById($item['servico']);
            $this->db->beginTransaction();

            if($item['marca']){

                $marcaRow = array(
                    'marca' => $item['marca'],
                    'cliente_id' => Zend_Auth::getInstance()->getIdentity()->id,
                    'processo' => $item['processo'] ? $item['processo'] : null,
                    'acompanhamento' => 0,
                    'urgencia' => $item['urgencia']  ? 1 : 0,
                    'observacao' => $item['observacao'] ? $item['observacao'] : null
                );

                $servicosAcompanhamento = $servicoModel->getAcompanhamentos();

                if(in_array($item['servico'], $servicosAcompanhamento))
                    $marcaRow['acompanhamento'] = 1;

                $idMarca = $marcaMapper->inserir($marcaRow);
                $marcasIDS[] = $idMarca;

                $dadosMarca = array(
                    'marca_id' => $idMarca,
                    'servico_id' => $item['servico']
                );

                $dadosServico = array(
                    'pedido_id' => $idPedido,
                    'servico_id' => $servico["id"],
                    'valor' => $servico["valor"],
                );

                // pedido_has_servico    ,    // marca_has_servico
                $p = $pedidoMapper->inserirItem($dadosServico, $dadosMarca);
                if(!$p)
                    $this->addFlashMessage(array('Erro na operação. Contacte o nosso atendimento.', 0), array('controller' => 'order','action' => 'step3'));

                $this->db->commit();
            }
            else
            {
                $this->addFlashMessage(array('Marca não definida.', 0), array('controller' => 'order','action' => 'step2'));
            }
        }

        foreach (array_unique($marcasIDS) as $id){
            $phm['pedido_id'] = $idPedido;
            $phm['marca_id'] = $id;
            $phm['cliente_id'] = Zend_Auth::getInstance()->getIdentity()->id;
            $pedidoMapper->createPedidoHasMarca($phm);
        }


        if ($dadosPedido['total'] <= 0) {
            $pedidoHasMarca = new Application_Model_PedidoHasMarca();

            $marcas = $marcaMapper->getAllById($marcasIDS);
            $pedidoHasMarca->confirma($idPedido, $marcas);
            unset($_SESSION['Order_Site']);

            $this->sendMail($this->user['email'], $this->user['nome']);
            $this->router->gotoRoute(array('controller' => 'order', 'action' => 'cupom'), 'default', true);
        }

        if ($hashParceiro) {
            $parceiroModel = new Application_Model_Parceiro();
            $parceiro = $parceiroModel->getByHash($hashParceiro);
            if ($parceiro['id'] AND $parceiro['active']) {
                $clienteMapper = new Application_Model_Cliente();
                $clienteMapper->updateParceiro($this->user['id'], $parceiro['id']);
            }
        }

        unset($_SESSION['Order_Site']);
        $busca_session = new Zend_Session_Namespace('Busca_Site');
        $busca_session->unsetAll();

        $this->sendMail($this->user['email'], $this->user['nome']);
        $this->router->gotoRoute(array('controller' => 'order', 'action' => 'pagamento', 'c' => sha1($idPedido), 'is-cupom' => $cupom ), 'default', true);
    }

    public function sendMail($mail, $nome){
        $email = new Application_Model_Emails();
        $email->sendEmail(array('mail' => $mail, 'nome' => $nome), "MAVIP - Pedido realizado com sucesso", "pedido", array());
    }

    public function pagamentoAction()
    {

        $this->view->classe = 'login';
        $this->view->headTitle('Minha Conta');

        $marcaMapper = new Application_Model_Marca();
        $pedidoMapper = new Application_Model_Pedido();
        $this->view->pedido = $pedidoMapper->getByHash($this->_getParam('c'));

        if (!$this->view->pedido['id']) {
            $this->router->gotoRoute(array('controller' => 'order', 'action' => 'index'), 'default', true);
        }


        $pagSeguroModel = new Application_Model_PagseguroTransacao();
        $this->view->token = $pagSeguroModel->pagamento($this->view->pedido, $this->user);
        //   $this->view->marca = $marcaMapper->getById($this->view->pedido['marca_id']);
    }

    public function sucessoAction()
    {
        $this->view->classe = 'login';
        $this->view->headTitle('Sucesso');
    }

    public function cupomAction()
    {
        $this->view->classe = 'login';
        $this->view->headTitle('Sucesso');
    }

    public function canceladoAction()
    {
        $pedidoMapper = new Application_Model_Pedido();
        $pedidoMapper->cancela($this->_getParam('code'));
    }

    public function addCartAction()
    {
        if (!$this->auth->hasIdentity())
            $this->router->gotoRoute(array('controller' => 'account', 'action' => 'login'), 'default', true);

        if(!$this->get_numeric($this->_getParam('service')))
            $this->router->gotoRoute(array(), 'default', true);

        $servicoModel = new Application_Model_Servico();
        $servico = $servicoModel->getById($this->_getParam('service'));

        if ($servico['vendavel'])
            $this->router->gotoRoute(array('controller' => 'order', 'action' => 'index', 's' => $servico['id']), 'default', true);

        $this->router->gotoRoute(array('controller' => 'order', 'action' => 'index'), 'default');
    }

    public function delCartAction()
    {
        unset($_SESSION['Order_Site']['servicos'][$this->getParam('service')]);
        $this->router->gotoRoute(array('controller' => 'order', 'action' => 'step2'), 'default', true);
    }

    public function addCupomAction()
    {
        $order_session = new Zend_Session_Namespace('Order_Site');


        if ($this->_getParam("cupom")) {
            $cupomModel = new Application_Model_Cupom();
            $cupom = $cupomModel->getByChave($this->_getParam("cupom"));


            if ($cupom["active"]) {
                $_SESSION['Order_Site']['cupom'] = $cupom['id'];
            }
        }

        $this->router->gotoRoute(array('controller' => 'order', 'action' => 'step3'), 'default', true);
    }

    public function enviaContratoAction(){

        if(empty($this->user['email']))
            $this->addFlashMessage(array('Favor, informar seu email em:  Minha Conta >> Editar meus dados', 0), array('controller' => 'order','action' => 'step2'));

        $modelServico = new Application_Model_Servico();
        $modelEmail = new Application_Model_Emails();

        $itens = $_SESSION['Order_Site']['servicos'];

        foreach ($itens as $item){
            $servico = $modelServico->getById($item['servico']);
            if($servico['contrato_pdf']){
                $modelEmail->sendEmail(array('mail' => $this->user['email'], 'nome' => 'Cliente MAVIP'), 'SEU CONTRATO ESTÁ DISPONÍVEL', 'contrato',
                    array('servico' => $servico['termo'], 'pdf' => $servico['contrato_pdf']));
            }
        }
        $this->addFlashMessage(array('Contrato enviado com sucesso para seu email.', 1), array('controller' => 'order','action' => 'step2'));
    }

    public function get_numeric($val) {
        if (is_numeric($val)) {
            return $val + 0;
        }
        return 0;
    }
}
