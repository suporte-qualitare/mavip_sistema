<?php

/**
 *
 * @author max_holanda
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * ExibeAcao helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_ExibeCategoria {

    /**
     * @var Zend_View_Interface 
     */
    public $view;

    /**
     *  
     */
    public function exibeCategoria($nome) {
        $nome = str_replace(', ', '<br />', $nome);
        $nome = str_replace(' e ', '<br />', $nome);
        return $nome;
    }

    /**
     * coloca o separador se preciso (/)
     */

    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

}
