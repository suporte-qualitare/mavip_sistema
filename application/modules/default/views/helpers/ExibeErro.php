<?php

class Zend_View_Helper_ExibeErro {

    public function ExibeErro(array $msg = null) {
        $retorno = "";

        if ($msg) {
            foreach ($msg as $m) {

                switch ($m[1]) {
                    case 0:
                        $classe = "error";
                        break;
                    case 1:
                        $classe = "success";
                        break;
                    default:
                        $classe = "error";
                        break;
                }
                $retorno .= " <script>
                            blurt(
                                '" . $m[0] . "',
                                '',
                                '" . $classe . "'
                            );
                        </script>";
            }
        }


        return $retorno;
    }

}

?>
