<?php

/**
 *
 * @author max_holanda
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * ExibeUrlAmigavel helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_ExibeIdAmigavel {

    public $view;

    /**
     *  @param $tipo define o tipo de conteudo 1= noticia,2=agenda cultural
     */
    public function exibeIdAmigavel($conteudo, $id) {
        $array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç", "‘", "‘", "!", "@", "#", "$", "%", "¨", "&", "*", "(", ")", "-", "=", "´", "`", "[", "]", "{", "}", "~", "^", ",", "<", ".", ">", ";", ":", "/", "?", "\\", "|", "¹", "²", "³", "£", "¢", "¬", "§", "ª", "º", "°", "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç", " ", '"', "'");
        $array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C", "-", '`', "`");

        return strtolower(str_replace($array1, $array2, $conteudo)) . "-" . $id;
    }

    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

}
