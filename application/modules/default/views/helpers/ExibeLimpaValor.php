<?php
/**
 *
 * @author max_holanda
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * ExibeAcao helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_ExibeLimpaValor {
	
	/**
	 * @var Zend_View_Interface 
	 */
	public $view;
	
	/**
	 *  
	 */
	public function exibeLimpaValor($valor) {
            $valor = str_replace(',', '', $valor);
            $valor = str_replace('.', '', $valor);
            return $valor;
		
	}
	/**
	 * coloca o separador se preciso (/)
	 */

	/**
	 * Sets the view field 
	 * @param $view Zend_View_Interface
	 */
	public function setView(Zend_View_Interface $view) {
		$this->view = $view;
	}
}
