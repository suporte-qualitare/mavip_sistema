<?php

/**
 *
 * @author max_holanda
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * ExibeUrlAmigavel helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_ExibeMensagemBusca {

    public $view;

    /**
     *  @param $tipo define o tipo de conteudo 1= noticia,2=agenda cultural
     */
    public function exibeMensagemBusca($info, $marca = NULL) {

        if ($info == 0) {
            return 'não pode ser registrada!';
        } elseif ($info == 1) {
            return "Alguém anterior a você tem a preferência pela marca <span class=\"u-orangeText\">\"$marca\"</span>, você pode não conseguir o Registro.";
        } elseif ($info == 2) {
            return "Parabéns! A marca <span class=\"u-orangeText\">\"$marca\"</span> tem fortes chances de ser registrada por você!";
        } elseif ($info == 3) {
            return "Parabéns! A marca <span class=\"u-orangeText\">\"$marca\"</span> tem fortes chances de ser registrada por você!";
        } elseif ($info == -1) {
            return '"O banco de dados do INPI está indisponível no momento", faça seu cadastro e retornaremos com o resultado de sua pesquisa quando o sistema estiver disponível. ';
        } else {
            return 0;
        }
        
        
        // LOGICA ANTERIOR
//        if ($info == 0) {
//            return 'não pode ser registrada!';
//        } elseif ($info == 1) {
//            return 'exite decisão confusa, e ela pode não ser registrada';
//        } elseif ($info == 2) {
//            return 'tem grandes chances de ser registrada por você!';
//        } elseif ($info == 3) {
//            return 'tem grandes chances de ser registrada por você!';
//        } elseif ($info == -1) {
//            return '"O banco de dados do INPI está indisponível no momento", faça seu cadastro e retornaremos com o resultado de sua pesquisa quando o sistema estiver disponível. ';
//        } else {
//            return 0;
//        }
    }

    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

}
