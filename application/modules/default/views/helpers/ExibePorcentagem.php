<?php

/**
 *
 * @author max_holanda
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * ExibeUrlAmigavel helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_ExibePorcentagem {

    public $view;

    /**
     *  @param $tipo define o tipo de conteudo 1= noticia,2=agenda cultural
     */
    public function exibePorcentagem ($info) {
        
        if($info==0){
            return 0;
        }elseif($info==1){
            return 50;
        }elseif($info==2){
            return 90;
        }elseif($info==3){
            return 100;
        }else{
            return 0;
        };
        
    }

    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

}
