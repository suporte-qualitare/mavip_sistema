<?php
session_start();
if(isset($_SESSION['aluno']['id'])) {
	wp_redirect(get_bloginfo('url').'/area-restrita/'); exit;
}


require_once("gerencia/includes/config.php");
require_once("gerencia/includes/funcoes.php");
require_once("gerencia/includes/Util.php");

$fields = array(
	'nome' => 'Nome',
	'email' => 'Email',
	'telefone' => 'Telefone',
	'senha' => 'Senha',
	'area' => 'Área de Conhecimento',
	'cidade' => 'Cidade',
	'cidade_estudar' => 'Cidade onde pretende estudar',
	'tipo' => 'Tipo'
);

	if(isset($_POST['userType']) && $_POST['userType'] != ''):

		if($_POST['userType'] == 'new'){
			$validation = array();
			foreach($_POST as $field => $value){
				if($value == ''){
					$validation[] = $field;
				}else{
					${$field} = $value;
				}
			}

			if(empty($validation)){
				$dados = array();
				$dados['email'] = $email;
				$dados['senha'] = crypt($senha);
				$dados['nome'] = $nome;
				$dados['tipo'] = $tipo;
				$dados['fone'] = $telefone;
				$dados['area'] = $area;
				$dados['cidade'] = utf8_decode($cidade);
				$dados['dtnasc'] = '2020-01-01';
				$dados['tipo'] = 'Estudante';
				//create user
				$novo = new Alunos(0);
				$dados_escapado =array();
				foreach ($dados as $chave => $campo) {
					$dados_escapado[$chave] = mysql_real_escape_string($campo);
				}
				$novo->setData($dados_escapado);
				if($novo->save()) {
					Alunos::Autentica(trim($_POST['email']),$_POST['senha']);
					/*if($_SESSION['curso']['id']) {
						$idCurso  = isset($_SESSION['curso']['id']) ? $_SESSION['curso']['id'] : "";
						$tipoItem = isset($_SESSION['curso']['tipo']) ? $_SESSION['curso']['tipo']: "";
						unset($_SESSION['curso']);

						Util::showMsg("Obrigado pela confirmação dos dados",$base_url."/cadastro-cursos/?tipo=".$tipoItem."&id=".$idCurso);
						exit;
					}*/
					
					$news = new Cursos($_POST['curso']);
					$news->matricula($novo->id, true);
					
					
					Util::showMsg("Obrigado pelo cadastro. Você será redirecionado para sua área.",$base_url."/meus-cursos/");
					//var_dump("Obrigado pelo cadastro. Você será redirecionado para a área restrita.",$base_url."/area-restrita/");
					exit;
					//$msgResponse = "Dados salvos com sucesso. Efetue Login.";
				} else {
					$msgResponse =  "Erro, entre em contato com o suporte.";
				}
				//redirect to list
			}else{
				$fields_fail = array();
				foreach($validation as $row => $value){
					$fields_fail[] = $fields[$value];
				}
				$validation_message = "Os campos ". implode(', ', $fields_fail) . ' são obrigatórios';
			}

			
		}else{
			if(!Alunos::Autentica($_POST['email'],$_POST['senha'])) {
				// echo "nao logado";
				$erros['incorreto']='Senha incorreta';
				// exit;
			}else{
				// echo "logado";
				wp_redirect(get_bloginfo('url').'/meus-cursos/'); exit;
			}
			//redirect to list
		}

	endif;
?>
 <script>
	var curso = true, meuscursos = false;
</script>
<?php include('header-cadastro.php'); ?>

	<!-- Google Code for Convers&atilde;o - Landing Campanha de Matr&iacute;culas Conversion Page --><script type="text/javascript">/* <![CDATA[ */var google_conversion_id = 1021037192;var google_conversion_language = "en";var google_conversion_format = "3";var google_conversion_color = "ffffff";var google_conversion_label = "hMaFCPDcuW8QiJXv5gM";var google_remarketing_only = false;/* ]]> */</script><script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script><noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1021037192/?label=hMaFCPDcuW8QiJXv5gM&amp;guid=ON&amp;script=0"/></div></noscript>


	<script type="text/javascript">
		var ajax_url = '<?php echo admin_url("admin-ajax.php"); ?>';
	</script> 
	<script type="text/javascript">
    var ajax_url = '<?php echo admin_url("admin-ajax.php"); ?>';
</script>

<!-- End Facebook Pixel Code -->
	<div class="container">
		<div class="top">
			<img class="inscrevase" src="<?php echo get_template_directory_uri(); ?>/assets/public/imagens/inscrevase.png" alt="">
			<figure class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/public/imagens/logo.png" alt=""></figure>
			<!-- <div class="texto">
				<p><span>PÓS IDE</span><span>PROFISSIONAIS</span><span> BRILHANTES</span><span>PENSAM</span><span>ALÉM</span><span>DÚVIDAS LIGUE</span><span>0800 081 3256</span></p>
			</div> -->
			<img class="text-cadastro" src="<?php echo get_template_directory_uri(); ?>/assets/public/imagens/textobg.png" alt="" alt="">
			<form action="" method="POST" id="poside">
				<fieldset>
					 <?php
                if(!empty($validation)):
                    if(count($validation) == 1):
                        ?>
                        <p class="validaForm"> O campo <?php echo $fields[$validation[0]] ?> é obrigatório! </p>
                        <script>
                        	setTimeout(function(){
                        $(document).ready(function(){setTimeout(function(){$(".validaForm").fadeToggle()},5000)});},500);</script>

                    <?php else: ?>
                        <p class="validaForm"> <?php echo $validation_message; ?> </p>
                        <script>
                        	setTimeout(function(){
                        $(document).ready(function(){setTimeout(function(){$(".validaForm").fadeToggle()},5000)});},500);</script>

                        <?php
                    endif;
                endif;
                ?>

					<?php if(!empty($erros)): ?>
						<p style="color: white; background: #12769a;"> <?php echo $erros['incorreto']; ?> </p>
					<?php endif; ?>
					<input type="hidden" name="userType" id="userType" value="new">
					<input type="hidden" name="ready" id="ready" value="no">
					<input type="hidden" id="token_rdstation" name="token_rdstation" value="d93488b6450b3bf52fc54ad7a43bdf6a">
					<input type="hidden" id="identificador" name="identificador" value="Campanha Matricula">
					<input type="text" name="email" id="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>" placeholder="E-MAIL">
					<input type="text" name="nome" id="nome" value="<?php echo isset($_POST['nome']) ? $_POST['nome'] : ''; ?>" placeholder="NOME COMPLETO">
					<input type="tel" name="telefone" id="telefone" value="<?php echo isset($_POST['telefone']) ? $_POST['telefone'] : ''; ?>" placeholder="TELEFONE">
					<input type="text" name="cidade" id="cidade" value="<?php echo isset($_POST['cidade']) ? $_POST['cidade'] : ''; ?>" placeholder="CIDADE">
					<input type="hidden" id="area-atuacao" name="area-atuacao" value="">
					<input type="password" name="senha" placeholder="SENHA">
					<a class="bt_continue" href="javascript:void(0)" title="Continuar">CONTINUAR</a>
				</fieldset>
				<fieldset class="continue" id="continuar">
				

					<?php
						$cursos = new Cursos();
						$cidades = $cursos->getAllCidades();
					?>

				<select name="cidade_estudar" id="cidade_estudar">
					<option value="">Cidade onde Deseja Estudar</option>
					<?php foreach($cidades as $selectcidade):?>
						<?php
							if($selectcidade == ''){continue;}
						?>
						<option value="<?php echo  $selectcidade; ?>" <?php echo $cidade == $selectcidade ? 'selected="selected"' : '' ?>> <?php echo $selectcidade != 'joao pessoa' ? utf8_encode(ucfirst($selectcidade)) : 'João Pessoa'; ?></option>
					<?php endforeach; ?>
				</select>
					
				<select name="area" id="area">
					<option value="">Área de Conhecimento</option>
					<?
					$profissoes = Profissoes::getLast(999,"nome","ASC");
					$sel = "";
					foreach ($profissoes as $profissao) {
						$sel = ($dados["area"] === utf8_encode($profissao['nome'])) ? ' selected="selected"':"";
						?>
						<option value="<?php echo utf8_encode($profissao['id']);?>"<?php echo $sel; ?>><?php echo utf8_encode($profissao['nome']);?></option>
					<? } ?>
					<option value="OUTROS">OUTROS</option>
				</select>
				
				<select name="curso" id="curso">
					<option value="">Selecione o Curso</option>
					
				</select>
				</fieldset>
			</form>
			<img class="logo-bottom" src="<?php echo get_template_directory_uri(); ?>/assets/public/imagens/logo-bottom.png" alt="" alt="">
		</div>
		<div class="footer">
		</div>
	</div>

	<?php include('footer-cadastro.php'); ?>
	<script type="text/javascript">
		$('#area').change(function(){
			$('#area-atuacao').val($("#area option:selected").text());
		})
	</script>
