<?php


class Gerenciador_BancodemarcasController extends Abstract_Gerenciador_Controller_CrudController {

    public function init(){
     
        parent::init();
        $this->setTitle('Banco de marcas');
    }

    public function getRepository() {
        return new Application_Model_DbTable_BancoDeMarcas();
    }

    public function listAction(){
    
            
      $select = $this->db->select()
                ->from('banco_de_marcas as b', array('marca', 'tipo'))
                ->join('categoria as c', 'b.classe = c.id_inpi', array('c.nome as categoria'))
                ->where('marca != ?', '-')->order('marca');
              
            if($this->getParam('tipo') ){
                $select->where('b.tipo LIKE ?', "%".$this->getParam('tipo')."%");
            }

            if($this->getParam('letra') ){
                $select->where('marca LIKE  ?', $this->getParam('letra')."%");
            }
            
         $page = $this->_getParam('page', 1);
         $dados = $this->db->fetchAll($select);

         $adapter = new Zend_Paginator_Adapter_Array($dados);
         $paginate = new Zend_Paginator($adapter);
         $paginate->setCurrentPageNumber($page)
             ->setItemCountPerPage(150);
        if ($paginate)
             $this->view->assign('paginator', $paginate);
           
        
        $this->view->title = 'Banco de marcas';
        $this->view->tipo = $this->getParam('tipo') ? $this->getParam('tipo') : 'Diversos';
        $this->view->grid = $this->getColumns();
        $this->view->total = count($dados);
    }

   

    public function getColumns() {
        return
                array(
                    'marca' => array('label' => 'Marca'),
                    'b.tipo' => array('label' => 'Tipo'),
                    'categoria' => array('label' => 'Classe'),             
        );
        exit();
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Banner($isEditing);
        return $form;
    } 

}