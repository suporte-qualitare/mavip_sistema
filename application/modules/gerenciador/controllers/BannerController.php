<?php

class Gerenciador_BannerController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Banner');
        $this->setMenu('blog');
        $this->setViewRenderNew('banner/new.phtml');
        $this->setViewRenderUpdate('banner/new.phtml');
        //$this->setViewRenderList('banner/list.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Banner();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    //'titulo' => array('label' => 'titulo'),
                    'active' => array('label' => 'Ativo','type'=>'boolean'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Banner($isEditing);
        return $form;
    } 

    public function arrayFiles() {
        return array(
            array('col' => 'arquivo'),
            
            );
    }
   
}

