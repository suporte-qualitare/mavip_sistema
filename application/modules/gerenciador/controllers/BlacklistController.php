<?php

class Gerenciador_BlacklistController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Blacklist');
        $this->view->page = 'Pesquisas';
        $this->view->action = 'Adicionar/Atualizar';
        $this->setViewRenderNew('blacklist/new.phtml');
        $this->setViewRenderUpdate('blacklist/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Blacklist();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'email' => array('label' => 'E-mail'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Blacklist($isEditing);
        return $form;
    }
    
  

}

