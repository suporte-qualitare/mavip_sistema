<?php

class Gerenciador_BlogController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Blog');
        $this->setMenu('blog');
        $this->setViewRenderList('blog/list.phtml');
        $this->setViewRenderNew('blog/new.phtml');
        $this->setViewRenderUpdate('blog/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Blog();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'categoria' => array('label' => 'Categoria'),
                    'titulo' => array('label' => 'Título'),
                    'data_inicio' => array('label' => 'Data Início', 'type' => 'date','config'=>array("format"=>"dd/MM/yyyy HH:mm")),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Blog($isEditing);
        return $form;
    }
    
    public function getRecords() {
        $select = $this->db->select()
                ->from('blog', array('*'))
                ->joinLeft('categoria_blog', 'blog.categoria_blog_id = categoria_blog.id', array('categoria_blog.nome as categoria'))
                ->order('blog.id desc');
        return $this->db->fetchAll($select);
    }
    
    public function arrayFiles() {
        return array(
            array('col' => 'imagem','width'=>'922','height'=>'538','type'=>'crop'),
            
            );
    }
    public function arrayDatas() {
        return array(array('col' => 'data_inicio','type'=>'datetime'));
    }

   

}

