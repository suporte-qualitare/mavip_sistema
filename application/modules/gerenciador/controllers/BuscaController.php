<?php

class Gerenciador_BuscaController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Pesquisas');
        $this->setMenu('busca');
        $this->setViewRenderNew('categoria/new.phtml');
        $this->setViewRenderUpdate('categoria/new.phtml');
        $this->setViewRenderList('busca/list.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Busca();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'marca' => array('label' => 'Marca'),
                    'nome' => array('label' => 'Nome'),
                    'telefone' => array('label' => 'Telefone'),
                    'resultado' => array('label' => 'Resultado', 'type' => 'resultado'),
                    'email' => array('label' => 'E-mail'),
                    'estilo' => array('label' => 'Estilo'),
                    'tipo' => array('label' => 'Tipo'),
                    'categoria' => array('label' => 'Categoria'),
                    'id_inpi' => array('label' => 'Cat INPI'),
                    'ip' => array('label' => 'IP'),
                    'data_cadastro' => array('label' => 'Data / Hora', 'type' => 'date', 'config' => array('format' => 'dd/MM/yyyy HH:mm'))
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Categoria($isEditing);
        return $form;
    }

    public function getRecords() {
        $select = $this->db->select()
                ->from('busca')
                ->joinLeft('categoria', 'busca.categoria_id = categoria.id', array('categoria.nome as categoria', 'id_inpi'))
                ->limit(1);
        return $this->db->fetchAll($select);
    }

    public function deleteLoteAction() {

        try {

            foreach ($_POST["selecao"] as $chave => $valor) {
                $row = $this->getRepository()->fetchRow(array('id = ?' => $chave));
                if ($this->getDeleteFinal()) {
                    foreach ($this->arrayFiles() as $conf) {
                        $this->deleteFile($row[$conf['col']], $conf);
                    }
                    $row->delete();
                } else {
                    $this->getRepository()->update(array('active' => -1), array('id = ?' => $this->_getParam('id')));
                }
                $this->log->create($this->_usuario->id, $this->getMessages('delete', $row->toArray()));
            }

            $this->addFlashMessage(array('Conteúdo excluído com sucesso', 1), array('action' => 'list'));
        } catch (Exception $exc) {
            $this->addFlashMessage(array('Não foi possível excluir conteúdo, provavelmente exitem dependencia de outros conteúdos', 0), array('action' => 'list'));
        }
    }

    public function datatableAction() {

        $colunas = array_keys($this->getColumns());

        $ordem = $this->_getParam('order');

        $select = $this->db->select()
                ->from('busca', array('id', 'marca', 'nome', 'telefone', 'resultado', 'email', 'estilo', 'tipo', 'ip', 'data_cadastro','mavipse'))
                ->joinLeft('categoria', 'busca.categoria_id = categoria.id', array('categoria.nome as categoria', 'id_inpi'))
                ->limit($this->_getParam('length', 20), $this->_getParam('start', 0))
                ->order($colunas[$ordem[0]["column"] - 1] . ' ' . $ordem[0]["dir"]);

            if ($this->_getParam('nlogo')){
               $select->where('estilo LIKE  ?', 'texto');
     
            }
           
           if ($this->getParam('resultado')){
                if ($this->getParam('resultado') == "3"){
                    $select->where('resultado LIKE  ?', "0");
                } else {
                    $select->where('resultado LIKE  ?', "{$this->getParam('resultado')}");
                }
           }
        
        
        if ($this->_getParam('search')) {
            $search = $this->_getParam('search');
            $select->where('busca.marca like (?) OR busca.email like (?) OR categoria.nome like (?) OR categoria.nome like (?) OR id_inpi = ? OR estilo like (?) OR ip like (?)', '%' . $search["value"] . '%');
        }
        $paginas = $this->db->fetchAll($select);

        $selectTotal = $this->db->select()
                ->from('busca', 'count(busca.id) as total')
                ->joinLeft('categoria', 'busca.categoria_id = categoria.id', array('categoria.nome as categoria', 'id_inpi'));

        $total = $this->db->fetchRow($selectTotal);


        if ($this->_getParam('search')) {
            $search = $this->_getParam('search');
            $selectTotal->where('busca.marca like (?) OR busca.email like (?) OR categoria.nome like (?) OR categoria.nome like (?) OR id_inpi = ? OR estilo like (?) OR ip like (?)', '%' . $search["value"] . '%');
        }
        $totalFiltro = $this->db->fetchRow($selectTotal);

        $array = array();
        foreach ($paginas as $row) {
            $dados = array();

            $dados['excluir'] = "-";
             $dados['email'] = "emailtestador@site.com";
         
            foreach ($this->getColumns() as $cha => $col) {
                if ($cha == 'resultado') {
                    if($row[$cha]){
                        $dados[$cha] = $row[$cha];
                        switch ($dados[$cha]) {
                            case '-2':
                                $dados[$cha] = "Não concluiu a pesquisa";
                                break;
                            case '-1':
                                $dados[$cha] = "Sistema inpi OFF";
                                break;
                            case "1": 
                                $dados[$cha] = "Crítica : 52%";
                                break;
                            case "2":
                                $dados[$cha] = "Viável : 89%";
                                break;
                            case "3":
                                $dados[$cha] = '100%';
                                break;
                            default:
                                $dados[$cha] = 'Não concluiu a pesquisa';
                        }
                    }else{
                        $dados[$cha] = 'Inviável ou não pode ser registrada';
                    }

                 } else {
                    $dados[$cha] = $row[$cha];
                }
                
                if ($cha == 'data_cadastro') {
                    $dados[$cha] = date("d/m/Y H:i:s", strtotime($row[$cha]));
                }
             
            }
            
         
            
            $dados['acoes'] = $this->view->partial('busca/acoes.phtml', array('row' => $row, 'resource' => $this->view->resource, 'acl' => $this->view->acl, 'usuario' => $this->view->usuario));
            array_push($array, $dados);
             
        }

        $this->view->grid = array(
            'draw' => $this->_getParam('draw'),
            'recordsTotal' => $total['total'],
            'recordsFiltered' => $totalFiltro['total'],
            'data' => $array
        );
        echo json_encode($this->view->grid);
        exit;
    }


    public function gerarEmailsAction() {

        $data_inicio = $this->_getParam('data_inicio');
        $data_fim = $this->_getParam('data_fim');

        $select = $this->db->select()->from('busca', array('email'));

        if (!empty($data_inicio)) {
            $dt = new Zend_Date($data_inicio);
            $select->where('DATE(data_cadastro) >= DATE(?)', $dt->get('yyyy-MM-dd'));
        }
        if (!empty($data_fim)) {
            $dt = new Zend_Date($data_fim);
            $select->where('DATE(data_cadastro) <= DATE(?)', $dt->get('yyyy-MM-dd'));
        }
        $select->where('email IS NOT NULL')->order('id desc');

        $this->view->emails = $this->db->fetchAll($select);

        $this->_helper->layout->disableLayout();
    }

    public function gerarInformacoesAction() {

        $data_inicio = $this->_getParam('data_inicio');
        $data_fim = $this->_getParam('data_fim');

        $select = $this->db->select()->from('busca', array('busca.nome', 'telefone','marca', 'resultado'))
                    ->joinLeft('categoria', 'categoria.id = busca.categoria_id ', 'categoria.nome as classe');

        if (!empty($data_inicio)) {
            $dt = new Zend_Date($data_inicio);
            $select->where('DATE(busca.data_cadastro) >= DATE(?)', $dt->get('yyyy-MM-dd'));
        }
        if (!empty($data_fim)) {
            $dt = new Zend_Date($data_fim);
            $select->where('DATE(busca.data_cadastro) <= DATE(?)', $dt->get('yyyy-MM-dd'));
        }
        $select->where('busca.nome IS NOT NULL')->order('busca.id desc');

        $this->view->records = $this->db->fetchAll($select);

        $this->_helper->layout->disableLayout();
    }

    public function mavipseAction(){
        $idBusca = $this->_getParam('id');
        if($idBusca){
            $buscaModel = new Application_Model_Busca();
            $records = $buscaModel->getById($idBusca);
            if($records){
                $data = $records->toArray();
                $tbmvse =  new Application_Model_DbTable_Mavipse();
                $row = $tbmvse->createRow();
                $row->user_id = $this->_usuario->id;
                $row->marca = $data['marca'];
                $row->responsavel = $data['nome'];
                $row->telefone_1 = $data['telefone'];
                $row->email = $data['email'];
                if($row->save()){
                    $buscaModel->setMavipse($idBusca);
                    $this->addFlashMessage(array('Conversão realizada com sucesso.', 1), array('action' => 'list'));
                }


            }
        }

    }

}
