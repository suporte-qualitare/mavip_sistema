<?php

class Gerenciador_BuscaexcecaoController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Impedimentos');
        $this->view->page = 'Pesquisas';
        $this->view->action = 'Adicionar/Atualizar';
        $this->setViewRenderNew('buscaexcecao/new.phtml');
        $this->setViewRenderUpdate('buscaexcecao/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_BuscaExcecao();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'nome' => array('label' => 'Nome'),
                   
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Buscaexcecao($isEditing);
        return $form;
    }
    
  

}

