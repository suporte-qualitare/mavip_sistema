<?php

class Gerenciador_BuscatermoController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Pesquisas');
        $this->view->page = 'Exceções';
        $this->view->action = 'Adicionar/Atualizar';
        $this->setViewRenderNew('buscatermo/new.phtml');
        $this->setViewRenderUpdate('buscatermo/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_BuscaTermo();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'nome' => array('label' => 'Nome'),
                    'id_inpi' => array('label' => 'ID do INPI'),
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Buscatermo($isEditing);
        return $form;
    }
    
  

}

