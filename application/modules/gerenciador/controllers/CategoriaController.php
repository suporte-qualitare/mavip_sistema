<?php

class Gerenciador_CategoriaController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Pesquisas');
        $this->view->page = 'Categorias';
        $this->view->action = 'Adicionar/Atualizar';
        $this->setViewRenderNew('categoria/new.phtml');
        $this->setViewRenderUpdate('categoria/new.phtml');
        $this->setViewRenderList('categoria/list.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Categoria();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'nome' => array('label' => 'Nome'),
                    'tipo' => array('label' => 'Tipo'),
                    'id_inpi' => array('label' => 'ID do INPI'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Categoria($isEditing);
        return $form;
    }
    
  

}

