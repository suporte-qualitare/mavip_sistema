<?php

class Gerenciador_CategoriablogController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Categorias Blog');
        $this->setMenu('blog');
        $this->setViewRenderNew('categoriablog/new.phtml');
        $this->setViewRenderUpdate('categoriablog/new.phtml');
        $this->setViewRenderList('categoriablog/list.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Categoriablog();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'nome' => array('label' => 'Nome'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Categoriablog($isEditing);
        return $form;
    }

}
