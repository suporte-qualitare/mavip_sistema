<?php

class Gerenciador_ClienteController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Clientes');
        $this->setMenu('clientes');
        $this->setViewRenderView('cliente/view.phtml');
        $this->setViewRenderUpdate('cliente/new.phtml');
        $this->setViewRenderList('cliente/list.phtml');
        date_default_timezone_set('America/Recife');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Cliente();
    }

    public function getRecords() {

        if($this->_getParam('categoria')  === 'com-processo'){
            $select = $this->db->select()
                ->from('pedido_has_marca', array('status'))
                ->join('pedido','pedido_has_marca.pedido_id = pedido.id',array(''))
                ->join('cliente','pedido.cliente_id = cliente.id',array('*'))
                ->group('cliente.id')
                ->where('pedido_has_marca.status NOT IN (?)', array('cancelado', 'aguardando pagamento', 'novo'))
                ->order('cliente.data_cadastro desc');
        }
        else
        {
            $select = $this->db->select()
                ->from('cliente')->order('id desc');

            if($this->_getParam('categoria') === 'cnpj')
                $select->where("cnpj > ''");

            if($this->_getParam('categoria') === 'sem-cnpj')
                $select->where("cnpj = '' or cnpj is null");
        }

        $this->view->categoria = $this->_getParam('categoria') ?: '';
        $result = $this->db->fetchAll($select);
        $this->view->total = count($result);

        return  $result;
    }

    public function getColumns() {
        return
            array(
                'id' => array('label' => '#'),
                'razao_social' => array('label' => 'Nome'),
                'nome' => array('label' => 'Responsável'),
                'login' => array('label' => 'Login'),
                'email' => array('label' => 'E-mail'),
                'telefone' => array('label' => 'Telefone'),
                'celular' => array('label' => 'Celular'),
                'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date', 'config'=>array("format"=>"dd/MM/yyyy HH:mm")),
                'cnpj' => array('label' => 'CNPJ', 'type' => 'number'),
            );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Cliente($isEditing);
        return $form;
    }

    public function getFormAgente($isEditing = 0) {
        $form = new Application_Form_Gerenciador_ClienteAgente($isEditing);
        return $form;
    }

    public function getFormObservacoes($isEditing = 0) {
        $form = new Application_Form_Gerenciador_ClienteObservacoes($isEditing);
        return $form;
    }

    public function viewAction() {
        $select = $this->db->select()
            ->from('cliente')
            ->joinLeft('pais','cliente.pais_id = pais.id',array('pais.nome as pais'))
            ->joinLeft('cidade','cliente.cidade_id = cidade.id',array('cidade.nome as cidade'))
            ->joinLeft('estado','cidade.estado_id = estado.id',array('estado.nome as estado','estado.sigla as sigla'))
            ->joinLeft('cliente_natureza','cliente.cliente_natureza_id = cliente_natureza.id',array('cliente_natureza.nome as natureza'))
            ->where('cliente.id = ?', $this->_getParam('id'));

        $this->view->row = $this->db->fetchRow($select);

        $pedidosModel = new Application_Model_Pedido();
        $this->view->pedidos = $pedidosModel->getAllByCliente($this->_getParam('id'));

        $marcasModel = new Application_Model_Marca();
        $this->view->marcas = $marcasModel->getAllByCliente($this->_getParam('id'));

        $this->view->title = $this->getTitle();
        $this->view->action = "Visualizar";

        $modelLead = new Application_Model_ClienteLead();

        $this->view->leadFields = $modelLead->getLeadFields();

        $recordsLead = $modelLead->getLeadOrigensByCliente($this->_getParam('id'));
        if($recordsLead){
            foreach ($recordsLead as $row){
                $records[] =  $row['valor'];
            }
        }
        $this->view->leadOrigens = $records;

        $modelClienteAgendamentos = new Application_Model_ClienteAgendamentos();
        $this->view->agendamentos = $modelClienteAgendamentos->getById($this->_getParam('id'));

        // mavipse
        $dbtb = new Application_Model_DbTable_Mavipse();
        $rowM = $dbtb->fetchRow(array("cliente_id = ?" => $this->_getParam('id')));
        if($rowM){
            $this->view->mavipse = $rowM->toArray();
        }

        if ($this->view->row['agente_id']) {
            $users_model = new Application_Model_Users();
            $this->view->user = $users_model->getRow($this->view->row['agente_id']);
        }
            
        $this->view->mavipseRows = $dbtb->fetchAll()->toArray();

        $select2 = $this->db->select()
                ->from('cliente')
                ->where('active = ?', 1)
                ->order('nome', 'asc');

        $this->view->clientes = $this->db->fetchAll($select2);


        $this->renderScript($this->getViewRenderView());
    }

    protected function preparingData($postDate, $isEditing = 0, $row = null) {
        if (!$postDate["senha"]) {
            unset($postDate["senha"]);
        } else {
            $postDate["salt"] = SHA1(date('Ymdhisu') . mt_rand(1000, 9999));
            $postDate["senha"] = SHA1($postDate["senha"] . $postDate["salt"]);
        }

        return $postDate;
    }

    public function gerargruAction()
    {
        $select = $this->db->select()
            ->from('cliente')
            ->where('cliente.id = ?', $this->_getParam('id'));
        $this->view->row = $this->db->fetchRow($select);
    }

    public function gerarpeticionamentoAction()
    {
        $select = $this->db->select()
            ->from('cliente')
            ->where('cliente.id = ?', $this->_getParam('id'));
        $this->view->row = $this->db->fetchRow($select);
    }

    public function updateAction() {

        if($this->_getParam('p'))
            $form = $this->getFormObservacoes($this->_getParam('id'));
        else
            $form = $this->getForm($this->_getParam('id'));

        $rep = $this->getRepository();
        $row = $rep->fetchRow(array('id = ?' => $this->_getParam('id')));
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                $postData = $this->preparingData($form->getValues(), 1, $row);
                $row->setFromArray($postData);
                $this->beforeSaving($row, $form->getValues(), 1);
                $row->save();
                $this->afterSaving($row, $form->getValues(), 1);
                $this->log->create($this->_usuario->id, $this->getMessages('update', $row->toArray()));
                if($_POST['observacoes_internas'])
                    $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'view', 'id' => $this->_getParam('id')));

                $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'list'));
            }
        } else {
            $form->populate($this->fillsEdit($row->toArray()));
        }
        $this->view->form = $form;
        $this->view->title = $this->getTitle();
        $this->view->row = $row;

        $this->renderScript($this->getViewRenderUpdate());
    }

    public function agenteAction () {
        $id = $this->_getParam('id');
        $form = $this->getFormAgente($id);

        $rep = $this->getRepository();
        $row = $rep->fetchRow(array('id = ?' => $this->_getParam('id')));

        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                $postData = $this->preparingData($form->getValues(), 1, $row);
                $row->setFromArray($postData);
                $this->beforeSaving($row, $form->getValues(), 1);
                $row->save();
                $this->afterSaving($row, $form->getValues(), 1);
                $this->log->create($this->_usuario->id, $this->getMessages('update', $row->toArray()));
                // if($_POST['observacoes_internas'])
                //     $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'view', 'id' => $this->_getParam('id')));

                $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'view', 'id' => $id));
            }
        } else {
            $form->populate($this->fillsEdit($row->toArray()));
        }

        $this->view->form = $form;
        $this->view->title = 'Editar Agente';
        
    }

    public function getFormAgendamentos($isEditing = 0) {
        $form = new Application_Form_Gerenciador_ClienteAgendamentos($isEditing);
        return $form;
    }

    public function getFormLeadFields($isEditing = 0) {
        $form = new Application_Form_Gerenciador_ClienteLeadFields($isEditing);
        return $form;
    }

    public function newLeadFieldAction()
    {
        if(!empty($_POST['valor']))
        {
            $modelLead = new Application_Model_ClienteLead();
            if($modelLead->createLeadField($_POST['valor'])){
                $this->addFlashMessage(array('Lead adicionada com sucesso', 1), array('action' => 'view', 'id' => $_POST['cliente_id']));
            }
            $this->addFlashMessage(array('Opção já existente. Tente novamente com outro valor', 0), array('action' => 'new-lead-field', 'id' => $_POST['cliente_id']));
        }

        $this->view->form  = $this->getFormLeadFields();
        $rep = new Application_Model_DbTable_ClienteLeadFields();
        $this->view->cliente = $this->_getParam('id');
    }


    public function leadorigensAction()
    {
        if(!empty($_POST['cliente_id']))
        {
            $modelLead = new Application_Model_ClienteLead();
            if($modelLead->updateLead($_POST));
            $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'view', 'id' => $_POST['cliente_id']));
        }

        $this->addFlashMessage(array('Erro ao salvar', 0), array('action' => 'list'));
    }

    public function newAgendamentoAction() {

        if($this->_request->isPost()) {
            if($this->_getParam('cliente_id')){
                $modelAgendamento = new Application_Model_ClienteAgendamentos();
                $success = $modelAgendamento->create($this->getAllParams());
                if($success){
                    $this->notificationSendAgendamento($this->getAllParams());
                    $this->addFlashMessage(array('Agendamento criado com sucesso', 1), array('action' => 'view', 'id' => $this->_getParam('cliente_id')));
                }

            }
        }
        $this->view->title =  "Agendamento";
        $this->view->form  = $this->getFormAgendamentos();
        $rep = new Application_Model_DbTable_Cliente();
        $this->view->row = $rep->fetchRow(array('id = ?' => $this->_getParam('id')));
    }

    public function updateAgendamentoAction() {

        $form  = $this->getFormAgendamentos($this->_getParam('id'));
        $rep = new Application_Model_DbTable_ClienteAgendamentos();
        $row = $rep->fetchRow(array('id = ?' => $this->_getParam('id')));
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                $postData = $this->preparingData($form->getValues(), 1, $row);
                $row->setFromArray($postData);
                if($row->toArray()['data']){
                    $data =  new Zend_Date($row->toArray()['data']);
                    $row->data = $data->toString('YYYY-MM-dd HH:mm');
                }
                $row->save();
                $this->log->create($this->_usuario->id, $this->getMessages('update', $row->toArray()));
                $this->addFlashMessage(array('Agendamento editado com sucesso', 1), array('action' => 'view', 'id' => $row->toArray()['cliente_id']));
            }
        } else {
            if($row->toArray()['data']){
                $data =  new Zend_Date($row->toArray()['data']);
                $row->data = $data->toString('dd/MM/YYYY HH:mm');
            }
            $form->populate($this->fillsEdit($row->toArray()));
        }

        $this->view->form = $form;
        $this->view->row = $row;
    }

    public function  exportLeadsAction() {

        $modelLeads = new Application_Model_ClienteLead();
        $records = $modelLeads->getAll();

        if(!count($records))
            return;

        foreach($records as $row){
            $dados[] = array(
                "Nome"                   =>        utf8_decode($row['nome']),
                "Data de Cadastro"       =>        $row['data_cadastro'] ? date_format (new DateTime($row['data_cadastro']), 'd-m-Y') : '',
                "Origem(s)"               =>       utf8_decode($row['valor']),
            );
        }

        $filename = "relatorio-de-leads-".date('d-m-y');
        header("Pragma: no-cache");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename.csv");
        header ("Content-Description: Relatorio de leads Mavip" );
        header('Content-Transfer-Encoding: binary');
        $output = fopen("php://output", "w");
        $header = array_keys($dados[0]);
        fputcsv($output, $header);

        foreach($dados as $row){
            fputcsv($output, $row);
        }
        fclose($output);
        exit;
    }


    public function notificationSendAgendamento($param) {

        if(!$param['sendClient'])
            return false;

        $modelUsers = new Application_Model_Users();
        $modelCliente = new Application_Model_Cliente();

        $user = $modelUsers->getByName($param['consultor']);
        $cliente = $modelCliente->getById($param[ 'cliente_id']);
        if($user && $cliente &&  $param['data']){
            $data =  new Zend_Date($param['data']);
            $param['data'] = $data->toString('dd/MM/YYYY HH:mm');
            $param['nome'] = $cliente['nome'];

            $modelEmail = new Application_Model_Emails();

            if($param['sendClient']){
                $clienteEmail = array('mail' => $cliente['email'], 'nome'=> $cliente['nome']);
                $modelEmail->sendEmail($clienteEmail, "MAVIP - Novo agendamento de {$cliente['nome']}", 'agendamento-cliente', $param);
            }
            $userEmail = array('mail' => $user['email'], 'nome'=> $user['name']);
            $modelEmail->sendEmail($userEmail, "MAVIP - Novo agendamento: {$cliente['nome']}", 'agendamento-mavip', $param);
        }
    }

    public function espelharProcessoAction()
    {

        $buscaEspelhamento = isset($_POST['procurar']) ?  $_POST['procurar'] : false;

        if($buscaEspelhamento)
        {
            $select = $this->db->select()
            ->from('clientes_espelhamentosdeprocessos', array())
            ->joinLeft('cliente','clientes_espelhamentosdeprocessos.cliente_id = cliente.id', array('cliente.id as cliente'))
            ->where('clientes_espelhamentosdeprocessos.marca_id = ?', $buscaEspelhamento);
            $result = $this->db->fetchCol($select);
            echo json_encode($result);
            die;
        }

        $clientes = $_POST['cliente_id'];

        $marca = $_POST['marca_id'];

        $db = new Application_Model_DbTable_ClientesEspelhamentosdeprocessos();

         $db->delete(array(
                'marca_id = ?' => $marca
        ));
        
        if($clientes && $marca)
        {
            foreach($clientes as $cliente)
            {
                $row = $db->createRow();
                $row->marca_id = $marca;
                $row->cliente_id = $cliente;
                $row->data_cadastro = date('Y-m-d h:i:s');
                $row->save();
            }
            echo json_encode(1);
            die;

        }
        echo json_encode(2);
        die;
    }

}

