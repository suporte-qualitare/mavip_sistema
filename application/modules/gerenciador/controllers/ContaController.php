<?php

class Gerenciador_ContaController extends Abstract_Gerenciador_Controller_AbstractController {

    public function loginAction() {
        $this->_helper->layout->setLayout('gerenciador-hide');

        $form = new Application_Form_Gerenciador_Login();
        if ($this->_request->isPost()) {

            if ($form->isValid($_POST)) {

                $adapter = new Zend_Auth_Adapter_DbTable(
                        $this->db, 'users', 'login', 'password', 'SHA1(CONCAT(?,salt))'
                );

                $adapter->setIdentity($form->getValue('login'))->setCredential($form->getValue('senha'));

                $result = $this->auth->authenticate($adapter);

                if ($result->isValid()) {

                    $usuario = $adapter->getResultRowObject();
                    $this->auth->getStorage()->write($usuario);
                    //$this->log->create($usuario->id, 'Entrou no sistema');
                    $this->router->gotoRoute(array(), 'gerenciador', true);
                }
            } else {
                exit('erro');
            }
            $this->addFlashMessage(array('Login ou senha inválida', 0), array('action' => 'login'));
        }
    }

    public function logoutAction() {

        $this->log->create($this->_usuario->id, 'Saiu do sistema');

        $this->auth->clearIdentity();
        $this->addFlashMessage(array('Logout realizado com sucesso', 1));
        $this->router->gotoRoute(array('controller' => 'conta', 'action' => 'login'), 'gerenciador', true);
    }

    public function cidadesAction() {

        header('Cache-Control: no-cache');
        header('Content-type: application/json; charset="utf-8"', true);

        $cidadesModel = new Application_Model_Cidades();
        $cids = $cidadesModel->getAllToSelect($this->_getParam('estado', '15'));
        $cidades = array();
        foreach ($cids as $chave => $valor) {
            $cidades[] = array(
                'id' => $chave,
                'nome' => $valor,
            );
        }

        print json_encode($cidades);
        exit;
    }

}
