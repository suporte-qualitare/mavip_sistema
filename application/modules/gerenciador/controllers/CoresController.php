<?php

class Gerenciador_CoresController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Cor');
        $this->setMenu('cores');
        $this->setViewRenderNew('cores/new.phtml');
        $this->setViewRenderUpdate('cores/new.phtml');
    
    }

    public function getRepository() {
        return new Application_Model_DbTable_Cores();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'nome' => array('label' => 'Título'),
                    'hexadecimal' => array('label' => 'Cor', 'type' => 'color')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Cores($isEditing);
        return $form;
    }
    
   
}
