<?php

class Gerenciador_CupomController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Cupom');
        $this->view->page = 'Adicionar/Atualizar';
        $this->setViewRenderNew('cupom/new.phtml');
        $this->setViewRenderUpdate('cupom/new.phtml');
        $this->setViewRenderList('cupom/list.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Cupom();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'nome' => array('label' => 'Nome'),
                    'chave' => array('label' => 'Chave'),
                    'desconto' => array('label' => 'Desconto'),
                    'tipo' => array('label' => 'Tipo'),
                    'active' => array('label' => 'Ativo','type'=>'boolean'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Cupom($isEditing);
        return $form;
    }
    
    public function newmultiAction() {

        $form = new Application_Form_Gerenciador_CupomMulti(false);

        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                try {
                    $this->db->beginTransaction();
                    $postData = $this->preparingData($form->getValues(), 0, $row);
                    
                    for($i = 1;$i<=$postData['quantidade'];$i++){
                        $row = $this->getRepository()->createRow($postData);
                        $row['nome'] = $postData['nome'].' - '.$i;
                        $this->beforeSaving($row, $form->getValues(), 0);
                        $row->save();
                        $this->afterSaving($row, $form->getValues(), 0);
                    }

                    $this->db->commit();
                    $this->log->create($this->_usuario->id, $this->getMessages('new', $row->toArray()));
                    $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'list'));
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                    exit;
                    $this->db->rollBack();
                    $this->addFlashMessage(array('Erro ao salvar conteúdo', 0));
                    $this->router->gotoRoute(array());
                }
            }
        }
        $this->view->form = $form;
        $this->view->title = $this->getTitle();
        
    }
    
    protected function afterSaving($row, $formValues, $isEditing = 0) {
        if(!$row["chave"]){
            $row["chave"] = md5('mmavip'.$row["id"]);
            $row->save();
        }
        return $row;
    }
    
    
    
   
}

