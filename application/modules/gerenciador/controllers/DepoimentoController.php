<?php

class Gerenciador_DepoimentoController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Depoimento');
        $this->setMenu('blog');  
        $this->setViewRenderNew('depoimento/new.phtml');
        $this->setViewRenderUpdate('depoimento/new.phtml');
    }

    public function getRepository() {        
        return new Application_Model_DbTable_Depoimento();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'nome' => array('label' => 'Nome'),
                    'depoimento' => array('label' => 'Depoimento'),
                    'data_cadastro' => array('label' => 'Data de Cadastro', 'type' => 'date', 'config' => array("format" => "dd/MM/yyyy HH:mm:ss"))                    
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Depoimento($isEditing);
        return $form;
    }

    public function getRecords() {        
        $select = $this->db->select()
                ->from('depoimentos', array('*'));                
        return $this->db->fetchAll($select);
    }

    public function arrayFiles() {
        return array(
            array('col' => 'imagem', 'width' => '200', 'height' => '225', 'type' => 'crop'),
        );
    }

    public function arrayDatas() {
        return array(array('col' => 'data_cadastro', 'type' => 'datetime'));
    }

}
