<?php

class Gerenciador_EmpresaController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Empresas Encontradas');
        $this->setMenu('leitura');
        $this->setViewRenderList('empresa/view.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Empresa();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'processo' => array('label' => 'Processo'),
                    'ipas' => array('label' => 'IPAS'),
                    'nome' => array('label' => 'Nome'),
                    'cnpj' => array('label' => 'CNPJ'),
                    'email' => array('label' => 'Email')
        );
    }

    public function getForm($isEditing = 0) {
        //$form = new Application_Form_Gerenciador_Empresa($isEditing);
        //return $form;
    }

    public function exportarAction() {

        $name = strtoupper($this->_getParam('id')) . '__' . date("d-m-Y__H:i:s");
        $quebra = chr(13) . chr(10);

        $caminho = PATH_ARQS.'empresas';
        if (!file_exists($caminho)) {
            mkdir($caminho, 0755, true);
        }
        
        $handle = fopen("$caminho/$name.txt", "w");

        $empresaModel = new Application_Model_Empresa();
        $res = $empresaModel->getAllIPAS($this->_getParam('id'));

        foreach ($res as $value) {
            if ($value['nome'] != '' || $value['nome'] != NULL) {
                fwrite($handle, $value['nome'] . $quebra);
            }
        }

        fclose($handle);

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename("$caminho/$name.txt"));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize("$caminho/$name.txt"));
        readfile("$caminho/$name.txt");
        exit;
    }

}
