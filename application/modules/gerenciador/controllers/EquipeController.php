<?php

class Gerenciador_EquipeController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Equipe');
        $this->setMenu('equipe');
        $this->setViewRenderNew('equipe/new.phtml');
        $this->setViewRenderUpdate('equipe/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Equipe();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'nome' => array('label' => 'Nome'),
                    'descricao' => array('label' => 'Resumo'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Equipe($isEditing);
        return $form;
    }
    
    public function arrayFiles() {
        return array(
            array('col' => 'imagem','width'=>'310','height'=>'350','type'=>'crop'),
            
            );
    }

    

}
