<?php

class Gerenciador_FaqController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('FAQ');
        $this->setDeleteFinal(0);
        $this->setMenu('blog');
        //$this->setViewRenderList('remessa/list.phtml');
        $this->setViewRenderNew('faq/new.phtml');
        $this->setViewRenderUpdate('faq/new.phtml');
        //$this->setViewRenderView('remessa/view.phtml');
    
    }

    public function getRepository() {
        return new Application_Model_DbTable_Faq();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'titulo' => array('label' => 'Título'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        
        $form = new Application_Form_Gerenciador_Faq($isEditing);
        return $form;
    }
    
   
}
