<?php

class Gerenciador_ItensController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Características dos planos');
        $this->setMenu('Características');
        $this->setViewRenderNew('itens/new.phtml');
        $this->setViewRenderUpdate('itens/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Itens();
    }

    public function getColumns() {
        return
                array(
                    'ordem' => array('label' => 'Ordem'),
                    'descricao' => array('label' => 'Característica'),
                    'destaque' => array('label' => 'Destaque', 'type' => 'boolean'),
                    'novidade' => array('label' => 'Novo', 'type' => 'boolean')
        );
    }

    public function getForm($isEditing = 0) {
        
        $form = new Application_Form_Gerenciador_Itens($isEditing);
        return $form;
    }
    
   
}
