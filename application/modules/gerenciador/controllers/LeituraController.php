<?php

class Gerenciador_LeituraController extends Abstract_Gerenciador_Controller_CrudController
{

    public function init()
    {
        parent::init();
        $this->setTitle('Leitura');
        $this->setMenu('leitura');
        $this->setViewRenderNew('leitura/new.phtml');
        $this->setViewRenderView('leitura/view.phtml');
        $this->setViewRenderList('leitura/list.phtml');
    }

    public function getRepository()
    {
        return new Application_Model_DbTable_Leitura();
    }

    public function getColumns()
    {
        return
            array(
                'id' => array('label' => '#'),
                'data' => array('label' => 'Data Arquivo', 'type' => 'date', 'config' => array('format' => 'dd/MM/yyyy')),
                'quant' => array('label' => 'Itens da Leitura', 'size' => '3%'),
                'data_cadastro' => array('label' => 'Data de Cadastro', 'type' => 'date', 'config' => array('format' => 'dd/MM/yyyy HH:mm:ss'))
            );
    }

    public function getForm($isEditing = 0)
    {
        $form = new Application_Form_Gerenciador_Leitura($isEditing);
        return $form;
    }

    public function getRecords()
    {
        $select = $this->db->select()->from("leitura")
            ->joinLeft("marca_historico", "leitura.id = marca_historico.leitura_id", "count(marca_historico.id) as quant")
            ->group("leitura.id");

        return $this->db->fetchAll($select);
    }

    public function viewAction()
    {
        $this->view->row = $this->getRepository()->fetchRow(array('id = ?' => $this->_getParam('id')));

        $select = $this->db->select()->from("marca_historico")
            ->join("marca", "marca_historico.marca_id = marca.id", "marca.marca")
            ->joinLeft("marca_historico_mensagem", "marca_historico_mensagem.codigo_despacho = marca_historico.codigo_despacho")
            ->join("cliente", "cliente.id = marca.cliente_id", array("cliente.id as id_cliente", "cliente.nome as cliente_nome"))
            ->where("leitura_id = ?", $this->getParam("id"))
            ->order("marca_historico.id");

        $this->view->historico = $this->db->fetchAll($select);
        $this->view->title = $this->getTitle();
        $this->renderScript($this->getViewRenderView());
    }

    /**
     * @throws Zend_Form_Exception
     */
    public function newAction()
    {

        $form = $this->getForm();

        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                try {
                 //   header("Content-Type: application/xml; charset=utf-8");

                    $this->db->beginTransaction();

                    $postData = $this->preparingData($form->getValues(), 0);

                    $fileName = PATH_ARQS . 'leitura/' . $postData['arquivo'];

                    $xml = simplexml_load_file($fileName);

                    $atributos = $xml->attributes();

                    $id = $atributos['numero'];
                    $data = $atributos['data'];

                    $leiturarDb = $this->getRepository();

                    $lei = $leiturarDb->fetchRow(array('id = ?' => $id));

                    if ($lei) {
                        $this->addFlashMessage(array('Não foi possível fazer a leitura desse arquivo, pois o mesmo ja consta no sistema.', 0));
                        $this->router->gotoRoute(array());
                    }

                    $data = new Zend_Date($data, 'dd/MM/yyyy');
                    $postData["data"] = $data->get('yyyy-MM-dd');
                    $postData["users_id"] = $this->_usuario->id;
                    $postData["id"] = $id;


                    $row = $this->getRepository()->createRow($postData);
                    $this->beforeSaving($row, $form->getValues(), 0);
                    $row->save();

                    $i = 1;

                    $marcaDb = new Application_Model_DbTable_Marca();
                    $marcaHistoricoDb = new Application_Model_DbTable_MarcaHistorico();
                    $clienteModel = new Application_Model_Cliente();
                    $marcaModel = new Application_Model_Marca();
                    $empresaDb = new Application_Model_DbTable_Empresa();
                    $bancodemarcasDb = new Application_Model_DbTable_BancoDeMarcas();

                    $buscaModel = new Application_Model_Busca();

                    foreach ($xml as $processo) {

                            $atrProcesso = $processo->attributes();
                            $despacho = $processo->despachos->despacho;
                            $atrDespacho = $despacho->attributes();
                            $atrTitular = $processo->titulares->titular;
                          
                            // inserir dados na tabela Alertas
                            if ($despacho['codigo'] == 'IPAS029' ||
                                $despacho['codigo'] == 'IPAS404' ||
                                $despacho['codigo'] == 'IPAS161' ||
                                $despacho['codigo'] == 'IPAS139' ||
                                $despacho['codigo'] == 'IPAS157' ||
                                $despacho['codigo'] == 'IPAS161' ||
                                $despacho['codigo'] == 'IPAS024' ||
                                $despacho['codigo'] == 'IPAS161' ||
                                $despacho['codigo'] == 'IPAS423' ||
                                $despacho['codigo'] == 'IPAS359' ||
                                $despacho['codigo'] == 'IPAS005' ||
                                $despacho['codigo'] == 'IPAS304' ||
                                $despacho['codigo'] == 'IPAS414' ||
                                $despacho['codigo'] == 'IPAS033' ||
                                $despacho['codigo'] == 'IPAS047' ||
                                $despacho['codigo'] == 'IPAS091' ||
                                $despacho['codigo'] == 'IPAS112' ||
                                $despacho['codigo'] == 'IPAS113' ||
                                $despacho['codigo'] == 'IPAS009' ||
                                $despacho['codigo'] == 'IPAS135' ||
                                $despacho['codigo'] == 'IPAS421' ||
                                $despacho['codigo'] == 'IPAS142' 
                                ){
                                $alertasModel = new Application_Model_Alertas();
                                $dados = array('ipas' => $atrDespacho['codigo'],
                                               'razao' => $atrTitular['nome-razao-social'],
                                               'processo' => $atrProcesso['numero']);
                                $alertasModel->insert($dados);
                            }


                            // SE FOR CONCESSAO DE REGISTRO
                            if($despacho['codigo'] == 'IPAS158' && $atrProcesso['numero']) {
                                $rowMarca = $marcaDb->fetchRow(array('processo = ?' => $atrProcesso['numero'] , 'active = ?' => 1), 'id desc');

                                if($rowMarca){
                                    $rowMarca->publicacao_inpi = date('Y-m-d');
                                    $rowMarca->save();
                                }
                            }

                            /*
                            // INSERE  NA TABELA DE BANCO DE MARCAS
                            if ($despacho['codigo'] == 'IPAS106' ||
                                $despacho['codigo'] == 'IPAS161') {

                                // CONSULTA SE E SEM APOSTILHA
                                if ($buscaModel->semApostilhaProcesso((string)$atrProcesso['numero'])) {

                                    $bancodemarcas = $bancodemarcasDb->fetchRow(array('processo = ?' => $atrProcesso['numero']));

                                    if (!$bancodemarcas) {
                                        $marca = $buscaModel->getNomeEmpresa((string)$atrProcesso['numero']);
                                        $classe = $buscaModel->getClasse((string)$atrProcesso['numero']);
                                        $natureza = $buscaModel->getNatureza((string)$atrProcesso['numero']);

                                       $bdmarcas = $bancodemarcasDb->createRow();

                                        $bdmarcas->processo = (int)$atrProcesso['numero'];
                                        $bdmarcas->marca = $marca;
                                        $bdmarcas->classe = (int)$classe;
                                        $bdmarcas->tipo = $natureza;
                                        $bdmarcas->save();
                                    }
                                }
                            }
*/
                            // INSERE DETERMINADOS PROCESSOS NA TABELA DE EMPRESA
                            if ($despacho['codigo'] == 'IPAS157' ||
                                $despacho['codigo'] == 'IPAS161' ||
                                $despacho['codigo'] == 'IPAS414') {

                                $empresa = $empresaDb->fetchRow(array('processo = ?' => $atrProcesso['numero']));
                                if (!$empresa) {
                                    $emp = $empresaDb->createRow();
                                    $emp->processo = $atrProcesso['numero'];
                                    $emp->ipas = $despacho['codigo'];
                                    $emp->save();
                                }
                            }

                            set_time_limit(360);
                            //$marca = $marcaDb->fetchRow(array('processo = ?' => $atrProcesso['numero']));

                            $marcas = $marcaDb->fetchAll(array('processo = ?' => $atrProcesso['numero']));

                            foreach ($marcas as $marca) {

                                $marcaHistorico = $marcaHistoricoDb->createRow();
                                $marcaHistorico->leitura_id = $row['id'];
                                $marcaHistorico->marca_id = $marca['id'];
                                $marcaHistorico->codigo_despacho = $atrDespacho['codigo'];
                                $marcaHistorico->texto = $atrDespacho['nome'];
                                $marcaHistorico->texto_complementar = $despacho->{"texto-complementar"};
                                $marcaHistorico->processo = $atrProcesso['numero'];
                                $marcaHistorico->save();

                                $marca->atual = 1;
                                $marca->save();

                                $cliente = $clienteModel->getById($marca['cliente_id']);
                                $mensagemHitorico = $marcaModel->getHistoricoMensagem($atrDespacho['codigo']);

                                if ($mensagemHitorico["id"]) {
                                    $titulo = $mensagemHitorico["titulo"];
                                    $mensagem = $mensagemHitorico["mensagem"];
                                } else {
                                    $titulo = $atrDespacho['nome'];
                                    $mensagem = $despacho->{"texto-complementar"};
                                }

                                $email = new Application_Model_Emails();

                                if ($marca["acompanhamento"]) {
                                    $return = $email->sendEmail(array('mail' => $cliente['email'], 'nome' => $cliente['nome']), "MAVIP - Atualização de marca", "atualizacao-marca", array('nome' => $cliente['nome'], 'marca' => $marca['marca'] . " (" . $marca['id'] . ")", 'processo' => $atrProcesso['numero'], 'historico' => $titulo, 'mensagem' => $mensagem));
                                }
                                //  Exigência de Mérito
                                if ($atrDespacho['codigo'] == 'IPAS136'){
                                    $email->sendEmail(array('mail' => $cliente['email'] , 'nome' => $cliente['nome']), "MAVIP - Despacho:  Exigência de Mérito", "atualizacao-marca-admin", array('nome' => $cliente['nome'], 'historico' => $titulo, 'marca' => $marca['marca'] . " (" . $marca['id'] . ")", 'processo' => $atrProcesso['numero'], 'mensagem' => 'Entre no site e contrate o serviço Cumprimento de Exigência.'));
                                }

                                $return = $email->sendEmail(array('mail' => 'contato@mavip.com.br', 'nome' => "Admin MAVIP"), "MAVIP - Atualização de marca", "atualizacao-marca-admin", array('nome' => $cliente['nome'], 'historico' => $titulo, 'marca' => $marca['marca'] . " (" . $marca['id'] . ")", 'processo' => $atrProcesso['numero'], 'mensagem' => $mensagem));
                            }

                    }

                    $this->afterSaving($row, $form->getValues(), 0);
                    $this->db->commit();
                    $this->log->create($this->_usuario->id, $this->getMessages('new', $row->toArray()));
                    $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'list'));
                } catch (Exception $ex) {

                    echo $ex;
                    exit('erro');
                    $this->db->rollBack();
                    $this->addFlashMessage(array('Erro ao salvar conteúdo', 0));
                    $this->router->gotoRoute(array());
                }
            }
        }

        $this->view->form = $form;
        $this->view->title = $this->getTitle();
        $this->renderScript($this->getViewRenderNew());
    }

    function utf8_for_xml($string)
    {
        return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
    }

}
