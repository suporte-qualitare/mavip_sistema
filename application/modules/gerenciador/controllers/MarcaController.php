<?php

class Gerenciador_MarcaController extends Abstract_Gerenciador_Controller_CrudController
{

    public function init()
    {
        parent::init();
        $this->setTitle('Marcas');
        $this->setMenu('clientes');
        $this->setViewRenderList('marca/list.phtml');
        $this->setViewRenderView('marca/view.phtml');
        $this->setViewRenderUpdate('marca/update.phtml');
        $this->setViewRenderNew('marca/new.phtml');
    }

    public function updateAction()
    {
        $form = $this->getForm($this->_getParam('id'));
        $rep = $this->getRepository();
        $row = $rep->fetchRow(array('id = ?' => $this->_getParam('id')));
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                $postData = $this->preparingData($form->getValues(), 1, $row);
                $row->setFromArray($postData);
                $this->beforeSaving($row, $form->getValues(), 1);
                $row->save();
                $this->afterSaving($row, $form->getValues(), 1);
                $this->log->create($this->_usuario->id, $this->getMessages('update', $row->toArray()));
                if ($this->_getParam('op')) {
                    $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('controller' => 'operacao', 'action' => 'concluir', 'id' => $this->_getParam('op'), 'marca' => $this->_getParam('id')));
                }

                $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'view', 'id' => $this->_getParam('id')));
            }
        } else {
            $form->populate($this->fillsEdit($row->toArray()));
        }
        $this->view->form = $form;
        $this->view->title = $this->getTitle();
        $this->view->row = $row;


        $this->renderScript($this->getViewRenderUpdate());
    }

    public function getRepository()
    {
        return new Application_Model_DbTable_Marca();
    }

    public function getRecords()
    {
        $select = $this->getMarcas();
        //inserindo a listagem de estados para o devido filtro
        $estados = $this->db->select()->from('estado');
        $estados = $this->db->fetchAll($estados);
        $this->view->estados = $estados;
   
        return $this->db->fetchAll($select);
    }

    //selecionando a marca e suas consultas
    private function getMarcas()
    {
        $select = $this->db->select()
            ->from('marca', array('MAX(marca_historico.codigo_despacho) as despacho', '*'))
            ->join('cliente', 'marca.cliente_id = cliente.id', array('cliente.nome as cliente', 'cliente.cnpj as cnpj'))
            ->joinLeft('cidade', 'cliente.cidade_id = cidade.id', array(''))
            ->joinLeft('estado', 'cidade.estado_id = estado.id', array(''))
            ->joinLeft('pedido_has_marca', 'marca.id = pedido_has_marca.marca_id', 'status')
            ->join('pedido', 'pedido_has_marca.pedido_id = pedido.id', array('pedido.id as pedido'))
            ->joinLeft('marca_historico', 'marca.id = marca_historico.marca_id', array('marca_historico.marca_id'));

        //todos os filtros do ajax possível
        if (isset($_GET['uf'])) {
            $select->where('estado.id = ?', $_GET['uf']);
        }

        if (isset($_GET['cidade'])) {
            $select->where('cidade.id = ?', $_GET['cidade']);
        }

        if (isset($_GET['processo'])) {
            $select->where('processo = ?', $_GET['processo']);
        }

        if (isset($_GET['registro'])) {
            $modelMarcaHistorico = new Application_Model_MarcaHistorico();

            $despaschos = $modelMarcaHistorico->getDespachos($_GET['registro']);
        
            if(count($despaschos))
                $select->where('marca_historico.codigo_despacho IN (?)', $despaschos);
        }

        if ($this->_getParam('categoria') == 'contratadas') {
            $select->where('pedido_has_marca.status in (?)', array(
                'pago', 'processando', 'concluido', 'liberado',
                'assumido - processando', 'concedido'
            ));
        }

        if ($this->_getParam('categoria') == 'externas') {
            $select->where('pedido_has_marca.status in (?) ', array('novo', 'aguardando_pagamento', 'cancelado'));
        }

        $select->group('marca.id');
        $select->where('marca.active = ?', 1);
        $select->order('marca.id desc');


        //colocando a categoria quem vem como parâmetro para filtrar no ajax
        $this->view->categoria = $this->_getParam('categoria');

        return $select;
    }

    //filtrar as marcas pela cidade, estado, processo, registro
    public function filterAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->getResponse()->setHttpResponseCode(200);
        $select = $this->getMarcas();
        $select = $this->db->fetchAll($select);
        $table = '';
        foreach ($select as $s) {
            $ativo = $s['ativo'];
            $warning = isset($ativo) && !$ativo ? "warning $ativo" : '';
            $table .= "<tr class='gradeX $warning'>";
            foreach ($this->getColumns() as $column => $inf) {
                if ($inf['type'] == 'date' && $s[$column]) {
                    $date = new Zend_Date($s[$column]);
                    $date_t = $date->get($inf['config']['format'] ? $inf['config']['format'] : 'dd/MM/yyyy');
                    $table .= "<td>$date_t</td>";
                } elseif ($inf['type'] == 'boolean') {
                    $bool = $s[$column] ? 'Sim' : 'Não';
                    $table .= "<td>$bool</td>";
                } else {
                    $table .= "<td>$s[$column]</td>";
                }
            }
            $id = $s['id'];
            $ancora_visualizar = $this->view->url(array('action' => 'view', 'id' => $id));
            $table .= "<td> <a class='btn btn-info btn-xs' href='$ancora_visualizar' id='notification' data-original-title='' title=''>
            Visualizar
            </a>";

            if ($s['active']) {
                $ancora_inativar = $this->view->url(array('action' => 'active', 'id' => $id, 'active' => '0'));
                $table .= "<a class='btn btn-danger btn-xs' href='$ancora_inativar' id='notification' data-original-title='' title=''>
            Inativar
            </a>";
            } else {
                $ancora_ativar = $this->view->url(array('action' => 'active', 'id' => $id, 'active' => '1'));
                $table .= "<a class='btn btn-danger btn-xs' href='$ancora_ativar' id='notification' data-original-title='' title=''>
                    Ativar
            </a>";
            }

            $ancora_editar = $this->view->url(array('action' => 'update', 'id' => $id));

            $table .= "<a class='btn btn-warning btn-xs' href='$ancora_editar' id='notification' data-original-title='' title=''>
            Editar
            </a>";

            $editado = $s['editado'] == 1 ? 'checked' : '';
            $table .= "</td>";
            $table .= "<td><center><input $editado value='$id' type='checkbox' name='marca' class='check_marca'></center></td>";
            $table .= "<tr>";
        }
        $this->_helper->json(['success' => true, 'message' => 'Dados inseridos com successo', 'data' => $table]);
    }

    public function getColumns()
    {
        return
            array(
                'id' => array('label' => '#'),
                'marca' => array('label' => 'Marca'),
                'despacho' => array('label' => 'Despacho'),
                'processo' => array('label' => 'Nº Processo'),
                'pedido' => array('label' => 'Pedido'),
                'cliente' => array('label' => 'Cliente'),
                'cnpj' => array('label' => 'CNPJ / CPF'),
                'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date',  'config' => array("format" => "dd/MM/yyyy HH:mm")),
                'acompanhamento' => array('label' => 'Acompanhamento', 'type' => 'boolean'),
                'data_acompanhamento' => array('label' => 'Fim de acompanhamento', 'type' => 'date'),
                'publicacao_inpi' => array('label' => 'Data de publicação do processo', 'type' => 'date'),
                'active' => array('label' => 'Ativo', 'type' => 'boolean'),
                'contato' => array('label' => 'Contato', 'type' => 'boolean'),  
                'problema' => array('label' => 'Problema', 'type' => 'boolean'), 
                'esconder' => array('label' => 'Esconder', 'type' => 'boolean'), 
            );
        exit();
    }

    public function newAction()
    {
        $form = $this->getForm();

        if ($this->_request->isPost()) {

            $rep = $this->getRepository()->fetchRow(array('cliente_id = ?' => $_POST["cliente_id"], 'processo = ?' => $_POST["processo"]));
            if (!$rep and $form->isValid($_POST)) {
                try {
                    $this->db->beginTransaction();
                    $postData = $this->preparingData($form->getValues(), 0, $row);
                    $row = $this->getRepository()->createRow($postData);
                    $this->beforeSaving($row, $form->getValues(), 0);
                    $row->save();
                    $this->afterSaving($row, $form->getValues(), 0);
                    $this->db->commit();
                    $this->log->create($this->_usuario->id, $this->getMessages('new', $row->toArray()));
                    $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'list'));
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                    exit;
                    $this->db->rollBack();
                    $this->addFlashMessage(array('Erro ao salvar conteúdo', 0));
                    $this->router->gotoRoute(array());
                }
            } else {
                if ($rep) {
                    $this->addFlashMessageDirect(array('Processo duplicado para o cliente selecionado', 0));
                }
                $form->populate($_POST);
            }
        }
        $this->view->form = $form;
        $this->view->title = $this->getTitle();
        $this->view->action = "Adicionar/Atualizar";
        $this->renderScript($this->getViewRenderNew());
    }

    public function getForm($isEditing = 0)
    {
        $form = new Application_Form_Gerenciador_Marca($isEditing);
        return $form;
    }

    public function viewAction()
    {

        $marc = new Application_Model_DbTable_Marca();

        $select = $this->db->select()
            ->from('marca')
            ->joinLeft('cliente', 'marca.cliente_id = cliente.id', array('cliente.id as cliente_id', 'cliente.nome as cliente', 'cnpj', 'email', 'telefone', 'celular'))
            ->joinLeft('categoria', 'marca.categoria_id = categoria.id', array('categoria.id_inpi as classe'))
            ->where('marca.id = ?', $this->_getParam('id'));
        $marca = $this->db->fetchRow($select);


        // SETA COMO NULL A MARCA DEPOIS DA VISUALIZACAO
        $mar = $marc->fetchRow(array('id = ?' => $this->_getParam('id')));
        $mar->atual = NULL;
        $mar->save();

        $this->view->row = $marca;

        $this->view->title = $this->getTitle();
        $this->view->action = "Visualizar";
        $this->renderScript($this->getViewRenderView());
    }

    protected function preparingData($postDate, $isEditing = 0, $row = null)
    {

        $postDate = parent::preparingData($postDate, $isEditing, $row);

        if (!$postDate["processo"]) {
            $postDate["processo"] = null;
        }

        if (!$postDate["cliente_id"]) {
            unset($postDate["cliente_id"]);
        }
        if (!$postDate["publicacao_inpi"]) {
            unset($postDate["publicacao_inpi"]);
        } else {
            $dataInpi = new Zend_Date($postDate["publicacao_inpi"]);
            $postDate["publicacao_inpi"] = $dataInpi->get('yyyy-MM-dd');
        }

        if (!$postDate["data_publicacao_processo"]) {
            unset($postDate["data_publicacao_processo"]);
        } else {
            $dataP = new Zend_Date($postDate["data_publicacao_processo"]);
            $postDate["data_publicacao_processo"] = $dataP->get('yyyy-MM-dd');
        }

        if (!$postDate["data_acompanhamento"]) {
            unset($postDate["data_acompanhamento"]);
        } else {
            $dataAtual = new Zend_Date();
            $dataAcompanhamento = new Zend_Date($postDate["data_acompanhamento"], 'dd/MM/yyyy');

            $data = new Zend_Date($postDate["data_acompanhamento"]);
            $postDate["data_acompanhamento"] = $data->get('yyyy-MM-dd');
        }

        return $postDate;
    }

    protected function fillsEdit($dados)
    {
        $dados = parent::fillsEdit($dados);
        if (!$dados["data_acompanhamento"]) {
            $dados["data_acompanhamento"] = "";
        } else {
            $data = new Zend_Date($dados["data_acompanhamento"]);
            $dados["data_acompanhamento"] = $data->get('dd/MM/yyyy');
        }

        if (!$dados["publicacao_inpi"]) {
            $dados["publicacao_inpi"] = "";
        } else {
            $dataInpi = new Zend_Date($dados["publicacao_inpi"]);
            $dados["publicacao_inpi"] = $dataInpi->get('dd/MM/yyyy');
        }

        return $dados;
    }

    public function acompanhamentosVencidosAction()
    {
        $this->setMenu('acompanhamentos-vencidos');

        $sql = 'SELECT CASE 
                    WHEN marca.data_cadastro <= NOW() - interval 1 YEAR
                    THEN "REGISTRO_VENCIDO"
                    ELSE "REGISTRO_OK" 
                END AS registro_v,
                CASE 
                    WHEN pedido.data_cadastro<= NOW() - interval 1 YEAR
                    THEN "PROCESSO_VENCIDO"
                    ELSE "PROCESSO_OK"
                END AS processo_v, 
                `marca`.*, `cliente`.`nome` AS `cliente`, `cliente`.`cnpj`, `cliente`.`id` AS `cliente_id` 
                FROM `marca` 
                INNER JOIN `cliente` ON marca.cliente_id = cliente.id 
                LEFT join `pedido` on marca.id = pedido.marca_id 
                WHERE (marca.data_acompanhamento < "' . date('Y-m-d') . '") 
                    AND (marca.oculta_prazo = 0)
                    AND (marca.acompanhamento = 1) AND (marca.active = 1) ORDER BY `marca`.`data_acompanhamento` desc';


        $this->view->records = $this->db->fetchAll($sql);
    }

    public function editadoAction()
    {
        $tbMarcas = new Application_Model_DbTable_Marca();
        if ($_POST['marca']) {
            $marca = $_POST['marca'];
            $value = $_POST['check'] ? 1 : 0;
            try {
                $row = $tbMarcas->fetchRow(array("id = ?" => $marca));
                if ($row) {
                    $row->editado = $value;
                    $row->save();
                }
                echo json_encode(1);
                exit;
            } catch (Exception $e) {
                echo json_encode($e->getMessage());
                exit;
            }
        }

        $rows = $tbMarcas->fetchAll();
        foreach ($rows as $marca) {
            $marca->editado = 0;
            $marca->save();
        }
        echo json_encode(1);
        exit;
    }

    public function ocultarAction()
    {

        try {

            $data = array(
                'oculta_prazo' => true
            );

            $table = new Zend_Db_Table('marca');
            $where = $this->db->quoteInto('id = ?', $this->_request->getPost('id'));
            $table->update($data, $where);

            echo json_encode(1);
            exit;
        } catch (Exception $e) {
            echo json_encode($e->getMessage());
            exit;
        }
    }

    public function problemaAction()
    {
        try {
            $isChecked = filter_var($this->_request->getPost('checked'), FILTER_VALIDATE_BOOLEAN); // Get the checkbox state from the POST data

            $data = array(
                'problema' => $isChecked // Use the checkbox state to update the database
            );

            $table = new Zend_Db_Table('marca');
            $where = $this->db->quoteInto('id = ?', $this->_request->getPost('id'));
            $table->update($data, $where);

            echo json_encode(1);
            exit;
        } catch (Exception $e) {
            echo json_encode($e->getMessage());
            exit;
        }
    }

    public function contatoAction()
    {
        try {
            $isChecked = filter_var($this->_request->getPost('checked'), FILTER_VALIDATE_BOOLEAN); // Get the checkbox state from the POST data

            $data = array(
                'contato' => $isChecked // Use the checkbox state to update the database
            );

            $table = new Zend_Db_Table('marca');
            $where = $this->db->quoteInto('id = ?', $this->_request->getPost('id'));
            $table->update($data, $where);

            echo json_encode(1);
            exit;
        } catch (Exception $e) {
            echo json_encode($e->getMessage());
            exit;
        }
    }

    public function esconderAction()
    {
        try {
            $isChecked = filter_var($this->_request->getPost('esconder'), FILTER_VALIDATE_BOOLEAN); // Get the checkbox state from the POST data

            $data = array(
                'esconder' => $isChecked // Use the checkbox state to update the database
            );

            $table = new Zend_Db_Table('marca');
            $where = $this->db->quoteInto('id = ?', $this->_request->getPost('id'));
            $table->update($data, $where);

            echo json_encode(1);
            exit;
        } catch (Exception $e) {
            echo json_encode($e->getMessage());
            exit;
        }
    }

}
