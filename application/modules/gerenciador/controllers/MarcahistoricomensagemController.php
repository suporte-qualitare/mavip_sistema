<?php

class Gerenciador_MarcahistoricomensagemController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Histórico de Mensagens');
        $this->setMenu('leitura');
        $this->setViewRenderNew('marcahistoricomensagem/new.phtml');
        $this->setViewRenderUpdate('marcahistoricomensagem/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_MarcaHistoricoMensagem();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'codigo_despacho' => array('label' => 'Código de despacho'),
                    'prazo' => array('label' => 'Prazo'),
                    'titulo' => array('label' => 'Título'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_MarcaHistoricoMensagem($isEditing);
        return $form;
    }
    
  

}

