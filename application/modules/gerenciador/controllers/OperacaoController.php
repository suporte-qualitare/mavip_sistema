<?php /** @noinspection ALL */

class Gerenciador_OperacaoController extends Abstract_Gerenciador_Controller_CrudController
{

    public function init()
    {
        parent::init();
        $this->setTitle('Operação');
        $this->setMenu('Operação');

        if ($this->_getParam('status') == 'prazo') {
            $this->setOption(array('prazo' => '1', 'page' => 'Prazos'));
            $this->view->action = 'Prazos';
        } else if($this->_getParam('status') == 'concedido'){
            $this->setOption(array('prazo' => '0', 'page' => 'Concessões de registros'));
            $this->view->action = 'Concessões de registros';
        }
        else{
            $this->setOption(array('prazo' => '0', 'page' => 'Contratações'));
            $this->view->action = 'Contratações';
        }

        $this->setViewRenderList('operacao/list.phtml');
    }

    public function getRepository()
    {
        return new Application_Model_DbTable_Marca();
    }

    public function getRecords()
    {
        // liberado
        if ($this->_getParam('status') == 'prazo') {
            $status = "( status = 'concluido' or status = 'novo' or  status = 'perdeu' )";
        }
        else if ($this->_getParam('status') == 'concedido'){
            $status = "( status = 'concedido' )";
        }
        else{
            $status = "( status = 'pago' or status = 'processando' or status = 'assumido - processando' )";
        }

        if (!$this->view->isAllowed('operacao', 'listall')) {
            if ($this->_getParam('status') == 'prazo')
                $order_by = 'prazo desc';
            else
                $order_by = 'pedido.data_cadastro desc';

            $db = Zend_Db_Table_Abstract::getDefaultAdapter();
            $sql = "select marca_historico.id as mh_id, 
            (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1)) as data_leitura,
            (select termo from  servico where id = (select servico_id from marca_has_servico where marca_has_servico.marca_id = marca_historico.marca_id limit 1)) as servico,
            (select mh.codigo_despacho from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as despacho,
            (select mh.texto from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as texto,
            (select mh.ocultar from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as ocultar,
            (select marca.oculta_prazo from marca as mrh where mrh.id = pedido_has_marca.marca_id order by id desc limit 1) as oculta_prazo,
            (select mh.leitura_id from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as revista,
            (select prazo from marca_historico_mensagem where marca_historico_mensagem.codigo_despacho = despacho order by id desc limit 1) as prazo,
            marca.marca as marca, 
            marca.id as marca_id, 
            cliente.razao_social as cliente, 
            users.name as responsavel,
            marca.processo as processo,
            pedido.data_cadastro,
            pedido_has_marca.status,
            pedido_has_marca.responsavel_users_id,
            pedido_has_marca.novo_prazo,
            pedido_has_marca.data_prazo,
            pedido_has_marca.info_gru,
            pedido_has_marca.observacao,
            pedido_has_marca.marca_id, DATEDIFF(CURDATE(), pedido.data_cadastro) as diff,
            pedido.id
                    from pedido 
                    join cliente on (pedido.cliente_id = cliente.id)
                    left join pedido_has_marca on (pedido.id = pedido_has_marca.pedido_id)
                    left join users on (pedido_has_marca.responsavel_users_id = users.id)
                    left join marca on (pedido_has_marca.marca_id = marca.id and marca.active = 1)
                    left join marca_historico on (marca.id = marca_historico.marca_id)
                    left join categoria on (marca.categoria_id = categoria.id)
                    where {$status} or pedido_has_marca.responsavel_users_id = {$this->_usuario->id}
                    group by marca.id
                    order by {$order_by}";

            $stmt = $db->query($sql);
            $res = $stmt->fetchAll();

            if($_SERVER['HTTP_HOST'] != 'localhost'){
                foreach ($res as $row){
                    if($row['despacho'] === 'IPAS158' && $row['status'] !== 'concedido'){
                        $this->concessaoregistro($row);
                    }
                }
            }
      
            return $res;


        } else {
            if ($this->_getParam('status') == 'prazo')
                $order_by = '(prazo - leitura)>0 DESC,(prazo - leitura) desc';
            else
                $order_by = 'pedido.data_cadastro desc';

            $db = Zend_Db_Table_Abstract::getDefaultAdapter();
            $sql = "select marca_historico.id as mh_id, 
            (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1)) as data_leitura,
            (select termo from  servico where id = (select servico_id from marca_has_servico where marca_has_servico.marca_id = marca_historico.marca_id limit 1)) as servico,
            (select mh.codigo_despacho from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as despacho,
            (select mh.texto from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as texto,
            (select mh.ocultar from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as ocultar,
            (select marca.oculta_prazo from marca as mrh where mrh.id = pedido_has_marca.marca_id order by id desc limit 1) as oculta_prazo,
            (select mh.leitura_id from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as revista,
            (select prazo from marca_historico_mensagem where marca_historico_mensagem.codigo_despacho = despacho order by id desc limit 1) as prazo,
            marca.marca as marca, 
            marca.id as marca_id, 
            cliente.razao_social as cliente, 
            users.name as responsavel,
            categoria.id_inpi as ncl,
            marca.processo as processo,
            pedido.data_cadastro,
            pedido_has_marca.status,
            pedido_has_marca.info_gru,
            pedido_has_marca.novo_prazo,
            pedido_has_marca.responsavel_users_id,
            pedido_has_marca.data_prazo,
            pedido_has_marca.observacao,
            pedido_has_marca.marca_id, DATEDIFF(CURDATE(), pedido.data_cadastro) as diff,
            DATEDIFF(CURDATE(), (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1))) as leitura,
            pedido.id
                    from pedido
                    join cliente on (pedido.cliente_id = cliente.id)
                    left join pedido_has_marca on (pedido.id = pedido_has_marca.pedido_id)
                    left join users on (pedido_has_marca.responsavel_users_id = users.id)
                    left join marca on (pedido_has_marca.marca_id = marca.id and marca.active = 1)
                    left join categoria on (marca.categoria_id = categoria.id)
                    left join marca_historico on (marca.id = marca_historico.marca_id)
                    where  $status and oculta_prazo = 0
                    group by marca.id
                    order by {$order_by}";

            $stmt = $db->query($sql);
            $res = $stmt->fetchAll();
            $res2 = [];

            if($_SERVER['HTTP_HOST'] != 'localhost'){
                foreach ($res as $key =>$row){
                    if($row['despacho'] === 'IPAS158' && $row['status'] !== 'concedido'){
                        $this->concessaoregistro($row);
                    }

                    if (!$row['prazo']){
                       $row['prazo'] = 30;
                    }
                    $res2[] = $row;
                }
            }
            else {
                $res2 = $res;
            }

            return $res2;
        }
    }


    public function getColumns()
    {
        return
            array(
                'id' => array('label' => '#'),
                'data_leitura' => array('label' => 'Tempo', 'type' => 'diff'),
                'servico' => array('label' => 'Contrato'),
                'revista' => array('label' => 'Nº Revista'),
                'texto' => array('label' => 'Despacho'),
                'prazo' => array('label' => 'Prazo'),
                'cliente' => array('label' => 'Cliente'),
                'marca' => array('label' => 'Marca'),
                'ncl'  => array('label' => 'NCL'),
                'processo' => array('label' => 'N Processo', 'type' => 'processo'),
                //  'processo_arquivo' => array('label' => 'Documento'),
                'responsavel' => array('label' => 'Responsável'),
                'status' => array('label' => 'Status'),
                'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
            );
    }

    public function getForm($isEditing = 0)
    {
        if ($this->getParam('id')) {
            $id = $this->getParam('id');
        } else {
            $id = null;
        }

        $form = new Application_Form_Gerenciador_Marca($isEditing, null, $id);
        return $form;
    }

    public function novoPrazoAction()
    {
        if (count($_POST)) {

            $pedidos = $_POST['dados'];
            $pedidoMapper = new Application_Model_PedidoHasMarca();
            foreach ($pedidos as $p) {
                if (!$pedidoMapper->concederPrazo(sha1($p))) {
                    echo json_encode(0);
                    exit;
                }
                $this->log->create($this->_usuario->id, 'concedeu prazo extra de 3 dias do Marca de ID: ' . $p);
            }
            echo json_encode(1);
            exit;
        } elseif ($this->_getParam('marca_id')) {
            $pedidoMapper = new Application_Model_PedidoHasMarca();
            $pedidoMapper->concederPrazo(sha1($this->_getParam('marca_id')));
            $this->log->create($this->_usuario->id, 'concedeu prazo extra de 3 dias da Marca de ID: ' . $this->_getParam('marca_id'));
            $this->addFlashMessage(array('Prazo extra de 3 dias concedido com sucesso', 1), array('action' => 'list'));
        }
        $this->addFlashMessage(array('Erro ao realizar prazo', 1), array('action' => 'list'));
    }

    public function listConcluidoAction()
    {
        $this->view->title = 'Operação';
        $this->view->page = 'Contratações concluídas';

        $status = "status = 'concluido' ";


        if (!$this->view->isAllowed('operacao', 'list-concluido')) {

            $db = Zend_Db_Table_Abstract::getDefaultAdapter();
            $sql = "select marca_historico.id as mh_id, 
            (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1)) as data_leitura,
            (select mh.codigo_despacho from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as despacho,
            (select mh.texto from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as texto,
            (select prazo from marca_historico_mensagem where marca_historico_mensagem.codigo_despacho = despacho order by id desc limit 1) as prazo,
            marca.marca as marca, 
            cliente.razao_social as cliente, 
            users.name as responsavel,
            marca.marca as marca,
            marca.processo as processo,
            pedido.data_cadastro,
            pedido_has_marca.status,
            pedido_has_marca.novo_prazo,
            pedido_has_marca.marca_id, DATEDIFF(CURDATE(), pedido.data_cadastro) as diff,
            pedido.id
                    from pedido 
                    join cliente on (pedido.cliente_id = cliente.id)
                    left join pedido_has_marca on (pedido.id = pedido_has_marca.pedido_id)
                    left join users on (pedido_has_marca.responsavel_users_id = users.id)
                    left join marca on (pedido_has_marca.marca_id = marca.id and marca.active = 1)
                    left join marca_historico on (marca.id = marca_historico.marca_id)
                    where $status or responsavel_users_id = $this->_usuario->id
                    group by pedido.id";
            $stmt = $db->query($sql);
            $res = $stmt->fetchAll();

            return $res;

        } else {

            $db = Zend_Db_Table_Abstract::getDefaultAdapter();
            $sql = "select marca_historico.id as mh_id, 
            (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1)) as data_leitura,
            (select mh.codigo_despacho from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as despacho,
            (select mh.texto from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as texto,
            (select prazo from marca_historico_mensagem where marca_historico_mensagem.codigo_despacho = despacho order by id desc limit 1) as prazo,
            marca.marca as marca, 
            cliente.razao_social as cliente, 
            users.name as responsavel,
            marca.marca as marca,
            marca.processo as processo,
            pedido.data_cadastro,
            pedido_has_marca.status,
            pedido_has_marca.novo_prazo,
            pedido_has_marca.marca_id, DATEDIFF(CURDATE(), pedido.data_cadastro) as diff,
            DATEDIFF(CURDATE(), (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1))) as leitura,
            pedido.id
                    from pedido
                    join cliente on (pedido.cliente_id = cliente.id)
                    left join pedido_has_marca on (pedido.id = pedido_has_marca.pedido_id)
                    left join users on (pedido_has_marca.responsavel_users_id = users.id)
                    left join marca on (pedido_has_marca.marca_id = marca.id and marca.active = 1)
                    left join marca_historico on (marca.id = marca_historico.marca_id)
                    where  $status
                    group by pedido.id
                    order by pedido.data_cadastro desc";

            $stmt = $db->query($sql);
            $res = $stmt->fetchAll();


            $this->view->records = $res;
        }
    }

    public function concluirAction()
    {

        try
        {
            $select = $this->db->select()
                ->from('pedido', array('pedido.id as id', '*'))
                ->join('pedido_has_marca', 'pedido.id = pedido_has_marca.pedido_id', array('status', 'info_gru', 'responsavel_users_id'))
                ->join('marca', 'pedido_has_marca.marca_id = marca.id', array('marca.id as marca_id', 'marca.marca as marca', 'tipo', 'estilo', 'logo', 'categoria_id', 'acompanhamento', 'observacao'))
                ->join('cliente', 'pedido.cliente_id = cliente.id', array('cliente.razao_social as cliente', 'cnpj', 'email', 'telefone', 'celular'))
                ->where('pedido_has_marca.marca_id = ?', $this->_getParam('marca'))
                ->where('pedido_has_marca.pedido_id = ?', $this->_getParam('id'))->limit(1);
            $this->view->row = $this->db->fetchRow($select);


            $select = $this->db->select()
                ->from('marca', array("publicacao_inpi as DATE_FORMAT(publicacao_inpi,'%dd/%mm/%Y')", '*'))
                ->where('id = ?', $this->view->row['marca_id'])->limit(1);
            $marca = $this->db->fetchRow($select);

            if(count($marca) && !empty( $marca['publicacao_inpi'])){
                $data = new Zend_Date($marca["publicacao_inpi"]);
                $marca["publicacao_inpi"] = $data->get('dd/MM/yyyy');
            }

            $this->view->marca = $marca;
            $this->view->title = $this->getTitle();
            $pedidoDb = new Application_Model_DbTable_PedidoHasMarca();
            $pedidoMarca = $pedidoDb->fetchRow(array('pedido_id = ?' =>  $this->_getParam('id'), 'marca_id = ?' => $this->_getParam('marca')));

        }
        catch (Exception $e)
        {
            echo "<span style='color: red; font-weight: bold'>  Erro na página.  Contacte o suporte.</span> Detalhes:  <br><br>";
            echo $e->getMessage();
            exit;
        }

        $form = new Application_Form_Gerenciador_MarcaConcluir();

        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {

                try {
                    $rep = $this -> getRepository();
                    $row = $rep -> fetchRow(array('id = ?' => $this -> _getParam('marca')));

                    if (!$row)
                        $this -> addFlashMessage(array('Erro ao salvar', 0), array('action' => 'concluir', 'marca' => $this -> _getParam('marca')));

                    $postData = $this -> preparingData($form -> getValues(), 1, $row);
                    $pedidoMarca["gru_paga"] = $postData["gru_paga"];
                    $pedidoMarca['observacao'] = $postData['observacao'];
                    $pedidoMarca -> save();
                    unset($postData['observacao']);

                    if (!$postData["processo"]) {
                        $postData["processo"] = null;
                    }

                    if (!$postData["nosso_numero"]) {
                        $postData["nosso_numero"] = null;
                    }


                    if (!$postData["publicacao_inpi"]) {
                        unset($postData["publicacao_inpi"]);
                    } else{
                        $dataInpi = new Zend_Date($postData["publicacao_inpi"]);
                        $postData["publicacao_inpi"] = $dataInpi->get('yyyy-MM-dd');
                    }

                    $row -> setFromArray($postData);
                    $row -> save();

                    if ($this -> getParam('atualizar') != 1) {

                        if ($row["processo"] AND $postData["nosso_numero"]) {

                            $pedidoDb = new Application_Model_DbTable_PedidoHasMarca();
                            $pedidoMarca = $pedidoDb -> fetchRow(array('pedido_id = ?' => $this -> _getParam('id'),  'marca_id = ?' => $this -> _getParam('marca')));

                            if (($pedidoMarca["status"] == "assumido" || $pedidoMarca["status"] == "liberado" || $pedidoMarca["status"] == "assumido - processando") AND $pedidoMarca["gru_paga"] == '1') {

                                $pedidoMarca["status"] = "concluido";
                                if ($pedidoMarca -> save()) {
                                    $clienteDb = new Application_Model_DbTable_Cliente();
                                    $cliente = $clienteDb -> fetchRow(array('id = ?' => $pedidoMarca["cliente_id"]));
                                    $email = new Application_Model_Emails();
                                    $email -> sendEmail(array('mail' => $cliente['email'], 'nome' => $cliente['nome']), "MAVIP - Pedido de Registro Concluído", "pedido-concluido", array('nome' => $cliente['nome']));

                                }

                                $pedidoMapper = new Application_Model_PedidoHasMarca();
                                $pedidoMapper -> inativarPrazo(sha1($this -> _getParam('marca')));

                                $this->afterSaving($row, $form->getValues(), 1);
                                $this->log->create($this->_usuario->id, $this->getMessages('update', $row->toArray()));
                                $this->addFlashMessage(array('Processo concluido com sucesso.', 1), array('controller' => 'operacao', 'action' => 'list'));
                            }
                        }
                    }

                }
                catch (Exception $e)
                {
                    echo $e->getMessage();
                    exit;
                }

                $this->afterSaving($row, $form->getValues(), 1);
                $this->log->create($this->_usuario->id, $this->getMessages('update', $row->toArray()));
                $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('controller' => 'operacao', 'action' => 'list'));
            }
        } else {
            $dados = $this->view->marca;
            $dados["gru_paga"] = $pedidoMarca["gru_paga"];
            $dados['observacao'] = $pedidoMarca['observacao'];
            $form->populate($this->fillsEdit($dados));
        }
        $this->view->form = $form;
        $this->view->page = "Prazos";
        $this->view->action = "Concluir";
        $this->renderScript('operacao/concluir.phtml');
    }

    public function cancelarAction()
    {
        if ($this->_getParam('id')) {
            $pedidoMapper = new Application_Model_PedidoHasMarca();
            $pedidoMapper->cancela(sha1($this->_getParam('marca')));
            $pedidoMapper->inativarPrazo(sha1($this->_getParam('marca')));

            $this->log->create($this->_usuario->id, 'cancelou a MARCA de ID: ' . $this->_getParam('marca'));

            $this->addFlashMessage(array('Cancelamento realizada com sucesso', 1), array('action' => 'list'));
        }
        $this->addFlashMessage(array('Erro ao realizar cancelar', 1), array('action' => 'list'));
    }

    public function assumirAction()
    {
        if ($this->_getParam('marca')) {
            $pedidoMapper = new Application_Model_DbTable_PedidoHasMarca();
            $row = $pedidoMapper->fetchRow(array('pedido_id = ?' =>  $this->_getParam('id') ,  'marca_id = ?' => $this->_getParam('marca')));

            if(!$row)
                $this->addFlashMessage(array('Erro. Processo não localizado.', 0), array('action' => 'list'));

            if ($row['status'] == 'pago') {
                $row->responsavel_users_id = $this->_usuario->id;
                $row->status = 'assumido - processando';
                $row->save();
                $this->log->create($this->_usuario->id, 'assumiu a MARCA de ID: ' . $this->_getParam('marca'));
                $this->addFlashMessage(array('Processo assumido com sucesso', 1), array('action' => 'list'));
            }
            else{
                $this->addFlashMessage(array('Erro. Processo não pago.', 0), array('action' => 'list'));
            }
        }
    }

    public function liberadoAction()
    {
        if ($this->_getParam('marca_id')) {
            $pedidoMapper = new Application_Model_DbTable_PedidoHasMarca();
            $row = $pedidoMapper->fetchRow(array('pedido_id = ?' => $this->_getParam('id'), 'marca_id = ?' => $this->_getParam('marca_id')));
            $row->responsavel_users_id = $this->_usuario->id;
            $row->status = 'liberado';
            $row->save();
            $this->log->create($this->_usuario->id, 'liberou a Marca de ID: ' . $this->_getParam('marca_id'));

            $this->addFlashMessage(array('Processo liberado ', 1), array('action' => 'list'));
        }
        $this->addFlashMessage(array('Erro ao realizar baixar', 1), array('action' => 'list'));
    }

    public function gerargruAction()
    {
        $select = $this->db->select()
            ->from('cliente')
            ->join('pedido', 'cliente.id = pedido.cliente_id ', array())
            ->where('pedido.id = ?', $this->_getParam('id'));
        $this->view->row = $this->db->fetchRow($select);
    }

    public function gerarpeticionamentoAction()
    {
        $select = $this->db->select()
            ->from('cliente')
            ->join('pedido', 'cliente.id = pedido.cliente_id ', array())
            ->where('pedido.id = ?', $this->_getParam('id'));
        $this->view->row = $this->db->fetchRow($select);
    }

    public function decenioAction()
    {
        if($this->_getParam('status') == 'concluido'){
            $where = "marca.active = 1 and ocultar_decenio = 1";
            $decenio = "Decênios Concluídos";
            $status = 'concluido';
        }
        else{
            $where1 = "DATE_SUB(curdate(), INTERVAL '10.0' YEAR_MONTH) > marca.publicacao_inpi  and 
                                        (DATE_SUB(CURDATE(), INTERVAL '10.6' YEAR_MONTH)) <= marca.publicacao_inpi and ocultar_decenio = 0 and marca.active = 1  or";
            $where2 = "(DATE_SUB(CURDATE(), INTERVAL 9 YEAR)) >= marca.publicacao_inpi  and  
                                        (DATE_SUB(CURDATE(), INTERVAL 10 YEAR)) <= marca.publicacao_inpi and ocultar_decenio = 0 and marca.active = 1";
            $where = $where1.$where2;
            $decenio = "Decênios";
            $status = 'andamento';
        }

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "  select DISTINCT marca.id, marca, users.name, processo,publicacao_inpi, ocultar_decenio, categoria.nome as categoria, categoria.id_inpi,
                  (select nome from cliente WHERE id = marca.cliente_id limit 1) as cliente
                  from marca
                  JOIN pedido_has_marca on marca_id = marca.id
                  LEFT JOIN categoria on marca.categoria_id = categoria.id
                  LEFT JOIN users on pedido_has_marca.responsavel_users_id = users.id
                  WHERE {$where}
                  ORDER BY marca.publicacao_inpi asc";

        $stmt = $db->query($sql);
        $this->view->records = $stmt->fetchAll();
        $this->view->title = "Operação";
        $this->view->decenio = $decenio;
        $this->view->status = $status;
        $this->view->action = $decenio;

    }

    public function alertasAction()
    {
        $this->view->title = "Alertas / Newslleter";
        $modelAlertas = new Application_Model_Alertas();
        if (isset($_POST['filtro'])) {
            $records = $modelAlertas->getByIpas($_POST['ipas']);
        } else {
            $records = $modelAlertas->getAll();
        }

        $page = $this->_getParam('page', 1);
        $adapter = new Zend_Paginator_Adapter_Array($records);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(20);
        $this->view->assign('paginator', $paginator);
        $this->view->perid = "-";
        $this->view->prazo = "-";

    }

    public function documentosAction()
    {

        $this->view->title = "Operação";
        $this->view->action = "Documentos";

        if ($this->getParam('id')) {

            $id = $this->getParam('id');
            $processoModel = new Application_Model_Processo();
            $records = $processoModel->getById($id);

            $this->view->subTitle = "PEDIDO  $id";
            if ($records) {
                $this->view->records = $records;
            } else {
                $this->view->records = false;
            }

        }
    }

    // ocultando o processo em prazos
    public function ocultarAction()
    {
        try{
            $marcaHistoricoDb = new Application_Model_DbTable_MarcaHistorico();
            $marcaDb = new Application_Model_DbTable_Marca();
            if ($_POST['dados']) {
                // echo json_encode($_POST['dados']);
                foreach ($_POST['dados'] as $p) {
                    $where = $this->montaWhere($p);
                    $marcaHistorico = $marcaHistoricoDb->fetchRow($where);
                    if ($marcaHistorico) {
                        $marcaHistorico->ocultar = '1';
                        $marcaHistorico->save();
                    }
                    else{
                        $marcaRow = $marcaDb->fetchRow(array('id = ?' =>  abs($p)));
                        if($marcaRow){
                            $marcaRow->oculta_prazo = 1;
                            $marcaRow->save();
                        }
                        else{
                            echo json_encode($p);
                            exit;
                        }
                    }
                }
                echo json_encode(true);
                exit;
            } elseif ($this->_getParam('processo')) {
                $marcaHistorico = $marcaHistoricoDb->fetchRow(array('processo = ?' => $this->_getParam('id')), 'leitura_id desc');
                if ($marcaHistorico)
                    $marcaHistorico->ocultar = '1';
                $marcaHistorico->save();
                $this->log->create($this->_usuario->id, 'ocultou o prazo do processo de ID: ' . $this->_getParam('id'));
                $this->addFlashMessage(array('Movido para prazos concluidos.', 1), array('action' => 'list', 'status' => 'prazo'));
            }
        }
        catch (Exception $e){
            echo json_encode($e->getMessage());
            exit;
        }

        $this->addFlashMessage(array('Erro ao ocultar prazo', 1), array('action' => 'list'));
    }

    private function montaWhere($data){

        $data = explode("-", $data);

        if($data[0] != 0){
            $data =   array('leitura_id = ?' => abs($data[0]),  'marca_id = ?' => abs($data[1]));
        }
        else{
            $data = array('leitura_id IS NULL', 'marca_id = ?' => abs($data[1]));
        }
        return  $data;
    }

    // ativa a exibição do processo em prazos
    /*    public function exibirAction()
        {

            $marcaHistoricoDb = new Application_Model_DbTable_MarcaHistorico();
            if ($this->getParam('pr')) {
                $marcaHistorico = $marcaHistoricoDb->fetchRow(array('processo = ?' => $this->getParam('pr')), 'leitura_id desc');
                if ($marcaHistorico)
                    $marcaHistorico->ocultar = '0';
                $marcaHistorico->save();
                $this->addFlashMessage(array('Movido para prazos.', 1), array('action' => 'ocultos'));
            }
        }*/

// exibi uma lista de processos ocultados pelo usuario
    public function ocultosAction()
    {

        $order_by = 'pedido.data_cadastro desc';

        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $sql = "select marca_historico.id as mh_id, 
    (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1)) as data_leitura,
    (select mh.codigo_despacho from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as despacho,
    (select mh.texto from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as texto,
     (select marca.oculta_prazo from marca as mh where mh.id = pedido_has_marca.marca_id order by id desc limit 1) as oculta_prazo,
    (select mh.ocultar from marca_historico as mh where mh.marca_id = pedido_has_marca.marca_id order by id desc limit 1) as ocultar,
    (select prazo from marca_historico_mensagem where marca_historico_mensagem.codigo_despacho = despacho order by id desc limit 1) as prazo,
    marca.marca as marca,
    pedido.id as pedido_id,
    cliente.razao_social as cliente, 
    users.name as responsavel,
    marca.processo as processo,
    pedido_has_marca.status,
    pedido_has_marca.info_gru,
    pedido_has_marca.novo_prazo,
    pedido_has_marca.responsavel_users_id,
    pedido_has_marca.data_prazo,
    pedido_has_marca.marca_id, DATEDIFF(CURDATE(), pedido.data_cadastro) as diff,
    DATEDIFF(CURDATE(), (select data from leitura where id = (select leitura_id from marca_historico where marca.id = marca_historico.marca_id order by id desc limit 1))) as leitura,
    pedido.id
            from pedido
            join cliente on (pedido.cliente_id = cliente.id)
            join pedido_has_marca  on (pedido.id = pedido_has_marca.pedido_id)
            left join users on (pedido_has_marca.responsavel_users_id = users.id)
            join marca on (pedido_has_marca.marca_id = marca.id and marca.active = 1)
            left join marca_historico on (marca.id = marca_historico.marca_id)
            where (ocultar = '1' or  oculta_prazo = 1) and pedido_has_marca.status != 'concedido'
            group by marca.id
            order by {$order_by} ";

        $stmt = $db->query($sql);
        $res = $stmt->fetchAll();

        if($_SERVER['HTTP_HOST'] != 'localhost'){
            foreach ($res as $row){
                if($row['despacho'] === 'IPAS158' && $row['status'] !== 'concedido'){
                    $this->concessaoregistro($row);
                }
            }
        }

        $this->view->title = 'Operação';
        $this->view->action = 'Prazos Concluídos';
        $this->view->records = $res;
    }

    public function gruAction()
    {
        if (isset($_POST['marca_id'])) {

            $dados = array('id' => $_POST['marca_id'],
                'gru' => $_POST['gru'],
                'gru_pgo' => $_POST['gru_pgo']);

            $pedidoHasMarcaModel = new Application_Model_PedidoHasMarca();
            $update = $pedidoHasMarcaModel->infoGruPedido($dados);

            if ($update)
                $this->addFlashMessage(array('Informação da GRU registrada com sucesso', 1), array('action' => 'list', 'status' => 'prazo'));
        }

        $this->addFlashMessage(array('Erro ao registrar GRU', 0), array('action' => 'list', 'status' => 'prazo'));
    }

    public function concessaoregistro($row){
        $modelPedidoHasMarca = new Application_Model_PedidoHasMarca();
        $modelPedidoHasMarca->setStatus($row['marca_id'], 'concedido');
    }

    public function ocultarDecenioAction() {

        if ($this->_getParam('id')) {
            $marcaMapper = new Application_Model_DbTable_Marca();
            $row = $marcaMapper->fetchRow(array('id = ?' => $this->_getParam('id')));
            $row->ocultar_decenio = 1;
            if($row->save()){
                $this->log->create($this->_usuario->id, 'Ocultou decênio da Marca de ID: ' . $this->_getParam('id'));
                $this->addFlashMessage(array('Operação realizada com sucesso. ', 1), array('action' => 'decenio', 'status' => $this->_getParam('status')));
            }
        }
        $this->addFlashMessage(array('Erro ao realizar operação', 0), array('action' => 'decenio', 'status' => $this->_getParam('status')));
    }

}
