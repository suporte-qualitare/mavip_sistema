<?php

class Gerenciador_OptionsController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Textos');
        $this->setMenu('blog');
        $this->setViewRenderNew('options/new.phtml');
        $this->setViewRenderUpdate('options/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Options();
    }

    public function getColumns() {
        return
            array(
                'id' => array('label' => '#'),
                'name' => array('label' => 'Nome'),
            );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Options($isEditing);
        return $form;
    }
    
  

}

