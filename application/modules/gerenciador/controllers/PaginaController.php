<?php

class Gerenciador_PaginaController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Página');
        $this->setMenu('blog');

        $this->setViewRenderUpdate('pagina/update.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Pagina();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'titulo' => array('label' => 'titulo')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Pagina($isEditing);
        return $form;
    }

    public function updateAction() {
        $form = $this->getForm($this->_getParam('id'));
        $rep = $this->getRepository();
        $row = $rep->fetchRow(array('id = ?' => $this->_getParam('id')));
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                $postData = $this->preparingData($form->getValues(), 1, $row);
                $row->setFromArray($postData);
                $this->beforeSaving($row, $form->getValues(), 1);
                $row->save();
                $this->afterSaving($row, $form->getValues(), 1);
                $this->log->create($this->_usuario->id, $this->getMessages('update', $row->toArray()));
                $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array());
            }
        } else {
            $form->populate($this->fillsEdit($row->toArray()));

        }
        $this->view->form = $form;
        $this->view->title = $this->getTitle();
        $this->view->row = $row;
        if($this->_getParam('id')==2){
          $this->renderScript($this->getViewRenderUpdate());
        }else{
          $this->renderScript($this->getViewRenderUpdate());
        }
    }

    public function precosAction() {
      $configsModel = new Application_Model_Configs();
      $this->configs = $configsModel->getAllToArray();
      //var_dump($this->configs);

      $this->view->campos = array(
        'precos_mercado_titulo1'=>'Título 1',
        'precos_mercado_valor1'=>'Valor 1',
        'precos_mercado_complemento1'=>'Complemento 1',
        'precos_mercado_titulo2'=>'Título 2',
        'precos_mercado_valor2'=>'Valor 2',
        'precos_mercado_complemento2'=>'Complemento 2',
        'precos_mercado_titulo3'=>'Título 3',
        'precos_mercado_valor3'=>'Valor 3',
        'precos_mercado_complemento3'=>'Complemento 3',
        'precos_mercado_titulo4'=>'Título 4',
        'precos_mercado_valor4'=>'Valor 4',
        'precos_mercado_complemento4'=> 'Complemento 4',
        
          
        'tooltip_pesquisa'=>'Tooltip Pesquisa Básica',
        'tooltip_rev_processo'=>'Tooltip Revisão Mavip do Processo',
        'tooltip_rev_marca'=>'Tooltip Revisão Mavip da Logomarca',
        'tooltip_pedido'=>'Tooltip Pedido de Registro da Marca',
        'tooltip_diagnostico'=>'Tooltip Diagnóstico de Classificação',
        'tooltip_garantia'=>'Tooltip Garantia Mavip',
        'tooltip_acompanhamento'=>'Tooltip Acompanhamento',
        'tooltip_terceiros'=>'Tooltip Carta para Terceiros',
        'tooltip_consultor'=>'Tooltip Consultor (Vídeo Conferência)',
        'tooltip_titularidade'=>'Tooltip  Recurso Administrativo Simples',
        'tooltip_rec_administrativo_simples'=>'Tooltip  Recurso Administrativo Simples',
        
      );

      $form = new Application_Form_Gerenciador_Config(array('campos'=>$this->view->campos));
      if ($this->_request->isPost()) {
          if ($form->isValid($_POST)) {
              $dados = $form->getValues();
              foreach($this->view->campos as $cha => $valor){
                  $configsModel->save($cha,$dados[$cha]);
              }
              $this->log->create($this->_usuario->id, 'Alterou as configs');
              $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array());
          }
      } else {
        $populate = array();
        foreach ($this->view->campos as $key => $value) {
          $populate[$key] = $this->configs[$key];
        }
        $form->populate($populate);
      }


      $this->view->form = $form;
    }


    public function tooltipsAccountAction(){
       
        $configsModel = new Application_Model_Configs();
        $this->configs = $configsModel->getAllToArray();
  
        $this->view->campos = array(
            'cnpj'=>'Tooltip CNPJ / CPF',
            'rg'=>'Tooltip RG',
            'natureza_juridica'=>'Tooltip Natureza Jurídica',
            'login'=>'Tooltip Login',
            'razao_social'=>'Tooltip Razão Social',
          );
    
          $form = new Application_Form_Gerenciador_Config(array('campos'=>$this->view->campos));
      if ($this->_request->isPost()) {
      
          if ($form->isValid($_POST)) {
              $dados = $form->getValues();
              foreach($this->view->campos as $cha => $valor){
                  $configsModel->save($cha,$dados[$cha]);
              }
              $this->log->create($this->_usuario->id, 'Alterou as configs');
              $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'tooltips-account'));
          }
      } else {
        $populate = array();
        foreach ($this->view->campos as $key => $value) {
          $populate[$key] = $this->configs[$key];
        }
        $form->populate($populate);
      }


      $this->view->form = $form;
    }
    
}
