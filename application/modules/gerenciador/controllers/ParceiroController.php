<?php

class Gerenciador_ParceiroController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Convênios');
        $this->setMenu('clientes');
        $this->view->page = 'Adicionar/Atualizar';
        $this->setViewRenderNew('parceiro/new.phtml');
        $this->setViewRenderUpdate('parceiro/new.phtml');
        $this->setViewRenderList('parceiro/list.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Parceiro();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'nome' => array('label' => 'Nome'),
                    'categoria' => array('label' => 'Categoria'),
                    'hash' => array('label' => 'URL', 'type' => 'url'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Parceiro($isEditing);
        return $form;
    }

    /*protected function preparingData($postDate, $isEditing = 0, $row = null) {
        if (!$postDate["senha"]) {
            unset($postDate["senha"]);
        } else {
            $postDate["senha"] = SHA1($postDate["senha"]);
        }

        return $postDate;
    }*/

    public function getRecords() {
        $select = $this->db->select()
                ->from('parceiro', array('*'))
                ->joinLeft('parceiro_categoria', 'parceiro.parceiro_categoria_id = parceiro_categoria.id', array('parceiro_categoria.nome as categoria'))
                ->joinLeft('users', 'parceiro.id = users.id', array('users.name as nome'));
        return $this->db->fetchAll($select);
    }

    protected function beforeSaving($row, $formValues, $isEditing = 0) {
        $row = parent::beforeSaving($row, $formValues, $isEditing);

        $userDb = new Application_Model_DbTable_Users();
        
        if ($isEditing) {
            $user = $userDb->fetchRow(array('id = ?'=>$row['id']));
        } else {
            $user = $userDb->createRow();
            
            $user['role_id'] = 2;
        }
        $user['name'] = $formValues['nome'];
        $user['login'] = $formValues['login'];
        
        
        if ($formValues["senha"]) {
            $user["salt"] = SHA1(date('Ymdhisu') . mt_rand(1000, 9999));
            $user["password"] = SHA1($formValues["senha"] . $user["salt"]);
        }
        
        $user->save();
        $row['id'] = $user['id'];
        return $row;
    }
    
    protected function fillsEdit($dados) {
        $dados = parent::fillsEdit($dados);
        
        $select = $this->db->select()
                ->from('users')
                ->where('id = ?',$dados['id']);
        $user = $this->db->fetchRow($select);
        

        $dados['nome'] = $user['name'];
        $dados['login'] = $user['login'];

        return $dados;
    }

}
