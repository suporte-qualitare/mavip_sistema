<?php

class Gerenciador_ParceirocategoriaController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Convênios &nbsp; &nbsp;  >  &nbsp;  &nbsp;  Categorias');
        $this->view->page = 'Categorias';
        $this->view->action = 'Adicionar/Atualizar';
        $this->setViewRenderNew('parceirocategoria/new.phtml');
        $this->setViewRenderUpdate('parceirocategoria/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_ParceiroCategoria();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'nome' => array('label' => 'Nome'),
                    'comissao' => array('label' => 'Comissão'),
                    'desconto' => array('label' => 'Desconto (%)'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_ParceiroCategoria($isEditing);
        return $form;
    }

   

}

