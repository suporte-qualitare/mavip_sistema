<?php

class Gerenciador_PedidoController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {

        parent::init();
       //$this->pedidomarcastatus();
        $this->setTitle('Pedidos');
        $this->setOption(array('page' => 'Abertos'));

        if($this->getParam("status")=="concluido"){
            $this->setOption(array('page' => 'Concluídos'));
        }

        if($this->getParam("status")=="cancelado"){
            $this->setOption(array('page' => 'Cancelados'));
        }

        $this->setViewRenderView('pedido/view.phtml');
        $this->setViewRenderList('pedido/list.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Pedido();
    }

    public function getRecords() {
        $select = $this->db->select()
            ->from('pedido_has_marca', array('status', 'info_gru', 'gru_paga', 'novo_prazo', 'data_prazo'))
            ->join('marca', 'pedido_has_marca.marca_id = marca.id', array('marca.id as marcaid','marca.marca as marca', 'marca.processo as processo'))
            ->join('pedido', 'pedido_has_marca.pedido_id = pedido.id', array('pedido.id as id', '*'))
            ->join('cliente', 'pedido.cliente_id = cliente.id', array('cliente.nome as cliente'))
            ->join('marca_has_servico', 'marca.id = marca_has_servico.marca_id', array(''))
            ->join('servico', 'marca_has_servico.servico_id = servico.id', array('servico.titulo as servico'))
            ->order('pedido.id desc');

        if($this->getParam("status")=="cancelado"){
            $select->where("status = 'cancelado' or status = 'perdeu'");
        }elseif($this->getParam("status")=="concluido"){
            $select->where("status = 'concluido'");
        }else{
            $select->where("status  = 'novo'");
        }

        return $this->db->fetchAll($select);
    }

    public function getColumns() {
        return
            array(
                'id' => array('label' => 'Pedido', 'style' => 'width:3%;'),
                'cliente' => array('label' => 'Cliente', 'style' => 'width:15%;'),
                'marca' => array('label' => 'Marca', 'style' => 'width:15%;'),
                'processo' => array('label' => 'Processo', 'style' => 'width:10%;'),
                'servico' => array('label' => 'Serviço', 'style' => 'width:15%;'),
                'total' => array('label' => 'Valor pago', 'style' => 'width:10%;'),
                'status' => array('label' => 'Status', 'style' => 'width:6%;'),
                'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date'),
            );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Categoria($isEditing);
        return $form;
    }

    public function viewAction() {

        if(!$this->_getParam('marca'))
            return false;

        $select = $this->db->select()
            ->from('pedido_has_marca', array('status', 'info_gru', 'gru_paga', 'novo_prazo', 'data_prazo'))
            ->joinLeft('marca', 'pedido_has_marca.marca_id = marca.id',
                array('marca.id as marca_id_', 'marca.marca', 'marca.observacao as marca_obs', 'marca.logo',  'marca.urgencia as urgencia' , 'marca.processo', 'marca.estilo', 'marca.publicacao_inpi', 'marca.acompanhamento', 'marca.data_acompanhamento', 'marca.tipo',
                    'marca.nosso_numero'))
            ->joinLeft('pedido', 'pedido_has_marca.pedido_id = pedido.id', array('pedido.id as id', '*'))
            ->joinLeft('cliente', 'pedido.cliente_id = cliente.id', array('cliente.nome as cliente'))
            ->joinLeft('marca_has_servico', 'marca.id = marca_has_servico.marca_id', array(''))
            ->joinLeft('servico', 'marca_has_servico.servico_id = servico.id', array('servico.id as servico_id', 'servico.titulo as servico'))
            ->joinLeft('categoria', 'marca.categoria_id = categoria.id', array('categoria.id_inpi', 'categoria.nome as categoria'))
            ->where('pedido_has_marca.marca_id = ?', $this->_getParam('marca'))
            ->order('pedido.id desc');

        $data = $this->db->fetchAll($select);
        $this->view->rows = $data;

        $select2 = $this->db->select()
            ->from('pedido', array())
            ->join('cliente', 'pedido.cliente_id = cliente.id', array('cliente.id as cliente_id', '*'))
            ->where('pedido.id = ?', $this->_getParam('id'));
        $this->view->cliente = $this->db->fetchRow($select2);

        $this->view->title = "Pedido ";
        $this->view->action = "Visualizar";
        $this->renderScript($this->getViewRenderView());
    }

    public function pagarAction() {
        if ($this->_getParam('id')) {
            $pedidoMapper = new Application_Model_PedidoHasMarca();
            $pedidoMapper->confirma($this->_getParam('id'));

            $this->log->create($this->_usuario->id, 'pagou pagou o pedido de ID: ' . $this->_getParam('id'));

            $this->addFlashMessage(array('Baixa realizada com sucesso', 1), array('action' => 'list'));
        }
        $this->addFlashMessage(array('Erro ao realizar baixar', 1), array('action' => 'list'));
    }

    public function cancelarAction() {

        if ($this->_getParam('marca')) {
            $pedidoMapper = new Application_Model_PedidoHasMarca();
            $pedidoMapper->cancela(sha1($this->_getParam('marca')));

            $this->log->create($this->_usuario->id, 'cancelou o pedido da marca de ID: ' . $this->_getParam('marca'));

            $this->addFlashMessage(array('Cancelamento realizada com sucesso', 1), array('action' => 'list'));
        }
        $this->addFlashMessage(array('Erro ao realizar cancelar', 1), array('action' => 'list'));
    }

    public function pedidomarcastatus(){

        $modelPedido = new Application_Model_Pedido();
        $modelPedHasMarca = new Application_Model_PedidoHasMarca();

        $pedidos = $modelPedido->getAll();

        foreach ($pedidos as $pedido){
           $modelPedHasMarca->pedidoMarcaStatus($pedido);
        }

        echo "Finalizado com sucesso"; exit;
    }

}
