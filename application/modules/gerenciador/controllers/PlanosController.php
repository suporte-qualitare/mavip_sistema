<?php

class Gerenciador_PlanosController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Planos');
        $this->setViewRenderNew('planos/new.phtml');
        $this->setViewRenderUpdate('planos/new.phtml');
        //$this->setViewRenderList('textobotao/list.phtml');
    }

    
    public function listAction() {
        $modelPlanos = new Application_Model_Planos();
        $this->view->title = 'Planos';
        $this->view->grid = array('columns' => $this->getColumns(),
                                  'records' => $modelPlanos->getAll());
    }

    public function getRepository() {
        return new Application_Model_DbTable_Planos();
    }

    public function getColumns() {
        return
                array(
                    'ordem'   =>  array('label' => 'Ordem'),   
                    'titulo' => array('label' => 'Título exibido'),
                    'servico'  => array('label' => 'Serviço'),
        );
    }

    public function getForm($isEditing = 0) {
        $modelItens = new Application_Model_Itens();
        $planosItensDb = new Application_Model_DbTable_PlanosItens();

        $select = $this->db->select()->from("planos_itens")
            ->where('plano_id = ?', $this->_getParam('id'));
   
        $this->view->planoItens = $this->db->fetchAll($select);
        $this->view->itens = $modelItens->getAll();

        $form = new Application_Form_Gerenciador_Planos($isEditing);
        return $form;
    }

    protected function beforeSaving($row, $formValues, $isEditing = 0) {
        $row = parent::beforeSaving($row, $formValues, $isEditing);

        $this->db->delete('planos_itens', array(
            'plano_id = ?' => $row['id'],
        ));

        $planosItensDb = new Application_Model_DbTable_PlanosItens();
        $itens = $_POST['plano_itens'];
        foreach($itens as $item){
            $rowP = $planosItensDb->createRow();
            $rowP->plano_id = $row['id'];
            $rowP->item_id = $item['item_id'];
            $rowP->save();
        }

        return $row;
    }

}
