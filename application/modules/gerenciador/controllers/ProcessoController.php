<?php

class Gerenciador_ProcessoController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
       // $this->pedidomarca();
        $this->setTitle('Processo');
        $this->setMenu('processo');
        $this->setViewRenderNew('processo/new.phtml');
        $this->setViewRenderUpdate('processo/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Processo();
    }

    public function getRecords() {

        if ($this->getParam("operacao")) {
            $action = 'concluir';
            $controller = 'operacao';
            $module = NULL;
            $params = array(
                'id' => $this->getParam("operacao"),
                'marca' => $this->getParam('marca')
            );
        } else if ($this->getParam("pedido")) {
            $action = 'view';
            $controller = 'pedido';
            $module = NULL;
            $params = array(
                'id' => $this->getParam("pedido"),
                'marca' => $this->getParam('marca')
            );
        }else if($this->getParam("id")){
            $action = 'view';
            $controller = 'pedido';
            $module = NULL;
            $params = array(
                'id' => $this->getParam("id"),
                'marca' => $this->getParam('marca')
            );
        }

        if($this->getParam("view") === 'marca'){
            $action = 'view';
            $controller = 'marca';
            $module = NULL;
            $params = array(
                'id' => $this->getParam('marca')
            );
        }

        $this->_helper->redirector($action, $controller, $module, $params);

    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'id_pedido' => array('label' => 'Pedido'),
                    'id_marca' => array('label' => 'Marca'),
                    'descricao' => array('label' => 'Descrição'),
                    'data_cadastro' => array('label' => 'Data de cadastro', 'type' => 'date')
        );
    }

    public function getForm($isEditing = 0) {
        
        if ($this->getParam("operacao")) {
            $id_pedido = $this->getParam("operacao");
        } else if ($this->getParam("pedido")) {
            $id_pedido = $this->getParam("pedido");
        }

        $id_marca =  $this->getParam('marca');

        $dbMarca = new Application_Model_DbTable_Marca();
        $row = $dbMarca->fetchRow(array('id = ?' => $id_marca));
        $this->view->marca = $row['marca'];
        
        $form = new Application_Form_Gerenciador_Processo($isEditing, null, $id_pedido, $id_marca);
        return $form;
    }

    public function arrayFiles() {
        return array(
            array('col' => 'arquivo', 'width' => '310', 'height' => '350', 'type' => 'crop'),
        );
    }

    public function pedidomarca(){

        try{
            $processoDb = new Application_Model_DbTable_Processo();
            $rows = $processoDb->fetchAll();

            $dbPedidoHasMarca =  new Application_Model_DbTable_PedidoHasMarca();
            foreach ($rows as $row) {
                $rowPhm = $dbPedidoHasMarca->fetchRow(array('pedido_id = ?' => $row['id_pedido']));
                $row['id_marca'] = $rowPhm['marca_id'];
                $row->save();
            }
        }
        catch (Exception $e){
            echo $e->getMessage();
            exit;
        }
        exit('FINALIZADO');
    }

}
