<?php

class Gerenciador_ServicoController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Serviços');
        $this->setMenu('servico');
        
        $this->setViewRenderUpdate('servico/new.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Servico();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'titulo' => array('label' => 'titulo'),
                    'valor' => array('label' => 'Valor'),
                    'acompanhamento' => array('label' => 'Acompanhamento', 'type' => 'boolean'),
                    'ordem' => array('label' => 'Ordem'),
                    
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Servico($isEditing);
        return $form;
    }
    
    
  

}

