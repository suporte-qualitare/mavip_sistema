<?php

class Gerenciador_TelevendaController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Televenda');
        $this->setMenu('blog');
        $this->setViewRenderNew('televenda/new.phtml');
        $this->setViewRenderUpdate('televenda/new.phtml');
        //$this->setViewRenderList('textobotao/list.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Televenda();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'texto' => array('label' => 'Texto'),
                    'telefone' => array('label' => 'Telefone')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Televenda($isEditing);
        return $form;
    }

}
