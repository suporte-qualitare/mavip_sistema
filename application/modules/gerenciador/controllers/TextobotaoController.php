<?php

class Gerenciador_TextobotaoController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Texto Botão');
        $this->setMenu('blog');
        $this->setViewRenderNew('textobotao/new.phtml');
        $this->setViewRenderUpdate('textobotao/new.phtml');
        //$this->setViewRenderList('textobotao/list.phtml');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Textobotao();
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'texto' => array('label' => 'Nome')                    
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Textobotao($isEditing);
        return $form;
    }

}
