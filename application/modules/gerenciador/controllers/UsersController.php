<?php

class Gerenciador_UsersController extends Abstract_Gerenciador_Controller_CrudController {

    public function init() {
        parent::init();
        $this->setTitle('Usuários');
        $this->setDeleteFinal(0);
        $this->setWhere('active >= 0');
        $this->setViewRenderNew('users/new.phtml');
        $this->setViewRenderView('users/view.phtml');
        $this->setViewRenderUpdate('users/new.phtml');
        $this->setMenu('users');
    }

    public function getRepository() {
        return new Application_Model_DbTable_Users();
    }
    
    public function getRecords() {
        $select = $this->db->select()->from('users')
                ->joinLeft('acl_roles', 'users.role_id = acl_roles.id', array('acl_roles.name as role'));
        return $this->db->fetchAll($select);
    }

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'name' => array('label' => 'Nome'),
                    'login' => array('label' => 'Login'),
                    'role' => array('label' => 'Tipo')
        );
    }

    public function getForm($isEditing = 0) {
        $form = new Application_Form_Gerenciador_Usuarios($isEditing);
        return $form;
    }

    protected function preparingData($postDate, $isEditing = 0, $row = null) {
        if (!$postDate["password"]) {
            unset($postDate["password"]);
        } else {
            $postDate["salt"] = SHA1(date('Ymdhisu') . mt_rand(1000, 9999));
            $postDate["password"] = SHA1($postDate["password"] . $postDate["salt"]);
        }

        return $postDate;
    }

    public function viewAction() {
        $usurs = new Application_Model_Users();
        $this->view->row = $usurs->getRow($this->_getParam('id'));

        $this->view->title = $this->getTitle();
        $this->renderScript($this->getViewRenderView());
    }
    
    public function passwordAction() {
        
        $form = new Application_Form_Gerenciador_AlterarSenha();
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                try {
                    
                    $dados = $form->getValues();

                    $dados['id'] = $this->_usuario->id;
                    $userMapper = new Application_Model_Users();

                    if (!empty($dados["nova_senha"])) {
                        if ($dados["nova_senha"] != $dados["senha_confirm"]) {
                            $this->addFlashMessage(array('A nova senha e a confirmação não estão iguais', 0));
                            $this->router->gotoRoute(array('controller' => 'users', 'action' => 'password'));
                        }
                    }


                    $retorno_senha = $userMapper->checkPassword($dados['id'], $dados['senha']);



                    if ($retorno_senha) {
                        $this->db->beginTransaction();

                        $retorno = $userMapper->updateSenha($dados);

                        $this->db->commit();
                        $this->addFlashMessage(array('Senha alterada com sucesso', 1));
                        $this->router->gotoRoute(array('controller' => 'users', 'action' => 'password'));
                    } else {
                        $this->addFlashMessageDirect(array('A senha atual digitada esta errada', 0));
                    }
                } catch (Exception $exc) {
                    echo $exc->getMessage();exit;
                    $this->db->rollBack();
                    $this->addFlashMessageDirect(array('Não foi possivel editar', 0));
                }
            } else {
                $this->addFlashMessageDirect(array('Verfique as informações', 0));
            }
        }


        $this->view->form = $form;
    }
    
   

}
