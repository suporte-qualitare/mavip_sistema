<?php

class Zend_View_Helper_ExibeMoeda {

    public function ExibeMoeda($valor) {
        return 'R$ ' . number_format($valor, 2, ',', '.');
    }

}

?>
