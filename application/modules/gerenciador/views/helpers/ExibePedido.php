<?php

class Zend_View_Helper_ExibePedido {

    public function exibePedido($pedido) {
        return str_pad($pedido, 12, '0', STR_PAD_LEFT);
    }

}

?>
