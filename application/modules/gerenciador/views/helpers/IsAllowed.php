<?php

/**
 *
 * @author max_holanda
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * ExibeAcao helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_IsAllowed {

    /**
     * @var Zend_View_Interface 
     */
    public $view;

    /**
     *  
     */
    public function isAllowed($area, $acao, $config = false) {
        if ($this->view->acl->has($area)) {
            return $this->view->acl->isAllowed($this->view->usuario->role_id, $area, $acao);
        } else {
            return false;
        }
    }

    /**
     * coloca o separador se preciso (/)
     */

    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

}
