<?php

class Zend_View_Helper_ShowMessenger {

    public function ShowMessenger($msg = null) {
        if(!$msg){
            return;
        }
        $retorno = "";


        foreach ($msg as $m) {

            switch ($m[1]) {
                case 0:
                    $classe = "alert-danger";
                    break;
                case 1:
                    $classe = "alert-success";
                    break;
                default:
                    $classe = "alert-info";
                    break;
            }
            $retorno .= '<div class="alert alert-block  ' . $classe . ' fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>' . $m[0] . '</div>';
        }

        return $retorno;
    }

}

?>
