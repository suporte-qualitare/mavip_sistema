'use strict';

// Main
(function (window, document, $) {
    'use strict';

    window.mod = {};

    $(function () {
        // Carregando os modulos
        window.mod.common = new window.mod.common();

        var bodyClasses = $('body').attr('class').split(' ');
        $.each(bodyClasses, function (key, val) {
            val = val.replace(/[-]/g, '');
            if (window.mod[val] !== undefined) {
                // console.log(key + ' => ' + val);
                window.mod[val] = new window.mod[val]();
            }
        });
    });

    $(".checkLoginInpi").click(function () {
        $(".login-inip").fadeToggle();
    });
})(window, document, jQuery);
'use strict';

// Common
window.mod.common = function () {

    // Scope
    var that = this;

    var init = function init() {
        console.log('[brz] begin common.js');
    };

    init();
};
'use strict';

// Contact
window.mod.contact = function () {

    // Scope
    var that = this;

    var username = '';

    var init = function init() {
        console.log('[brz] begin contact.js');
        initListeners();
    };

    var initListeners = function initListeners() {
        $('.Contact-next').click(function (e) {
            var step = $(this).data('step');
            if (step == 2) {
                username = $('input[name="nome"]').val();
                if (username == '') {
                    alert('Insira o seu nome');

                    return false;
                }
                console.log(username);
                $('.Contact-username').text(username);
            }
            $('.Contact-step').addClass('hidden');
            $('.Contact-step--' + step).removeClass('hidden');
        });


        var enviando_formulario = false;
        $('.Contact-form').submit(function () {
            var obj = this;
            var form = $(obj);

            var dados = new FormData(obj);
            var submit_btn = $('.submit_ajax');
            var submit_btn_text = submit_btn.text();
            function volta_submit() {
                submit_btn.removeAttr('disabled');
                submit_btn.text(submit_btn_text);
                enviando_formulario = false;
            }
            console.log(form.attr('action'));
            if (!enviando_formulario) {
                $.ajax({
                    // Antes do envio
                    beforeSend: function () {
                        enviando_formulario = true;
                        submit_btn.attr('disabled', true);
                        submit_btn.text('Enviando...');

                        // Remove o erro (se existir)
                        $('.error').remove();
                    },
                    url: form.attr('action'),
                    type: form.attr('method'),
                    data: dados,
                    processData: false,
                    cache: false,
                    contentType: false,
                    success: function (data) {

                        if (data.retorno == 'ok') {
                            form[0].reset();
                            submit_btn.text(data.msn);
                        } else {
                            volta_submit();
                            submit_btn.after('<p class="error">' + data.msn + '</p>');
                        }
                    },
                    error: function (request, status, error) {
                        volta_submit();
                        alert(request.responseText);
                    }
                });
            }

            return false;
        });


    };

    init();
};
'use strict';

// Home
window.mod.home = function () {

    // Scope
    var that = this;

    var owl;

    var init = function init() {
        console.log('[brz] begin home.js');
        initInviewListener();
        initCarousel();
    };

    var initInviewListener = function initInviewListener() {
        $('.Achievements-numberSpan').one('inview', function (event, isInView) {
            if (isInView) {
                // element is now visible in the viewport
                $(this).countTo({
                    from: 0,
                    to: $(this).data('number'),
                    speed: 1000,
                    refreshInterval: 50
                });
            } else {
                // element has gone out of viewport
            }
        });
    };

    var initCarousel = function initCarousel() {



        $('#carousel').owlCarousel({
            // navigation : false, // Show next and prev buttons
            items: 1,
            itemsCustom: false,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [980, 1],
            itemsTablet: [768, 1],
            itemsTabletSmall: false,
            itemsMobile: [479, 1],
            singleItem: false,
            itemsScaleUp: false,
            slideSpeed: 300,
            loop: true,
        });

        owl = $('#carousel').data('owlCarousel');

        $('.Testimonial-arrow--prev').click(function () {
            owl.prev();
        });

        $('.Testimonial-arrow--next').click(function () {
            owl.next();
        });
    };

    init();
};
'use strict';

//Carousel Sponsors
$(document).ready(function () {


    $(".Sponsors").owlCarousel({
        autoPlay: 1000, //Set AutoPlay to 3 seconds
        slideSpeed: 300,
        paginationSpeed: 400,
        dots: false,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 4
            }
        }

    });

    var owl2 = $('.Sponsors').data('owlCarousel');

    $('.sponsors-arrow--prev').click(function () {
        owl2.prev();
    });

    $('.sponsors-arrow--next').click(function () {
        owl2.next();
    });

});

//Carousel DepoimentosFace
$(document).ready(function () {


    $(".DepoimentosFace").owlCarousel({
        autoPlay: 1000, //Set AutoPlay to 3 seconds
        slideSpeed: 300,
        paginationSpeed: 400,
        dots: true,
        dotsEach: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            992: {
                items: 1
            }
        }

    });

    var owl2 = $('.DepoimentosFace').data('owlCarousel');

    $('.face-arrow--prev').click(function () {
        owl2.prev();
    });

    $('.face-arrow--next').click(function () {
        owl2.next();
    });

});


//Carousel Banner Rotativo
$(document).ready(function () {


    $(".bannerotativo").owlCarousel({
        autoPlay: 1000, //Set AutoPlay to 3 seconds
        slideSpeed: 300,
        paginationSpeed: 400,
        dots: true,
        dotsEach: true,
        nav: false,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            992: {
                items: 1
            }
        }

    });

    var owl2 = $('.bannerotativo').data('owlCarousel');

    $('.bannerotativo-arrow--prev').click(function () {
        owl2.prev();
    });

    $('.bannerotativo-arrow--next').click(function () {
        owl2.next();
    });

});

// Home 2
window.mod.home2 = function () {

    // Scope
    var that = this;

    var owl;

    var init = function init() {
        console.log('[brz] begin home2.js');
        owl = [];
        initCarousel();
        initFormListeners();




    };



    var initCarousel = function initCarousel() {

        var options = {
            slideSpeed: 300,
            paginationSpeed: 400,
            mouseDrag: false,
            pagination: false,
            loop: true,
            margin: 10,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 3
                },
                1350: {
                    items: 5
                }
            },
            itemsCustom: [[0, 1], [320, 1], [760, 3], [1400, 5]]
                    // items : 5, //10 items above 1000px browser width
                    // itemsDesktop : [1100,3], //5 items between 1000px and 901px
                    // itemsDesktopSmall : [900,3], // betweem 900px and 601px
                    // itemsTablet: [600,1], //2 items between 600 and 0
                    // itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
        };
        $('#carousel1').owlCarousel(options);
        $('#carousel2').owlCarousel(options);
        $('#carousel3').owlCarousel(options);

        owl.push($('#carousel1').data('owlCarousel'));
        owl.push($('#carousel2').data('owlCarousel'));
        owl.push($('#carousel3').data('owlCarousel'));

        $('.Category-arrow--prev').click(function () {
            var reference = $(this).data('reference');
            owl[reference].prev();
        });

        $('.Category-arrow--next').click(function () {
            var reference = $(this).data('reference');
            owl[reference].next();
        });

        $('.sessao-comercio').hide();
        $('.sessao-produto').hide();
        $('.sessao-servico').hide();

    };

    var initFormListeners = function initFormListeners() {

        $(".HomeForm").submit(function () {
            if (!$('input[name="estilo"]:checked').val()) {
                blurt('selecione o estilo da marca', '', 'error');
                return false;
            }

            console.log($('input[name="tipo"]:checked').val());

            if ($('input[name="tipo"]:checked').val() == 'produto') {
                if ($('input[name="categoria_produto"]:checked').length != '1') {
                    blurt('selecione a categoria do produto', '', 'error');
                    return false;
                }
            } else if ($('input[name="tipo"]:checked').val() == 'servico') {
                if ($('input[name="categoria_servico"]:checked').length != '1') {
                    blurt('selecione a categoria do serviço', '', 'error');
                    return false;
                }
            } else if ($('input[name="tipo"]:checked').val() == 'comercio') {
                if ($('input[name="categoria_comercio"]:checked').length != '1') {
                    blurt('selecione a categoria do comércio', '', 'error');
                    return false;
                }
            } else {
                blurt('selecione o tipo da marca', '', 'error');
                return false;
            }

            $(".uil-facebook-css").fadeToggle();
            $(".Button--home2").css("opacity", "0");

            $(".Icon.Icon--magnifierAfter:after").css("display", "none");
            return true;

        });
        $('input[name="tipo"]').change(function () {
            if ($('input[name="tipo"]:checked').val() == 'servico') {
                $('.sessao-produto').hide();
                $('.sessao-comercio').hide();
                $('.sessao-servico').show();
            } else if ($('input[name="tipo"]:checked').val() == 'produto') {
                $('.sessao-produto').show();
                $('.sessao-comercio').hide();
                $('.sessao-servico').hide();
            } else if ($('input[name="tipo"]:checked').val() == 'comercio') {
                $('.sessao-produto').hide();
                $('.sessao-comercio').show();
                $('.sessao-servico').hide();
            } else {
                $('.sessao-produto').hide();
                $('.sessao-servico').hide();
                $('.sessao-comercio').hide();
            }
        });




        $('.ChangeData').click(function (e) {
            $('.ChangeData-input').removeClass('hidden');
            $('.SubSection--userData > .Page-text').addClass('hidden');
            $(this).addClass('hidden');
        });
    };

    init();
};
'use strict';

// Order
window.mod.order = function () {

    // Scope
    var that = this;
    var data = [];

    var init = function init() {
        console.log('[brz] begin order.js');
        initInput();
        initAddListener();
    };

    var initInput = function initInput() {
        //data.push({description: 'Serviço de registro', price: valorRegistro});
        //data.push({description: 'Taxa do Guia de Recolhimento da União (GRU)', price: valorGru});
    };

    var initAddListener = function initAddListener() {


        $('.check-termo').click(function () {
            if (!$('input[name="terms1"]:checked').length) {
                $(".terms1.alert-campos").fadeToggle();
                setTimeout(function () {
                    $(".terms1.alert-campos").fadeToggle();
                }, 6000);
                return false;
            }
        });
        $('.Service-button').click(function (e) {
            var elemento = $(this);
            var price = elemento.data('price');
            var description = elemento.data('description');
            var service = elemento.data('service');
            $.ajax({
                type: "POST",
                url: baseUrl + "/order/add-cart",
                data: {acompanhamento: service}
            }).done(function (j) {
                data.push({description: description, price: price});
                var template = $('#orderItemTemplate').html();
                template = template.replace('{{description}}', description);
                template = template.replace('{{price}}', 'R$ ' + formatReal(price));
                $('.Order-items').append(template);
                var subtotal = 'R$ ' + formatReal(j.subtotal);
                var total = 'R$ ' + formatReal(j.total);
                var desconto = 'R$ ' + formatReal(j.desconto);
                $('.Total-value').text(total);
                $('.Subtotal-value').text(subtotal);
                $('.Total-desconto-value').text(desconto);
                $('.add-services-text').hide();

                elemento.parents('li.Service').remove();
            });




        });

        $('.service-selecionado').click();
    };

    var calcSubtotal = function calcSubtotal() {
        var subtotal = 0;
        for (var i = 0; i < data.length; i++) {
            console.log(data[i].price);
            subtotal += data[i].price;
        }
        return Math.ceil(subtotal);
    };

    var formatReal = function formatReal(int) {
        var tmp = int + '';
        tmp = tmp.replace(/([0-9]{2})$/g, ',$1');
        if (tmp.length > 6)
            tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2');

        return tmp;
    };

    init();
};
'use strict';

// Register
window.mod.register = function () {

    // Scope
    var that = this;

    var init = function init() {
        console.log('[brz] begin register.js');
        initButtonListeners();
    };
    var loginValido = false;
    var initButtonListeners = function initButtonListeners() {
        $('.Next-button').click(function (e) {
            var option = $(this).data('step');

            switch (option) {
                case 1:
                    if (validaStep1()) {
                        $('#step1').addClass('hidden');
                        $('#step2').removeClass('hidden');
                        $('#step3').addClass('hidden');
                    }
                    break;
                case 2:
                    $('#step1').addClass('hidden');
                    $('#step2').addClass('hidden');
                    $('#step3').removeClass('hidden');
                    break;
            }
        });

        $('#form_register').submit(function (e) {
            if (validaStep1() && validaStep2()) {
                return true;
            }
            return false;
        });


        $('#pais').change(function () {
            if ($(this).val() == '26') {
                $('.row_estado').show();
            } else {
                $('.row_estado').hide();
            }

        });

        $('#natureza').change(function () {
            if ($(this).val() == '2') {
                console.log('fisica')
                $('.label_cnpj').hide();
                $('.label_cpf').show();
                $("#cnpj").mask("999.999.999-99");
                $(".ctn-atividade").show();
            } else {
                $('.label_cnpj').show();
                $('.label_cpf').hide();
                $("#cnpj").mask("99.999.999/9999-99");
                $(".ctn-atividade").hide();
            }
            if ($(this).val() == '1' || $(this).val() == '2'){
                $('.label_rg').show();
                console.log('log rg');
            }else{
                $('.label_rg').hide();
            }

        });



        var xhr;
        $('#login').keyup(function () {
            var login = $(this).val();
            if (xhr && xhr.readyState != 4) {
                xhr.abort();
            }
            xhr = $.ajax({
                type: "POST",
                url: baseUrl + "/account/valida-login",
                data: {login: login}
            }).done(function (j) {
                if (j.valid) {

                    loginValido = true;
                } else {
                    loginValido = false;

                }
            });
        });
        var cidadeCod; 
        $('#estado').change(function () {
            if ($(this).val()) {
                $('#cidade').html('<option value="">Carregando...</option>').show();
                var estado = $(this).val();
                $.ajax({
                    type: "POST",
                    url: baseUrl + "/account/busca-cidades",
                    data: {estado: estado}
                }).done(function (j) {
                    var options = '';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                    }
                    $('#cidade').html(options).show();

                    $('#cidade').val(cidadeCod);
                });

            } else {
                $('#cidade').html('<option value="">– Escolha um estado –</option>');
            }
        });

        $('#cep').blur(function () {

            if ($(this).val().length >= 9) {
                var cep = $(this).val();

                $.ajax({
                    type: "POST",
                    url: baseUrl + "/account/retorna-endereco",
                    data: "cep=" + cep,
                    success: function (msg) {



                        $('#endereco').val(msg[0].endereco);
                        $('#estado').val(msg[0].estado);
                        $('#bairro').val(msg[0].bairro);
                        $('#estado').change();
                        cidadeCod = msg[0].cidade;
                        //$('#estado').change();
                        $('#cidade').val(msg[0].cidade);

                    }
                });
            }
        });

    };

    var validaStep1 = function validaStep1() {



        if ($("#nome").val() == "") {
            $(".nome.alert-campos").fadeToggle();
            $(".nome.radio-red").fadeToggle();
            $("#nome").addClass("alert-bord");
            $("#nome").focus();

            setTimeout(function () {
                $(".nome.alert-campos").fadeToggle();
                $(".nome.radio-red").fadeToggle();
                $("#nome").removeClass("alert-bord");
            }, 6000);

            return false;
        } else if ($("#email").val() == "") {
            $(".email.alert-campos").fadeToggle();
            $(".email.radio-red").fadeToggle();
            $("#email").addClass("alert-bord");
            $("#email").focus();

            setTimeout(function () {
                $(".email.alert-campos").fadeToggle();
                $(".email.radio-red").fadeToggle();
                $("#email").removeClass("alert-bord");
            }, 6000);

            return false;
        } else if ($("#login").val() == "") {
            $(".login.alert-campos").fadeToggle();
            $(".login.radio-red").fadeToggle();
            $("#login").addClass("alert-bord");

            $("#login").focus();

            setTimeout(function () {
                $(".login.alert-campos").fadeToggle();
                $(".login.radio-red").fadeToggle();
                $("#login").removeClass("alert-bord");
            }, 6000);

            return false;
        } else if (!loginValido) {

            $(".logininvalido.alert-campos").fadeToggle();
            $(".logininvalido.radio-red").fadeToggle();
            $("#logininvalido").addClass("alert-bord");
            $("#logininvalido").focus();

            setTimeout(function () {
                $(".logininvalido.alert-campos").fadeToggle();
                $(".logininvalido.radio-red").fadeToggle();
                $("#logininvalido").removeClass("alert-bord");
            }, 6000);
            return false;
        } else if ($("#senha").val() == "" || $("#senha_confirm").val() == "" || $("#senha").val() != $("#senha_confirm").val()) {
            $(".senha.alert-campos").fadeToggle();
            $(".senha.radio-red").fadeToggle();
            $("#senha").addClass("alert-bord");

            $(".confirmar-senha.alert-campos").fadeToggle();
            $(".confirmar-senha.radio-red").fadeToggle();
            $("#confirmar-senha").addClass("alert-bord");

            $("#senha").focus();

            setTimeout(function () {
                $(".senha.alert-campos").fadeToggle();
                $(".senha.radio-red").fadeToggle();
                $("#senha").removeClass("alert-bord");

                $(".confirmar-senha.alert-campos").fadeToggle();
                $(".confirmar-senha.radio-red").fadeToggle();
                $("#confirmar-senha").removeClass("alert-bord");

            }, 6000);

            return false;
        } else if ($("#pergunta").val() == "") {
            $(".pergunta.alert-campos").fadeToggle();
            $(".pergunta.radio-red").fadeToggle();
            $("#pergunta").addClass("alert-bord");

            $("#pergunta").focus();

            setTimeout(function () {
                $(".pergunta.alert-campos").fadeToggle();
                $(".pergunta.radio-red").fadeToggle();
                $("#pergunta").removeClass("alert-bord");
            }, 6000);

            return false;
        } else if ($("#resposta").val() == "") {
            $(".perg-secreta.alert-campos").fadeToggle();
            $(".perg-secreta.radio-red").fadeToggle();
            $("#perg-secreta").addClass("alert-bord");

            $("#resposta").focus();
            setTimeout(function () {
                $(".perg-secreta.alert-campos").fadeToggle();
                $(".perg-secreta.radio-red").fadeToggle();
                $("#perg-secreta").removeClass("alert-bord");
            }, 6000);

            return false;
        } else if (!$('input[name="terms1"]:checked').length) {
            $(".terms1.alert-campos").fadeToggle();
            setTimeout(function () {
                $(".terms1.alert-campos").fadeToggle();
            }, 6000);
            return false;
        } else if (!$('input[name="terms2"]:checked').length) {
            $(".terms1.alert-campos").fadeToggle();
            setTimeout(function () {
                $(".terms1.alert-campos").fadeToggle();
            }, 6000);
            return false;
        }



        return true;
    }

    var validaStep2 = function validaStep2() {

        if ($("#natureza").val() == "") {
            $(".natureza.alert-campos").fadeToggle();
            $("#natureza").addClass("alert-bord");

            $("#naturez").focus();

            setTimeout(function () {
                $(".natureza.alert-campos").fadeToggle();
                $("#natureza").removeClass("alert-bord");
            }, 6000);

            return false;
        } else if ($("#cnpj").val() == "") {
            $(".cnpj.alert-campos").fadeToggle();
            $(".cnpj.radio-red").fadeToggle();
            $("#cnpj").addClass("alert-bord");

            $("#cnpj").focus();

            setTimeout(function () {
                $(".cnpj.alert-campos").fadeToggle();
                $(".cnpj.radio-red").fadeToggle();
                $("#cnpj").removeClass("alert-bord");
            }, 6000);

            return false;
        } else if ($("#razao").val() == "") {
            $(".razao.alert-campos").fadeToggle();
            $(".razao.radio-red").fadeToggle();
            $("#razao").addClass("alert-bord");

            $("#razao").focus();

            setTimeout(function () {
                $(".razao.alert-campos").fadeToggle();
                $(".razao.radio-red").fadeToggle();
                $("#razao").removeClass("alert-bord");
            }, 6000);

            return false;
        } else if ($("#pais").val() == "") {
            $(".pais.alert-campos").fadeToggle();
            $("#pais").addClass("alert-bord");
            $("#pais").focus();

            setTimeout(function () {
                $(".pais.alert-campos").fadeToggle();
                $("#pais").removeClass("alert-bord");
            }, 6000);

            return false;
        } else if ($("#estado").val() == "") {
            $(".estado.alert-campos").fadeToggle();
            $("#estado").addClass("alert-bord");

            setTimeout(function () {
                $(".estado.alert-campos").fadeToggle();
                $("#estado").removeClass("alert-bord");
            }, 6000);

            $("#estado").focus();

            return false;
        } else if ($("#cidade").val() == "") {
            $(".cidade.alert-campos").fadeToggle();
            $("#cidade").addClass("alert-bord");

            setTimeout(function () {
                $(".cidade.alert-campos").fadeToggle();
                $("#cidade").removeClass("alert-bord");
            }, 3000);

            $("#cidade").focus();

            return false;
        } else if ($("#endereco").val() == "") {
            $(".endereco.alert-campos").fadeToggle();
            $(".endereco.radio-red").fadeToggle();
            $("#endereco").addClass("alert-bord");

            setTimeout(function () {
                $(".endereco.alert-campos").fadeToggle();
                $(".endereco.radio-red").fadeToggle();
                $("#endereco").removeClass("alert-bord");
            }, 3000);

            $("#endereco").focus();

            return false;
        } else if ($("#cep").val() == "") {
            $(".cep.alert-campos").fadeToggle();
            $(".cep.radio-red").fadeToggle();
            $("#cep").addClass("alert-bord");

            setTimeout(function () {
                $(".cep.alert-campos").fadeToggle();
                $(".cep.radio-red").fadeToggle();
                $("#cep").removeClass("alert-bord");
            }, 3000);

            $("#cep").focus();

            return false;
        } /*else if ($("#telefone").val() == "") {
         $(".telefone.alert-campos").fadeToggle();
         $(".telefone.radio-red").fadeToggle();
         $("#telefone").addClass("alert-bord");
         
         setTimeout(function () {
         $(".telefone.alert-campos").fadeToggle();
         $(".telefone.radio-red").fadeToggle();
         $("#telefone").removeClass("alert-bord");
         }, 3000);
         
         $("#telefone").focus();
         
         return false;
         } else if ($("#celular").val() == "") {
         $(".celular.alert-campos").fadeToggle();
         $(".celular.radio-red").fadeToggle();
         $("#celular").addClass("alert-bord");
         
         setTimeout(function () {
         $(".celular.alert-campos").fadeToggle();
         $(".celular.radio-red").fadeToggle();
         $("#celular").removeClass("alert-bord");
         }, 3000);
         
         $("#celular").focus();
         
         return false;
         } else if ($("#fax").val() == "") {
         $(".fax.alert-campos").fadeToggle();
         $(".fax.radio-red").fadeToggle();
         $("#fax").addClass("alert-bord");
         
         setTimeout(function () {
         $(".fax.alert-campos").fadeToggle();
         $(".fax.radio-red").fadeToggle();
         $("#fax").removeClass("alert-bord");
         }, 3000);
         
         $("#fax").focus();
         
         return false;
         }*/
        return true;
    }



    init();
};
'use strict';


// edit_account
window.mod.edit_account = function () {

    // Scope
    var that = this;

    var init = function init() {
        console.log('[brz] begin edit_account.js');
        initButtonListeners();
    };

    var initButtonListeners = function initButtonListeners() {


        $('#estado').change(function () {
            if ($(this).val()) {
                $('#cidade').html('<option value="">Carregando...</option>').show();
                var estado = $(this).val();
                $.ajax({
                    type: "POST",
                    url: baseUrl + "/account/busca-cidades",
                    data: {estado: estado}
                }).done(function (j) {
                    var options = '';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                    }
                    $('#cidade').html(options).show();

                    $('#cidade').val(cidadeCod);
                });

            } else {
                $('#cidade').html('<option value="">– Escolha um estado –</option>');
            }
        });

    };

    init();
};
'use strict';

// Result
window.mod.result = function () {

    // Scope
    var that = this;

    var init = function init() {
        console.log('[brz] begin result.js');
        initAnimations();


    };




    var initAnimations = function initAnimations() {
        var animationNumber = $('.Gauge').data('animation');
        var pointerClass = 'Gauge-pointer--animation' + animationNumber;
        var scoreClass = 'Gauge-score--color' + animationNumber;
        console.log(pointerClass, scoreClass);
        $('.Gauge-pointer').addClass(pointerClass);
        $('.Gauge-score').addClass(scoreClass);
        $('.Gauge-scoreNumber').countTo({
            from: 0,
            to: $('.Gauge-scoreNumber').data('number'),
            speed: 1000,
            refreshInterval: 50
        });
    };

    init();
};
'use strict';

// Common
window.mod.service_panel = function () {

    // Scope
    var that = this;

    var init = function init() {
        console.log('[brz] begin service_panel.js');
        initButtonListener();
        hideDetails();
    };

    var initButtonListener = function initButtonListener() {
        $('.ServiceItem-toggleDetails').click(function (e) {
            id = $(this).data('id');
            $('#marca_'+id).toggle('slow');
            var btnText = $(this);
            if (btnText.hasClass('bt-ativo')) {
                btnText.text('Ocultar detalhes').removeClass('bt-ativo');

            } else {
                btnText.text('Mostrar detalhes').addClass('bt-ativo');
            }
        });

        //$('.ServiceItem-toggleDetails').click();
    };

    var hideDetails = function hideDetails() {};

    init();
};
'use strict';

(function ($) {
    $.fn.countTo = function (options) {
        // merge the default plugin settings with the custom options
        options = $.extend({}, $.fn.countTo.defaults, options || {});

        // how many times to update the value, and how much to increment the value on each update
        var loops = Math.ceil(options.speed / options.refreshInterval),
                increment = (options.to - options.from) / loops;

        return $(this).each(function () {
            var _this = this,
                    loopCount = 0,
                    value = options.from,
                    interval = setInterval(updateTimer, options.refreshInterval);

            function updateTimer() {
                value += increment;
                loopCount++;
                $(_this).html(value.toFixed(options.decimals));

                if (typeof options.onUpdate == 'function') {
                    options.onUpdate.call(_this, value);
                }

                if (loopCount >= loops) {
                    clearInterval(interval);
                    value = options.to;

                    if (typeof options.onComplete == 'function') {
                        options.onComplete.call(_this, value);
                    }
                }
            }
        });
    };

    $.fn.countTo.defaults = {
        from: 0, // the number the element should start at
        to: 100, // the number the element should end at
        speed: 1000, // how long it should take to count between the target numbers
        refreshInterval: 100, // how often the element should be updated
        decimals: 0, // the number of decimal places to show
        onUpdate: null, // callback method for every time the element is updated,
        onComplete: null};
})(jQuery);
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
    return typeof obj;
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
};

/**
 * author Christopher Blum
 *    - based on the idea of Remy Sharp, http://remysharp.com/2009/01/26/element-in-view-event-plugin/
 *    - forked from http://github.com/zuk/jquery.inview/
 */
(function (factory) {
    if (typeof define == 'function' && define.amd) {
        // AMD
        define(['jquery'], factory);
    } else if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object') {
        // Node, CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
})(function ($) {

    var inviewObjects = [],
            viewportSize,
            viewportOffset,
            d = document,
            w = window,
            documentElement = d.documentElement,
            timer;

    $.event.special.inview = {
        add: function add(data) {
            inviewObjects.push({data: data, $element: $(this), element: this});
            // Use setInterval in order to also make sure this captures elements within
            // "overflow:scroll" elements or elements that appeared in the dom tree due to
            // dom manipulation and reflow
            // old: $(window).scroll(checkInView);
            //
            // By the way, iOS (iPad, iPhone, ...) seems to not execute, or at least delays
            // intervals while the user scrolls. Therefore the inview event might fire a bit late there
            //
            // Don't waste cycles with an interval until we get at least one element that
            // has bound to the inview event.
            if (!timer && inviewObjects.length) {
                timer = setInterval(checkInView, 250);
            }
        },
        remove: function remove(data) {
            for (var i = 0; i < inviewObjects.length; i++) {
                var inviewObject = inviewObjects[i];
                if (inviewObject.element === this && inviewObject.data.guid === data.guid) {
                    inviewObjects.splice(i, 1);
                    break;
                }
            }

            // Clear interval when we no longer have any elements listening
            if (!inviewObjects.length) {
                clearInterval(timer);
                timer = null;
            }
        }
    };

    function getViewportSize() {
        var mode,
                domObject,
                size = {height: w.innerHeight, width: w.innerWidth};

        // if this is correct then return it. iPad has compat Mode, so will
        // go into check clientHeight/clientWidth (which has the wrong value).
        if (!size.height) {
            mode = d.compatMode;
            if (mode || !$.support.boxModel) {
                // IE, Gecko
                domObject = mode === 'CSS1Compat' ? documentElement : // Standards
                        d.body; // Quirks
                size = {
                    height: domObject.clientHeight,
                    width: domObject.clientWidth
                };
            }
        }

        return size;
    }

    function getViewportOffset() {
        return {
            top: w.pageYOffset || documentElement.scrollTop || d.body.scrollTop,
            left: w.pageXOffset || documentElement.scrollLeft || d.body.scrollLeft
        };
    }

    function checkInView() {
        if (!inviewObjects.length) {
            return;
        }

        var i = 0,
                $elements = $.map(inviewObjects, function (inviewObject) {
                    var selector = inviewObject.data.selector,
                            $element = inviewObject.$element;
                    return selector ? $element.find(selector) : $element;
                });

        viewportSize = viewportSize || getViewportSize();
        viewportOffset = viewportOffset || getViewportOffset();

        for (; i < inviewObjects.length; i++) {
            // Ignore elements that are not in the DOM tree
            if (!$.contains(documentElement, $elements[i][0])) {
                continue;
            }

            var $element = $($elements[i]),
                    elementSize = {height: $element[0].offsetHeight, width: $element[0].offsetWidth},
            elementOffset = $element.offset(),
                    inView = $element.data('inview');

            // Don't ask me why because I haven't figured out yet:
            // viewportOffset and viewportSize are sometimes suddenly null in Firefox 5.
            // Even though it sounds weird:
            // It seems that the execution of this function is interferred by the onresize/onscroll event
            // where viewportOffset and viewportSize are unset
            if (!viewportOffset || !viewportSize) {
                return;
            }

            if (elementOffset.top + elementSize.height > viewportOffset.top && elementOffset.top < viewportOffset.top + viewportSize.height && elementOffset.left + elementSize.width > viewportOffset.left && elementOffset.left < viewportOffset.left + viewportSize.width) {
                if (!inView) {
                    $element.data('inview', true).trigger('inview', [true]);
                }
            } else if (inView) {
                $element.data('inview', false).trigger('inview', [false]);
            }
        }
    }

    $(w).on('scroll resize scrollstop', function () {
        viewportSize = viewportOffset = null;
    });

    // IE < 9 scrolls to focused elements without firing the "scroll" event
    if (!documentElement.addEventListener && documentElement.attachEvent) {
        documentElement.attachEvent('onfocusin', function () {
            viewportOffset = null;
        });
    }
});
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
    return typeof obj;
} : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj;
};

/*
 *  jQuery OwlCarousel v1.3.3
 *
 *  Copyright (c) 2013 Bartosz Wojciechowski
 *  http://www.owlgraphic.com/owlcarousel/
 *
 *  Licensed under MIT
 *
 */

/*JS Lint helpers: */
/*global dragMove: false, dragEnd: false, $, jQuery, alert, window, document */
/*jslint nomen: true, continue:true */

if (typeof Object.create !== 'function') {
    Object.create = function (obj) {
        function F() {}
        F.prototype = obj;
        return new F();
    };
}
(function ($, window, document) {

    // var Carousel = {
    //     init: function init(options, el) {
    //         var base = this;

    //         base.$elem = $(el);
    //         base.options = $.extend({}, $.fn.owlCarousel.options, base.$elem.data(), options);

    //         base.userOptions = options;
    //         base.loadContent();
    //     },

    //     loadContent: function loadContent() {
    //         var base = this,
    //             url;

    //         function getData(data) {
    //             var i,
    //                 content = '';
    //             if (typeof base.options.jsonSuccess === 'function') {
    //                 base.options.jsonSuccess.apply(this, [data]);
    //             } else {
    //                 for (i in data.owl) {
    //                     if (data.owl.hasOwnProperty(i)) {
    //                         content += data.owl[i].item;
    //                     }
    //                 }
    //                 base.$elem.html(content);
    //             }
    //             base.logIn();
    //         }

    //         if (typeof base.options.beforeInit === 'function') {
    //             base.options.beforeInit.apply(this, [base.$elem]);
    //         }

    //         if (typeof base.options.jsonPath === 'string') {
    //             url = base.options.jsonPath;
    //             $.getJSON(url, getData);
    //         } else {
    //             base.logIn();
    //         }
    //     },

    //     logIn: function logIn() {
    //         var base = this;

    //         base.$elem.data('owl-originalStyles', base.$elem.attr('style'));
    //         base.$elem.data('owl-originalClasses', base.$elem.attr('class'));

    //         base.$elem.css({ opacity: 0 });
    //         base.orignalItems = base.options.items;
    //         base.checkBrowser();
    //         base.wrapperWidth = 0;
    //         base.checkVisible = null;
    //         base.setVars();
    //     },

    //     setVars: function setVars() {
    //         var base = this;
    //         if (base.$elem.children().length === 0) {
    //             return false;
    //         }
    //         base.baseClass();
    //         base.eventTypes();
    //         base.$userItems = base.$elem.children();
    //         base.itemsAmount = base.$userItems.length;
    //         base.wrapItems();
    //         base.$owlItems = base.$elem.find('.owl-item');
    //         base.$owlWrapper = base.$elem.find('.owl-wrapper');
    //         base.playDirection = 'next';
    //         base.prevItem = 0;
    //         base.prevArr = [0];
    //         base.currentItem = 0;
    //         base.customEvents();
    //         base.onStartup();
    //     },

    //     onStartup: function onStartup() {
    //         var base = this;
    //         base.updateItems();
    //         base.calculateAll();
    //         base.buildControls();
    //         base.updateControls();
    //         base.response();
    //         base.moveEvents();
    //         base.stopOnHover();
    //         base.owlStatus();

    //         if (base.options.transitionStyle !== false) {
    //             base.transitionTypes(base.options.transitionStyle);
    //         }
    //         if (base.options.autoPlay === true) {
    //             base.options.autoPlay = 5000;
    //         }
    //         base.play();

    //         base.$elem.find('.owl-wrapper').css('display', 'block');

    //         if (!base.$elem.is(':visible')) {
    //             base.watchVisibility();
    //         } else {
    //             base.$elem.css('opacity', 1);
    //         }
    //         base.onstartup = false;
    //         base.eachMoveUpdate();
    //         if (typeof base.options.afterInit === 'function') {
    //             base.options.afterInit.apply(this, [base.$elem]);
    //         }
    //     },

    //     eachMoveUpdate: function eachMoveUpdate() {
    //         var base = this;

    //         if (base.options.lazyLoad === true) {
    //             base.lazyLoad();
    //         }
    //         if (base.options.autoHeight === true) {
    //             base.autoHeight();
    //         }
    //         base.onVisibleItems();

    //         if (typeof base.options.afterAction === 'function') {
    //             base.options.afterAction.apply(this, [base.$elem]);
    //         }
    //     },

    //     updateVars: function updateVars() {
    //         var base = this;
    //         if (typeof base.options.beforeUpdate === 'function') {
    //             base.options.beforeUpdate.apply(this, [base.$elem]);
    //         }
    //         base.watchVisibility();
    //         base.updateItems();
    //         base.calculateAll();
    //         base.updatePosition();
    //         base.updateControls();
    //         base.eachMoveUpdate();
    //         if (typeof base.options.afterUpdate === 'function') {
    //             base.options.afterUpdate.apply(this, [base.$elem]);
    //         }
    //     },

    //     reload: function reload() {
    //         var base = this;
    //         window.setTimeout(function () {
    //             base.updateVars();
    //         }, 0);
    //     },

    //     watchVisibility: function watchVisibility() {
    //         var base = this;

    //         if (base.$elem.is(':visible') === false) {
    //             base.$elem.css({ opacity: 0 });
    //             window.clearInterval(base.autoPlayInterval);
    //             window.clearInterval(base.checkVisible);
    //         } else {
    //             return false;
    //         }
    //         base.checkVisible = window.setInterval(function () {
    //             if (base.$elem.is(':visible')) {
    //                 base.reload();
    //                 base.$elem.animate({ opacity: 1 }, 200);
    //                 window.clearInterval(base.checkVisible);
    //             }
    //         }, 500);
    //     },

    //     wrapItems: function wrapItems() {
    //         var base = this;
    //         base.$userItems.wrapAll('<div class=\'owl-wrapper\'>').wrap('<div class=\'owl-item\'></div>');
    //         base.$elem.find('.owl-wrapper').wrap('<div class=\'owl-wrapper-outer\'>');
    //         base.wrapperOuter = base.$elem.find('.owl-wrapper-outer');
    //         base.$elem.css('display', 'block');
    //     },

    //     baseClass: function baseClass() {
    //         var base = this,
    //             hasBaseClass = base.$elem.hasClass(base.options.baseClass),
    //             hasThemeClass = base.$elem.hasClass(base.options.theme);

    //         if (!hasBaseClass) {
    //             base.$elem.addClass(base.options.baseClass);
    //         }

    //         if (!hasThemeClass) {
    //             base.$elem.addClass(base.options.theme);
    //         }
    //     },

    //     updateItems: function updateItems() {
    //         var base = this,
    //             width,
    //             i;

    //         if (base.options.responsive === false) {
    //             return false;
    //         }
    //         if (base.options.singleItem === true) {
    //             base.options.items = base.orignalItems = 1;
    //             base.options.itemsCustom = false;
    //             base.options.itemsDesktop = false;
    //             base.options.itemsDesktopSmall = false;
    //             base.options.itemsTablet = false;
    //             base.options.itemsTabletSmall = false;
    //             base.options.itemsMobile = false;
    //             return false;
    //         }

    //         width = $(base.options.responsiveBaseWidth).width();

    //         if (width > (base.options.itemsDesktop[0] || base.orignalItems)) {
    //             base.options.items = base.orignalItems;
    //         }
    //         if (base.options.itemsCustom !== false) {
    //             //Reorder array by screen size
    //             base.options.itemsCustom.sort(function (a, b) {
    //                 return a[0] - b[0];
    //             });

    //             for (i = 0; i < base.options.itemsCustom.length; i += 1) {
    //                 if (base.options.itemsCustom[i][0] <= width) {
    //                     base.options.items = base.options.itemsCustom[i][1];
    //                 }
    //             }
    //         } else {

    //             if (width <= base.options.itemsDesktop[0] && base.options.itemsDesktop !== false) {
    //                 base.options.items = base.options.itemsDesktop[1];
    //             }

    //             if (width <= base.options.itemsDesktopSmall[0] && base.options.itemsDesktopSmall !== false) {
    //                 base.options.items = base.options.itemsDesktopSmall[1];
    //             }

    //             if (width <= base.options.itemsTablet[0] && base.options.itemsTablet !== false) {
    //                 base.options.items = base.options.itemsTablet[1];
    //             }

    //             if (width <= base.options.itemsTabletSmall[0] && base.options.itemsTabletSmall !== false) {
    //                 base.options.items = base.options.itemsTabletSmall[1];
    //             }

    //             if (width <= base.options.itemsMobile[0] && base.options.itemsMobile !== false) {
    //                 base.options.items = base.options.itemsMobile[1];
    //             }
    //         }

    //         //if number of items is less than declared
    //         if (base.options.items > base.itemsAmount && base.options.itemsScaleUp === true) {
    //             base.options.items = base.itemsAmount;
    //         }
    //     },

    //     response: function response() {
    //         var base = this,
    //             smallDelay,
    //             lastWindowWidth;

    //         if (base.options.responsive !== true) {
    //             return false;
    //         }
    //         lastWindowWidth = $(window).width();

    //         base.resizer = function () {
    //             if ($(window).width() !== lastWindowWidth) {
    //                 if (base.options.autoPlay !== false) {
    //                     window.clearInterval(base.autoPlayInterval);
    //                 }
    //                 window.clearTimeout(smallDelay);
    //                 smallDelay = window.setTimeout(function () {
    //                     lastWindowWidth = $(window).width();
    //                     base.updateVars();
    //                 }, base.options.responsiveRefreshRate);
    //             }
    //         };
    //         $(window).resize(base.resizer);
    //     },

    //     updatePosition: function updatePosition() {
    //         var base = this;
    //         base.jumpTo(base.currentItem);
    //         if (base.options.autoPlay !== false) {
    //             base.checkAp();
    //         }
    //     },

    //     appendItemsSizes: function appendItemsSizes() {
    //         var base = this,
    //             roundPages = 0,
    //             lastItem = base.itemsAmount - base.options.items;

    //         base.$owlItems.each(function (index) {
    //             var $this = $(this);
    //             $this.css({ 'width': base.itemWidth }).data('owl-item', Number(index));

    //             if (index % base.options.items === 0 || index === lastItem) {
    //                 if (!(index > lastItem)) {
    //                     roundPages += 1;
    //                 }
    //             }
    //             $this.data('owl-roundPages', roundPages);
    //         });
    //     },

    //     appendWrapperSizes: function appendWrapperSizes() {
    //         var base = this,
    //             width = base.$owlItems.length * base.itemWidth;

    //         base.$owlWrapper.css({
    //             'width': width * 2,
    //             'left': 0
    //         });
    //         base.appendItemsSizes();
    //     },

    //     calculateAll: function calculateAll() {
    //         var base = this;
    //         base.calculateWidth();
    //         base.appendWrapperSizes();
    //         base.loops();
    //         base.max();
    //     },

    //     calculateWidth: function calculateWidth() {
    //         var base = this;
    //         base.itemWidth = Math.round(base.$elem.width() / base.options.items);
    //     },

    //     max: function max() {
    //         var base = this,
    //             maximum = (base.itemsAmount * base.itemWidth - base.options.items * base.itemWidth) * -1;
    //         if (base.options.items > base.itemsAmount) {
    //             base.maximumItem = 0;
    //             maximum = 0;
    //             base.maximumPixels = 0;
    //         } else {
    //             base.maximumItem = base.itemsAmount - base.options.items;
    //             base.maximumPixels = maximum;
    //         }
    //         return maximum;
    //     },

    //     min: function min() {
    //         return 0;
    //     },

    //     loops: function loops() {
    //         var base = this,
    //             prev = 0,
    //             elWidth = 0,
    //             i,
    //             item,
    //             roundPageNum;

    //         base.positionsInArray = [0];
    //         base.pagesInArray = [];

    //         for (i = 0; i < base.itemsAmount; i += 1) {
    //             elWidth += base.itemWidth;
    //             base.positionsInArray.push(-elWidth);

    //             if (base.options.scrollPerPage === true) {
    //                 item = $(base.$owlItems[i]);
    //                 roundPageNum = item.data('owl-roundPages');
    //                 if (roundPageNum !== prev) {
    //                     base.pagesInArray[prev] = base.positionsInArray[i];
    //                     prev = roundPageNum;
    //                 }
    //             }
    //         }
    //     },

    //     buildControls: function buildControls() {
    //         var base = this;
    //         if (base.options.navigation === true || base.options.pagination === true) {
    //             base.owlControls = $('<div class=\'owl-controls\'/>').toggleClass('clickable', !base.browser.isTouch).appendTo(base.$elem);
    //         }
    //         if (base.options.pagination === true) {
    //             base.buildPagination();
    //         }
    //         if (base.options.navigation === true) {
    //             base.buildButtons();
    //         }
    //     },

    //     buildButtons: function buildButtons() {
    //         var base = this,
    //             buttonsWrapper = $('<div class=\'owl-buttons\'/>');
    //         base.owlControls.append(buttonsWrapper);

    //         base.buttonPrev = $('<div/>', {
    //             'class': 'owl-prev',
    //             'html': base.options.navigationText[0] || ''
    //         });

    //         base.buttonNext = $('<div/>', {
    //             'class': 'owl-next',
    //             'html': base.options.navigationText[1] || ''
    //         });

    //         buttonsWrapper.append(base.buttonPrev).append(base.buttonNext);

    //         buttonsWrapper.on('touchstart.owlControls mousedown.owlControls', 'div[class^=\'owl\']', function (event) {
    //             event.preventDefault();
    //         });

    //         buttonsWrapper.on('touchend.owlControls mouseup.owlControls', 'div[class^=\'owl\']', function (event) {
    //             event.preventDefault();
    //             if ($(this).hasClass('owl-next')) {
    //                 base.next();
    //             } else {
    //                 base.prev();
    //             }
    //         });
    //     },

    //     buildPagination: function buildPagination() {
    //         var base = this;

    //         base.paginationWrapper = $('<div class=\'owl-pagination\'/>');
    //         base.owlControls.append(base.paginationWrapper);

    //         base.paginationWrapper.on('touchend.owlControls mouseup.owlControls', '.owl-page', function (event) {
    //             event.preventDefault();
    //             if (Number($(this).data('owl-page')) !== base.currentItem) {
    //                 base.goTo(Number($(this).data('owl-page')), true);
    //             }
    //         });
    //     },

    //     updatePagination: function updatePagination() {
    //         var base = this,
    //             counter,
    //             lastPage,
    //             lastItem,
    //             i,
    //             paginationButton,
    //             paginationButtonInner;

    //         if (base.options.pagination === false) {
    //             return false;
    //         }

    //         base.paginationWrapper.html('');

    //         counter = 0;
    //         lastPage = base.itemsAmount - base.itemsAmount % base.options.items;

    //         for (i = 0; i < base.itemsAmount; i += 1) {
    //             if (i % base.options.items === 0) {
    //                 counter += 1;
    //                 if (lastPage === i) {
    //                     lastItem = base.itemsAmount - base.options.items;
    //                 }
    //                 paginationButton = $('<div/>', {
    //                     'class': 'owl-page'
    //                 });
    //                 paginationButtonInner = $('<span></span>', {
    //                     'text': base.options.paginationNumbers === true ? counter : '',
    //                     'class': base.options.paginationNumbers === true ? 'owl-numbers' : ''
    //                 });
    //                 paginationButton.append(paginationButtonInner);

    //                 paginationButton.data('owl-page', lastPage === i ? lastItem : i);
    //                 paginationButton.data('owl-roundPages', counter);

    //                 base.paginationWrapper.append(paginationButton);
    //             }
    //         }
    //         base.checkPagination();
    //     },
    //     checkPagination: function checkPagination() {
    //         var base = this;
    //         if (base.options.pagination === false) {
    //             return false;
    //         }
    //         base.paginationWrapper.find('.owl-page').each(function () {
    //             if ($(this).data('owl-roundPages') === $(base.$owlItems[base.currentItem]).data('owl-roundPages')) {
    //                 base.paginationWrapper.find('.owl-page').removeClass('active');
    //                 $(this).addClass('active');
    //             }
    //         });
    //     },

    //     checkNavigation: function checkNavigation() {
    //         var base = this;

    //         if (base.options.navigation === false) {
    //             return false;
    //         }
    //         if (base.options.rewindNav === false) {
    //             if (base.currentItem === 0 && base.maximumItem === 0) {
    //                 base.buttonPrev.addClass('disabled');
    //                 base.buttonNext.addClass('disabled');
    //             } else if (base.currentItem === 0 && base.maximumItem !== 0) {
    //                 base.buttonPrev.addClass('disabled');
    //                 base.buttonNext.removeClass('disabled');
    //             } else if (base.currentItem === base.maximumItem) {
    //                 base.buttonPrev.removeClass('disabled');
    //                 base.buttonNext.addClass('disabled');
    //             } else if (base.currentItem !== 0 && base.currentItem !== base.maximumItem) {
    //                 base.buttonPrev.removeClass('disabled');
    //                 base.buttonNext.removeClass('disabled');
    //             }
    //         }
    //     },

    //     updateControls: function updateControls() {
    //         var base = this;
    //         base.updatePagination();
    //         base.checkNavigation();
    //         if (base.owlControls) {
    //             if (base.options.items >= base.itemsAmount) {
    //                 base.owlControls.hide();
    //             } else {
    //                 base.owlControls.show();
    //             }
    //         }
    //     },

    //     destroyControls: function destroyControls() {
    //         var base = this;
    //         if (base.owlControls) {
    //             base.owlControls.remove();
    //         }
    //     },

    //     next: function next(speed) {
    //         var base = this;

    //         if (base.isTransition) {
    //             return false;
    //         }

    //         base.currentItem += base.options.scrollPerPage === true ? base.options.items : 1;
    //         if (base.currentItem > base.maximumItem + (base.options.scrollPerPage === true ? base.options.items - 1 : 0)) {
    //             if (base.options.rewindNav === true) {
    //                 base.currentItem = 0;
    //                 speed = 'rewind';
    //             } else {
    //                 base.currentItem = base.maximumItem;
    //                 return false;
    //             }
    //         }
    //         base.goTo(base.currentItem, speed);
    //     },

    //     prev: function prev(speed) {
    //         var base = this;

    //         if (base.isTransition) {
    //             return false;
    //         }

    //         if (base.options.scrollPerPage === true && base.currentItem > 0 && base.currentItem < base.options.items) {
    //             base.currentItem = 0;
    //         } else {
    //             base.currentItem -= base.options.scrollPerPage === true ? base.options.items : 1;
    //         }
    //         if (base.currentItem < 0) {
    //             if (base.options.rewindNav === true) {
    //                 base.currentItem = base.maximumItem;
    //                 speed = 'rewind';
    //             } else {
    //                 base.currentItem = 0;
    //                 return false;
    //             }
    //         }
    //         base.goTo(base.currentItem, speed);
    //     },

    //     goTo: function goTo(position, speed, drag) {
    //         var base = this,
    //             goToPixel;

    //         if (base.isTransition) {
    //             return false;
    //         }
    //         if (typeof base.options.beforeMove === 'function') {
    //             base.options.beforeMove.apply(this, [base.$elem]);
    //         }
    //         if (position >= base.maximumItem) {
    //             position = base.maximumItem;
    //         } else if (position <= 0) {
    //             position = 0;
    //         }

    //         base.currentItem = base.owl.currentItem = position;
    //         if (base.options.transitionStyle !== false && drag !== 'drag' && base.options.items === 1 && base.browser.support3d === true) {
    //             base.swapSpeed(0);
    //             if (base.browser.support3d === true) {
    //                 base.transition3d(base.positionsInArray[position]);
    //             } else {
    //                 base.css2slide(base.positionsInArray[position], 1);
    //             }
    //             base.afterGo();
    //             base.singleItemTransition();
    //             return false;
    //         }
    //         goToPixel = base.positionsInArray[position];

    //         if (base.browser.support3d === true) {
    //             base.isCss3Finish = false;

    //             if (speed === true) {
    //                 base.swapSpeed('paginationSpeed');
    //                 window.setTimeout(function () {
    //                     base.isCss3Finish = true;
    //                 }, base.options.paginationSpeed);
    //             } else if (speed === 'rewind') {
    //                 base.swapSpeed(base.options.rewindSpeed);
    //                 window.setTimeout(function () {
    //                     base.isCss3Finish = true;
    //                 }, base.options.rewindSpeed);
    //             } else {
    //                 base.swapSpeed('slideSpeed');
    //                 window.setTimeout(function () {
    //                     base.isCss3Finish = true;
    //                 }, base.options.slideSpeed);
    //             }
    //             base.transition3d(goToPixel);
    //         } else {
    //             if (speed === true) {
    //                 base.css2slide(goToPixel, base.options.paginationSpeed);
    //             } else if (speed === 'rewind') {
    //                 base.css2slide(goToPixel, base.options.rewindSpeed);
    //             } else {
    //                 base.css2slide(goToPixel, base.options.slideSpeed);
    //             }
    //         }
    //         base.afterGo();
    //     },

    //     jumpTo: function jumpTo(position) {
    //         var base = this;
    //         if (typeof base.options.beforeMove === 'function') {
    //             base.options.beforeMove.apply(this, [base.$elem]);
    //         }
    //         if (position >= base.maximumItem || position === -1) {
    //             position = base.maximumItem;
    //         } else if (position <= 0) {
    //             position = 0;
    //         }
    //         base.swapSpeed(0);
    //         if (base.browser.support3d === true) {
    //             base.transition3d(base.positionsInArray[position]);
    //         } else {
    //             base.css2slide(base.positionsInArray[position], 1);
    //         }
    //         base.currentItem = base.owl.currentItem = position;
    //         base.afterGo();
    //     },

    //     afterGo: function afterGo() {
    //         var base = this;

    //         base.prevArr.push(base.currentItem);
    //         base.prevItem = base.owl.prevItem = base.prevArr[base.prevArr.length - 2];
    //         base.prevArr.shift(0);

    //         if (base.prevItem !== base.currentItem) {
    //             base.checkPagination();
    //             base.checkNavigation();
    //             base.eachMoveUpdate();

    //             if (base.options.autoPlay !== false) {
    //                 base.checkAp();
    //             }
    //         }
    //         if (typeof base.options.afterMove === 'function' && base.prevItem !== base.currentItem) {
    //             base.options.afterMove.apply(this, [base.$elem]);
    //         }
    //     },

    //     stop: function stop() {
    //         var base = this;
    //         base.apStatus = 'stop';
    //         window.clearInterval(base.autoPlayInterval);
    //     },

    //     checkAp: function checkAp() {
    //         var base = this;
    //         if (base.apStatus !== 'stop') {
    //             base.play();
    //         }
    //     },

    //     play: function play() {
    //         var base = this;
    //         base.apStatus = 'play';
    //         if (base.options.autoPlay === false) {
    //             return false;
    //         }
    //         window.clearInterval(base.autoPlayInterval);
    //         base.autoPlayInterval = window.setInterval(function () {
    //             base.next(true);
    //         }, base.options.autoPlay);
    //     },

    //     swapSpeed: function swapSpeed(action) {
    //         var base = this;
    //         if (action === 'slideSpeed') {
    //             base.$owlWrapper.css(base.addCssSpeed(base.options.slideSpeed));
    //         } else if (action === 'paginationSpeed') {
    //             base.$owlWrapper.css(base.addCssSpeed(base.options.paginationSpeed));
    //         } else if (typeof action !== 'string') {
    //             base.$owlWrapper.css(base.addCssSpeed(action));
    //         }
    //     },

    //     addCssSpeed: function addCssSpeed(speed) {
    //         return {
    //             '-webkit-transition': 'all ' + speed + 'ms ease',
    //             '-moz-transition': 'all ' + speed + 'ms ease',
    //             '-o-transition': 'all ' + speed + 'ms ease',
    //             'transition': 'all ' + speed + 'ms ease'
    //         };
    //     },

    //     removeTransition: function removeTransition() {
    //         return {
    //             '-webkit-transition': '',
    //             '-moz-transition': '',
    //             '-o-transition': '',
    //             'transition': ''
    //         };
    //     },

    //     doTranslate: function doTranslate(pixels) {
    //         return {
    //             '-webkit-transform': 'translate3d(' + pixels + 'px, 0px, 0px)',
    //             '-moz-transform': 'translate3d(' + pixels + 'px, 0px, 0px)',
    //             '-o-transform': 'translate3d(' + pixels + 'px, 0px, 0px)',
    //             '-ms-transform': 'translate3d(' + pixels + 'px, 0px, 0px)',
    //             'transform': 'translate3d(' + pixels + 'px, 0px,0px)'
    //         };
    //     },

    //     transition3d: function transition3d(value) {
    //         var base = this;
    //         base.$owlWrapper.css(base.doTranslate(value));
    //     },

    //     css2move: function css2move(value) {
    //         var base = this;
    //         base.$owlWrapper.css({ 'left': value });
    //     },

    //     css2slide: function css2slide(value, speed) {
    //         var base = this;

    //         base.isCssFinish = false;
    //         base.$owlWrapper.stop(true, true).animate({
    //             'left': value
    //         }, {
    //             duration: speed || base.options.slideSpeed,
    //             complete: function complete() {
    //                 base.isCssFinish = true;
    //             }
    //         });
    //     },

    //     checkBrowser: function checkBrowser() {
    //         var base = this,
    //             translate3D = 'translate3d(0px, 0px, 0px)',
    //             tempElem = document.createElement('div'),
    //             regex,
    //             asSupport,
    //             support3d,
    //             isTouch;

    //         tempElem.style.cssText = '  -moz-transform:' + translate3D + '; -ms-transform:' + translate3D + '; -o-transform:' + translate3D + '; -webkit-transform:' + translate3D + '; transform:' + translate3D;
    //         regex = /translate3d\(0px, 0px, 0px\)/g;
    //         asSupport = tempElem.style.cssText.match(regex);
    //         support3d = asSupport !== null && asSupport.length === 1;

    //         isTouch = 'ontouchstart' in window || window.navigator.msMaxTouchPoints;

    //         base.browser = {
    //             'support3d': support3d,
    //             'isTouch': isTouch
    //         };
    //     },

    //     moveEvents: function moveEvents() {
    //         var base = this;
    //         if (base.options.mouseDrag !== false || base.options.touchDrag !== false) {
    //             base.gestures();
    //             base.disabledEvents();
    //         }
    //     },

    //     eventTypes: function eventTypes() {
    //         var base = this,
    //             types = ['s', 'e', 'x'];

    //         base.ev_types = {};

    //         if (base.options.mouseDrag === true && base.options.touchDrag === true) {
    //             types = ['touchstart.owl mousedown.owl', 'touchmove.owl mousemove.owl', 'touchend.owl touchcancel.owl mouseup.owl'];
    //         } else if (base.options.mouseDrag === false && base.options.touchDrag === true) {
    //             types = ['touchstart.owl', 'touchmove.owl', 'touchend.owl touchcancel.owl'];
    //         } else if (base.options.mouseDrag === true && base.options.touchDrag === false) {
    //             types = ['mousedown.owl', 'mousemove.owl', 'mouseup.owl'];
    //         }

    //         base.ev_types.start = types[0];
    //         base.ev_types.move = types[1];
    //         base.ev_types.end = types[2];
    //     },

    //     disabledEvents: function disabledEvents() {
    //         var base = this;
    //         base.$elem.on('dragstart.owl', function (event) {
    //             event.preventDefault();
    //         });
    //         base.$elem.on('mousedown.disableTextSelect', function (e) {
    //             return $(e.target).is('input, textarea, select, option');
    //         });
    //     },

    //     gestures: function gestures() {
    //         /*jslint unparam: true*/
    //         var base = this,
    //             locals = {
    //             offsetX: 0,
    //             offsetY: 0,
    //             baseElWidth: 0,
    //             relativePos: 0,
    //             position: null,
    //             minSwipe: null,
    //             maxSwipe: null,
    //             sliding: null,
    //             dargging: null,
    //             targetElement: null
    //         };

    //         base.isCssFinish = true;

    //         function getTouches(event) {
    //             if (event.touches !== undefined) {
    //                 return {
    //                     x: event.touches[0].pageX,
    //                     y: event.touches[0].pageY
    //                 };
    //             }

    //             if (event.touches === undefined) {
    //                 if (event.pageX !== undefined) {
    //                     return {
    //                         x: event.pageX,
    //                         y: event.pageY
    //                     };
    //                 }
    //                 if (event.pageX === undefined) {
    //                     return {
    //                         x: event.clientX,
    //                         y: event.clientY
    //                     };
    //                 }
    //             }
    //         }

    //         function swapEvents(type) {
    //             if (type === 'on') {
    //                 $(document).on(base.ev_types.move, dragMove);
    //                 $(document).on(base.ev_types.end, dragEnd);
    //             } else if (type === 'off') {
    //                 $(document).off(base.ev_types.move);
    //                 $(document).off(base.ev_types.end);
    //             }
    //         }

    //         function dragStart(event) {
    //             var ev = event.originalEvent || event || window.event,
    //                 position;

    //             if (ev.which === 3) {
    //                 return false;
    //             }
    //             if (base.itemsAmount <= base.options.items) {
    //                 return;
    //             }
    //             if (base.isCssFinish === false && !base.options.dragBeforeAnimFinish) {
    //                 return false;
    //             }
    //             if (base.isCss3Finish === false && !base.options.dragBeforeAnimFinish) {
    //                 return false;
    //             }

    //             if (base.options.autoPlay !== false) {
    //                 window.clearInterval(base.autoPlayInterval);
    //             }

    //             if (base.browser.isTouch !== true && !base.$owlWrapper.hasClass('grabbing')) {
    //                 base.$owlWrapper.addClass('grabbing');
    //             }

    //             base.newPosX = 0;
    //             base.newRelativeX = 0;

    //             $(this).css(base.removeTransition());

    //             position = $(this).position();
    //             locals.relativePos = position.left;

    //             locals.offsetX = getTouches(ev).x - position.left;
    //             locals.offsetY = getTouches(ev).y - position.top;

    //             swapEvents('on');

    //             locals.sliding = false;
    //             locals.targetElement = ev.target || ev.srcElement;
    //         }

    //         function dragMove(event) {
    //             var ev = event.originalEvent || event || window.event,
    //                 minSwipe,
    //                 maxSwipe;

    //             base.newPosX = getTouches(ev).x - locals.offsetX;
    //             base.newPosY = getTouches(ev).y - locals.offsetY;
    //             base.newRelativeX = base.newPosX - locals.relativePos;

    //             if (typeof base.options.startDragging === 'function' && locals.dragging !== true && base.newRelativeX !== 0) {
    //                 locals.dragging = true;
    //                 base.options.startDragging.apply(base, [base.$elem]);
    //             }

    //             if ((base.newRelativeX > 8 || base.newRelativeX < -8) && base.browser.isTouch === true) {
    //                 if (ev.preventDefault !== undefined) {
    //                     ev.preventDefault();
    //                 } else {
    //                     ev.returnValue = false;
    //                 }
    //                 locals.sliding = true;
    //             }

    //             if ((base.newPosY > 10 || base.newPosY < -10) && locals.sliding === false) {
    //                 $(document).off('touchmove.owl');
    //             }

    //             minSwipe = function minSwipe() {
    //                 return base.newRelativeX / 5;
    //             };

    //             maxSwipe = function maxSwipe() {
    //                 return base.maximumPixels + base.newRelativeX / 5;
    //             };

    //             base.newPosX = Math.max(Math.min(base.newPosX, minSwipe()), maxSwipe());
    //             if (base.browser.support3d === true) {
    //                 base.transition3d(base.newPosX);
    //             } else {
    //                 base.css2move(base.newPosX);
    //             }
    //         }

    //         function dragEnd(event) {
    //             var ev = event.originalEvent || event || window.event,
    //                 newPosition,
    //                 handlers,
    //                 owlStopEvent;

    //             ev.target = ev.target || ev.srcElement;

    //             locals.dragging = false;

    //             if (base.browser.isTouch !== true) {
    //                 base.$owlWrapper.removeClass('grabbing');
    //             }

    //             if (base.newRelativeX < 0) {
    //                 base.dragDirection = base.owl.dragDirection = 'left';
    //             } else {
    //                 base.dragDirection = base.owl.dragDirection = 'right';
    //             }

    //             if (base.newRelativeX !== 0) {
    //                 newPosition = base.getNewPosition();
    //                 base.goTo(newPosition, false, 'drag');
    //                 if (locals.targetElement === ev.target && base.browser.isTouch !== true) {
    //                     $(ev.target).on('click.disable', function (ev) {
    //                         ev.stopImmediatePropagation();
    //                         ev.stopPropagation();
    //                         ev.preventDefault();
    //                         $(ev.target).off('click.disable');
    //                     });
    //                     handlers = $._data(ev.target, 'events').click;
    //                     owlStopEvent = handlers.pop();
    //                     handlers.splice(0, 0, owlStopEvent);
    //                 }
    //             }
    //             swapEvents('off');
    //         }
    //         base.$elem.on(base.ev_types.start, '.owl-wrapper', dragStart);
    //     },

    //     getNewPosition: function getNewPosition() {
    //         var base = this,
    //             newPosition = base.closestItem();

    //         if (newPosition > base.maximumItem) {
    //             base.currentItem = base.maximumItem;
    //             newPosition = base.maximumItem;
    //         } else if (base.newPosX >= 0) {
    //             newPosition = 0;
    //             base.currentItem = 0;
    //         }
    //         return newPosition;
    //     },
    //     closestItem: function closestItem() {
    //         var base = this,
    //             array = base.options.scrollPerPage === true ? base.pagesInArray : base.positionsInArray,
    //             goal = base.newPosX,
    //             closest = null;

    //         $.each(array, function (i, v) {
    //             if (goal - base.itemWidth / 20 > array[i + 1] && goal - base.itemWidth / 20 < v && base.moveDirection() === 'left') {
    //                 closest = v;
    //                 if (base.options.scrollPerPage === true) {
    //                     base.currentItem = $.inArray(closest, base.positionsInArray);
    //                 } else {
    //                     base.currentItem = i;
    //                 }
    //             } else if (goal + base.itemWidth / 20 < v && goal + base.itemWidth / 20 > (array[i + 1] || array[i] - base.itemWidth) && base.moveDirection() === 'right') {
    //                 if (base.options.scrollPerPage === true) {
    //                     closest = array[i + 1] || array[array.length - 1];
    //                     base.currentItem = $.inArray(closest, base.positionsInArray);
    //                 } else {
    //                     closest = array[i + 1];
    //                     base.currentItem = i + 1;
    //                 }
    //             }
    //         });
    //         return base.currentItem;
    //     },

    //     moveDirection: function moveDirection() {
    //         var base = this,
    //             direction;
    //         if (base.newRelativeX < 0) {
    //             direction = 'right';
    //             base.playDirection = 'next';
    //         } else {
    //             direction = 'left';
    //             base.playDirection = 'prev';
    //         }
    //         return direction;
    //     },

    //     customEvents: function customEvents() {
    //         /*jslint unparam: true*/
    //         var base = this;
    //         base.$elem.on('owl.next', function () {
    //             base.next();
    //         });
    //         base.$elem.on('owl.prev', function () {
    //             base.prev();
    //         });
    //         base.$elem.on('owl.play', function (event, speed) {
    //             base.options.autoPlay = speed;
    //             base.play();
    //             base.hoverStatus = 'play';
    //         });
    //         base.$elem.on('owl.stop', function () {
    //             base.stop();
    //             base.hoverStatus = 'stop';
    //         });
    //         base.$elem.on('owl.goTo', function (event, item) {
    //             base.goTo(item);
    //         });
    //         base.$elem.on('owl.jumpTo', function (event, item) {
    //             base.jumpTo(item);
    //         });
    //     },

    //     stopOnHover: function stopOnHover() {
    //         var base = this;
    //         if (base.options.stopOnHover === true && base.browser.isTouch !== true && base.options.autoPlay !== false) {
    //             base.$elem.on('mouseover', function () {
    //                 base.stop();
    //             });
    //             base.$elem.on('mouseout', function () {
    //                 if (base.hoverStatus !== 'stop') {
    //                     base.play();
    //                 }
    //             });
    //         }
    //     },

    //     lazyLoad: function lazyLoad() {
    //         var base = this,
    //             i,
    //             $item,
    //             itemNumber,
    //             $lazyImg,
    //             follow;

    //         if (base.options.lazyLoad === false) {
    //             return false;
    //         }
    //         for (i = 0; i < base.itemsAmount; i += 1) {
    //             $item = $(base.$owlItems[i]);

    //             if ($item.data('owl-loaded') === 'loaded') {
    //                 continue;
    //             }

    //             itemNumber = $item.data('owl-item');
    //             $lazyImg = $item.find('.lazyOwl');

    //             if (typeof $lazyImg.data('src') !== 'string') {
    //                 $item.data('owl-loaded', 'loaded');
    //                 continue;
    //             }
    //             if ($item.data('owl-loaded') === undefined) {
    //                 $lazyImg.hide();
    //                 $item.addClass('loading').data('owl-loaded', 'checked');
    //             }
    //             if (base.options.lazyFollow === true) {
    //                 follow = itemNumber >= base.currentItem;
    //             } else {
    //                 follow = true;
    //             }
    //             if (follow && itemNumber < base.currentItem + base.options.items && $lazyImg.length) {
    //                 base.lazyPreload($item, $lazyImg);
    //             }
    //         }
    //     },

    //     lazyPreload: function lazyPreload($item, $lazyImg) {
    //         var base = this,
    //             iterations = 0,
    //             isBackgroundImg;

    //         if ($lazyImg.prop('tagName') === 'DIV') {
    //             $lazyImg.css('background-image', 'url(' + $lazyImg.data('src') + ')');
    //             isBackgroundImg = true;
    //         } else {
    //             $lazyImg[0].src = $lazyImg.data('src');
    //         }

    //         function showImage() {
    //             $item.data('owl-loaded', 'loaded').removeClass('loading');
    //             $lazyImg.removeAttr('data-src');
    //             if (base.options.lazyEffect === 'fade') {
    //                 $lazyImg.fadeIn(400);
    //             } else {
    //                 $lazyImg.show();
    //             }
    //             if (typeof base.options.afterLazyLoad === 'function') {
    //                 base.options.afterLazyLoad.apply(this, [base.$elem]);
    //             }
    //         }

    //         function checkLazyImage() {
    //             iterations += 1;
    //             if (base.completeImg($lazyImg.get(0)) || isBackgroundImg === true) {
    //                 showImage();
    //             } else if (iterations <= 100) {
    //                 //if image loads in less than 10 seconds
    //                 window.setTimeout(checkLazyImage, 100);
    //             } else {
    //                 showImage();
    //             }
    //         }

    //         checkLazyImage();
    //     },

    //     autoHeight: function autoHeight() {
    //         var base = this,
    //             $currentimg = $(base.$owlItems[base.currentItem]).find('img'),
    //             iterations;

    //         function addHeight() {
    //             var $currentItem = $(base.$owlItems[base.currentItem]).height();
    //             base.wrapperOuter.css('height', $currentItem + 'px');
    //             if (!base.wrapperOuter.hasClass('autoHeight')) {
    //                 window.setTimeout(function () {
    //                     base.wrapperOuter.addClass('autoHeight');
    //                 }, 0);
    //             }
    //         }

    //         function checkImage() {
    //             iterations += 1;
    //             if (base.completeImg($currentimg.get(0))) {
    //                 addHeight();
    //             } else if (iterations <= 100) {
    //                 //if image loads in less than 10 seconds
    //                 window.setTimeout(checkImage, 100);
    //             } else {
    //                 base.wrapperOuter.css('height', ''); //Else remove height attribute
    //             }
    //         }

    //         if ($currentimg.get(0) !== undefined) {
    //             iterations = 0;
    //             checkImage();
    //         } else {
    //             addHeight();
    //         }
    //     },

    //     completeImg: function completeImg(img) {
    //         var naturalWidthType;

    //         if (!img.complete) {
    //             return false;
    //         }
    //         naturalWidthType = _typeof(img.naturalWidth);
    //         if (naturalWidthType !== 'undefined' && img.naturalWidth === 0) {
    //             return false;
    //         }
    //         return true;
    //     },

    //     onVisibleItems: function onVisibleItems() {
    //         var base = this,
    //             i;

    //         if (base.options.addClassActive === true) {
    //             base.$owlItems.removeClass('active');
    //         }
    //         base.visibleItems = [];
    //         for (i = base.currentItem; i < base.currentItem + base.options.items; i += 1) {
    //             base.visibleItems.push(i);

    //             if (base.options.addClassActive === true) {
    //                 $(base.$owlItems[i]).addClass('active');
    //             }
    //         }
    //         base.owl.visibleItems = base.visibleItems;
    //     },

    //     transitionTypes: function transitionTypes(className) {
    //         var base = this;
    //         //Currently available: 'fade', 'backSlide', 'goDown', 'fadeUp'
    //         base.outClass = 'owl-' + className + '-out';
    //         base.inClass = 'owl-' + className + '-in';
    //     },

    //     singleItemTransition: function singleItemTransition() {
    //         var base = this,
    //             outClass = base.outClass,
    //             inClass = base.inClass,
    //             $currentItem = base.$owlItems.eq(base.currentItem),
    //             $prevItem = base.$owlItems.eq(base.prevItem),
    //             prevPos = Math.abs(base.positionsInArray[base.currentItem]) + base.positionsInArray[base.prevItem],
    //             origin = Math.abs(base.positionsInArray[base.currentItem]) + base.itemWidth / 2,
    //             animEnd = 'webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend';

    //         base.isTransition = true;

    //         base.$owlWrapper.addClass('owl-origin').css({
    //             '-webkit-transform-origin': origin + 'px',
    //             '-moz-perspective-origin': origin + 'px',
    //             'perspective-origin': origin + 'px'
    //         });
    //         function transStyles(prevPos) {
    //             return {
    //                 'position': 'relative',
    //                 'left': prevPos + 'px'
    //             };
    //         }

    //         $prevItem.css(transStyles(prevPos, 10)).addClass(outClass).on(animEnd, function () {
    //             base.endPrev = true;
    //             $prevItem.off(animEnd);
    //             base.clearTransStyle($prevItem, outClass);
    //         });

    //         $currentItem.addClass(inClass).on(animEnd, function () {
    //             base.endCurrent = true;
    //             $currentItem.off(animEnd);
    //             base.clearTransStyle($currentItem, inClass);
    //         });
    //     },

    //     clearTransStyle: function clearTransStyle(item, classToRemove) {
    //         var base = this;
    //         item.css({
    //             'position': '',
    //             'left': ''
    //         }).removeClass(classToRemove);

    //         if (base.endPrev && base.endCurrent) {
    //             base.$owlWrapper.removeClass('owl-origin');
    //             base.endPrev = false;
    //             base.endCurrent = false;
    //             base.isTransition = false;
    //         }
    //     },

    //     owlStatus: function owlStatus() {
    //         var base = this;
    //         base.owl = {
    //             'userOptions': base.userOptions,
    //             'baseElement': base.$elem,
    //             'userItems': base.$userItems,
    //             'owlItems': base.$owlItems,
    //             'currentItem': base.currentItem,
    //             'prevItem': base.prevItem,
    //             'visibleItems': base.visibleItems,
    //             'isTouch': base.browser.isTouch,
    //             'browser': base.browser,
    //             'dragDirection': base.dragDirection
    //         };
    //     },

    //     clearEvents: function clearEvents() {
    //         var base = this;
    //         base.$elem.off('.owl owl mousedown.disableTextSelect');
    //         $(document).off('.owl owl');
    //         $(window).off('resize', base.resizer);
    //     },

    //     unWrap: function unWrap() {
    //         var base = this;
    //         if (base.$elem.children().length !== 0) {
    //             base.$owlWrapper.unwrap();
    //             base.$userItems.unwrap().unwrap();
    //             if (base.owlControls) {
    //                 base.owlControls.remove();
    //             }
    //         }
    //         base.clearEvents();
    //         base.$elem.attr('style', base.$elem.data('owl-originalStyles') || '').attr('class', base.$elem.data('owl-originalClasses'));
    //     },

    //     destroy: function destroy() {
    //         var base = this;
    //         base.stop();
    //         window.clearInterval(base.checkVisible);
    //         base.unWrap();
    //         base.$elem.removeData();
    //     },

    //     reinit: function reinit(newOptions) {
    //         var base = this,
    //             options = $.extend({}, base.userOptions, newOptions);
    //         base.unWrap();
    //         base.init(options, base.$elem);
    //     },

    //     addItem: function addItem(htmlString, targetPosition) {
    //         var base = this,
    //             position;

    //         if (!htmlString) {
    //             return false;
    //         }

    //         if (base.$elem.children().length === 0) {
    //             base.$elem.append(htmlString);
    //             base.setVars();
    //             return false;
    //         }
    //         base.unWrap();
    //         if (targetPosition === undefined || targetPosition === -1) {
    //             position = -1;
    //         } else {
    //             position = targetPosition;
    //         }
    //         if (position >= base.$userItems.length || position === -1) {
    //             base.$userItems.eq(-1).after(htmlString);
    //         } else {
    //             base.$userItems.eq(position).before(htmlString);
    //         }

    //         base.setVars();
    //     },

    //     removeItem: function removeItem(targetPosition) {
    //         var base = this,
    //             position;

    //         if (base.$elem.children().length === 0) {
    //             return false;
    //         }
    //         if (targetPosition === undefined || targetPosition === -1) {
    //             position = -1;
    //         } else {
    //             position = targetPosition;
    //         }

    //         base.unWrap();
    //         base.$userItems.eq(position).remove();
    //         base.setVars();
    //     }

    // };

    // $.fn.owlCarousel = function (options) {
    //     return this.each(function () {
    //         if ($(this).data('owl-init') === true) {
    //             return false;
    //         }
    //         $(this).data('owl-init', true);
    //         var carousel = Object.create(Carousel);
    //         carousel.init(options, this);
    //         $.data(this, 'owlCarousel', carousel);
    //     });
    // };

    // $.fn.owlCarousel.options = {

    //     items: 5,
    //     itemsCustom: false,
    //     itemsDesktop: [1199, 4],
    //     itemsDesktopSmall: [979, 3],
    //     itemsTablet: [768, 2],
    //     itemsTabletSmall: false,
    //     itemsMobile: [479, 1],
    //     singleItem: false,
    //     itemsScaleUp: false,

    //     slideSpeed: 200,
    //     paginationSpeed: 800,
    //     rewindSpeed: 1000,

    //     autoPlay: false,
    //     stopOnHover: false,

    //     navigation: false,
    //     navigationText: ['prev', 'next'],
    //     rewindNav: true,
    //     scrollPerPage: false,

    //     pagination: true,
    //     paginationNumbers: false,

    //     responsive: true,
    //     responsiveRefreshRate: 200,
    //     responsiveBaseWidth: window,

    //     baseClass: 'owl-carousel',
    //     theme: 'owl-theme',

    //     lazyLoad: false,
    //     lazyFollow: true,
    //     lazyEffect: 'fade',

    //     autoHeight: false,

    //     jsonPath: false,
    //     jsonSuccess: false,

    //     dragBeforeAnimFinish: true,
    //     mouseDrag: true,
    //     touchDrag: true,

    //     addClassActive: false,
    //     transitionStyle: false,

    //     beforeUpdate: false,
    //     afterUpdate: false,
    //     beforeInit: false,
    //     afterInit: false,
    //     beforeMove: false,
    //     afterMove: false,
    //     afterAction: false,
    //     startDragging: false,
    //     afterLazyLoad: false
    // };
    
    
    $(".bt-descricao").click(function () {
        $(this).parent().parent().find(".formulario-descricao").fadeToggle();
    });

    $(".fechar-form").click(function () {
        $(this).parent().parent().parent().find(".formulario-descricao").fadeToggle();
    });


    $(".Category-radio").click(function () {
        $("#btn-pesquisa").css("opacity", "1");
        var target = $("#btn-pesquisa");
        if (target.length) {
            $('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 1000);
        }

    });

        console.log('entrou');
        $('html, body').stop().animate({
          scrollTop: $("#conteudo-blog").offset().top
      }, 1000);
   

    


    $("#form-busca").submit(function () {


        if ($("#email").val() == "") {
            $(".email.alert-campos").fadeToggle();
            $(".email.radio-red").fadeToggle();
            $("#email").addClass("alert-bord");

            setTimeout(function () {
                $(".email.alert-campos").fadeToggle();
                $(".email.radio-red").fadeToggle();
                $("#email").removeClass("alert-bord");
            }, 3000);

            $("#email").focus();

            return false;
        }

        if ($("#marca").val() == "") {
            $(".marca.alert-campos").fadeToggle();
            $(".marca.radio-red").fadeToggle();
            $("#marca").addClass("alert-bord");

            $("#marca").focus();

            setTimeout(function () {
                $(".marca.alert-campos").fadeToggle();
                $(".marca.radio-red").fadeToggle();
                $("#marca").removeClass("alert-bord");
            }, 3000);

            return false;
        }

        return true;

        $(".uil-facebook-css").fadeToggle();
        $(".div-load button").css("opacity", "0");

        $(".Icon.Icon--magnifierAfter:after").css("display", "none");

    });



})(jQuery, window, document);