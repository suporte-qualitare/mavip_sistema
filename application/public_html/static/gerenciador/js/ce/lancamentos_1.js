//VEIO MANTER? BOA SORTE! :D
$('#select2Categoria').select2({
    formatNoMatches: function (text) {
        // Add the AJAX behavior to the anchor, but do it asynchronously
        // This way we give time to select2 to render the layout before we configure it
        var nome = "" + text;

        return "Categoria não existe. <br><a href='#' id='suggestButtonCategoria'  value='" + nome + "'>Criar nova Categoria</a>";
        // onclick='addTipoDespesaToDatabase(" + text + ")'
        //onclick = 'addTipoDespesaToDatabase(\"" + nome + "\")'
    },
});
$('#select2CategoriaReceita').select2({
    formatNoMatches: function (text) {
        // Add the AJAX behavior to the anchor, but do it asynchronously
        // This way we give time to select2 to render the layout before we configure it
        var nome = "" + text;

        return "Categoria não existe. <br><a href='#' id='suggestButtonCategoria'  value='" + nome + "'>Criar nova Categoria</a>";
        // onclick='addTipoDespesaToDatabase(" + text + ")'
        //onclick = 'addTipoDespesaToDatabase(\"" + nome + "\")'
    },
});
$('#select2Conta').select2({
    formatNoMatches: function (text) {
        // Add the AJAX behavior to the anchor, but do it asynchronously
        // This way we give time to select2 to render the layout before we configure it
        var nome = "" + text;

        return "Conta não existe.<br> <a href='#' id='suggestButtonConta'  value='" + nome + "'>Criar nova Conta</a>";
        // onclick='addTipoDespesaToDatabase(" + text + ")'
        //onclick = 'addTipoDespesaToDatabase(\"" + nome + "\")'
    },
});
$('#select2Conta').on("select2-open", function () {
    $('.select2-drop').filter(function () {
        return $(this).css('display') == 'block';
    }).find($('.select2-input')).on('focus change keyup paste focusout', function () {
        if ($('#suggestButtonConta').length > 0) {
            document.getElementById('suggestButtonConta').onclick = function () {
                $.ajax({
                    type: "POST",
                    url: '/Contas/CreateAjax',
                    data: {
                        "Cor": "39372",
                        "Nome": $('#select2-drop').find('.select2-input').val(),
                        "Valor": "0",
                        "TelaInicial": true
                    },
                    dataType: "json",
                    success: function (data) {
                        contaAdicionada(data);
                    }
                });
            }
        }
    });
    console.log("ok");
});
$('#select2Categoria').on("select2-open", function () {
    $('.select2-drop').filter(function () {
        return $(this).css('display') == 'block';
    }).find($('.select2-input')).on('focus change keyup paste focusout', function () {
        if ($('#suggestButtonCategoria').length > 0) {
            document.getElementById('suggestButtonCategoria').onclick = function () {
                $.ajax({
                    type: "POST",
                    url: '/TipoDespesas/CreateAjax',
                    data: {
                        "Cor": "39372",
                        "Nome": $('#select2-drop').find('.select2-input').val()
                    },
                    dataType: "json",
                    success: function (data) {
                        tipoDespesaAdicionada(data);
                    }
                });
            }
        }
    });
});
$('#select2CategoriaReceita').on("select2-open", function () {
    $('.select2-drop').filter(function () {
        return $(this).css('display') == 'block';
    }).find($('.select2-input')).on('focus change keyup paste focusout', function () {
        if ($('#suggestButtonCategoria').length > 0) {
            document.getElementById('suggestButtonCategoria').onclick = function () {
                $.ajax({
                    type: "POST",
                    url: '/TipoReceitas/CreateAjax',
                    data: {
                        "Cor": "39372",
                        "Nome": $('#select2-drop').find('.select2-input').val()
                    },
                    dataType: "json",
                    success: function (data) {
                        tipoReceitasAdicionada(data);
                    }
                });
            }
        }
    });
});
function contaAdicionada(id) {
    $('.select2-drop').filter(function () {
        return $(this).css('display') == 'block';
    })
        .find('ul')
        .append('<li class="select2-results-dept-0 select2-result select2-result-selectable"><div class="select2-result-label"><span class="select2-match"></span>' + $('#select2-drop').find('.select2-input').val() + '</div></li>');
    $('#select2Conta').append('<option value=' + id + '>' + $('#select2-drop').find('.select2-input').val() + '</option>');
    $('#select2Conta').find('option[value=' + id + ']').attr('selected', 'selected');
    $('#s2id_select2Conta').find('.select2-chosen').html($('#select2-drop').find('.select2-input').val());
    $('#select2Conta').select2('close');

    notificacao("Sucesso!", "Seus dados foram salvos.", "im-happy", "success-notice");

}
function tipoReceitasAdicionada(id) {
    $('.select2-drop').filter(function () {
        return $(this).css('display') == 'block';
    })
        .find('ul')
        .append('<li class="select2-results-dept-0 select2-result select2-result-selectable"><div class="select2-result-label"><span class="select2-match"></span>' + $('#select2-drop').find('.select2-input').val() + '</div></li>');
    $('#select2CategoriaReceita').append('<option value=' + id + '>' + $('#select2-drop').find('.select2-input').val() + '</option>');
    $('#select2CategoriaReceita').find('option[value=' + id + ']').attr('selected', 'selected');
    $('#s2id_select2CategoriaReceita').find('.select2-chosen').html($('#select2-drop').find('.select2-input').val());
    $('#select2CategoriaReceita').select2('close');

    notificacao("Sucesso!", "Seus dados foram salvos.", "im-happy", "success-notice");

}
function tipoDespesaAdicionada(id) {
    $('.select2-drop').filter(function () {
        return $(this).css('display') == 'block';
    })
        .find('ul')
        .append('<li class="select2-results-dept-0 select2-result select2-result-selectable"><div class="select2-result-label"><span class="select2-match"></span>' + $('#select2-drop').find('.select2-input').val() + '</div></li>');
    $('#select2Categoria').append('<option value=' + id + '>' + $('#select2-drop').find('.select2-input').val() + '</option>');
    $('#select2Categoria').find('option[value=' + id + ']').attr('selected', 'selected');
    $('#s2id_select2Categoria').find('.select2-chosen').html($('#select2-drop').find('.select2-input').val());
    $('#select2Categoria').select2('close');

    notificacao("Sucesso!", "Seus dados foram salvos.", "im-happy", "success-notice");

}