INSERT INTO `acl_roles` (`id`, `name`) VALUES (1, 'Administrador');

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('painel', 'list', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('painel', 'list', 1, 1);

INSERT INTO `users` (`id`, `name`, `login`, `password`, `salt`, `email`, `date_create`, `active`, `role_id`) VALUES
(NULL, 'admin', 'admin', SHA1('123123'), '', NULL, NULL, '1', '1');

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('users', 'list', NULL),
('users', 'new', NULL),
('users', 'update', NULL),
('users', 'delete', NULL),
('users', 'active', NULL),
('users', 'password', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('users', 'list', 1, 1),
('users', 'new', 1, 1),
('users', 'update', 1, 1),
('users', 'delete', 1, 0),
('users', 'active', 1, 1),
('users', 'password', 1, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('parceiro', 'list', NULL),
('parceiro', 'new', NULL),
('parceiro', 'update', NULL),
('parceiro', 'delete', NULL),
('parceiro', 'active', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('parceiro', 'list', 1, 1),
('parceiro', 'new', 1, 1),
('parceiro', 'update', 1, 1),
('parceiro', 'delete', 1, 1),
('parceiro', 'active', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('parceirocategoria', 'list', NULL),
('parceirocategoria', 'new', NULL),
('parceirocategoria', 'update', NULL),
('parceirocategoria', 'delete', NULL),
('parceirocategoria', 'active', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('parceirocategoria', 'list', 1, 1),
('parceirocategoria', 'new', 1, 1),
('parceirocategoria', 'update', 1, 1),
('parceirocategoria', 'delete', 1, 1),
('parceirocategoria', 'active', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('categoriablog', 'list', NULL),
('categoriablog', 'new', NULL),
('categoriablog', 'update', NULL),
('categoriablog', 'delete', NULL),
('categoriablog', 'active', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('categoriablog', 'list', 1, 1),
('categoriablog', 'new', 1, 1),
('categoriablog', 'update', 1, 1),
('categoriablog', 'delete', 1, 1),
('categoriablog', 'active', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('cupom', 'list', NULL),
('cupom', 'new', NULL),
('cupom', 'newmulti', NULL),
('cupom', 'update', NULL),
('cupom', 'delete', NULL),
('cupom', 'active', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('cupom', 'list', 1, 1),
('cupom', 'new', 1, 1),
('cupom', 'newmulti', 1, 1),
('cupom', 'update', 1, 1),
('cupom', 'delete', 1, 1),
('cupom', 'active', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('categoriablog', 'list', NULL),
('categoriablog', 'new', NULL),
('categoriablog', 'update', NULL),
('categoriablog', 'delete', NULL),
('categoriablog', 'active', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('categoriablog', 'list', 1, 1),
('categoriablog', 'new', 1, 1),
('categoriablog', 'update', 1, 1),
('categoriablog', 'delete', 1, 1),
('categoriablog', 'active', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('pagina', 'update', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('pagina', 'update', 1, 1);


INSERT INTO `pagina` (`id`, `titulo`, `texto`, `description`, `keywords`) VALUES
(1, 'Quem Somos', '', '', '');


INSERT INTO `cliente_natureza` (`id`, `nome`) VALUES
(1, 'Pessoa Jurídica'),
(2, 'Pessoa Física'),
(3, 'Microempresa assim definida em lei'),
(4, 'Associação com intuito não econômico'),
(5, 'Sociedade com intuito não econômico'),
(6, 'Órgão Público'),
(7, 'Instituição de Ensino e Pesquisa'),
(8, 'Empresa de Pequeno Porte assim definidas em lei'),
(9, 'Cooperativa assim definida em lei'),
(10, 'Microempreendedor Individual - MEI');


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('busca', 'list', NULL),
('busca', 'delete', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('busca', 'list', 1, 1),
('busca', 'delete', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('categoria', 'list', NULL),
('categoria', 'new', NULL),
('categoria', 'update', NULL),
('categoria', 'delete', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('categoria', 'list', 1, 1),
('categoria', 'new', 1, 1),
('categoria', 'update', 1, 1),
('categoria', 'delete', 1, 1);

INSERT INTO `configs` (`chave`, `valor`) VALUES
('registro_valor', '1000.00'),
('gru_valor', '355.00'),
('renovacao_valor', '1200.00'),
('acompanhamento_valor', '800.00'),
('gru_valor_pf', '355.00');


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('pedido', 'list', NULL),
('pedido', 'view', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('pedido', 'list', 1, 1),
('pedido', 'view', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('cliente', 'list', NULL),
('cliente', 'view', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('cliente', 'list', 1, 1),
('cliente', 'view', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('marca', 'list', NULL),
('marca', 'view', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('marca', 'list', 1, 1),
('marca', 'view', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('leitura', 'list', NULL),
('leitura', 'new', NULL),
('leitura', 'view', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('leitura', 'list', 1, 1),
('leitura', 'new', 1, 1),
('leitura', 'view', 1, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('buscatermo', 'list', NULL),
('buscatermo', 'new', NULL),
('buscatermo', 'update', NULL),
('buscatermo', 'delete', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('buscatermo', 'list', 1, 1),
('buscatermo', 'new', 1, 1),
('buscatermo', 'update', 1, 1),
('buscatermo', 'delete', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('buscaexcecao', 'list', NULL),
('buscaexcecao', 'new', NULL),
('buscaexcecao', 'update', NULL),
('buscaexcecao', 'delete', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('buscaexcecao', 'list', 1, 1),
('buscaexcecao', 'new', 1, 1),
('buscaexcecao', 'update', 1, 1),
('buscaexcecao', 'delete', 1, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('cliente', 'update', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('cliente', 'update', 1, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('marca', 'update', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('marca', 'update', 1, 1);

INSERT INTO `acl_roles` (`id`, `name`) VALUES (2, 'Parceiro');

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('painel', 'list', 2, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('users', 'password', 2, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('painel', 'rendimento', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('painel', 'rendimento', 2, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('pedido', 'pagar', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('pedido', 'pagar', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('configs', 'list', NULL),
('configs', 'update', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('configs', 'list', 1, 1),
('configs', 'update', 1, 1);



INSERT INTO `acl_roles` (`id`, `name`) VALUES (3, 'Atendimento');

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('painel', 'list', 3, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('users', 'password', 3, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('operacao', 'list', NULL),
('operacao', 'decenio', NULL),
('operacao', 'listall', NULL),
('operacao', 'assumir', NULL),
('operacao', 'concluir', NULL);


INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('operacao', 'list', 1, 1),
('operacao', 'decenio', 1, 1),
('operacao', 'listall', 1, 1),
('operacao', 'assumir', 1, 1),
('operacao', 'concluir', 1, 1),
('operacao', 'list', 3, 1),
('operacao', 'assumir', 3, 1),
('operacao', 'concluir', 3, 1);



INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('servico', 'list', NULL),
('servico', 'update', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('servico', 'list', 1, 1),
('servico', 'update', 1, 1);

INSERT INTO `servico` (`id`, `titulo`, `descricao`, `valor`, `valor_gru`, `valor_gru_pj`, `vendavel`, `gru`, `ordem`) VALUES
(1, 'Pedido de registro de marca', 'Segunda Fase do registro de uma marca. O pedido de registro é caracterizado pela existência do processo na base de dados do INPI, onde após publicado inicia-se a contagem dos prazos para manifestação do Órgão e de terceiros interessados.', '1680.00', '142.00', '355.00', 1, 1, 2),
(2, 'Acompanhamento de processo', 'Semelhante a um seguro automotivo, o acompanhamento de processo é realizado toda semana para identificar movimento no processo de pedido de registro de uma marca. Caso seja identificado movimentação o empresário é notificado para que sejam tomadas as providências no prado ordinário de 60 dias, e em alguns casos extraordinário mais 30 dias, sob pena de arquivamento definitivo do processo. Em caso de exigência, arquivamento ou indeferimento sem manifestação o empresário perderá todo tempo e dinheiro investido.', '240.00', NULL, NULL, 1, 0, 3),
(3, 'Registro de Marca (Prazo Ordinário)', 'Terceira Fase processual, é a mais aguardada por todos. A partir da publicação do Registro da Marca, o empresário poderá usar a marca por 10 anos consecutivos.', '1680.00', '298.00', '745.00', 1, 1, 4),
(4, 'Registro de Marca (Prazo Extraordinário)', 'Terceira Fase processual, é a mais aguardada por todos. A partir da publicação do Registro da Marca, o empresário poderá usar a marca por 10 anos consecutivos.', '1680.00', '446.00', '1115.00', 1, 1, 5),
(5, 'Pesquisa de marca/ Busca de Marca', 'É a Primeira Fase do registro e um dos momentos mais importantes do processo de registro de uma marca. Através da Pesquisa/Busca o empresário fica sabendo se a marca é inviável, viável ou crítica. Lembrando que no processo de registro está presente o “Fator Subjetivo”, caracterizado pela decisão de um analista do Instituto Nacional da Propriedade Industrial - INPI.\r\n', NULL, NULL, NULL, 0, 0, 1),
(6, 'Manifestação Oposição', 'Em alguns momentos o empresário poderá se manifestar no prazo de 60 dias contados da publicação pelo INPI do processo ou da posição, garantindo o direito de ampla defesa da marca.', NULL, NULL, NULL, 0, 0, 6),
(7, 'Cumprimento de exigência', 'Quando o pedido de registro de marca não atender ao padrão exigido pelo INPI, será publicado uma exigência para cumprimento no prazo de 60 dias sob pena de arquivamento do processo. O pedido realizado pela ferramenta Mavip nunca recebeu um pedido de exigência.', NULL, NULL, NULL, 0, 0, 7),
(8, 'Renovação do Registro de Marca (Prazo Ordinário)', 'Passado 10 anos de uso de uma Marca Registrada é necessário renovar o registro por mais 10 anos, ou seja, iguais períodos sucessivos.', '1068.00', '426.00', '1065.00', 1,1, 8),
(9, 'Renovação do Registro de Marca (Prazo Extraordinário)', 'Passado 10 anos de uso de uma Marca Registrada é necessário renovar o registro por mais 10 anos, ou seja, iguais períodos sucessivos.', '1068.00', '644.00', '1610.00', 1, 1, 9),
(10, 'Transferência de titularidade', 'Caso a empresa necessite transferir os direitos sobre a marca, seja por venda da marca ou por alteração no CNPJ é necessário comunicar ao INPI essa alteração.', NULL, NULL, NULL, 0, 0, 10),
(11, 'Pedido de registro de programa de computador', 'Proteção conferida para algoritmos, telas e rotinas de um programa de computador (software), durante 50 anos. É uma ferramenta importantíssima para comprovação de propriedade, assim como, comprova que a empresa possui título(s) de investimento em tecnologia.', NULL, NULL, NULL, 0, 0, 11),
(12, 'Pesquisa de patente ou modelo de utilidade', 'Primeira Fase do pedido de patente ou modelo de utilidade, é caracterizado pela busca de viabilidade do pedido no INPI.', NULL, NULL, NULL, 0, 0, 12),
(13, 'Pedido de patente ou modelo de utilidade', 'Segunda Fase do pedido de patente ou modelo de utilidade. O pedido de patente é caracterizado pelo conjunto descritivo, com imagem ou desenhos, capazes de identificar a novidade e aplicação industrial.', NULL, NULL, NULL, 0, 0, 13),
(14, 'Acompanhamento da patente ou modelo de utilidade', 'Semelhante a um seguro automotivo, o acompanhamento de processo é realizado toda semana para identificar movimento no processo e pedido de patente ou modelo de utilidade. Caso seja identificado movimentação o inventor é notificado para que sejam tomadas as providências no prado determinado pelo INPI.', NULL, NULL, NULL, 0, 0, 14),
(15, 'Patente ou modelo de utilidade', 'Terceira Faseprocessual, é a mais aguardada pelos inventores. A partir da publicação da patente ou modelo de utilidade, o inventor poderá explorar por 20 anos consecutivos.', NULL, NULL, NULL, 0, 0, 15);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('operacao', 'gerargru', NULL),
('operacao', 'gerarpeticionamento', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('operacao', 'gerargru', 1, 1),
('operacao', 'gerarpeticionamento', 1, 1),
('operacao', 'gerargru', 3, 1),
('operacao', 'gerarpeticionamento', 3, 1);


INSERT INTO `acl_roles` (`id`, `name`) VALUES (4, 'Colaborador');

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('painel', 'list', 4, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('users', 'password', 4, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('operacao', 'list', 4, 1),
('operacao', 'listall', 4, 1),
('operacao', 'assumir', 4, 1),
('operacao', 'concluir', 4, 1),
('operacao', 'decenio', 4, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('operacao', 'gerargru', 4, 1),
('operacao', 'gerarpeticionamento', 4, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('marca', 'list', 4, 1),
('marca', 'view', 4, 1),
('marca', 'update', 4, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('marca', 'new', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('marca', 'new', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('blog', 'list', NULL),
('blog', 'new', NULL),
('blog', 'update', NULL),
('blog', 'delete', NULL),
('blog', 'active', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('blog', 'list', 1, 1),
('blog', 'new', 1, 1),
('blog', 'update', 1, 1),
('blog', 'delete', 1, 1),
('blog', 'active', 1, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('servico', 'active', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('servico', 'active', 1, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('marcahistoricomensagem', 'list', NULL),
('marcahistoricomensagem', 'new', NULL),
('marcahistoricomensagem', 'update', NULL),
('marcahistoricomensagem', 'delete', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('marcahistoricomensagem', 'list', 1, 1),
('marcahistoricomensagem', 'new', 1, 1),
('marcahistoricomensagem', 'update', 1, 1),
('marcahistoricomensagem', 'delete', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('busca', 'delete-lote', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('busca', 'delete-lote', 1, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('blacklist', 'list', NULL),
('blacklist', 'new', NULL),
('blacklist', 'update', NULL),
('blacklist', 'delete', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('blacklist', 'list', 1, 1),
('blacklist', 'new', 1, 1),
('blacklist', 'update', 1, 1),
('blacklist', 'delete', 1, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('faq', 'list', NULL),
('faq', 'new', NULL),
('faq', 'update', NULL),
('faq', 'active', NULL),
('faq', 'delete', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('faq', 'list', 1, 1),
('faq', 'new', 1, 1),
('faq', 'update', 1, 1),
('faq', 'active', 1, 1),
('faq', 'delete', 1, 1);


INSERT INTO `acl_roles` (`id`, `name`) VALUES (5, 'Editor');

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('painel', 'list', 5, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('users', 'password', 5, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('blog', 'list', 5, 1),
('blog', 'new', 5, 1),
('blog', 'update', 5, 1),
('blog', 'delete', 5, 1),
('blog', 'active', 5, 1);


INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('faq', 'list', 5, 1),
('faq', 'new', 5, 1),
('faq', 'update', 5, 1),
('faq', 'active', 5, 1),
('faq', 'delete', 5, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('pagina', 'update', 5, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('busca', 'datatable', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('busca', 'datatable', 1, 1);




INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('pedido', 'cancelar', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('pedido', 'cancelar', 1, 1);


INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('busca', 'list', 4, 1),
('busca', 'datatable', 4, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('pedido', 'list', 4, 1),
('pedido', 'view', 4, 1);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('cliente', 'list', 4, 1),
('cliente', 'view', 4, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('cliente', 'viewinpi', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('cliente', 'viewinpi', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('options', 'list', NULL),
('options', 'update', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('options', 'list', 1, 1),
('options', 'update', 1, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('equipe', 'list', NULL),
('equipe', 'new', NULL),
('equipe', 'update', NULL),
('equipe', 'delete', NULL),
('equipe', 'active', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('equipe', 'list', 1, 1),
('equipe', 'new', 1, 1),
('equipe', 'update', 1, 1),
('equipe', 'delete', 1, 1),
('equipe', 'active', 1, 1);

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('banner', 'list', NULL),
('banner', 'new', NULL),
('banner', 'update', NULL),
('banner', 'delete', NULL),
('banner', 'active', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('banner', 'list', 1, 1),
('banner', 'new', 1, 1),
('banner', 'update', 1, 1),
('banner', 'delete', 1, 1),
('banner', 'active', 1, 1);


INSERT INTO `configs` (`chave`, `valor`) VALUES
('precos_mercado_titulo1', 'INPI'),
('precos_mercado_valor1', 'R$ 142'),
('precos_mercado_complemento1', 'COMPLEXO'),

('precos_mercado_titulo2', 'GRANDES ESCRITÓRIOS'),
('precos_mercado_valor2', 'R$ 1.800'),
('precos_mercado_complemento2', 'CARO'),

('precos_mercado_titulo3', 'MAVIP'),
('precos_mercado_valor3', '* R$ 99,97'),
('precos_mercado_complemento3', 'EFICIENTE');

INSERT INTO `pagina` (`id`, `titulo`, `texto`, `description`, `keywords`) VALUES
(2, 'Preços', '', '', '');

INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('pagina', 'precos', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('pagina', 'precos', 1, 1);


INSERT INTO `acl_resources` (`id`, `privilege`, `parent_id`) VALUES
('depoimento', 'new', NULL),
('depoimento', 'update', NULL);

INSERT INTO `acl_access` (`resource_id`, `privilege`, `role_id`, `allow`) VALUES
('depoimento', 'new', 1, 1),
('depoimento', 'update', 1, 1);