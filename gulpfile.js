const gulp = require('gulp')
const util = require('gulp-util')
const sequence = require('run-sequence')

require('./tasks/task-scss')
//require('./tasks/task-scripts')
require('./tasks/task-watch')

gulp.task('default', () => {
	if(util.env.production){
		sequence('scss.build' /*'scripts.build'*/)
	} else{
		sequence('scss.build', /*'scripts.build',*/ 'watch')
	}
});
