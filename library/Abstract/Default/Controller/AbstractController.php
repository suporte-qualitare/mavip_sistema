<?php

abstract class Abstract_Default_Controller_AbstractController extends Zend_Controller_Action {

    /**
     *
     * @var Zend_Auth 
     */
    public $auth;
    public $_flashMessenger;

    /**
     *
     * @var Zend_Controller_Action_Helper_Redirector
     */
    public $router;

    /**
     * monta a exibição da listagem de conteudo
     * @var Array 
     */
    public $grid;

    /**
     *
     * @var Util_Acl_MyACL
     */
    public $acl;

    /**
     *
     * @var Zend_Db_Adapter_Abstract
     */
    public $db;
    private $resource;
    private $privilege;
    protected $user;

    public function init() {
        
        $configsModel = new Application_Model_Configs();
        $this->configs = $configsModel->getAllToArray();
        $this->view->configs = $this->configs;
        
        $order_session = new Zend_Session_Namespace('Order_Site');
        $this->view->order_session = $order_session;
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->router = $this->_helper->getHelper('Redirector');
        
        $auth_session = new Zend_Session_Namespace('Zend_Auth_Office');
        $auth_session->setExpirationSeconds(3600); // 1 hora, 3600 secs

        $this->auth = Zend_Auth::getInstance();
        $this->auth->setStorage(new Zend_Auth_Storage_Session('Zend_Auth_Mavip'));
        $this->view->auth = $this->auth;
        
        if ($this->auth->hasIdentity()) {
            $associadoMapper = new Application_Model_Cliente();
            $temp = $this->auth->getIdentity();
            $this->user = $associadoMapper->getById($temp->id);
            $this->view->user = $this->user;

        }

      
        $this->db = Zend_Db_Table::getDefaultAdapter();
        $this->view->msg = $this->_flashMessenger->getMessages();
        
        $optionsModel = new Application_Model_Options();
        $this->view->options = $optionsModel->getAllToArray();
        
        $televendaModel = new Application_Model_Televenda();
        $this->view->televenda = $televendaModel->getAll();
    }

    protected function addFlashMessage(array $msg, $redirect = array()) {
        $this->_flashMessenger->addMessage($msg);

        if (count($redirect)) {
            $this->router->gotoRoute($redirect);
        }
    }

    protected function addFlashMessageDirect(array $msg) {
        $this->view->msg[] = $msg;
    }

    protected function getResource() {
        if (!$this->resource) {
            $this->resource = $this->_request->getControllerName();
        }
        $this->view->resource = $this->resource;
        return $this->resource;
    }

    protected function setResource($resource) {
        $this->view->resource = $resource;
        $this->resource = $resource;
    }

    protected function getPrivilege() {
        if (!$this->privilege) {
            $this->privilege = $this->_request->getActionName();
        }
        $this->view->privilege = $this->privilege;
        return $this->privilege;
    }

    protected function setPrivilege($privilege) {
        $this->view->privilege = $privilege;
        $this->privilege = $privilege;
    }

    

    protected function testaBloqueio() {
        $this->addFlashMessage(array('Funcionalidade inativa no momento', 0));
        $this->router->gotoRoute(array('controller' => 'painel', 'action' => 'list'));
    }

}
