<?php

abstract class Abstract_Gerenciador_Controller_AbstractController extends Zend_Controller_Action {

    /**
     *
     * @var Zend_Auth 
     */
    public $auth;
    public $_flashMessenger;

    /**
     *
     * @var Zend_Controller_Action_Helper_Redirector
     */
    public $router;

    /**
     * monta a exibição da listagem de conteudo
     * @var Array 
     */
    public $grid;

    /**
     *
     * @var Util_Acl_MyACL
     */
    public $acl;
    private $resource;
    private $privilege;
    protected $user;

    /**
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $db;

    public function init() {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->router = $this->_helper->getHelper('Redirector');
        $this->log = new Application_Model_Logs();
        $this->db = Zend_Db_Table::getDefaultAdapter();

        $this->acl = new Util_Acl_MyACL($this->db);
        $this->view->acl = $this->acl;

        $auth_session = new Zend_Session_Namespace('Zend_Auth_App');
        $auth_session->setExpirationSeconds(3600); // 1 hora, 3600 secs
        
        $this->auth = Zend_Auth::getInstance();
        $this->auth->setStorage(new Zend_Auth_Storage_Session('Zend_Auth_App'));
        

        $this->_usuario = $this->auth->getIdentity();
        $this->view->usuario = $this->auth->getIdentity();
        
        if ($this->auth->hasIdentity() AND $this->_request->getControllerName() != 'conta' AND $this->_request->getControllerName() != 'error') {
            
            $usuarios = new Application_Model_Users();
            //$user = $usuarios->getRow($this->_usuario->id);
            //$this->view->usuario = $user;
            
            if(!$this->_usuario){
                $this->auth->clearIdentity();
                $this->router->gotoRoute(array('controller'=>'conta','action'=>'login'), 'gerenciador', true);
            }
            
            

            if (!$this->acl->isAllowed($this->_usuario->role_id, $this->getResource(), $this->getPrivilege())) {
                //$this->addFlashMessage(array('Acesso negado', 0), array('controller' => 'painel', 'action' => 'list'));
                throw new Exception('Sem Permissão');
            }
        } else {

            if ($this->_request->getControllerName() != 'conta' AND $this->_request->getControllerName() != 'error') {
                $this->router->gotoRoute(array('controller' => 'conta', 'action' => 'login'), 'gerenciador', true);
            }
        }

        $this->_helper->layout->setLayout('gerenciador');
        $this->view->msg = $this->_flashMessenger->getMessages();
    }

    protected function addFlashMessage(array $msg, $redirect = array()) {
        $this->_flashMessenger->addMessage($msg);

        if (count($redirect)) {
            $this->router->gotoRoute($redirect);
        }
    }

    protected function addFlashMessageDirect(array $msg) {
        $this->view->msg[] = $msg;
    }

    protected function getResource() {
        if (!$this->resource) {
            $this->resource = $this->_request->getControllerName();
        }
        $this->view->resource = $this->resource;
        return $this->resource;
    }

    protected function setResource($resource) {
        $this->view->resource = $resource;
        $this->resource = $resource;
    }

    protected function getPrivilege() {
        if (!$this->privilege) {
            $this->privilege = $this->_request->getActionName();
        }
        $this->view->privilege = $this->privilege;
        return $this->privilege;
    }

    protected function setPrivilege($privilege) {
        $this->view->privilege = $privilege;
        $this->privilege = $privilege;
    }

    protected function setMenu($menu) {
        $this->view->menu = $menu;
    }

}
