<?php

abstract class Abstract_Gerenciador_Controller_CrudController extends Abstract_Gerenciador_Controller_RestrictController {

    private $viewRenderList;
    private $viewRenderNew;
    private $viewRenderUpdate;
    private $viewRenderView;
    private $title;
    private $deleteFinal = 1;
    private $where;
    private $order;
    private $owner;
    private $ownerDate;
    private $option;

    /**
     * @return Zend_Db_Table_Abstract
     */
    abstract function getRepository();

    public function getMessages($chave, $dados, $chaveDado = 'nome') {

        $resultado = '';
        if ($dados[$chaveDado]) {
            $resultado = ': ' . $dados[$chaveDado];
        }

        $mensagem = array(
            'new' => 'Criou na seção ' . $this->getTitle() . $resultado,
            'update' => 'editou na seção ' . $this->getTitle() . $resultado,
            'delete' => 'excluiu na seção ' . $this->getTitle() . $resultado,
            'active' => 'ativou/inativou na seção ' . $this->getTitle() . $resultado,
        );
        return $mensagem[$chave];
    }

    /**
     * @return Zend_Form
     */
    abstract function getForm($isEditing = 0);

    public function getColumns() {
        return
                array(
                    'id' => array('label' => '#'),
                    'name' => array('label' => 'Nome'),
        );
    }

    public function listAction() {
        $records = $this->getRecords();
        $this->view->grid = array('columns' => $this->getColumns(), 'records' => $records);
        $this->view->title = $this->getTitle();
        $this->view->option = $this->getOption();
        $this->renderScript($this->getViewRenderList());
    }

    public function getRecords() {
        return $this->getRepository()->fetchAll($this->getWhere(), $this->getOrder())->toArray();
    }

    public function newAction() {

        $form = $this->getForm();

        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                try {
                    $this->db->beginTransaction();
                    $postData = $this->preparingData($form->getValues(), 0, $row);
                    $row = $this->getRepository()->createRow($postData);
                    $this->beforeSaving($row, $form->getValues(), 0);
                    $row->save();
                    $this->afterSaving($row, $form->getValues(), 0);
                    $this->db->commit();
                    $this->log->create($this->_usuario->id, $this->getMessages('new', $row->toArray()));
                    $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'list'));
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                    exit;
                    $this->db->rollBack();
                    $this->addFlashMessage(array('Erro ao salvar conteúdo', 0));
                    $this->router->gotoRoute(array());
                }
            }
        }

        $this->view->form = $form;
        $this->view->title = $this->getTitle();

//        if ($this->getViewRenderNew() == 'processo/new.phtml') {
//            $id_pedido = $form->getValues();
//            $url = $this->url(array('controller' => 'pedido', 'action' => 'view', 'id' => $id_pedido['id_pedido']));
//            header('Location: ' . $url);
//        } else {
//            $this->renderScript($this->getViewRenderNew());
//        }

        $this->renderScript($this->getViewRenderNew());
    }

    public function updateAction() {
        $form = $this->getForm($this->_getParam('id'));
        $rep = $this->getRepository();
        $row = $rep->fetchRow(array('id = ?' => $this->_getParam('id')));
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                $postData = $this->preparingData($form->getValues(), 1, $row);
                $row->setFromArray($postData);
                $this->beforeSaving($row, $form->getValues(), 1);
                $row->save();
                $this->afterSaving($row, $form->getValues(), 1);
                $this->log->create($this->_usuario->id, $this->getMessages('update', $row->toArray()));
                $this->addFlashMessage(array('Conteúdo salvo com sucesso', 1), array('action' => 'list'));
            }
        } else {
            $form->populate($this->fillsEdit($row->toArray()));
        }
        $this->view->form = $form;
        $this->view->title = $this->getTitle();
        $this->view->row = $row;


        $this->renderScript($this->getViewRenderUpdate());
    }

    public function viewAction() {
        $this->view->row = $this->getRepository()->fetchRow(array('id = ?' => $this->_getParam('id')));

        $this->view->title = $this->getTitle();
        $this->renderScript($this->getViewRenderView());
    }

    protected function fillsEdit($dados) {
        foreach ($this->arrayDatas() as $dataConf) {
            if (!$dados[$dataConf['col']]) {
                $dados[$dataConf['col']] = "";
            } else {
                if ($dataConf["type"] == "datetime") {
                    $data = new Zend_Date($dados[$dataConf['col']]);
                    $dados[$dataConf['col']] = $data->get('dd/MM/yyyy HH:mm');
                } else {
                    $data = new Zend_Date($dados[$dataConf['col']]);
                    $dados[$dataConf['col']] = $data->get('dd/MM/yyyy');
                }
            }
        }

        foreach ($this->arrayMoedas() as $dataConf) {
            if ($dados[$dataConf['col']]) {
                $dados[$dataConf['col']] = number_format($dados[$dataConf['col']], 2, ',', '.');
            }
        }


        return $dados;
    }

    public function deleteAction() {

        if ($this->_getParam('id')) {
            try {
                $row = $this->getRepository()->fetchRow(array('id = ?' => $this->_getParam('id')));
                if ($this->getDeleteFinal()) {
                    foreach ($this->arrayFiles() as $conf) {
                        $this->deleteFile($row[$conf['col']], $conf);
                    }
                    $row->delete();
                } else {
                    $this->getRepository()->update(array('active' => -1), array('id = ?' => $this->_getParam('id')));
                }
                $this->log->create($this->_usuario->id, $this->getMessages('delete', $row->toArray()));

                if ($this->getParam('origem') == 'operacao') {
                    $action = 'concluir';
                    $controller = 'operacao';
                    $module = NULL;
                    $params = array(
                        'id' => $this->getParam('pedido')
                    );

                    $this->_helper->redirector($action, $controller, $module, $params);
                }
            } catch (Exception $exc) {
                $this->addFlashMessage(array('Não foi possível excluir conteúdo, provavelmente exitem dependencia de outros conteúdos', 0), array('action' => 'list'));
            }
            $this->addFlashMessage(array('Conteúdo excluído com sucesso', 1), array('action' => 'list'));
        }
        $this->addFlashMessage(array('Não foi possível excluir conteúdo', 1), array('action' => 'list'));
    }

    public function activeAction() {
        if ($this->_getParam('id')) {
            $row = $this->getRepository()->fetchRow(array('id = ?' => $this->_getParam('id')));
            $this->getRepository()->update(array('active' => $this->_getParam('active')), array('id = ?' => $this->_getParam('id')));
            $this->log->create($this->_usuario->id, $this->getMessages('active', $row->toArray()));
            $this->addFlashMessage(array('Alteração realizada com sucesso', 1), array('action' => 'list'));
        }
        $this->addFlashMessage(array('Erro ao realizar alteração', 1), array('action' => 'list'));
    }

    protected function beforeSaving($row, $formValues, $isEditing = 0) {
        if ($isEditing) {
            if (in_array('date_update', $this->getRepository()->info('cols'))) {
                $row["date_create"] = new Zend_Db_Expr('NOW()');
            }
            if (in_array('data_edicao', $this->getRepository()->info('cols'))) {
                $row["data_edicao"] = new Zend_Db_Expr('NOW()');
            }
        } else {
            if (in_array('date_create', $this->getRepository()->info('cols'))) {
                $row["date_create"] = new Zend_Db_Expr('NOW()');
            }
            if (in_array('data_cadastro', $this->getRepository()->info('cols'))) {
                $row["data_cadastro"] = new Zend_Db_Expr('NOW()');
            }
        }
        return $row;
    }

    protected function afterSaving($row, $formValues, $isEditing = 0) {
        return $row;
    }

    protected function preparingData($postDate, $isEditing = 0, $row = null) {

        if (!$isEditing AND $this->getOwner()) {
            $postDate[$this->getOwner()] = $this->getOwnerDate();
        }

        foreach ($this->arrayDatas() as $dataConf) {
            if (!$postDate[$dataConf['col']]) {
                $postDate[$dataConf['col']] = null;
            } else {
                if ($dataConf['type'] == "datetime") {
                    $data = new Zend_Date($postDate[$dataConf['col']]);
                    $postDate[$dataConf['col']] = $data->get('yyyy-MM-dd HH:mm');
                } else {
                    $data = new Zend_Date($postDate[$dataConf['col']]);
                    $postDate[$dataConf['col']] = $data->get('yyyy-MM-dd');
                }
            }
        }

        foreach ($this->arrayFiles() as $conf) {
            if (!$postDate[$conf['col']]) {
                unset($postDate[$conf['col']]);
            } else {
                if ($row) {
                    $this->deleteFile($row[$conf['col']], $conf);
                }
                $this->resizeImage($postDate[$conf['col']], $conf);
            }
        }

        foreach ($this->arrayMoedas() as $conf) {
            if ($postDate[$conf['col']]) {
                $postDate[$conf['col']] = str_replace('.', '', $postDate[$conf['col']]);
                $postDate[$conf['col']] = str_replace(',', '.', $postDate[$conf['col']]);
            }
        }


        return $postDate;
    }

    protected function deleteFile($file, $conf) {
        $path = PATH_ARQS . $this->_request->getControllerName();
        $arq = $path . '/' . $file;
        if (file_exists($arq)) {
            @unlink($arq);
        }

        if ($conf['thumbs']) {
            foreach ($conf['thumbs'] as $cf) {
                $arq = $path . '/' . $cf['width'] . '_' . $cf['height'] . '/' . $file;

                if (file_exists($arq)) {
                    @unlink($arq);
                }
            }
        }
    }

    protected function resizeImage($file, $conf) {
        $path = PATH_ARQS . $this->_request->getControllerName();
        $imagem = $path . '/' . $file;

        if (file_exists($imagem) && !empty($file)) {
            $i = new m2brimagem($imagem);
            if ($i->valida() == "OK") {

                $i->redimensiona($conf['width'], $conf['height'], $conf['type']);
                $i->grava($imagem);


                foreach ($conf['thumbs'] as $cf) {
                    $dir = $path . '/' . $cf['width'] . '_' . $cf['height'];
                    if (!file_exists($dir)) {
                        mkdir($dir, 0755, true);
                    }
                    $i2 = new m2brimagem($imagem);
                    $i2->redimensiona($cf['width'], $cf['height'], $cf['type']);
                    $i2->grava($dir . '/' . $file);
                }
            }
        }
    }

    public function getViewRenderList() {
        if (!$this->viewRenderList) {
            $this->viewRenderList = 'crud/list.phtml';
        }
        return $this->viewRenderList;
    }

    public function setViewRenderList($viewRenderList) {
        $this->viewRenderList = $viewRenderList;
    }

    public function getViewRenderNew() {
        if (!$this->viewRenderNew) {
            $this->viewRenderNew = 'crud/new.phtml';
        }
        return $this->viewRenderNew;
    }

    public function setViewRenderNew($viewRenderNew) {
        $this->viewRenderNew = $viewRenderNew;
    }

    public function getViewRenderUpdate() {
        if (!$this->viewRenderUpdate) {
            $this->viewRenderUpdate = 'crud/update.phtml';
        }
        return $this->viewRenderUpdate;
    }

    public function setViewRenderUpdate($viewRenderUpdate) {
        $this->viewRenderUpdate = $viewRenderUpdate;
    }

    public function getViewRenderView() {
        if (!$this->viewRenderView) {
            $this->viewRenderView = 'crud/view.phtml';
        }
        return $this->viewRenderView;
    }

    public function setViewRenderView($viewRenderView) {
        $this->viewRenderView = $viewRenderView;
    }

    public function getTitle() {
        if (!$this->title) {
            return $this->_request->getControllerName();
        }
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setOption($option) {
        $this->option = $option;
    }

    public function getOption() {
        return $this->option;
    }

    public function getDeleteFinal() {
        return $this->deleteFinal;
    }

    public function setDeleteFinal($deleteFinal) {
        $this->deleteFinal = $deleteFinal;
    }

    public function getWhere() {
        if ($this->getOwner()) {
            $this->where = $this->getOwner() . " = " . $this->getOwnerDate();
        }
        return $this->where;
    }

    public function setWhere($where) {
        $this->where = $where;
    }

    public function getOrder() {
        return $this->order;
    }

    public function setOrder($order) {
        $this->order = $order;
    }

    function getOwner() {
        return $this->owner;
    }

    function setOwner($owner) {
        $this->owner = $owner;
    }

    function getOwnerDate() {
        $this->ownerDate = new Zend_Session_Namespace('owner_' . $this->_request->getControllerName());
        if (!$this->_hasParam("owner") && !$this->ownerDate->id) {
            //$this->addFlashMessage(array('Conteúdo excluído com sucesso', 0), array('controller'=>'painel','action' => 'list'));
            exit('aqui');
        }
        if ($this->_hasParam("owner")) {
            $this->setOwnerDate($this->_getParam("owner"));
        }
        return $this->ownerDate->id;
    }

    function setOwnerDate($id) {
        $this->ownerDate = new Zend_Session_Namespace('owner_' . $this->_request->getControllerName());
        $this->ownerDate->id = $id;
    }

    /*
     * array('col'=>'','width'=>'','height'=>'','type'=>'')
     */

    public function arrayFiles() {
        return array();
    }

    public function arrayDatas() {
        return array();
    }

    public function arrayMoedas() {
        return array();
    }

    public function arraySecundarios() {
        return array();
    }

}
