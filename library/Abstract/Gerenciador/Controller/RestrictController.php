<?php

abstract class Abstract_Gerenciador_Controller_RestrictController extends Abstract_Gerenciador_Controller_AbstractController {

    public function init() {
        parent::init();



        if ($this->auth->hasIdentity()) {
            $teste = new Zend_Session_Namespace('ckfinder');
            $teste->is_authorized = 'true';
            $teste->base_url = $this->view->baseUrl('/arqs/ckfinder/') ;
            $teste->base_url_absoluto = PATH_ARQS . 'ckfinder/';
        } else {
            $this->router->gotoRoute(array('controller' => 'conta', 'action' => 'login'), 'gerenciador', true);
        }
    }

}
