<?php

/**
 * Description of buscaCEP
 *
 * @author vinicius_eneas
 */
include('phpQuery-onefile.php');

class Util_BuscaCEP {

    protected $cep;

    //put your code here

    function simple_curl($url, $post = array(), $get = array()) {
        $url = explode('?', $url, 2);
        if (count($url) === 2) {
            $temp_get = array();
            parse_str($url[1], $temp_get);
            $get = array_merge($get, $temp_get);
        }

        $ch = curl_init($url[0] . "?" . http_build_query($get));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return curl_exec($ch);
    }

    public function getEndereco($cep) {

        $html = $this->simple_curl('http://m.correios.com.br/movel/buscaCepConfirma.do', array(
            'cepEntrada' => $cep,
            'tipoCep' => '',
            'cepTemp' => '',
            'metodo' => 'buscarCep'
        ));
        
        phpQuery::newDocumentHTML($html, $charset = 'iso-8859-1');

        $dados = array(
            'logradouro' => trim(pq('.caixacampobranco .resposta:contains("Logradouro: ") + .respostadestaque:eq(0)')->html()),
            'bairro' => trim(pq('.caixacampobranco .resposta:contains("Bairro: ") + .respostadestaque:eq(0)')->html()),
            'cidade/uf' => trim(pq('.caixacampobranco .resposta:contains("Localidade / UF: ") + .respostadestaque:eq(0)')->html()),
            'cep' => trim(pq('.caixacampobranco .resposta:contains("CEP: ") + .respostadestaque:eq(0)')->html())
        );

        $dados['cidade/uf'] = explode('/', $dados['cidade/uf']);
        $dados['cidade'] = trim($dados['cidade/uf'][0]);
        $dados['uf'] = trim($dados['cidade/uf'][1]);
        unset($dados['cidade/uf']);
        $this->cep = $dados;
        //var_dump($this->cep);die;
        return $this->cep;
    }

}
