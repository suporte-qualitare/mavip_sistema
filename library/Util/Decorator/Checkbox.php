<?

class Util_Decorator_Checkbox extends Zend_Form_Decorator_Abstract{

    

    public function buildInput()
 {
		  $element = $this->getElement();
        $helper = $element->helper;
        $name =  $element->getName();
        if (null !== ($belongsTo = $element->getBelongsTo())) {
            $name = $belongsTo . '['
                  . $name
                  . ']';
        }
        return $element->getView()->$helper(
                        $name, $element->getValue(), $element->getAttribs(), $element->options
        );
    }
    public function render($content) {
 
        $element = $this->getElement();
        $label = htmlentities($element->getLabel());
        $elementInput = $this->buildInput();

        $markup = '<label class="checkbox">'.$elementInput.$label.'</label>';
        return $markup;
    }

}