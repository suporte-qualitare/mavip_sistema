<?php

class Util_Decorator_CompositeFront2 extends Zend_Form_Decorator_Abstract {

    public function buildLabel() {
        $element = $this->getElement();
        $label = $element->getLabel();
        if ($translator = $element->getTranslator()) {
            $label = $translator->translate($label);
        }
        if ($element->isRequired()) {
            $label = $label . '*';
        }
        $label .= ':';
        return $element->getView()
                        ->formLabel($element->getName(), $label);
    }

    public function buildInput($erros) {
        $element = $this->getElement();
        $helper = $element->helper;

        $atrib = $element->getAttribs();
        if ($erros) {
            if (isset($atrib['class'])) {
                $atrib['class'] = $atrib['class'] . ' erro';
            } else {
                $atrib['class'] = 'erro';
            }
        }



        return $element->getView()->$helper(
                        $element->getName(), $element->getValue(), $atrib, $element->options
        );
    }

    public function buildErrors() {
        $element = $this->getElement();
        $messages = $element->getMessages();
        if (empty($messages)) {
            return '';
        }
        return '' . $element->getView()->formErrors($messages) . '';
    }

    public function buildDescription() {
        $element = $this->getElement();
        $desc = $element->getDescription();
        if (empty($desc)) {
            return '';
        }
        return $desc;
    }

    public function render($content) {
        $element = $this->getElement();
        if (!$element instanceof Zend_Form_Element) {
            return $content;
        }
        if (null === $element->getView()) {
            return $content;
        }

        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
        $label = $this->buildLabel();
        $errors = $this->buildErrors();
        $input = $this->buildInput($errors);
        $desc = $this->buildDescription();
        if ($desc) {
            $desc = '<p class="description">' . $desc . '</p>';
        }
        if ($errors) {
            $errors = '';
        }

        
        $output = $input . $desc . $errors;

        switch ($placement) {
            case (self::PREPEND):
                return $output . $separator . $content;
            case (self::APPEND):
            default:
                return $content . $separator . $output;
        }
    }

}