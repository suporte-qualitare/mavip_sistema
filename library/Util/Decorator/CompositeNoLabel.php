<?php

class Util_Decorator_CompositeNoLabel extends Zend_Form_Decorator_Abstract {

    public function buildInput() {
        $element = $this->getElement();
        $helper = $element->helper;
        return $element->getView()->$helper(
                        $element->getName(), $element->getValue(), $element->getAttribs(), $element->options
        );
    }

    public function buildErrors() {
        $element = $this->getElement();
        $messages = $element->getMessages();
        if (empty($messages)) {
            return '';
        }
        return $element->getView()->formErrors($messages);
    }

    public function render($content) {
        $element = $this->getElement();
        $name = htmlentities($element->getFullyQualifiedName());
        $label = $element->getLabel();
        $elementInput = $this->buildInput();
        $errors = $this->buildErrors();

        $markup = $elementInput.$errors;
        
        return $markup;
    }

}