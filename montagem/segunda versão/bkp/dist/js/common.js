

    $(document).ready(function() {
        //slide-obras
        
         var owl = $("#obras-slide");
 
          owl.owlCarousel({
              items : 5, //10 items above 1000px browser width
              itemsDesktop : [1000,4], //5 items between 1000px and 901px
              itemsDesktopSmall : [900,3], // betweem 900px and 601px
              itemsTablet: [668,2], //2 items between 600 and 0
              itemsMobile :[480,1] 
          });
         // Custom Navigation Events
          $(".next").click(function(){
            owl.trigger('owl.next');
          })
          $(".prev").click(function(){
            owl.trigger('owl.prev');
          })
        
        
        //svg 
        jQuery('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        jQuery.get(imgURL, function (data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');
        // Add replaced image's ID to the new SVG
        if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg');
        }
        // Remove any invalid XML tags
        $svg = $svg.removeAttr('xmlns:a');
        // Replace image with new SVG
        $img.replaceWith($svg);
        }, 'xml');
        });
        
//   //fecha o menu quando clica em algum item dele
//        $(".itens-menu ul").click(function() {
//            $(this).parent().click("li").slideToggle("slow");
//            $("#nav-icon3").toggleClass();
//        });
//        //toggle do menu
//        $("#nav-icon3").click(function() {
//            //anima o botao
//            $(this).toggleClass('open');
//            //aparece-esconde menu
//            $(".itens-menu").slideToggle("slow");
//        });
//        //fecha o menu quando clica fora 
//        $(document).on('click', function(event) {
//            if (!$(event.target).closest('.menu').length) {
//                if($('.itens-menu').is(':visible')){
//                    $(".itens-menu").slideToggle("slow");
//                    $("#nav-icon3").toggleClass('open');
//                }
//            }
//        });
//        
        if (screen.width<=1024) {
            //fecha o menu quando clica em algum item dele
            $(".top-line-nav ul").click(function() {
                $(this).parent().click("li").slideToggle("slow");
                $("#nav-icon3").toggleClass();
            });
            //toggle do menu
            $("#nav-icon3").click(function() {
                //anima o botao
                $(this).toggleClass('open');
                //aparece-esconde menu
                $(".top-line-nav").slideToggle("slow");
            });

            //fecha o menu quando clica fora 
            $(document).on('click', function(event) {
                if (!$(event.target).closest('#top-line').length) {
                    if($('.top-line-nav').is(':visible')){
                        $(".top-line-nav").slideToggle("slow");
                        $("#nav-icon3").toggleClass('open');
                    }
                }
            });
        }
        
        
        // navigation click actions	
        $('.scroll-link').on('click', function(event) {
            event.preventDefault();
            var sectionID = $(this).attr("data-id");
            scrollToID('#' + sectionID, 750);
        });

        var map, placesService, myMarkers = [], gInfoWindow, myLatLng, iw;

        myLatLng = new google.maps.LatLng(-7.0884389, -34.8407154);

        var mapOptions = {
            zoom: 15,
            center: myLatLng,
            scrollwheel: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById('mapa'), mapOptions);
        placesService = new google.maps.places.PlacesService(map);
        gInfoWindow = new google.maps.InfoWindow();

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Plenus Oceania'
//            icon: '../img/pin_mapa.svg',
        });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

        // scroll function
        function scrollToID(id, speed) {
            var offSet = 70;
            var targetOffset = $(id).offset().top - offSet;
            var mainNav = $('#main-nav');
            $('html,body').animate({scrollTop: targetOffset}, speed);
            if (mainNav.hasClass("open")) {
                mainNav.css("height", "1px").removeClass("in").addClass("collapse");
                mainNav.removeClass("open");
            }
        }
        if (typeof console === "undefined") {
            console = {
                log: function() {
                }
            };
        }

        
        //form
        var submitted = false;
        $("#contact-form").validate({
            errorElement: 'label',
            errorPlacement: function(error, element) {
                error.insertBefore(element); // default function
            },
            rules: {
                nome: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                assunto: {
                    required: true,
                },
                mensagem: {
                    required: true
                }
            },
            messages: {
                nome: {
                    required: 'Informe seu nome'
                },
                email: {
                    required: 'Informe seu email',
                    email: 'Email inválido'
                },
                assunto: {
                    required: 'Informe o assunto',
                },
                mensagem: {
                    required: "Escreva sua mensagem"
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    resetForm: true,
                    url: 'functions.php',
                    dataType: 'json',
                    beforeSubmit: function(formData, jqForm, options) {
                        jqForm.find('input[submit]').attr('disabled', true);
                        $(".loader").show();
                        $('.msg-contact').hide();
                    },
                    success: function(responseText, statusText, xhr, $form) {
//                    console.log(responseText);

                        if (responseText.success) {
                            //muda o nome
                            $('.msg-contact').html("Enviado!");
                            $('.msg-contact span').remove();
                            $('.msg-contact').show();
                        } else {
                            $('.msg-contact').html(responseText.error);
                            $('.msg-contact').show();
                        }
                        $(".loader").hide();

                    },
                    error: function(responseText, statusText, xhr, $form) {
                        $('.msg-contact').html("Tente novamente mais tarde.");
                        $(".loader").hide();
                        $('.msg-contact').show();
                    }
                });
            }
        });
        
        
    });
    
    
