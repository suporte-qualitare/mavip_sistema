// Contact
window.mod.contact = function() {

  // Scope
  var that = this;

  var username = '';

  var init = function() {
    console.log('[brz] begin contact.js');
    initListeners();
  };

  var initListeners = function(){
    $('.Contact-next').click(function(e){
      var step = $(this).data('step');
      if (step == 2) {
        username = $('input[name="name"]').val();
        if(username == ''){
          alert('Insira o seu nome');
          return false;
        }
        console.log(username);
        $('.Contact-username').text(username);
      }
      $('.Contact-step').addClass('hidden');
      $('.Contact-step--' + step).removeClass('hidden');

    });
  };



  init();

};
