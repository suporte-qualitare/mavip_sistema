// Home
window.mod.home = function() {

  // Scope
  var that = this;

  var owl;

  var init = function() {
    console.log('[brz] begin home.js');
    initInviewListener();
    initCarousel();
  };

  var initInviewListener = function(){
    $('.Achievements-numberSpan').one('inview', function(event, isInView) {
      if (isInView) {
        // element is now visible in the viewport
        $(this).countTo({
            from: 0,
            to: $(this).data('number'),
            speed: 1000,
            refreshInterval: 50
        });
      } else {
        // element has gone out of viewport
      }
    });
  };

  var initCarousel = function(){
    $('#carousel').owlCarousel({
       // navigation : false, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true
    });

    owl = $('#carousel').data('owlCarousel');

    $('.Testimonial-arrow--prev').click(function(){
      owl.prev();
    });

    $('.Testimonial-arrow--next').click(function(){
      owl.next();
    });
  };

  init();

};
