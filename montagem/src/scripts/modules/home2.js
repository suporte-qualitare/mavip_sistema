// Home 2
window.mod.home2 = function() {

  // Scope
  var that = this;

  var owl;

  var init = function() {
    console.log('[brz] begin home2.js');
    owl = [];
    initCarousel();
    initFormListeners();
  };


  var initCarousel = function(){
    console.log('init carousel1')
    var options = {
      slideSpeed : 300,
        paginationSpeed : 400,
        mouseDrag: false,
        pagination: false,
        itemsCustom: [
          [0,1],
          [320,1],
          [760,3],
          [1400,5]
        ]
        // items : 5, //10 items above 1000px browser width
        // itemsDesktop : [1100,3], //5 items between 1000px and 901px
        // itemsDesktopSmall : [900,3], // betweem 900px and 601px
        // itemsTablet: [600,1], //2 items between 600 and 0
        // itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    };
    $('#carousel1').owlCarousel(options);
    $('#carousel2').owlCarousel(options);

    owl.push($('#carousel1').data('owlCarousel'));
    owl.push($('#carousel2').data('owlCarousel'));

    $('.Category-arrow--prev').click(function(){
      var reference = $(this).data('reference');
      owl[reference].prev();
    });

    $('.Category-arrow--next').click(function(){
      var reference = $(this).data('reference');
      owl[reference].next();
    });
  };

  var initFormListeners = function(){
    $('.ChangeData').click(function(e){
      $('.ChangeData-input').removeClass('hidden');
      $('.SubSection--userData > .Page-text').addClass('hidden');
      $(this).addClass('hidden');
    });
  };

  init();

};
