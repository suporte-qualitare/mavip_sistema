// Order
window.mod.order = function() {

  // Scope
  var that = this;
  var data = [];

  var init = function() {
    console.log('[brz] begin order.js');
    initInput();
    initAddListener();
  };

  var initInput = function(){
    data.push({description: 'Serviço de registro', price: 1000});
    data.push({description: 'Taxa do Guia de Recolhimento da União (GRU)', price: 320});
  };

  var initAddListener = function(){
    $('.Service-button').click(function(e){
      var price = $(this).data('price');
      var description = $(this).data('description');
      data.push({description: description, price: price / 100});
      var template = $('#orderItemTemplate').html();
      template = template.replace('{{description}}', description);
      template = template.replace('{{price}}', 'R$ ' + formatReal(price));
      $('.Order-items').append(template);
      var subtotal = 'R$ ' + formatReal(calcSubtotal());
      $('.Total-value').text(subtotal);
      $('.Subtotal-value').text(subtotal);

      $(this).parents('li.Service').remove();
    });
  };

  var calcSubtotal = function(){
    var subtotal = 0;
    for (var i = 0; i < data.length; i++) {
      subtotal += data[i].price;
    }
    return Math.ceil(subtotal * 100);
  };

  var formatReal = function (int){
      var tmp = int+'';
      tmp = tmp.replace(/([0-9]{2})$/g, ',$1');
      if( tmp.length > 6 )
              tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2');

      return tmp;
  };

  init();

};
