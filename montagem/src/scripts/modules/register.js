// Register
window.mod.register = function() {

  // Scope
  var that = this;

  var init = function() {
    console.log('[brz] begin register.js');
    initButtonListeners();
  };

  var initButtonListeners = function(){
    $('.Next-button').click(function(e){
      var option = $(this).data('step');
      console.log(option);
      switch(option){
        case 1:
          if(validateTerms()){
            $('#step1').addClass('hidden');
            $('#step2').removeClass('hidden');
            $('#step3').addClass('hidden');
          }else{
            alert('Você precisa aceitar os termos');
          }
          break;
        case 2:
          $('#step1').addClass('hidden');
          $('#step2').addClass('hidden');
          $('#step3').removeClass('hidden');
          break;
      }
    });
  };

  var validateTerms = function(){
    return $('input[type="checkbox"]:checked').length >= 2;
  };

  init();

};
