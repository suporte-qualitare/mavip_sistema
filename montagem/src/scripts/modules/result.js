// Result
window.mod.result = function() {

  // Scope
  var that = this;

  var init = function() {
    console.log('[brz] begin result.js');
    initAnimations();
  };

  var initAnimations = function(){
    var animationNumber = $('.Gauge').data('animation');
    var pointerClass = 'Gauge-pointer--animation' + animationNumber;
    var scoreClass = 'Gauge-score--color' + animationNumber;
    console.log(pointerClass, scoreClass);
    $('.Gauge-pointer').addClass(pointerClass);
    $('.Gauge-score').addClass(scoreClass);
    $('.Gauge-scoreNumber').countTo({
        from: 0,
        to: $('.Gauge-scoreNumber').data('number'),
        speed: 1000,
        refreshInterval: 50
    });
  };

  init();

};
