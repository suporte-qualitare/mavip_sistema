// Common
window.mod.service_panel = function() {

  // Scope
  var that = this;

  var init = function() {
    console.log('[brz] begin service_panel.js');
    initButtonListener();
    hideDetails();
  };

  var initButtonListener = function(){
    $('.ServiceItem-toggleDetails').click(function(e){
      $(this).parents('.ServiceItem').find('.ServiceItem-details').toggle('slow');
      var btnText = $(this).text();
      if(btnText.toLowerCase() == 'mostrar detalhes'){
        $(this).text('Ocultar detalhes');
      }else{
        $(this).text('Mostrar detalhes');
      }
    });
  };

  var hideDetails = function(){
  };

  init();

};
