// quick search regex
var qsRegex;
var buttonFilter;

// init Isotope
var $grid = $('#Container_filtro').isotope({
    itemSelector: '.Category-item',
    layoutMode: 'fitRows',
    filter: function () {
        var $this = $(this);
        var searchResult = qsRegex ? $this.text().match(qsRegex) : true;
        var buttonResult = buttonFilter ? $this.is(buttonFilter) : true;
        return searchResult && buttonResult;
    }
});

$('#filters').on('click', 'label', function (e) {
    $('.Section--category').css("display", "block");
    $('#Container_filtro').css("display", "block");
    buttonFilter = $(this).attr('data-filter');
    console.log(buttonFilter);
    if (buttonFilter == '.produtos') {
        $("#categorias").text('Produto');
    }
    if (buttonFilter == '.servicos') {
        $("#categorias").text('Serviço');
    }
    if (buttonFilter == '.comercios') {
        $("#categorias").text('Comércio');
    }
    $grid.isotope();
});

// use value of search field to filter
var $quicksearch = $('#ModuleSearch').keyup(debounce(function () {    
    qsRegex = new RegExp(removeAcento($quicksearch.val()), 'gi');
    $grid.isotope();
}));


// change is-checked class on buttons
$('.button-group').each(function (i, buttonGroup) {
    var $buttonGroup = $(buttonGroup);
    $buttonGroup.on('click', 'label', function () {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $(this).addClass('is-checked');
    });
});


// debounce so filtering doesn't happen every millisecond
function debounce(fn, threshold) {
    var timeout;
    return function debounced() {
        if (timeout) {
            clearTimeout(timeout);
        }
        function delayed() {
            fn();
            timeout = null;
        }
        setTimeout(delayed, threshold || 100);
    };
}

function removeAcento(text) {
    text = text.toUpperCase();
    text = text.replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'a');
    text = text.replace(new RegExp('[ÉÈÊ]', 'gi'), 'e');
    text = text.replace(new RegExp('[ÍÌÎ]', 'gi'), 'i');
    text = text.replace(new RegExp('[ÓÒÔÕ]', 'gi'), 'o');
    text = text.replace(new RegExp('[ÚÙÛ]', 'gi'), 'u');
    text = text.replace(new RegExp('[Ç]', 'gi'), 'c');
    return text.toLowerCase();
}