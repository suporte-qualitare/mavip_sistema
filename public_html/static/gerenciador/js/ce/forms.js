

$(document).ready(function () {
    $('.datepicker').datetimepicker({
        locale: 'pt-BR',
        format: 'DD/MM/YYYY'
    });

    $(".real").maskMoney({prefix: 'R$ ', thousands: '.', decimal: ',', affixesStay: false, allowZero: true});

    $('.select2_categoria_receitas').select2({
        formatNoMatches: function (term) {
            return 'Categoria não encontrado. <button class="btn btn-primary btn-flat md-trigger bt_add_categoria_receitas" id="">Adicionar Categoria</button>';
        }
    });
    
    $('.select2_categoria_despesas').select2({
        formatNoMatches: function (term) {
            return 'Categoria não encontrado. <button class="btn btn-primary btn-flat md-trigger bt_add_categoria_despesas" id="">Adicionar Categoria</button>';
        }
    });

    $('.select2_conta').select2({
        formatNoMatches: function (term) {
            return 'Conta não encontrada. <button class="btn btn-primary btn-flat md-trigger bt_add_conta" id="">Adicionar Conta</button>';
        }
    });


    $('.select2_categoria_receitas').on("select2-open", function () {
        $('.select2-drop').filter(function () {
            return $(this).css('display') == 'block';
        }).find($('.select2-input')).on('focus change keyup paste focusout', function () {
            if ($('.bt_add_categoria_receitas').length > 0) {
                $('.bt_add_categoria_receitas').off("click");
                $('.bt_add_categoria_receitas').on("click", function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + '/categorias/add-receita-json',
                        data: {
                            "nome": $('#select2-drop').find('.select2-input').val()
                        },
                        dataType: "json",
                        success: function (data) {
                            if (data.sucesso) {
                                adicionaCategoriaReceita(data);
                                alerta(true, 'Categoria adcionada com sucesso');
                            } else {
                                alerta(false, 'Não foi possivel cadastrar categoria');
                            }
                            //
                        }
                    });
                });
            }
        });
    });

    function adicionaCategoriaReceita(dados) {
        $('.select2-drop').filter(function () {
            return $(this).css('display') == 'block';
        }).find('ul').append('<li class="select2-results-dept-0 select2-result select2-result-selectable"><div class="select2-result-label"><span class="select2-match"></span>' + $('#select2-drop').find('.select2-input').val() + '</div></li>');
        $('select.select2_categoria_receitas').append('<option value=' + dados.id + '>' + $('#select2-drop').find('.select2-input').val() + '</option>');
        $('select.select2_categoria_receitas').find('option[value=' + dados.id + ']').attr('selected', 'selected');
        $('.select2_categoria_receitas').find('.select2-chosen').html($('#select2-drop').find('.select2-input').val());
        $('select.select2_categoria_receitas').select2('close');
    }


    $('.select2_conta').on("select2-open", function () {
        $('.select2-drop').filter(function () {
            return $(this).css('display') == 'block';
        }).find($('.select2-input')).on('focus change keyup paste focusout', function () {
            if ($('.bt_add_conta').length > 0) {
                $('.bt_add_conta').off("click");
                $('.bt_add_conta').on("click", function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + '/contas/add-json',
                        data: {
                            "nome": $('#select2-drop').find('.select2-input').val()
                        },
                        dataType: "json",
                        success: function (data) {
                            if (data.sucesso) {
                                adicionaConta(data);
                                alerta(true, 'Conta adcionada com sucesso');
                            } else {
                                alerta(false, 'Não foi possivel cadastrar conta');
                            }
                            //
                        }
                    });
                });
            }
        });
    });

    function adicionaConta(dados) {
        $('.select2-drop').filter(function () {
            return $(this).css('display') == 'block';
        }).find('ul').append('<li class="select2-results-dept-0 select2-result select2-result-selectable"><div class="select2-result-label"><span class="select2-match"></span>' + $('#select2-drop').find('.select2-input').val() + '</div></li>');
        $('select.select2_conta').append('<option value=' + dados.id + '>' + $('#select2-drop').find('.select2-input').val() + '</option>');
        $('select.select2_conta').find('option[value=' + dados.id + ']').attr('selected', 'selected');
        $('.select2_conta').find('.select2-chosen').html($('#select2-drop').find('.select2-input').val());
        $('select.select2_conta').select2('close');
    }
    
    $('.select2_categoria_despesas').on("select2-open", function () {
        $('.select2-drop').filter(function () {
            return $(this).css('display') == 'block';
        }).find($('.select2-input')).on('focus change keyup paste focusout', function () {
            if ($('.bt_add_categoria_despesas').length > 0) {
                $('.bt_add_categoria_despesas').off("click");
                $('.bt_add_categoria_despesas').on("click", function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl + '/categorias/add-despesa-json',
                        data: {
                            "nome": $('#select2-drop').find('.select2-input').val()
                        },
                        dataType: "json",
                        success: function (data) {
                            if (data.sucesso) {
                                adicionaCategoriaDespesa(data);
                                alerta(true, 'Categoria adcionada com sucesso');
                            } else {
                                alerta(false, 'Não foi possivel cadastrar categoria');
                            }
                            //
                        }
                    });
                });
            }
        });
    });

    function adicionaCategoriaDespesa(dados) {
        $('.select2-drop').filter(function () {
            return $(this).css('display') == 'block';
        }).find('ul').append('<li class="select2-results-dept-0 select2-result select2-result-selectable"><div class="select2-result-label"><span class="select2-match"></span>' + $('#select2-drop').find('.select2-input').val() + '</div></li>');
        $('select.select2_categoria_despesas').append('<option value=' + dados.id + '>' + $('#select2-drop').find('.select2-input').val() + '</option>');
        $('select.select2_categoria_despesas').find('option[value=' + dados.id + ']').attr('selected', 'selected');
        $('.select2_categoria_despesas').find('.select2-chosen').html($('#select2-drop').find('.select2-input').val());
        $('select.select2_categoria_despesas').select2('close');
    }
    
    
    

    $('#form_add_receita').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            url = $(this).attr('action');
            
            var $btn = $(".sbmt_add_receita").button('loading')

            dados = $(this).serializeArray();
            console.log(dados);
            $.ajax({
                type: "POST",
                url: url,
                data: dados,
                dataType: "json",
                success: function (data) {
                    if (data.sucesso) {
                        alerta(true, 'Receita adicionada com sucesso');
                        $("#modal_lancamento").modal('hide');
                        location.href=data.url;
                    } else {
                        alerta(false, 'Não foi possivel cadastrar receita');
                    }

                }
            });
            $btn.button('reset');
            return false;
        } else {
            alerta(false, 'Preencha os dados corretamente');
        }
    });
    
    $('#form_add_despesa').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            url = $(this).attr('action');
            
            var $btn = $(".sbmt_add_despesa").button('loading')

            dados = $(this).serializeArray();
            console.log(dados);
            $.ajax({
                type: "POST",
                url: url,
                data: dados,
                dataType: "json",
                success: function (data) {
                    if (data.sucesso) {
                        alerta(true, 'Despesa adicionada com sucesso');
                        $("#modal_lancamento").modal('hide');
                        location.href=data.url;
                    } else {
                        alerta(false, 'Não foi possivel cadastrar despesa');
                    }

                }
            });
            $btn.button('reset');
            return false;
        } else {
            alerta(false, 'Preencha os dados corretamente');
        }
    });
    
    $('#form_add_transferencia').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            url = $(this).attr('action');
            
            var $btn = $(".sbmt_add_transferencia").button('loading')

            dados = $(this).serializeArray();
            console.log(dados);
            $.ajax({
                type: "POST",
                url: url,
                data: dados,
                dataType: "json",
                success: function (data) {
                    if (data.sucesso) {
                        alerta(true, 'Transferencia realizada com sucesso');
                        $("#modal_lancamento").modal('hide');
                        location.href=data.url;
                    } else {
                        alerta(false, 'Não foi possivel realizar transferencia');
                    }

                }
            });
            $btn.button('reset');
            return false;
        } else {
            alerta(false, 'Preencha os dados corretamente');
        }
    });


});
