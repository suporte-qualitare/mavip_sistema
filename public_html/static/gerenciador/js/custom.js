function alerta(sucesso,texto){
    if(sucesso){
        alertify.success(texto);
    }else{
        alertify.error(texto);
    }
    
}
$(document).ready(function () {
    $(".real").maskMoney({prefix: 'R$ ', thousands: '.', decimal: ',', affixesStay: false, allowZero: true});
    $(".decimal").maskMoney({thousands: '', decimal: '.', affixesStay: false, allowZero: false});

    $(".select2").select2();

    $("#cpf").mask("999.999.999-99");
    $('.datepicker').datetimepicker({
        locale: 'pt-BR',
        format: 'DD/MM/YYYY'
    });
    
    $('.datetimepicker').datetimepicker({
        locale: 'pt-BR',
        format: 'DD/MM/YYYY HH:mm'
    });
    
    $('.confirm').on('click', function () {
        return confirm('Tem certeza que deseja fazer essa ação?');
    });


    $('.abre_model_receita').on('click', function () {
        $.ajax({
            type: "GET",
            url: baseUrl+'/lancamentos/add-receita/conta/'+$('.abre_model_despesa').attr('conta'),
            success: function (data) {
                $("#modal_lancamento .modal-content").html(data);
            }
        });
    });
    
    $('.abre_model_despesa').on('click', function () {
        $.ajax({
            type: "GET",
            url: baseUrl+'/lancamentos/add-despesa/conta/'+$('.abre_model_despesa').attr('conta'),
            success: function (data) {
                $("#modal_lancamento .modal-content").html(data);
            }
        });
    });
    
    $('.abre_model_transferencia').on('click', function () {
        $.ajax({
            type: "GET",
            url: baseUrl+'/lancamentos/add-transferencia/conta/'+$('.abre_model_despesa').attr('conta'),
            success: function (data) {
                $("#modal_lancamento .modal-content").html(data);
            }
        });
    });
    
    
    

});
